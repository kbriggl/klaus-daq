
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sc is
   port(
      -- clk             : in  std_logic;
      fclk            : in  std_logic;  --clk 40MHz
      clk_strobe_5    : in  std_logic;  --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      cmd12           : in  std_logic_vector (15 downto 0);
      cmd34           : in  std_logic_vector (15 downto 0);
      cmd56           : in  std_logic_vector (15 downto 0);
      cmd_end         : in  std_logic;
      cmd_ready       : in  std_logic;
      rst             : in  std_logic;
      rx_data         : in  std_logic_vector (7 downto 0);
      spiroc_type     : in  std_logic;
      slab_num_asic   : in  std_logic_vector (15 downto 0);
      usb_strobe      : in  std_logic;
      usb_txen        : in  std_logic;
      writing         : in  std_logic;
      sc_clk1         : out std_logic;
      sc_clk2         : out std_logic;
      sc_clk3         : out std_logic;
      clk_40M_inhibit : out std_logic;
      sc_cmd_done     : out std_logic;
      sc_data_out     : out std_logic_vector (7 downto 0);
      sc_load         : out std_logic;
      sc_srin         : out std_logic;
      sc_wr_req       : out std_logic;
      sc_debug	      : out std_logic
      );
end sc;

architecture struct of sc is

   signal mem_clk      : std_logic;
   signal mem_we       : std_logic;
   signal mem_addr     : std_logic_vector(14 downto 0);
   signal mem_data_in  : std_logic_vector(7 downto 0);
   signal mem_data_out : std_logic_vector(7 downto 0);

   --type MEM_RAM_TYPE is array (((2**14) -1) downto 0) of std_logic_vector(7 downto 0);
   type MEM_RAM_TYPE is array (((2**15) -1) downto 0) of std_logic_vector(7 downto 0);
   signal mem_ram_table : MEM_RAM_TYPE := (others => "00000000");
   signal mem_addr_reg  : std_logic_vector(14 downto 0);
   component sc_ctrl is
      port (
         -- clk             : in  std_logic;
         fclk            : in  std_logic;  --clk 40MHz
         clk_strobe_5    : in  std_logic;  --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
         rst             : in  std_logic;
         usb_strobe      : in  std_logic;
         rx_data         : in  std_logic_vector (7 downto 0);
         mem_data_out    : in  std_logic_vector (7 downto 0);
         mem_data_in     : out std_logic_vector (7 downto 0);
         mem_addr        : out std_logic_vector (14 downto 0);
         mem_clk         : out std_logic;
         mem_we          : out std_logic;
         sc_cmd_done     : out std_logic;
         sc_clk1         : out std_logic;
         sc_clk2         : out std_logic;
         sc_clk3         : out std_logic;
         clk_40M_inhibit : out std_logic;
         sc_load         : out std_logic;
         sc_srin         : out std_logic;
         cmd12           : in  std_logic_vector (15 downto 0);
         cmd34           : in  std_logic_vector (15 downto 0);
         cmd56           : in  std_logic_vector (15 downto 0);
         cmd_ready       : in  std_logic;
         sc_wr_req       : out std_logic;
         usb_txen        : in  std_logic;
         writing         : in  std_logic;
         sc_data_out     : out std_logic_vector (7 downto 0);
         cmd_end         : in  std_logic;
         spiroc_type     : in  std_logic;
	 sc_debug	 : out std_logic;
         slab_num_asic   : in  std_logic_vector (15 downto 0));
   end component sc_ctrl;
begin

   ram_proc : process (mem_clk)         --fclk in fact
   begin
      if rising_edge(mem_clk) then
         if (clk_strobe_5 = '1') then
            if (mem_we = '1' or mem_we = 'H') then
               mem_ram_table(TO_INTEGER(unsigned(mem_addr))) <= mem_data_in;
            end if;
            mem_addr_reg <= mem_addr;
         end if;  --strobe
      end if;  --clk
   end process ram_proc;
   mem_data_out <= mem_ram_table(TO_INTEGER(unsigned(mem_addr_reg)));

   sc_ctrl_inst : sc_ctrl
      port map (
         -- clk             => clk,
         fclk            => fclk,
         clk_strobe_5    => clk_strobe_5,
         rst             => rst,
         usb_strobe      => usb_strobe,
         rx_data         => rx_data,
         mem_data_out    => mem_data_out,
         mem_data_in     => mem_data_in,
         mem_addr        => mem_addr,
         mem_clk         => mem_clk,
         mem_we          => mem_we,
         sc_cmd_done     => sc_cmd_done,
         sc_clk1         => sc_clk1,
         sc_clk2         => sc_clk2,
         sc_clk3         => sc_clk3,
         clk_40M_inhibit => clk_40M_inhibit,
         sc_load         => sc_load,
         sc_srin         => sc_srin,
         cmd12           => cmd12,
         cmd34           => cmd34,
         cmd56           => cmd56,
         cmd_ready       => cmd_ready,
         sc_wr_req       => sc_wr_req,
         usb_txen        => usb_txen,
         writing         => writing,
         sc_data_out     => sc_data_out,
         cmd_end         => cmd_end,
         spiroc_type     => spiroc_type,
	 sc_debug	 => sc_debug,
         slab_num_asic   => slab_num_asic
         );

end struct;
