
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

package adif2_types is

   --for 2 slabs:
   --constant C_NUM_ROW : integer := 2;   -- Number of the row
   --for 1 central slab:
	constant C_NUM_ROW : integer := 1;   -- Number of the row. put to 1 for simplicity

   constant C_ASIC_NUM_MAX : integer := 3;  -- Maximal number of the ASIC in one row. starts from 0

   constant C_NUM_WORD_HEAD : integer := 14;  -- Number of the words in header

   constant C_NUM_CALIB_BYTES : integer := 3;  -- Number of the calib data bytes

   constant C_RO_ADD_MAX : unsigned(14 downto 0) := "010100000000000";  -- Maximal readout address value, 4 ASICs, all full

   type HEADER is array(0 to C_NUM_WORD_HEAD) of unsigned(7 downto 0);

   type POINTER is array(0 to (C_ASIC_NUM_MAX)) of unsigned(14 downto 0);

   type DATA_OUT is array(1 to C_NUM_ROW) of std_logic_vector(7 downto 0);

   type CALIB_DATA is array(0 to C_NUM_CALIB_BYTES) of std_logic_vector(7 downto 0);

   type ADDR_POINTER is array(1 to C_NUM_ROW) of POINTER;

   type ASIC_DB is array(1 to C_NUM_ROW) of unsigned(3 downto 0);


   constant C_NUM_BYTES : integer := 14;  -- Number of the bytes

   type T_ARRAY_8BIT is array (natural range<>) of std_logic_vector(7 downto 0);

   -- command block record
   type T_CMD_REC is record
--              CMD_READY      : std_logic;
      PACKET_TYPE   : std_logic_vector(15 downto 0);
      PACKET_ID     : std_logic_vector(15 downto 0);
      TYPE_MODIFIER : std_logic_vector(15 downto 0);
      SPECIFIER     : std_logic_vector(15 downto 0);
      DATA_LENGTH   : std_logic_vector(15 downto 0);
      DATA          : std_logic_vector(15 downto 0);
      CRC           : std_logic_vector(15 downto 0);
   end record T_CMD_REC;

   -- constant command block record 
   constant C_CMD_REC : T_CMD_REC := (  --CMD_READY      => '0',
      PACKET_TYPE   => (others => '0'),
      PACKET_ID     => (others => '0'),
      TYPE_MODIFIER => (others => '0'),
      SPECIFIER     => (others => '0'),
      DATA_LENGTH   => (others => '0'),
      DATA          => (others => '0'),
      CRC           => (others => '0'));

   -- NOT USED YET. TODO
   -- number of configuration bits for spirocs.
   -- There are 2 types of spirocs and the selection is done by 1 bit, which is then passed to the sc_ctrl by the spiroc_type port
   --constant C_ASIC_TYPE0_BITS    : positive                     := 1186;  -- spiroc 2D.
   --constant C_ASIC_TYPE1_BITS    : positive                     := 929;   -- spiroc 2B. in bytes: 0x74.2
   -- derived constants
   --constant C_ASIC_TYPE1_MAXBYTE : std_logic_vector(7 downto 0) := 0x"74";
   --constant C_ASIC_TYPE1_MAXBIT  : std_logic_vector(2 downto 0) := "001";
   constant C_DC_COST : integer := 0;   --C_DC_COST=0 will disable dutycycle monitoring
                                        --C_DC_COST=9 will stop acq after 5 ms

   ----------------------------------------------------
   -- timing constants
   ----------------------------------------------------
   constant C_TIM_ACQ_RESET_LOW           : unsigned(15 downto 0)         := X"0004";  --delay of s01: how long (fclk pulses) to keep the reset_slab_acq_low.
   constant C_TIM_ACQ_RESET_PUL           : unsigned(15 downto 0)         := X"0004";  --s02: how long to wait after pwr_*_pul goes to 1 before releasing the reset
   constant C_TIM_ACQ_RESET_RELEASE_PHASE : std_logic_vector(3 downto 0)  := "0101";   --s02: clock phase of when to release the reset. uses clk_phase signal
   constant C_TIM_ACQ_RESET_HIGH          : unsigned(15 downto 0)         := X"0004";  --s1: how many slow clock ticks to wait after releasing the reset
   constant C_TIM_ACQ_TRIGEXT_DELAY       : unsigned(15 downto 0)         := X"0001";  --how many fclk clock tics after rising edge of clk_acq should be waited until the trig_ext is sent.
                                                                                       --value of 1 corresponds to delay of 100 ns due to internal delays
   constant C_TIM_ACQ_TRIGEXT_LENGTH      : unsigned(15 downto 0)         := X"0008";  --length of trig_ext pulse
--   constant C_TIM_ACQ_STOP_BXID_PATT      : std_logic_vector(15 downto 0) := "---------------0";  --stopping bxid matching pattern. Stop of acquisition will occur only in this bxid
   constant C_TIM_ACQ_STOP_DELAY          : unsigned(15 downto 0)         := X"0002";  -- how many fclk clock cycles to wait after the rising edge of clk_acq before stopping.
                                                                                       -- X"0002" corresponds to 100 ns

   constant C_TIM_VAL_RISE_PHASE : std_logic_vector(7 downto 0) := X"02";  --start phase (clk_phase) when to send the no_trig due to the missing validation
   constant C_TIM_VAL_FALL_PHASE : std_logic_vector(7 downto 0) := X"00";  --stop phase (clk_phase) when to stop sending the no_trig due to the missing validation

   constant C_TIM_CONV_DLY   : unsigned(15 downto 0) := X"0140";  --number of clock cycles (200 ns) to stabilize before starting the conversion
   constant C_TIM_CONV_WIDTH : unsigned(15 downto 0) := X"0020";  --number of clock cycles (200 ns) before start of completeness evaluation

   function F_MORSE_NUM_ENCODE(DATE : std_logic_vector(31 downto 0)) return bit_vector;  -- converts a date string (X"20190515") into series of blinks of LED to be clocked out from ROM128X1.The result is to be saved in INIT

   function i2c_addr_calc ( asic_index : unsigned(3 downto 0))	return std_logic_vector;

end adif2_types;

package body adif2_types is
   function F_MORSE_NUM_ENCODE(DATE : std_logic_vector(31 downto 0)) return bit_vector is
      variable morse : bit_vector(127 downto 0) := (others => '0');
      variable wi    : integer                        := 0;  --write index
   begin
      for digit in 7 downto 0 loop
         case DATE(digit*4+3 downto digit*4) is
            when "0000" => morse(wi+9 downto wi)  := "0000011111"; wi := wi + 10;
            when "0001" => morse(wi+5 downto wi)  := "000001"; wi := wi + 6;
            when "0010" => morse(wi+7 downto wi)  := "00000101"; wi := wi + 8;
            when "0011" => morse(wi+9 downto wi)  := "0000010101"; wi := wi + 10;
            when "0100" => morse(wi+11 downto wi) := "000001010101"; wi := wi + 12;
            when "0101" => morse(wi+13 downto wi) := "00000101010101"; wi := wi + 14;
            when "0110" => morse(wi+15 downto wi) := "0000010101010101"; wi := wi + 16;
            when "0111" => morse(wi+17 downto wi) := "000001010101010101"; wi := wi + 18;
            when "1000" => morse(wi+19 downto wi) := "00000101010101010101"; wi := wi + 20;
            when "1001" => morse(wi+21 downto wi) := "0000010101010101010101"; wi := wi + 22;
            when others => wi                     := wi + 5;
         end case;
      end loop;
      return morse;
   end function F_MORSE_NUM_ENCODE;

	-- i2c_addr calculate: 
   function i2c_addr_calc ( asic_index : unsigned(3 downto 0))	return std_logic_vector is
		variable addr : unsigned(3 downto 0);
   begin
		addr := asic_index + 1;
		return "100" & std_logic_vector(addr);	-- should changed!
	end function i2c_addr_calc;

end adif2_types;
