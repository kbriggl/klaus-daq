--
-- VHDL Architecture adif_lib.usb2dif.rtl
--
-- Created:
--          by - krivan.UNKNOWN (FEBPCX3865)
--          at - 15:30:36 17.02.2009
--
-- using Mentor Graphics HDL Designer(TM) 2005.3 (Build 75)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

entity usb2dif is
   port (
      -- clk          : in  std_logic;                      -- external clock
      fclk         : in  std_logic;                      --clk 40MHz
      clk_strobe_5 : in  std_logic;                      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in  std_logic;                      -- global reset
      usb_rxfn     : in  std_logic;                      -- external USB FIFO signal
      usb_rdn      : out std_logic;                      -- data transfer cycle
      usb_data     : in  std_logic_vector (7 downto 0);  -- USB FIFO data
      usb_strobe   : out std_logic;                      -- data strobe to FPGA register
      rx_data      : out std_logic_vector (7 downto 0);  -- FPGA register data (in)
      if_enable    : in  std_logic                       -- interface is enabled
      );
end entity usb2dif;

architecture rtl of usb2dif is

   type states is (INIT, S1, S2, S3, S4, S5);
   signal state, next_state : states;
   signal usb_rxfn_int      : std_logic;

begin
   
   P0 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            state <= INIT;
         elsif (clk_strobe_5 = '1') then
            state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process P0;

   P3 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            usb_rxfn_int <= '1';
         elsif (clk_strobe_5 = '1') then
            usb_rxfn_int <= usb_rxfn;
         end if;  --strobe
      end if;
   end process P3;

   P1 : process (if_enable, state, usb_rxfn_int)
   begin
      case state is
         when INIT =>                   -- wait for available data
            if (usb_rxfn_int = '0') and (if_enable = '1') then
               next_state <= S1;        -- data are available in the USB FIFO
            else
               next_state <= INIT;
            end if;
         when S1 =>                     -- start transfer cycle
            next_state <= S2;
         when S2 =>                     -- RDN to 0 one more cycle
            next_state <= S3;
         when S3 =>                     -- data are now available in the USB FIFO
            next_state <= S4;
         when S4 =>                     -- data are now in the DIF FPGA register
            next_state <= S5;
         when S5 =>                     --end of transfer cycle
            if usb_rxfn_int = '1' then
               next_state <= INIT;
            else
               next_state <= S5;
            end if;
      end case;
   end process P1;

   P2 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            usb_strobe <= '0';
            usb_rdn    <= '1';
            rx_data    <= (others => '0');
         elsif (clk_strobe_5 = '1') then
            case state is
               when INIT =>
                  usb_strobe <= '0';
                  usb_rdn    <= '1';
               when S1 =>
                  usb_rdn <= '0';
               when S2 => null;
               when S3 =>
                  rx_data <= usb_data;
               when S4 =>
                  usb_strobe <= '1';
               when S5 =>
                  usb_strobe <= '0';
                  usb_rdn    <= '1';
            end case;
         end if;  --strobe
      end if;
   end process P2;

end architecture rtl;

