
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity std_cmd_dec is
   port(
      -- system interface
      -- clk          : in std_logic;
      fclk         : in std_logic;                       --clk 40MHz
      clk_strobe_5 : in std_logic;                       --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;
      -- command reg and control
      cmd_ready    : in std_logic;
      cmd_end      : in std_logic;
      cmd12        : in std_logic_vector (15 downto 0);  --type_modifier
      cmd34        : in std_logic_vector (15 downto 0);
      cmd56        : in std_logic_vector (15 downto 0);

      std_dbg    : out std_logic;
      -- power control
      power_off  : out std_logic;
      power_on   : out std_logic;
      power_auto : out std_logic;
      power_sel  : out std_logic;
      power_ana  : out std_logic;
      power_dig  : out std_logic;
      power_sca  : out std_logic;
      power_adc  : out std_logic;
      power_dac  : out std_logic;
      power_all  : out std_logic;

      PWR_ENABLE_GR1 : out std_logic;   --power group enable
      PWR_ENABLE_GR2 : out std_logic;   --power group enable
      PWR_ENABLE_GR3 : out std_logic;   --power group enable
      PWR_ENABLE_GR4 : out std_logic;   --power group enable
      OC_ENABLE      : out std_logic;   --open colector enabled

      --sw_1V5to3V3 : out std_logic;
      --power_spare : out std_logic;
      --power_fe_en : out std_logic;
      sig_fe_en : out std_logic;        --enable signal buffers

      trig_cfg         : out std_logic_vector (15 downto 0);
      dly_reg          : out std_logic_vector (15 downto 0);
      ro_time          : out std_logic_vector (15 downto 0);
      dif_id           : out std_logic_vector (15 downto 0);
      timing_cfg       : out std_logic_vector (15 downto 0);
   
      -- reset control
      reset_dif        : out std_logic;
      reset_slab       : out std_logic;
      reset_all        : out std_logic;
      reset_sc         : out std_logic;
      reset_read_probe : out std_logic;
      --reset_read   : OUT    std_logic;
      --reset_probe  : OUT    std_logic;
      reset_cal        : out std_logic;
      -- clock control and misc status and control
      clk0_40M_en      : out std_logic;
      clk0_5M_en       : out std_logic;
      clk1_40M_en      : out std_logic;
      clk1_5M_en       : out std_logic;
      clk2_40M_en      : out std_logic;
      clk2_5M_en       : out std_logic;
      clk_read         : out std_logic;
      srin_read        : out std_logic;
      clk_probe        : out std_logic;
      srin_probe       : out std_logic;
      set_sleep        : out std_logic;
      spiroc_type      : out std_logic;  -- '0' - SPIROC1,  '1' - SPIROC2
      no_trig_enable   : out std_logic;
      slab_num_asic    : out std_logic_vector (15 downto 0);
      set_ready        : out std_logic;
      sc_select        : out std_logic;  -- select for probe(0) / sc(1) registers and probe/sc reset
      std_cmd_done     : out std_logic
      );
end std_cmd_dec;

architecture rtl of std_cmd_dec is
   
   type STATE_TYPE is (s0, s1, s02, s03, s04, s041,
                       s05, s06, s07, s08, s10, s12,
                       s1201a, s1201b, s1201c, s1201d,
                       s1202a, s1202b, s1202c,
                       s1203, s1204, s1205, s1206, s1207, s1208, s1209, s120A,
                       s16, s21, s22, s24, s30,
                       s60, s61, s62, s63, s64, s65, s70,
                       s80, s81, s82, s83, s84, s85,
                       s90, s91, s92, s93, s94);

   -- Declare current and next state signals
   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;

   signal end_std_cmd : std_logic;
   signal cmd_ok      : std_logic;

   signal power_reg   : std_logic_vector (15 downto 0);
   signal reset_reg   : std_logic_vector (15 downto 0);
   signal set_reg     : std_logic_vector (15 downto 0);
   signal puls_reg    : std_logic_vector (15 downto 0);
   signal ctrl_reg    : std_logic_vector (15 downto 0);
--  SIGNAL stat1_reg     :  std_logic_vector (15 DOWNTO 0);
--  SIGNAL stat2_reg     :  std_logic_vector (15 DOWNTO 0);
   signal dly_reg_int : std_logic_vector (15 downto 0);
   signal timing_cfg_int : std_logic_vector(15 downto 0) := X"00A0";

   signal out_num : unsigned(15 downto 0);
   signal count   : unsigned(31 downto 0);
begin

   dly_reg <= dly_reg_int;
   timing_cfg <= timing_cfg_int;

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (current_state, cmd_ready, cmd12, cmd34, cmd56,
                             out_num, cmd_ok, cmd_end, end_std_cmd, count)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            --wait for usb_command
            if cmd_ready = '1' then
               next_state <= s1;
            else
               next_state <= s0;
            end if;
         when s1 =>
            -- select command group
--      IF (cmd12(15 DOWNTO 8) = x"CC") AND cmd34 = x"0001" THEN
--              IF (cmd12(15 DOWNTO 8) = x"00") AND cmd34 = x"0000" THEN

--              IF  cmd12 = x"0002" OR cmd12 = x"0003" OR cmd12 = x"0004" OR cmd12 = x"0005" OR cmd12 = x"0006" OR 
--                  cmd12 = x"0007" OR cmd12 = x"0008" OR cmd12 = x"0010" OR cmd12 = x"0012" OR cmd12 = x"000F" OR 
--                       cmd12 = x"0022" OR cmd12 = x"0080" OR cmd12 = x"0081" OR cmd12 = x"0082" THEN
            
            case cmd12 is
--          WHEN x"CC02" => next_state <= s02; -- power control commands
--          WHEN x"CC03" => next_state <= s03; -- switch SPIROC1/SPIROC2
--          WHEN x"CC04" => next_state <= s04; -- reset control commands
--          WHEN x"CC05" => next_state <= s05; -- clock enable/disable commands
--          WHEN x"CC06" => next_state <= s06; -- set DIF commands
--          WHEN x"CC07" => next_state <= s07; -- no_trig enable/disable
--          WHEN x"CC08" => next_state <= s08; -- power pulsing (select)commands
--          WHEN x"CC10" => next_state <= s10; -- set control register
--          WHEN x"CC12" => next_state <= s12; -- (re) initialization
--          WHEN x"CC0F" => next_state <= s16; -- readout time
--          WHEN x"CC22" => next_state <= s22; -- set power pulsing timing
--          WHEN x"CC80" => next_state <= s60; -- set analog probe 
--          WHEN x"CC82" => next_state <= s80; -- set digital probe #1 or #2
--          WHEN OTHERS  => next_state <= s0;
               
               when x"0002" => next_state <= s02;  -- power control commands
               when x"0003" => next_state <= s03;  -- switch SPIROC1/SPIROC2
               when x"0004" => next_state <= s04;  -- reset control commands
               when x"0005" => next_state <= s05;  -- clock enable/disable commands
               when x"0006" => next_state <= s06;  -- set DIF commands
               when x"0007" => next_state <= s07;  -- no_trig enable/disable
               when x"0008" => next_state <= s08;  -- power pulsing (select)commands
                            -- X"0009" dutycycle monitor
               when x"0010" => next_state <= s10;  -- set control register
               when x"0012" => next_state <= s12;  -- (re) initialization
               when x"000F" => next_state <= s16;  -- readout time (length of acquisition)
               when x"0021" => next_state <= s21;  -- change the acquisition length
               when x"0022" => next_state <= s22;  -- set power pulsing timing
               when x"0024" => next_state <= s24;  -- set sc_select signal                            
               when x"0030" => next_state <= s30;  -- set trigger
               when x"0080" => next_state <= s60;  -- set analog probe 
               when x"0081" => next_state <= s70;  -- set DIF ID 
               when x"0082" => next_state <= s80;  -- set digital probe #1 or #2
               when others  => next_state <= s0;
            end case;
--      ELSE 
--        next_state <= s0;
--      END IF;
         when s02 =>
            next_state <= s90;
         when s03 =>
            next_state <= s90;
         when s04 =>
            next_state <= s041;
         when s041 =>
            if count < x"0000000F" then
               next_state <= s04;
            else
               next_state <= s90;
            end if;
         when s05 =>
            next_state <= s90;
         when s06 =>
            next_state <= s90;
         when s07 =>
            next_state <= s90;
         when s08 =>
            next_state <= s90;
         when s10 =>
            next_state <= s90;
            
         when s12 =>
            case cmd56 is
               when x"0001" => next_state <= s1201a;  -- GR2: Turn CALIB/CIB off      
               when x"0002" => next_state <= s1202a;  -- GR2: Turn CALIB/CIB on       
               when x"0003" => next_state <= s1203;   -- GR1: Turn HBU VDAC/VREF off  
               when x"0004" => next_state <= s1204;   -- GR1: Turn HBU VDAC/VREF on   
               when x"0005" => next_state <= s1205;   -- GR3: Turn HBU VDDA/HV off    
               when x"0006" => next_state <= s1206;   -- GR3: Turn HBU VDDA/HV on     
               when x"0007" => next_state <= s1207;   -- GR4: Turn HBU VDDD off       
               when x"0008" => next_state <= s1208;   -- GR4: Turn HBU VDDD on        
               when x"0009" => next_state <= s1209;   -- OC_ENABLE: Turn OC off       
               when x"000A" => next_state <= s120A;   -- OC_ENABLE: Turn OC on        
               when x"1000" => next_state <= s90;     -- TODO not yet implemented, Read power register
               when others  => next_state <= s90;
            end case;
            
         when s1201a =>
            next_state <= s1201b;
            
         when s1201b =>
            next_state <= s1201c;
            
         when s1201c =>
            if count < x"00020000" then
               next_state <= s1201b;
            else
               next_state <= s1201d;
            end if;

         when s1201d =>
            next_state <= s90;

         when s1202a =>
            next_state <= s1202b;
            
         when s1202b =>
            next_state <= s1202c;
            
         when s1202c =>
            if count < x"00040000" then
               next_state <= s1202b;
            else
               next_state <= s90;
            end if;

         when s1203 =>
            next_state <= s90;

         when s1204 =>
            next_state <= s90;
         when s1205 =>
            next_state <= s90;
         when s1206 =>
            next_state <= s90;
         when s1207 =>
            next_state <= s90;
         when s1208 =>
            next_state <= s90;
         when s1209 =>
            next_state <= s90;
         when s120A =>
            next_state <= s90;

         when s16 =>
            next_state <= s90;

         when s21 =>
            next_state <= s90;

         when s22 =>
            next_state <= s90;

         when s24 =>
            next_state <= s90;
            
         when s30 =>
            next_state <= s90;

         when s60 =>
            next_state <= s62;
         when s61 =>
            if out_num < unsigned(cmd56) then
               next_state <= s62;
            else
               next_state <= s65;
            end if;
         when s62 =>
            if count < x"00000010" then
               next_state <= s62;
            else
               next_state <= s63;
            end if;
         when s63 =>
            next_state <= s64;
         when s64 =>
            if count < x"00000010" then
               next_state <= s64;
            else
               next_state <= s61;
            end if;
         when s65 =>
            next_state <= s90;

         when s70 =>
            next_state <= s90;
            
         when s80 =>
            next_state <= s82;
         when s81 =>
            if out_num < unsigned(cmd56) then
               next_state <= s82;
            else
               next_state <= s85;
            end if;
         when s82 =>
            if count < x"00000010" then
               next_state <= s82;
            else
               next_state <= s83;
            end if;
         when s83 =>
            next_state <= s84;
         when s84 =>
            if count < x"00000010" then
               next_state <= s84;
            else
               next_state <= s81;
            end if;
         when s85 =>
            next_state <= s90;

         when s90 =>
            next_state <= s91;
         when s91 =>
            if cmd_ok = '1' then
               next_state <= s92;
            else
               next_state <= s0;
            end if;
         when s92 =>
            --wait for command completion
            if end_std_cmd = '1' then
               next_state <= s93;
            else
               next_state <= s92;
            end if;
         when s93 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s94;
            else
               next_state <= s93;
            end if;
         when s94 =>
            next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk, rst)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            end_std_cmd <= '0';
            cmd_ok      <= '0';

            std_dbg <= '0';

            PWR_ENABLE_GR1 <= '0';
            PWR_ENABLE_GR2 <= '0';
            PWR_ENABLE_GR3 <= '0';
            PWR_ENABLE_GR4 <= '0';
            OC_ENABLE      <= '0';
--         power_spare <= '0';
--         power_fe_en <= '1';
            sig_fe_en      <= '1';
            power_off      <= '0';
            power_on       <= '0';
            power_auto     <= '0';
            power_sel      <= '0';
            power_ana      <= '0';
            power_dig      <= '0';
            power_sca      <= '0';
            power_adc      <= '0';
            power_dac      <= '0';
            power_all      <= '0';
            power_reg      <= (others => '0');

            reset_dif        <= '0';
            reset_slab       <= '1';
            reset_all        <= '0';
            reset_sc         <= '0';	-- active high
            reset_read_probe <= '1';
            --reset_read  <= '1';
            --reset_probe  <= '1';
            reset_cal        <= '1';
            reset_reg        <= (others => '0');

            spiroc_type    <= '1';
            no_trig_enable <= '0';
            slab_num_asic  <= "0001000010000101";
            clk0_40M_en    <= '1';
            clk1_40M_en    <= '1';
            clk2_40M_en    <= '1';
            clk0_5M_en     <= '1';
            clk1_5M_en     <= '1';
            clk2_5M_en     <= '1';

            set_sleep <= '0';
            set_ready <= '0';

            --sw_1V5to3V3 <= '0';

            srin_read  <= '0';
            clk_read   <= '0';
            srin_probe <= '0';
            clk_probe  <= '0';
            out_num    <= (others => '0');
            count      <= (others => '0');


            trig_cfg    <= x"018C";
            ro_time     <= x"0080";
            dif_id      <= (others => '0');
            set_reg     <= (others => '0');
            dly_reg_int <= (others => '0');
            timing_cfg_int <= X"00"& X"A0";
            puls_reg    <= (others => '0');
            ctrl_reg    <= (others => '0');
--      stat1_reg <= (others => '0');
--      stat2_reg <= (others => '0');
            count       <= (others => '0');
            sc_select   <= '1';         --default selects sc mode (for older sw backward compatibility)
         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  end_std_cmd <= '0';
                  cmd_ok      <= '1';

                  std_dbg <= '0';

                  power_off  <= '0';
                  power_on   <= '0';
                  power_auto <= '0';
                  power_sel  <= '0';
                  power_ana  <= '0';
                  power_dig  <= '0';
                  power_sca  <= '0';
                  power_adc  <= '0';
                  power_dac  <= '0';
                  power_all  <= '0';
                  power_reg  <= (others => '0');

                  reset_dif        <= '0';
                  reset_slab       <= '1';
                  reset_all        <= '0';
                  reset_sc         <= '0';
                  reset_read_probe <= '1';
                  --reset_read <= '1';
                  --reset_probe <= '1';
                  reset_cal        <= '1';
                  reset_reg        <= (others => '0');

                  set_sleep  <= '0';
                  set_ready  <= '0';
                  --spiroc_type <= '1';
                  srin_read  <= '0';
                  clk_read   <= '0';
                  srin_probe <= '0';
                  clk_probe  <= '0';
                  out_num    <= (others => '0');
                  count      <= (others => '0');

                  set_reg     <= (others => '0');
                  dly_reg_int <= dly_reg_int;
                  puls_reg    <= (others => '0');
                  ctrl_reg    <= (others => '0');
--        stat1_reg <= (others => '0');
--        stat2_reg <= (others => '0');
                  count       <= (others => '0');
               when s1 =>
                  std_dbg <= '1';

               when s02 =>              -- power control commands
                  if cmd56 = x"0000" then power_off     <= '1';
                  elsif cmd56 = x"0001" then power_on   <= '1';
                  elsif cmd56 = x"0002" then power_auto <= '1';

                  else
                     cmd_ok <= '0';
                  end if;
                  std_dbg <= '0';

               when s03 =>                -- switch SPIROC1/SPIROC2 and definition of the number of asic per slabs
                  slab_num_asic <= cmd56;
                  if cmd56(0) = '1' then
                     spiroc_type <= '1';  -- SPIROC2
                  else
                     spiroc_type <= '0';  -- SPIROC1
                  end if;
                  std_dbg <= '0';

               when s04 =>              -- reset control commands
                  if cmd56 = x"0001" then reset_dif           <= '1';
                  elsif cmd56 = x"0002" then reset_slab       <= '0';
                  elsif cmd56 = x"0004" then reset_all        <= '1';
                  elsif cmd56 = x"0008" then reset_sc         <= '1';
                  elsif cmd56 = x"0010" then reset_read_probe <= '0';
                                        --ELSIF cmd56 = x"0010" THEN reset_read  <= '0';
                  elsif cmd56 = x"0020" then reset_read_probe <= '0';
                                        --ELSIF cmd56 = x"0020" THEN reset_probe  <= '0';
                  elsif cmd56 = x"0100" then reset_cal        <= '0';
                  else
                     cmd_ok <= '0';
                  end if;
                  count   <= count + 1;
                  std_dbg <= '0';
               when s041 =>

               when s05 =>              -- enable/disable clock
                  
                  clk0_40M_en <= cmd56(0);

                  clk0_5M_en <= cmd56(1);

                  clk1_40M_en <= cmd56(2);

                  clk1_5M_en <= cmd56(3);

                  clk2_40M_en <= cmd56(4);

                  clk2_5M_en <= cmd56(5);

--        IF    cmd56(0) = '1' THEN clk0_40M_en <= '1'; -- enable clk0_40M
--        ELSE  clk0_40M_en <= '0'; -- disable clk1_40M
--        END IF;
--        IF    cmd56(1) = '1' THEN clk0_5M_en <= '1'; -- enable clk0_5M
--        ELSE  clk0_5M_en <= '0'; -- disable clk1_5M
--        END IF;
--      
--        IF    cmd56(2) = '1' THEN clk1_40M_en <= '1'; -- enable clk1_40M
--        ELSE  clk1_40M_en <= '0'; -- disable clk1_40M
--        END IF;
--        IF    cmd56(3) = '1' THEN clk1_5M_en <= '1'; -- enable clk1_5M
--        ELSE  clk1_5M_en <= '0'; -- disable clk1_5M
--        END IF;
--
--        IF    cmd56(4) = '1' THEN clk2_40M_en <= '1'; -- enable clk2_40M
--        ELSE  clk2_40M_en <= '0'; -- disable clk2_40M
--        END IF;
--        IF    cmd56(5) = '1' THEN clk2_5M_en <= '1'; -- enable clk2_5M
--        ELSE  clk2_5M_en <= '0'; -- disable clk2_5M
--        END IF;
                  std_dbg <= '0';

               when s06 =>              -- set DIF commands
                  if cmd56 = x"0001" then set_sleep    <= '1';
                  elsif cmd56 = x"0002" then set_ready <= '1';
                  else
                     cmd_ok <= '0';
                  end if;
                  std_dbg <= '0';
                  
               when s07 =>              -- no_trig enable/disable
                  if cmd56 = x"0001" then no_trig_enable <= '1';
                  else
                     no_trig_enable <= '0';
                  end if;
                  std_dbg <= '0';

               when s08 =>              -- power pulsing commands 
                  power_sel                        <= '1';
                  if cmd56(0) = '1' then power_ana <= '1';
                  else power_ana                   <= '0';
                  end if;
                  if cmd56(1) = '1' then power_dig <= '1';
                  else power_dig                   <= '0';
                  end if;
                  if cmd56(2) = '1' then power_sca <= '1';
                  else power_sca                   <= '0';
                  end if;
                  if cmd56(3) = '1' then power_adc <= '1';
                  else power_adc                   <= '0';
                  end if;
                  if cmd56(4) = '1' then power_dac <= '1';
                  else power_dac                   <= '0';
                  end if;
--        IF cmd56 = x"0020" THEN power_all  <= '1';
--        END IF;
                  std_dbg                          <= '0';
               when s10 =>              -- set control register
                  ctrl_reg <= cmd56;
                  std_dbg  <= '0';
                  
               when s12 =>              -- power control
                  count   <= (others => '0');
                  std_dbg <= '0';
                  
               when s1201a =>           -- disable the buffers
                  sig_fe_en <= '0';
               when s1201b =>           -- wait
               when s1201c =>           -- test if 100 ms
                  count <= count + 1;
               when s1201d =>           -- Turn CALIB/CIB off
                  PWR_ENABLE_GR2 <= '0';
--               power_spare <= '0';
--               power_fe_en <= '0';

               when s1202a =>           -- Turn CALIB/CIB on
                  PWR_ENABLE_GR2 <= '1';
                  --power_spare <= '1';
                  --power_fe_en <= '1';
                  sig_fe_en      <= '1';
               when s1202b =>           -- wait
               when s1202c =>           -- test if 2 sec
                  count <= count + 1;
                  
               when s1203 =>            -- turn HBU VDAC/VREF off
                  PWR_ENABLE_GR1 <= '0';
--               sw_1V5to3V3 <= '0';
               when s1204 =>            -- turn HBU VDAC/VREF on
                  PWR_ENABLE_GR1 <= '1';
--               sw_1V5to3V3 <= '1';
               when s1205 =>
                  PWR_ENABLE_GR3 <= '0';
               when s1206 =>
                  PWR_ENABLE_GR3 <= '1';
               when s1207 =>
                  PWR_ENABLE_GR4 <= '0';
               when s1208 =>
                  PWR_ENABLE_GR4 <= '1';
               when s1209 =>
                  OC_ENABLE <= '0';
               when s120A =>
                  OC_ENABLE <= '1';
               when s16 =>              -- readout time
                  ro_time <= cmd56;
                  std_dbg <= '0';
               when s21 =>              -- set the length of the bxid (in 25 ns steps)
                  if (cmd56(7 downto 3) /= "00000") then
                     timing_cfg_int <= cmd56;
                  end if;
                  std_dbg     <= '0';
               when s22 =>              -- set power pulsing delay register
                  dly_reg_int <= cmd56;
                  std_dbg     <= '0';
               when s24 =>              -- set sc_select
                  sc_select <= cmd56(0);
               when s30 =>              -- set trigger count
                  trig_cfg <= cmd56;
               when s60 =>              -- set analog probe
                  srin_read <= '1';
                  clk_read  <= '0';
                  count     <= (others => '0');
               when s61 =>
                  srin_read <= '0';
                  clk_read  <= '0';
                  count     <= (others => '0');
               when s62 =>
                  count <= count + 1;
               when s63 =>
                  clk_read <= '1';
                  out_num  <= out_num + 1;
                  count    <= (others => '0');
               when s64 =>
                  count <= count + 1;
               when s65 =>

               when s70 =>              -- set DIF ID
                  dif_id <= cmd56;

               when s80 =>              -- set digital probe
                  srin_probe <= '1';
                  clk_probe  <= '0';
                  count      <= (others => '0');
               when s81 =>
                  srin_probe <= '0';
                  clk_probe  <= '0';
                  count      <= (others => '0');
               when s82 =>
                  count <= count + 1;
               when s83 =>
                  clk_probe <= '1';
                  out_num   <= out_num + 1;
                  count     <= (others => '0');
               when s84 =>
                  count <= count + 1;
               when s85 =>

               when s90 =>
                  std_dbg <= '1';
               when s91 =>
                  reset_dif        <= '0';
                  reset_slab       <= '1';
                  reset_all        <= '0';
                  reset_sc         <= '0';	-- active high
                  reset_read_probe <= '1';
                  --reset_read  <= '1';
                  --reset_probe  <= '1';
                  reset_cal        <= '1';

                  set_sleep <= '0';
                  set_ready <= '0';

                  power_off  <= '0';
                  power_on   <= '0';
                  power_auto <= '0';
                  power_sel  <= '0';
                  power_ana  <= '0';
                  power_dig  <= '0';
                  power_sca  <= '0';
                  power_adc  <= '0';
                  power_dac  <= '0';
                  power_all  <= '0';

               when s92 =>
                  end_std_cmd <= '1';
               when s93 =>
--        end_std_cmd <= '0';
                  end_std_cmd <= '1';
               when s94 =>
                  std_dbg <= '0';
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;

   std_cmd_done <= end_std_cmd;
   -------------------------------------------------------------------
   --done_proc : process (end_std_cmd, rst)  --clk
   -------------------------------------------------------------------
   --begin
   --   if rst = '1' then
   --      std_cmd_done <= '0';
   --   else
   --      std_cmd_done <= end_std_cmd;
   --   end if;
   --end process done_proc;

end architecture rtl;
