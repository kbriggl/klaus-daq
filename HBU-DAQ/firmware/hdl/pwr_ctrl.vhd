
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwr_ctrl is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;
      set_sleep    : in std_logic;

      power_off    : in std_logic;
      power_on     : in std_logic;
      power_auto   : in std_logic;
      power_sel    : in std_logic;
      power_ana    : in std_logic;
      power_dig    : in std_logic;
      power_sca    : in std_logic;
      power_adc    : in std_logic;
      power_dac    : in std_logic;
      power_all    : in std_logic;
      pp_mandatory : in std_logic;      --powerpulsing must be set on when pp_mandatory=='1' !

      pwr_adc_pul : in std_logic;       --power control from the ACQ module
      pwr_ana_pul : in std_logic;
      pwr_dac_pul : in std_logic;
      pwr_dig_pul : in std_logic;
      pwr_sca_pul : in std_logic;

      power_puling_enabled : out std_logic;  --1 = power pulsing is enabled

      pwr_ana : out std_logic;
      pwr_dig : out std_logic;
      pwr_sca : out std_logic;
      pwr_adc : out std_logic;
      pwr_dac : out std_logic
      );
end pwr_ctrl;

architecture rtl of pwr_ctrl is

   signal power_puls_mode : std_logic := '0';
   signal pwr_ana_int     : std_logic := '0';
   signal pwr_dig_int     : std_logic := '0';
   signal pwr_sca_int     : std_logic := '0';
   signal pwr_adc_int     : std_logic := '0';
   signal pwr_dac_int     : std_logic := '0';

begin
   power_puling_enabled <= power_puls_mode;
   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            power_puls_mode <= '0';
            pwr_ana_int     <= '0';
            pwr_dig_int     <= '0';
            pwr_sca_int     <= '0';
            pwr_adc_int     <= '0';
            pwr_dac_int     <= '0';
         elsif (clk_strobe_5 = '1') then
            if power_off = '1' then
               power_puls_mode <= '0';
               pwr_ana_int     <= '0';
               pwr_dig_int     <= '0';
               pwr_sca_int     <= '0';
               pwr_adc_int     <= '0';
               pwr_dac_int     <= '0';
            elsif set_sleep = '1' then
               power_puls_mode <= '0';
               pwr_ana_int     <= '0';
               pwr_dig_int     <= '0';
               pwr_sca_int     <= '0';
               pwr_adc_int     <= '0';
               pwr_dac_int     <= '0';
            elsif power_on = '1' or power_all = '1' then
               power_puls_mode <= '0';
               pwr_ana_int     <= '1';
               pwr_dig_int     <= '1';
               pwr_sca_int     <= '1';
               pwr_adc_int     <= '1';
               pwr_dac_int     <= '1';
            elsif power_auto = '1' then
               power_puls_mode <= '1';
            elsif power_sel = '1' then
               power_puls_mode <= '0';
               pwr_ana_int     <= power_ana;
               pwr_dig_int     <= power_dig;
               pwr_sca_int     <= power_sca;
               pwr_adc_int     <= power_adc;
               pwr_dac_int     <= power_dac;
            --else --??? redundant
            --   pwr_ana_int <= pwr_ana_int;
            --   pwr_dig_int <= pwr_dig_int;
            --   pwr_sca_int <= pwr_sca_int;
            --   pwr_adc_int <= pwr_adc_int;
            --   pwr_dac_int <= pwr_dac_int;
            end if;  --power conditions
            if (pp_mandatory = '1') then  --enforce usage of powerpulsing when requested. overrides previous assignments
               power_puls_mode <= '1';
            end if;  --PP mandatory
         end if;  --strobe
      --else --fix: a latch here?
      --   power_puls_mode <= power_puls_mode;
      --   pwr_ana_int     <= pwr_ana_int;
      --   pwr_dig_int     <= pwr_dig_int;
      --   pwr_sca_int     <= pwr_sca_int;
      --   pwr_adc_int     <= pwr_adc_int;
      --   pwr_dac_int     <= pwr_dac_int;
      end if;  --clk
   end process output_proc;

   -----------------------------------------------------------------
   power_mode_proc : process (power_puls_mode, pwr_adc_int, pwr_adc_pul, pwr_ana_int, pwr_ana_pul, pwr_dac_int, pwr_dac_pul, pwr_dig_int, pwr_dig_pul, pwr_sca_int, pwr_sca_pul)
   -----------------------------------------------------------------
   begin
      if power_puls_mode = '1' then
         pwr_ana <= pwr_ana_pul;
         pwr_dig <= pwr_dig_pul;
         pwr_sca <= pwr_sca_pul;
         pwr_adc <= pwr_adc_pul;
         pwr_dac <= pwr_dac_pul;
      else
         pwr_ana <= pwr_ana_int;
         pwr_dig <= pwr_dig_int;
         pwr_sca <= pwr_sca_int;
         pwr_adc <= pwr_adc_int;
         pwr_dac <= pwr_dac_int;
      end if;
   end process power_mode_proc;

end architecture rtl;

