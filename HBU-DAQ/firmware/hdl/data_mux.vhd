
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

entity data_mux is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;

      ack_wr_req    : in std_logic;
      ack_data      : in std_logic_vector (7 downto 0);
      cal_wr_req    : in std_logic;
      cal_data_out  : in std_logic_vector (7 downto 0);
      ro_wr_req     : in std_logic;
      ro_data_out   : in std_logic_vector (7 downto 0);
      sc_wr_req     : in std_logic;
      sc_data_out   : in std_logic_vector (7 downto 0);
      stat_wr_req   : in std_logic;
      stat_data_out : in std_logic_vector (7 downto 0);

      wr_req  : out std_logic;
      tx_data : out std_logic_vector (7 downto 0)
      );
end data_mux;

architecture rtl of data_mux is

   signal tx_data_int : std_logic_vector (7 downto 0);

begin

   wr_req  <= ack_wr_req or cal_wr_req or ro_wr_req or sc_wr_req or stat_wr_req;
   tx_data <= tx_data_int;

   -----------------------------------------------------------------
   data_out_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            tx_data_int <= (others => '0');
         elsif (clk_strobe_5 = '1') then
            if ack_wr_req = '1' then
               tx_data_int <= ack_data;
            elsif cal_wr_req = '1' then
               tx_data_int <= cal_data_out;
            elsif ro_wr_req = '1' then
               tx_data_int <= ro_data_out;
            elsif sc_wr_req = '1' then
               tx_data_int <= sc_data_out;
            elsif stat_wr_req = '1' then
               tx_data_int <= stat_data_out;
            else
               tx_data_int <= tx_data_int;
            end if;
         end if;  --strobe
      end if;  --clk
   end process data_out_proc;
   
end architecture rtl;
