-------------------------------------------------------------------------------
-- Title      : clock control
-- Project    : 
-------------------------------------------------------------------------------
-- File       : clk_ctrl.vhd
-- Author     : Jiri Kvasnicka (jiri.kvasnicka@desy.de), (kvas@fzu.cz)
-- Company    : DESY / Institute of Physics ASCR
-- Created    : 2019-05-03
-- Last update: 2019-05-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Generates the clocks for whole DIF. Originally contained a lot of
-- asynchronous logic, which was slowly being removed and converged to a
-- mostly synchronous design
--          
-- clk_cnt       | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |...
-- clk_phase     | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |...
--                    ___.___.___.___                 ___.___.___.___             
-- clk_acq    ___.___/               \___.___.___.___/               \___.___.___.
-- (clk_5M)
-- acq_clk_sel : _________________________< s >___________________________< S >______
--                                   sampling point                 sampling point
--                ___                             ___                             ___
-- strobe_int    /   \___.___.___.___.___.___.___/   \___.___.___.___.___.___.___/
--                            ___                             ___                      
-- clk_strobe5   .___.___.___/   \___.___.___.___.___.___.___/   \___.___.___.___.___.
--    (the propagation delay from strobe_int is defined by length of strobe_pipe
--                ___.___.___.___.___.___.___.___
-- ms_tick_int   /                               \___.___.___.___.___.___.___.___.___.
--                            ___                             
-- ms_tick_pipe  .___.___.___/   \___.___.___.___.___.___.___.___.___.___.___.___.___._
--  - goes together with clk_strobe5 and it goes only for 1 fclk clock cycle
--                ___                             ___                             ___
-- clk_acq_rising/   \___.___.___.___.___.___.___/   \___.___.___.___.___.___.___/
--
--
-- for 4 us bxid length:
-- clk_cnt       |159|158|157|...| 81| 80| 79| 78| 77|...| 2 | 1 | 0 |159|158|157|...
-- clk_phase     |159|158|157|...| 81| 80| 79| 78| 77|...| 2 | 1 | 0 |159|158|157|...
--                    ___.___.....___.___.___                             .___.___
-- clk_acq    ___.___/                       \___.___.....___.___.___.___/
--                ___                                                 ___
-- clk_acq_rising/   \___.___.___.___.___.___.___.___.....___.___.___/   \___.___
--
-------------------------------------------------------------------------------
-- Copyright (c) 2019 DESY / Institute of Physics ASCR
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-00-00  1.0      kvas    Created
-- 2019-05-03  1.1      kvas    clock counter counts down. Variable bxid length
-- 2019-05-06  1.2      kvas    added faster clock for faster synchronizing
-- 2019-05-07  1.3      kvas    MMCM architecture changed significantly,
--                              added 160 and 320 MHz derived clocks
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity clk_ctrl is
   port(
      rst               : in std_logic;  -- for resseting of the DCM/PLL
      clk_board_100_buf : in std_logic;  -- direct input from oscillator. (100 MHz in DIF2)

      hdmi_clk_in      : in  std_logic;  -- clock from the HDMI. (needs to be driven from bufg to get from bank 13 to bank 35)
      hdmi_clk_dcm_buf : out std_logic;  -- DCM clock output after buffer (40 MHz from HDMI only)
      hdmi_locked      : out std_logic;  -- DCM locked on the clk input (hdmi_clk_out is stable)
      clk_syn          : in  std_logic;  -- Synchronize phase of the 5MHz/250kHz clock. Acts as synchronous reset. should come from a register clocked by fclk

      clk_sel   : out std_logic;        -- not used
      clk_duty  : out std_logic;        -- not used
      phase_det : out std_logic;        -- not used

      acq_clk_sel : in  std_logic;                     -- selects which clock to use. '0'=5MHz, '1'=250kHz
      bxid_length : in  std_logic_vector(7 downto 0);  -- length of the acquisition in the acq_clk_sel=1 mode. use 160 for testbeam mode (4us) and 8 for ILC mode
      clk_acq     : out std_logic;                     -- not a real clock, just a combinational signal

      --clock transition pre-warning signals
      clk_acq_rising  : out std_logic;  -- the clk_acq will go from 0 -> 1 in the next fclk clock cycle. Inhibit is ignored for this signal. fclk domain
      clk_acq_falling : out std_logic;  -- the clk_acq will go from 1 -> 0 in the next fclk clock cycle. Inhibit is ignored for this signal. fclk domain

      clk_readout : in std_logic;
      ro_clk_sel  : in std_logic;

      -- clk          : out std_logic;     --intrinsically bad. User logic generated 5MHz from fclk. To be removed
      fclk     : out std_logic;         --clk 40MHz
      fclk_160 : out std_logic;         -- 160 MHz clock hopefully in sync with fclk
      fclk_320 : out std_logic;         -- 320 MHz clock hopefully in sync with fclk


      clk_strobe_5 : out std_logic;                     --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      clk_phase    : out std_logic_vector(7 downto 0);  --the direct output of the clk cnt. counts down to 0, then restarts to bxid_length -1.

      srst_40 : out std_logic;          --generated reset after pll is locked
      ms_tick : out std_logic;          --a 1ms tick in 40MHz clock domain, synchronized with clk_strobe_5

--  clk0_40M_en : IN     std_logic;
--  clk0_5M_en  : IN     std_logic;
--  clk1_40M_en : IN     std_logic;
--  clk1_5M_en  : IN     std_logic;
--  clk2_40M_en : IN     std_logic;
--  clk2_5M_en  : IN     std_logic;
      clk_40M_inhibit : in std_logic;   -- when 1, the 40MHz output clock is not propagated to the ouputs
      clk_5M_inhibit  : in std_logic;   -- inhibits the ackquisition clock at the output during slowcontrol programming (due to the noise)

      clk0_40M_n : out std_logic;
      clk0_40M_p : out std_logic;
      clk0_5M_n  : out std_logic;
      clk0_5M_p  : out std_logic;
      clk1_40M_n : out std_logic;
      clk1_40M_p : out std_logic;
      clk1_5M_n  : out std_logic;
      clk1_5M_p  : out std_logic;
      clk2_40M_n : out std_logic;
      clk2_40M_p : out std_logic;
      clk2_5M_n  : out std_logic;
      clk2_5M_p  : out std_logic
      );

end clk_ctrl;

architecture struct of clk_ctrl is
   attribute ASYNC_REG : string;

   -- Internal signal declarations
   signal clk_5m_m : std_logic := '0';
   signal clk_40m  : std_logic := '0';  --40 MHz clock multiplexed from HDMI or onboard oscillator. 
   signal clk_160m : std_logic := '0';  --160 MHz clock multiplexed from HDMI or onboard oscillator. 
   signal clk_320m : std_logic := '0';  --320 MHz clock multiplexed from HDMI or onboard oscillator. 
--   SIGNAL clk_5m      : std_logic; --removed by Jiri
   signal clk_xm_m : std_logic := '0';
--   SIGNAL clk_xm       :  std_logic;  --removed by Jiri

   signal clk0_40m : std_logic := '0';
   signal clk1_40m : std_logic := '0';
   signal clk2_40m : std_logic := '0';
   signal clk0_5m  : std_logic := '0';
   signal clk1_5m  : std_logic := '0';
   signal clk2_5m  : std_logic := '0';

--   SIGNAL clk_5m_cnt : INTEGER RANGE 0 TO 7; --removed by Jiri
--   SIGNAL clk_5m_num : INTEGER RANGE 0 TO 7; --removed by Jiri

--   SIGNAL clk_xm_cnt : INTEGER RANGE 0 TO 31; --removed by Jiri
--   SIGNAL clk_xm_num : INTEGER RANGE 0 TO 31; --removed by Jiri

--   signal hdmi_clkfb_in          : std_logic; --added by jiri: DCM feadback in
   signal hdmi_in_bufg : std_logic;     --input clock going directly to the bufg

   signal hdmi_clk0                           : std_logic                    := '0';              --added by jiri: - signal from the hdmi ibufgds
   signal hdmi_clk0_buf                       : std_logic                    := '0';              --added by jiri: output clock 40MHz
   signal hdmi_clk160                         : std_logic;                                        --160 MHz derived from the HDMI clock
   signal hdmi_clk320                         : std_logic;                                        --320 MHz derived from the HDMI clock
   signal hdmi_clk_locked                     : std_logic                    := '0';
   signal hdmi_clk_locked_syn                 : std_logic_vector(3 downto 0) := (others => '0');  --synchronization to the board clock
   attribute ASYNC_REG of hdmi_clk_locked_syn : signal is "TRUE";

   signal clk_cnt         : unsigned (7 downto 0) := (others => '0');  --counts from 0 to 159
   signal clk_cnt_next    : unsigned (7 downto 0) := (others => '0');  -- next counter value
   signal acq_clk_sel_reg : std_logic             := '0';              -- acq_clk_sel registered only when cnt=0

   signal presence_counter_hdmi : unsigned(1 downto 0)         := (others => '0');  --counts the hdmi clock
   signal pc_sync_reg           : std_logic_vector(4 downto 0) := (others => '0');  --persence counter synchronisation registers
   signal detector_counter      : unsigned(2 downto 0)         := (others => '0');  -- count with the board 40MHz clock
   signal hdmi_clk_present      : std_logic                    := '0';              --indicates the presence of the hdmi clock
   signal clk40_selector        : std_logic                    := '0';

   attribute ASYNC_REG of pc_sync_reg : signal is "TRUE";

   signal mmcm_rst_cnt : unsigned(3 downto 0) := (others => '0');  -- reset counter for the mmcm. Needed for clkin_switching
   signal mmcm_rst     : std_logic            := '1';              --reset for the mmcm


   signal clk_board_40 : std_logic;     --PLL output. 40 MHz generated from board 100 MHz osciallator

   signal clk_board_fb     : std_logic;  --feedback for board clock pll
   signal clk_board_fb_buf : std_logic;  --BUFG buffered feedback

   signal clk_hdmi_fb     : std_logic;  --feadback output
   signal clk_hdmi_fb_buf : std_logic;  --buffered geedback

   -- signal clk_int                : std_logic;                                        --copy of the clk signal
   signal ms_tick_cnt            : unsigned(15 downto 0)        := (others => '0');
   signal clk_40M_inhibit_sync   : std_logic_vector(1 downto 0) := (others => '0');  --synchronization registers. High bit contains the synchronized signal
   signal clk_40M_D1, clk_40M_D2 : std_logic                    := '0';              --inputs to the 40MHz oddr registers

   attribute ASYNC_REG of clk_40M_inhibit_sync : signal is "TRUE";

   signal strobe_int   : std_logic                           := '0';              --strobe signal annoucing start of the 5 MHz cycle. fclk
   signal strobe_pipe  : std_logic_vector(2 downto 0)        := (others => '0');  -- pipeline register, shifted from 0 to 2. should help routing
   signal ms_tick_int  : std_logic                           := '0';
   signal ms_tick_pipe : std_logic_vector(strobe_pipe'range) := (others => '0');  --pipeline register - must be of a same length as a strobe_pipe

   signal clk_board_locked            : std_logic                    := '0';              --lock from the onboard oscillator
   signal locked_sync                 : std_logic_vector(2 downto 0) := (others => '0');  --synchronisation to the clk_40m (after bufg)
   attribute ASYNC_REG of locked_sync : signal is "TRUE";

   signal srst_40_pipe : std_logic_vector(2 downto 0) := (others => '0');                            --pipe register for srst to help routing
begin
   ms_tick_cnt_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then                                                                   --no reset is really necessary for the ms tick
         if (strobe_int = '1') then                                                                  --count only in 200 ns steps (5MHz)
            ms_tick_cnt <= ms_tick_cnt + 1;
            if (ms_tick_cnt >= 4999) then                                                            --5000x200ns = 1ms
               ms_tick_cnt <= (others => '0');
            end if;
         end if;
      end if;  --clk
   end process;
   ms_tick_int <= '1' when (ms_tick_cnt = to_unsigned(1, ms_tick_cnt'length)) else '0';              --pulse will be 8 cycles lon
   ms_tick_pipe_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         ms_tick_pipe <= ms_tick_pipe(ms_tick_pipe'high-1 downto 0) & (ms_tick_int and strobe_int);  --only 1 pulse will go through (in fclk domain)
      end if;
   end process;
   ms_tick <= ms_tick_pipe(ms_tick_pipe'high);

   ----------------------------------------------
   -- rst
   ----------------------------------------------

   locked_sync_proc : process(clk_40m)
   begin
      if (rising_edge(clk_40m)) then
         locked_sync <= locked_sync(locked_sync'high-1 downto 0) & clk_board_locked;
      end if;  --clk
   end process;

   locked_rst_por_100_SRL16E_inst : SRL16E generic map (INIT => X"FFFF")
      port map (A0  => '1', A1 => '1', A2 => '1', A3 => '1', D => '0',
                Q   => srst_40_pipe(0),
                CE  => locked_sync(locked_sync'high),
                CLK => clk_40m);
   srst_pipe_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         srst_40_pipe(srst_40_pipe'high downto 1) <= srst_40_pipe(srst_40_pipe'high-1 downto 0);  --first bit is assigned gy the srl
      end if;  --clk
   end process;
   srst_40 <= srst_40_pipe(srst_40_pipe'high);


   mmcm_rst_proc : process(clk_board_40)  --extends the reset pulse for a few cycles (according to mmcm_rst_cnt size)
   begin
      if rising_edge(clk_board_40) then
         --if ((hdmi_clk_locked_syn(hdmi_clk_locked_syn'high) and hdmi_clk_present) xor clk40_selector) = '1' then
         if (hdmi_clk_present xor clk40_selector) = '1' then
            --hdmi lock status changed
            mmcm_rst_cnt <= (mmcm_rst_cnt'range => '0');
            mmcm_rst     <= '1';          --start the reset
         else
            if mmcm_rst_cnt = (mmcm_rst_cnt'range => '1') then
               mmcm_rst <= '0';           --reset counter at max -> reset expired
            else
               mmcm_rst     <= '1';       --not yet reached the maximum, keep reset
               mmcm_rst_cnt <= mmcm_rst_cnt + 1;
            end if;  --mmcm_rst_cnt == (others=>'1')
         end if;
      end if;  --clk
   end process;


   ------------------------------------------------------------------------
   -- Conversion from 100 MHz  to 40 MHz
   ------------------------------------------------------------------------
   PLLE2_BASE_inst : PLLE2_BASE
      generic map (
         BANDWIDTH          => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
         CLKFBOUT_MULT      => 42,           -- Multiply value for all CLKOUT, (2-64)
         CLKFBOUT_PHASE     => 0.0,          -- Phase offset in degrees of CLKFB, (-360.000-360.000).
         CLKIN1_PERIOD      => 10.0,         -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
         -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
         CLKOUT0_DIVIDE     => 21,
         CLKOUT1_DIVIDE     => 1,
         CLKOUT2_DIVIDE     => 1,
         CLKOUT3_DIVIDE     => 1,
         CLKOUT4_DIVIDE     => 1,
         CLKOUT5_DIVIDE     => 1,
         -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
         CLKOUT0_DUTY_CYCLE => 0.5,
         CLKOUT1_DUTY_CYCLE => 0.5,
         CLKOUT2_DUTY_CYCLE => 0.5,
         CLKOUT3_DUTY_CYCLE => 0.5,
         CLKOUT4_DUTY_CYCLE => 0.5,
         CLKOUT5_DUTY_CYCLE => 0.5,
         -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
         CLKOUT0_PHASE      => 0.0,
         CLKOUT1_PHASE      => 0.0,
         CLKOUT2_PHASE      => 0.0,
         CLKOUT3_PHASE      => 0.0,
         CLKOUT4_PHASE      => 0.0,
         CLKOUT5_PHASE      => 0.0,
         DIVCLK_DIVIDE      => 5,            -- Master division value, (1-56)
         REF_JITTER1        => 0.02,         -- Reference input jitter in UI, (0.000-0.999).
         STARTUP_WAIT       => "TRUE"        -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
         )
      port map (
         -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
         CLKOUT0  => clk_board_40,           -- 1-bit output: CLKOUT0
         CLKOUT1  => open,                   -- 1-bit output: CLKOUT1
         CLKOUT2  => open,                   -- 1-bit output: CLKOUT2
         CLKOUT3  => open,                   -- 1-bit output: CLKOUT3
         CLKOUT4  => open,                   -- 1-bit output: CLKOUT4
         CLKOUT5  => open,                   -- 1-bit output: CLKOUT5
         -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
         CLKFBOUT => clk_board_fb,           -- 1-bit output: Feedback clock
         LOCKED   => clk_board_locked,       -- 1-bit output: LOCK
         CLKIN1   => clk_board_100_buf,      -- 1-bit input: Input clock
         -- Control Ports: 1-bit (each) input: PLL control ports
         PWRDWN   => '0',                    -- 1-bit input: Power-down
         RST      => rst,                    -- 1-bit input: Reset
         -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
         CLKFBIN  => clk_board_fb_buf        -- 1-bit input: Feedback clock
         );

   clk_sel   <= '0';
   clk_duty  <= '1';
   phase_det <= '1';

   fclk     <= clk_40m;
   fclk_160 <= clk_160m;
   fclk_320 <= clk_320m;

   clk_acq_reg : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         clk_acq <= clk_5m_m;
      end if;  --clk
   end process;

   hdmi_clk_dcm_buf <= hdmi_clk0_buf;

   clk_5m_m <= clk_readout when ro_clk_sel = '1' else clk_xm_m;

   MMCME2_ADV_inst : MMCME2_ADV
      generic map (
         BANDWIDTH            => "OPTIMIZED",    -- Jitter programming (OPTIMIZED, HIGH, LOW)
         CLKFBOUT_MULT_F      => 24.0,           -- Multiply value for all CLKOUT (2.000-64.000).
         CLKFBOUT_PHASE       => 0.0,            -- Phase offset in degrees of CLKFB (-360.000-360.000).
         -- CLKIN_PERIOD: Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
         CLKIN1_PERIOD        => 25.0,
         CLKIN2_PERIOD        => 25.0,
         -- CLKOUT0_DIVIDE - CLKOUT6_DIVIDE: Divide amount for CLKOUT (1-128)
         CLKOUT1_DIVIDE       => 6,
         CLKOUT2_DIVIDE       => 3,
         CLKOUT3_DIVIDE       => 1,
         CLKOUT4_DIVIDE       => 1,
         CLKOUT5_DIVIDE       => 1,
         CLKOUT6_DIVIDE       => 1,
         CLKOUT0_DIVIDE_F     => 24.0,           -- Divide amount for CLKOUT0 (1.000-128.000).
         -- CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for CLKOUT outputs (0.01-0.99).
         CLKOUT0_DUTY_CYCLE   => 0.5,
         CLKOUT1_DUTY_CYCLE   => 0.5,
         CLKOUT2_DUTY_CYCLE   => 0.5,
         CLKOUT3_DUTY_CYCLE   => 0.5,
         CLKOUT4_DUTY_CYCLE   => 0.5,
         CLKOUT5_DUTY_CYCLE   => 0.5,
         CLKOUT6_DUTY_CYCLE   => 0.5,
         -- CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for CLKOUT outputs (-360.000-360.000).
         CLKOUT0_PHASE        => 0.0,
         CLKOUT1_PHASE        => 15.0,           --160 MHz clock has much less fanout and needs to be delayed for 140 ps, otherwise timing closure won't work
         CLKOUT2_PHASE        => 0.0,
         CLKOUT3_PHASE        => 0.0,
         CLKOUT4_PHASE        => 0.0,
         CLKOUT5_PHASE        => 0.0,
         CLKOUT6_PHASE        => 0.0,
         CLKOUT4_CASCADE      => false,          -- Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
         COMPENSATION         => "ZHOLD",        -- ZHOLD, BUF_IN, EXTERNAL, INTERNAL
         DIVCLK_DIVIDE        => 1,              -- Master division value (1-106)
         -- REF_JITTER: Reference input jitter in UI (0.000-0.999).
         REF_JITTER1          => 0.02,
         REF_JITTER2          => 0.02,
         STARTUP_WAIT         => true,           -- Delays DONE until MMCM is locked (FALSE, TRUE)
         -- Spread Spectrum: Spread Spectrum Attributes
         SS_EN                => "FALSE",        -- Enables spread spectrum (FALSE, TRUE)
         SS_MODE              => "CENTER_HIGH",  -- CENTER_HIGH, CENTER_LOW, DOWN_HIGH, DOWN_LOW
         SS_MOD_PERIOD        => 10000,          -- Spread spectrum modulation period (ns) (VALUES)
         -- USE_FINE_PS: Fine phase shift enable (TRUE/FALSE)
         CLKFBOUT_USE_FINE_PS => false,
         CLKOUT0_USE_FINE_PS  => false,
         CLKOUT1_USE_FINE_PS  => false,
         CLKOUT2_USE_FINE_PS  => false,
         CLKOUT3_USE_FINE_PS  => false,
         CLKOUT4_USE_FINE_PS  => false,
         CLKOUT5_USE_FINE_PS  => false,
         CLKOUT6_USE_FINE_PS  => false
         )
      port map (
         -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
         CLKOUT0      => hdmi_clk0,              -- 1-bit output: CLKOUT0
         CLKOUT0B     => open,                   -- 1-bit output: Inverted CLKOUT0
         CLKOUT1      => hdmi_clk160,            -- 1-bit output: CLKOUT1
         CLKOUT1B     => open,                   -- 1-bit output: Inverted CLKOUT1
         CLKOUT2      => hdmi_clk320,            -- 1-bit output: CLKOUT2
         CLKOUT2B     => open,                   -- 1-bit output: Inverted CLKOUT2
         CLKOUT3      => open,                   -- 1-bit output: CLKOUT3
         CLKOUT3B     => open,                   -- 1-bit output: Inverted CLKOUT3
         CLKOUT4      => open,                   -- 1-bit output: CLKOUT4
         CLKOUT5      => open,                   -- 1-bit output: CLKOUT5
         CLKOUT6      => open,                   -- 1-bit output: CLKOUT6
         -- DRP Ports: 16-bit (each) output: Dynamic reconfiguration ports
         DO           => open,                   -- 16-bit output: DRP data
         DRDY         => open,                   -- 1-bit output: DRP ready
         -- Dynamic Phase Shift Ports: 1-bit (each) output: Ports used for dynamic phase shifting of the outputs
         PSDONE       => open,                   -- 1-bit output: Phase shift done
         -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
         CLKFBOUT     => clk_hdmi_fb,            -- 1-bit output: Feedback clock
         CLKFBOUTB    => open,                   -- 1-bit output: Inverted CLKFBOUT
         -- Status Ports: 1-bit (each) output: MMCM status ports
         CLKFBSTOPPED => open,                   -- 1-bit output: Feedback clock stopped
         CLKINSTOPPED => open,                   -- 1-bit output: Input clock stopped
         LOCKED       => hdmi_clk_locked,        -- 1-bit output: LOCK
         -- Clock Inputs: 1-bit (each) input: Clock inputs
         CLKIN1       => hdmi_clk_in,            -- 1-bit input: Clock
         CLKIN2       => clk_board_40,           -- 1-bit input: Secondary clock
         -- Control Ports: 1-bit (each) input: MMCM control ports
         CLKINSEL     => clk40_selector,         -- 1-bit input: Clock select, High=CLKIN1 Low=CLKIN2
         PWRDWN       => '0',                    -- 1-bit input: Power-down
         RST          => mmcm_rst,               -- 1-bit input: Reset
         -- DRP Ports: 7-bit (each) input: Dynamic reconfiguration ports
         DADDR        => (others => '0'),        -- 7-bit input: DRP address
         DCLK         => '0',                    -- 1-bit input: DRP clock
         DEN          => '0',                    -- 1-bit input: DRP enable
         DI           => (others => '0'),        -- 16-bit input: DRP data
         DWE          => '0',                    -- 1-bit input: DRP write enable
         -- Dynamic Phase Shift Ports: 1-bit (each) input: Ports used for dynamic phase shifting of the outputs
         PSCLK        => '0',                    -- 1-bit input: Phase shift clock
         PSEN         => '0',                    -- 1-bit input: Phase shift enable
         PSINCDEC     => '0',                    -- 1-bit input: Phase shift increment/decrement
         -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
         CLKFBIN      => clk_hdmi_fb_buf         -- 1-bit input: Feedback clock
         );
   hdmi_locked <= hdmi_clk_locked;

   strobe_pipe_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         strobe_pipe <= strobe_pipe(strobe_pipe'high-1 downto 0) & strobe_int;
      end if;
   end process;
   clk_strobe_5 <= strobe_pipe(strobe_pipe'high);

   strobe_int <= '1' when (clk_cnt(2 downto 0) = "111") else '0';

   clk_cnt_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         clk_cnt <= clk_cnt_next;
      end if;  --clk
   end process;
   clk_phase <= std_logic_vector(clk_cnt);

   clk_5xm_proc2 : process(acq_clk_sel_reg, bxid_length, clk_5M_inhibit, clk_cnt)  --rewriten, removed the register clk_xm_m 
   begin
      --compare whether the clock counter is . if the length is 160, it will compare with a valu shifted by 1
      if acq_clk_sel_reg = '1' then                                                --'0'=5MHz, '1'=250kHz
         if (clk_cnt >= unsigned('0' & bxid_length(bxid_length'high downto 1))) then
            clk_xm_m <= '1';
         else                                                                      --cnt above half of length
            clk_xm_m <= '0';
         end if;  --cnt above half of length
      else
         --acq_clk_sel_reg == 0
         clk_xm_m <= clk_cnt(2);
      end if;  --acq_clk_sel_reg
      if clk_5M_inhibit = '1' then                                                 -- clock will be overwritten by 0 if inhibit is active 
         clk_xm_m <= '0';
      end if;
   end process;

   clk_acq_rising_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         if clk_cnt = (clk_cnt'range => '0') then
            clk_acq_rising <= '1';
         else
            clk_acq_rising <= '0';
         end if;
      end if;  --clk
   end process;

   clk_acq_falling_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         if acq_clk_sel_reg = '1' then
            if clk_cnt = unsigned('0' & bxid_length(bxid_length'high downto 1)) then
               clk_acq_falling <= '1';
            else
               clk_acq_falling <= '0';
            end if;
         else
            if clk_cnt(2 downto 0) = "100" then
               clk_acq_falling <= '1';
            else
               clk_acq_falling <= '0';
            end if;
         end if;  --clk_sel
      end if;  --clk
   end process;

   acq_clk_sel_reg_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         -- propagate the acq_clk_sel only for clock counts 2 and 3
         if (std_match("00000001", clk_cnt)) then
            acq_clk_sel_reg <= acq_clk_sel;
         end if;  --clk_cnt match
      -- if (clk_cnt(clk_cnt'high downto 1) = (clk_cnt'high-1 downto 1 => '0', 0=>'1')) then --too complicated. std_match instead
      -- end if;
      end if;  --clk
   end process;

   clk_cnt_next_proc : process(acq_clk_sel_reg, bxid_length, clk_cnt, clk_syn)
      variable selected : unsigned(7 downto 0);
   begin
      --when the counter reached0, decide the next value (depends on the mode)   
      selected := clk_cnt;              -- counter by default. the rest of if-cases are overrides for special cases with higher priority
      if (clk_cnt = (clk_cnt'range => '0')) then
         --reached 0 in counting
         if (acq_clk_sel_reg = '1') then
            selected := unsigned(bxid_length);
         else
            selected := to_unsigned(8, 8);
         end if;
      end if;
      --synchronisation on clk_syn command has a priority
      if (clk_syn = '1') then
         selected := to_unsigned(1, selected'length);
      end if;
      clk_cnt_next <= selected - 1;
   end process;

   -----------------------------------------------------------------------------------
   -- global buffers
   -----------------------------------------------------------------------------------
   
   clk_hdmi_bufg_inst : BUFG
      port map (
         I => clk_hdmi_fb,
         O => clk_hdmi_fb_buf
         );
   
   dcm_out_bufg_inst : BUFG
      port map (
         I => hdmi_clk0,
         O => hdmi_clk0_buf
         );

   clk_board_fb_bufg_inst : BUFG
      port map (
         I => clk_board_fb,
         O => clk_board_fb_buf
         );

   clk_40m_BUFGMUX_inst : BUFG
      port map (
         O => clk_40m,                  -- 1-bit output: Clock output
         I => hdmi_clk0                 -- 1-bit input: Clock input
         );

   clk_160m_BUFGMUX_inst : BUFG
      port map (
         O => clk_160m,                 -- 1-bit output: Clock output
         I => hdmi_clk160               -- 1-bit input: Clock input
         );

   clk_320m_BUFGMUX_inst : BUFG
      port map (
         O => clk_320m,                 -- 1-bit output: Clock output
         I => hdmi_clk320               -- 1-bit input: Clock input
         );

   clk_in_bufg : BUFG
      port map (
         O => hdmi_in_bufg,             -- 1-bit output: Clock output
         I => hdmi_clk_in               -- 1-bit input: Clock input
         );

   -----------------------------------------------------------------------------------
   --  block for detecting the presence of the hdmi clock
   -----------------------------------------------------------------------------------
   presence_counter_proc : process(hdmi_in_bufg)
   begin
      if rising_edge(hdmi_in_bufg) then
         presence_counter_hdmi <= presence_counter_hdmi + 1;  --counts forever through overflow
      end if;  --clk
   end process;

   sync_registers_proc : process(clk_board_40)
   begin
      if rising_edge(clk_board_40) then
         --data is shifting from bit 0 to 3)
         pc_sync_reg <= pc_sync_reg(pc_sync_reg'high-1 downto 0) & presence_counter_hdmi(1);
      end if;  --clk
   end process;

   detector_counter_proc : process(clk_board_40)
   begin
      if rising_edge(clk_board_40) then
         if (pc_sync_reg(pc_sync_reg'high) xor pc_sync_reg(pc_sync_reg'high - 1)) = '1' then  --the transition of the hdmi counter detected
            detector_counter <= (others => '0');
            hdmi_clk_present <= '1';                                                          --in the transition of the hdmi clock counter we know, that to clock is there
         else
            if (detector_counter /= (detector_counter'range => '1')) then                     --count only if the counter have not reach its top value
               detector_counter <= detector_counter + 1;
               hdmi_clk_present <= '1';                                                       --the hdmi clock might still be there
            else
               hdmi_clk_present <= '0';                                                       --the clock can not be there when the counter reached its top
            end if;  --otherwise do not count anymore
         end if;  -- transition detect
      end if;  --clk
   end process;

   -- hdmi_clk_locked_syn_proc : process(clk_board_40)
   -- begin
   --    if rising_edge(clk_board_40) then
   --       hdmi_clk_locked_syn <= hdmi_clk_locked_syn(hdmi_clk_locked_syn'high-1 downto 0) & hdmi_clk_locked;
   --    end if;  --clk
   -- end process;

   clk40_selector_proc : process(clk_board_40)
   begin
      if rising_edge(clk_board_40) then
         --clk40_selector   <= hdmi_clk_locked_syn(hdmi_clk_locked_syn'high) and hdmi_clk_present;
         clk40_selector <= hdmi_clk_present;
      end if;
   end process;


   clk_40M_inhibit_sync_proc : process(clk_40m)
   begin
      if rising_edge(clk_40m) then
         clk_40M_inhibit_sync <= clk_40M_inhibit_sync(clk_40M_inhibit_sync'high-1 downto 0) & clk_40M_inhibit;  --shift in the new value
      end if;  --clk
   end process;

   clk_40M_D1 <= '0' when (clk_40M_inhibit_sync(clk_40M_inhibit_sync'high) = '1') else '1';
   clk_40M_D2 <= '0';

   -----------------------------------------------------------------------------------
   -- Output buffers
   -----------------------------------------------------------------------------------
   ODDR_clk0_40m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "OPPOSITE_EDGE",  -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',              -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")           -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk0_40m,                   -- 1-bit DDR output
         C  => clk_40m,                    -- 1-bit clock input
         CE => '1',                        -- 1-bit clock enable input
         D1 => clk_40M_D1,                 -- 1-bit data input (positive edge)
         D2 => clk_40M_D2,                 -- 1-bit data input (negative edge)
         R  => '0',                        -- 1-bit reset input
         S  => '0'                         -- 1-bit set input
         );

   ODDR_clk1_40m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "OPPOSITE_EDGE",  -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',              -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")           -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk1_40m,                   -- 1-bit DDR output
         C  => clk_40m,                    -- 1-bit clock input
         CE => '1',                        -- 1-bit clock enable input
         D1 => clk_40M_D1,                 -- 1-bit data input (positive edge)
         D2 => clk_40M_D2,                 -- 1-bit data input (negative edge)
         R  => '0',                        -- 1-bit reset input
         S  => '0'                         -- 1-bit set input
         );

   ODDR_clk2_40m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "OPPOSITE_EDGE",  -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',              -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")           -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk2_40m,                   -- 1-bit DDR output
         C  => clk_40m,                    -- 1-bit clock input
         CE => '1',                        -- 1-bit clock enable input
         D1 => clk_40M_D1,                 -- 1-bit data input (positive edge)
         D2 => clk_40M_D2,                 -- 1-bit data input (negative edge)
         R  => '0',                        -- 1-bit reset input
         S  => '0'                         -- 1-bit set input
         );

   clk0_40m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk0_40M_p,
         OB => clk0_40M_n,
         I  => clk0_40m
         );

   clk1_40m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk1_40M_p,
         OB => clk1_40M_n,
         I  => clk1_40m
         );

   clk2_40m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk2_40M_p,
         OB => clk2_40M_n,
         I  => clk2_40m
         );


   ODDR_clk0_5m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "SAME_EDGE",   -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',           -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")        -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk0_5m,                 -- 1-bit DDR output
         C  => clk_40m,                 -- 1-bit clock input
         CE => '1',                     -- 1-bit clock enable input
         D1 => clk_5m_m,                -- 1-bit data input (positive edge)
         D2 => clk_5m_m,                -- 1-bit data input (negative edge)
         R  => '0',                     -- 1-bit reset input
         S  => '0'                      -- 1-bit set input
         );
   ODDR_clk1_5m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "SAME_EDGE",   -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',           -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")        -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk1_5m,                 -- 1-bit DDR output
         C  => clk_40m,                 -- 1-bit clock input
         CE => '1',                     -- 1-bit clock enable input
         D1 => clk_5m_m,                -- 1-bit data input (positive edge)
         D2 => clk_5m_m,                -- 1-bit data input (negative edge)
         R  => '0',                     -- 1-bit reset input
         S  => '0'                      -- 1-bit set input
         );
   ODDR_clk2_5m_inst : ODDR
      generic map(
         DDR_CLK_EDGE => "SAME_EDGE",   -- "OPPOSITE_EDGE" or "SAME_EDGE" 
         INIT         => '0',           -- Initial value for Q port ('1' or '0')
         SRTYPE       => "SYNC")        -- Reset Type ("ASYNC" or "SYNC")
      port map (
         Q  => clk2_5m,                 -- 1-bit DDR output
         C  => clk_40m,                 -- 1-bit clock input
         CE => '1',                     -- 1-bit clock enable input
         D1 => clk_5m_m,                -- 1-bit data input (positive edge)
         D2 => clk_5m_m,                -- 1-bit data input (negative edge)
         R  => '0',                     -- 1-bit reset input
         S  => '0'                      -- 1-bit set input
         );

   clk0_5m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk0_5M_p,
         OB => clk0_5M_n,
         I  => clk0_5m
         );

   clk1_5m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk1_5M_p,
         OB => clk1_5M_n,
         I  => clk1_5m
         );

   clk2_5m_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => clk2_5M_p,
         OB => clk2_5M_n,
         I  => clk2_5m
         );

end struct;
