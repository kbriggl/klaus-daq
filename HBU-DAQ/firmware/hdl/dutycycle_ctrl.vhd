-------------------------------------------------------------------------------
-- Title      : duty cycle controller - packet handling
-- Project    : 
-------------------------------------------------------------------------------
-- File       : dutycycle_ctrl.vhd
-- Author     : Jiri Kvasnicka (jiri.kvasnicka@desy.de), (kvas@fzu.cz)
-- Company    : DESY / Institute of Physics ASCR
-- Created    : 2018-11-09
-- Last update: 2019-04-30
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: decodes the packets for configuration of the duty cycle
-------------------------------------------------------------------------------
-- Copyright (c) 2018 DESY / Institute of Physics ASCR
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-11-09  1.0      kvas    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.adif2_types.all;

entity dutycycle_ctrl is
   port (
      fclk         : in  std_logic;     -- main clock
      srst         : in  std_logic;     --sync reset (fclk domain)
      clk_strobe_5 : in  std_logic;
      ms_tick      : in  std_logic;     --1 pulse in slow clock domain (8 fclk tics)
      P_I_CMD_REC  : in  T_CMD_REC;     -- full command record
      cmd_ready    : in  std_logic;
      cmd_end      : in  std_logic;
      dc_cmd_done  : out std_logic;

      DC1_cost0 : out signed(15 downto 0);
      DC1_cost1 : out signed(15 downto 0);
      DC1_cost2 : out signed(15 downto 0);
      DC1_cost3 : out signed(15 downto 0);
      DC1_limit : out signed(47 downto 0);  --decimal=225000;  --5 ms acquisition with cost 9

      --second duty cycle governor: eliinates long-term heat production on POWERBOARD
      DC2_cost0 : out signed(15 downto 0);
      DC2_cost1 : out signed(15 downto 0);
      DC2_cost2 : out signed(15 downto 0);
      DC2_cost3 : out signed(15 downto 0);
      DC2_limit : out signed(47 downto 0)  --decimal=450000000, ~10 s of continous operation --TO_SIGNED(450000000, 48)
      );
end entity dutycycle_ctrl;

architecture rtl of dutycycle_ctrl is
   type STATE_TYPE is (s0,              --idle
                       s1,              --command received
                       s2,              --command for dutycycle
                       s3);             --waiting for the end

   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;
   signal s_cmd_done    : std_logic;    --command done (signal to the cmd_rx)

   --first duty cycle governor
   signal s_DC1_cost0 : signed(15 downto 0) := TO_SIGNED(-1, 16);         --must be a negative number.  counted when everything is off (powerpulsed)
   --three different costs represent different cunsumption levels. At the moment only cost1 is implemented
   signal s_DC1_cost1 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);  -- constant, that will be counted during active time
   signal s_DC1_cost2 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);  -- reserved for other states (conversion)
   signal s_DC1_cost3 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);  -- reserved for other states (readout)
   signal s_DC1_limit : signed(47 downto 0) := X"000000036EE8";           --decimal=225000;  --5 ms acquisition with cost 9
   --second duty cycle governor: eliinates long-term heat production on POWERBOARD
   signal s_DC2_cost0 : signed(15 downto 0) := TO_SIGNED(-1, 16);
   signal s_DC2_cost1 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);
   signal s_DC2_cost2 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);
   signal s_DC2_cost3 : signed(15 downto 0) := TO_SIGNED(C_DC_COST, 16);
   signal s_DC2_limit : signed(47 downto 0) := X"00001AD27480";           --decimal=450000000, ~10 s of continuous operation --TO_SIGNED(450000000, 48)
begin  -- architecture rtl
   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (srst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (P_I_CMD_REC, cmd_end, cmd_ready, current_state)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            --wait for usb_command
            if cmd_ready = '1' then
               next_state <= s1;
            else
               next_state <= s0;
            end if;
         when s1 =>
            case P_I_CMD_REC.TYPE_MODIFIER is
               when x"0009" => next_state <= s2;  -- set the acquisition duty cycle supervisors
               when others  => next_state <= s0;  --not for this module
            end case;
         when s2 =>
            next_state <= s3;
         when s3 =>
            if cmd_end = '1' then
               next_state <= s0;                  --command done
            else
               next_state <= s3;                  --waiting for end of command (from cmd_rx)
            end if;
         when others =>
            next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if srst = '1' then
            s_cmd_done  <= '0';
            s_DC1_cost0 <= TO_SIGNED(-1, 16);  --must be a negative number. 
            s_DC1_cost1 <= TO_SIGNED(C_DC_COST, 16);
            s_DC1_cost2 <= TO_SIGNED(C_DC_COST, 16);
            s_DC1_cost3 <= TO_SIGNED(C_DC_COST, 16);
            s_DC1_limit <= X"000000036EE8";    --decimal=225000;  --5 ms acquisition with cost 9
            s_DC2_cost0 <= TO_SIGNED(-1, 16);
            s_DC2_cost1 <= TO_SIGNED(C_DC_COST, 16);
            s_DC2_cost2 <= TO_SIGNED(C_DC_COST, 16);
            s_DC2_cost3 <= TO_SIGNED(C_DC_COST, 16);
            s_DC2_limit <= X"00001AD27480";    --decimal=450000000, ~10 s of continuous operation --TO_SIGNED(450000000, 48)
            s_cmd_done  <= '0';
         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  s_cmd_done <= '0';
               when s1 => null;
               when s2 =>                      --store the data according to specifier
                  case P_I_CMD_REC.SPECIFIER is
                     when X"DC00" => s_DC1_cost0                <= signed('1' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC01" => s_DC1_cost1                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC02" => s_DC1_cost2                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC03" => s_DC1_cost3                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC08" => s_DC1_limit (15 downto 0)  <= signed(P_I_CMD_REC.DATA);
                     when X"DC09" => s_DC1_limit (31 downto 16) <= signed(P_I_CMD_REC.DATA);
                     when X"DC0A" => s_DC1_limit (47 downto 32) <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC20" => s_DC2_cost0                <= signed('1' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC21" => s_DC2_cost1                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC22" => s_DC2_cost2                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC23" => s_DC2_cost3                <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when X"DC28" => s_DC2_limit (15 downto 0)  <= signed(P_I_CMD_REC.DATA);
                     when X"DC29" => s_DC2_limit (31 downto 16) <= signed(P_I_CMD_REC.DATA);
                     when X"DC2A" => s_DC2_limit (47 downto 32) <= signed('0' & P_I_CMD_REC.DATA(14 downto 0));
                     when others  => null;
                  end case;
               when s3 => s_cmd_done <= '1';
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;
   dc_cmd_done <= s_cmd_done;
   DC1_cost0   <= s_DC1_cost0;
   DC1_cost1   <= s_DC1_cost1;
   DC1_cost2   <= s_DC1_cost2;
   DC1_cost3   <= s_DC1_cost3;
   DC1_limit   <= s_DC1_limit;
   DC2_cost0   <= s_DC2_cost0;
   DC2_cost1   <= s_DC2_cost1;
   DC2_cost2   <= s_DC2_cost2;
   DC2_cost3   <= s_DC2_cost3;
   DC2_limit   <= s_DC2_limit;
end architecture rtl;
