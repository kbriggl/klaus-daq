----------------------------------------------------------------------------------
-- Company: DESY
-- Engineer: Jiri Kvasnicka
-- 
-- Create Date: 02/11/2016 03:09:57 PM
-- Design Name: 
-- Module Name: adif2_top - rtl
-- Project Name: 
-- Target Devices:  ZYNQ
-- Tool Versions: Vivado 2015.4
-- Description: Top module for AHCAL DIF based on Zynq
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.adif2_types.all;

entity adif2_top is
   generic (
      DATE       : std_logic_vector(31 downto 0) := X"20190916";
      SIMULATION : boolean                       := false
      );
   port (
      --  PUDC_B   : inout std_logic;   -- K16 , bank 34, pull-up enable pin. Tied down = pullups enabled during powerup. Tied high = pullup disabled during powerup.
      GCLK : in std_logic;              -- Y9  , bank 13, internal oscillator 100 MHz

      -- signals for uC on the Calib board
      BOOT        : out   std_logic;    -- R7  , bank 13, U5 -> J1.60, booting control of the uC
      NOTBOOT     : out   std_logic;    -- V10 , bank 13, U5 -> J1.62, booting control of the uC
      NOTRESIN    : out   std_logic;    -- W12 , bank 13, U4 -> J1.61, reset for microcontroller, active low
      UART_TX     : in    std_logic;    -- AB12, bank 13, J1 -> J1.79, serial data from uC on the Calib board
      UART_RX     : out   std_logic;    -- AA11, bank 13, J1 -> J1.81, serial data to uC on the Calib board
      UC_CLK_P    : out   std_logic;    -- B16 , bank 35, U9 -> J1.91, clock to calib board. Not used
      UC_CLK_N    : out   std_logic;    -- B17 , bank 35, U9 -> J1.93,
      -- Calib board SPI interface
      SCK0        : out   std_logic;    -- V9  , bank 13, U5 -> J1.112
      MISO0       : in    std_logic;    -- N22 , bank 34, J1.114
      MOSI0       : out   std_logic;    -- V8  , bank 13, U5 -> J1.118
      SSEL        : out   std_logic;    -- W8  , bank 13, U5 -> J1.120
      CALIB_SPARE : inout std_logic;    -- AA12, bank 13, J1 -> J1.77 spare line, not used

      --signals controling the POWER4 board
      PWR_ENABLE_GR1 : out std_logic;   -- Y11 , bank 13, J1.78, enables VREF_+4p5V and VDAC_+5V (for IDAC)
      PWR_ENABLE_GR2 : out std_logic;   -- Y10 , bank 13, J1.80, enables VDD_3d3V_1 for calib board and VCC_+11V (calib board)
      PWR_ENABLE_GR3 : out std_logic;   -- AB11, bank 13, J1.98, enables VDDA for HBU and HV1,HV2,HV3 high voltages for HBU
      PWR_ENABLE_GR4 : out std_logic;   -- AB10, bank 13, J1.100, enables VDDD for HBU

      --DIF buffer enables
      BUF0_OEQ  : out std_logic;        -- W7  , bank 13, output enable for buffer U1, active low (drives the OE/G3 pin of 74LV245APW
      BUF1_OEQ  : out std_logic;        -- W6  , bank 13, output enable for buffer U2, active low (drives the OE/G3 pin of 74LV245APW
      BUF2_OEQ  : out std_logic;        -- W5  , bank 13, output enable for buffer U3, active low (drives the OE/G3 pin of 74LV245APW
      BUF3_OEQ  : out std_logic;        -- U7  , bank 13, output enable for buffer U4, active low (drives the OE/G3 pin of 74LV245APW
      LVDS_EN   : out std_logic;        -- Y8  , bank 13, enable for the LVDS repaters U6-U10. Should be enabled all the time. For later debugging
      OC_ENABLE : out std_logic;        -- R15 , bank 34, Open collector driver enable (-> U18)

      --power pulsing signals
      PWR_ANALOG  : out std_logic;      -- W11 , bank 13, U4 -> J1.51, SPIROC Analogue part power pulsing control (pwr_on_a)
      PWR_DIGITAL : out std_logic;      -- V4  , bank 13, U3 -> J1.56, SPIROC Digital part power pulsing control (pwr_on_d)
      PWR_ADC     : out std_logic;      -- W10 , bank 13, U4 -> J1.55, SPIROC ADC Power pulsing control (pwr_on_adc)
      PWR_DAC     : out std_logic;      -- V12 , bank 13, U4 -> J1.57, SPIROC DAC Power pulsing control (pwr_on_dac)
      PWR_SCA     : out std_logic;      -- U6  , bank 13, U3 -> J1.58, Does not exist for SPIROC 2B anymore

      --HDMI interface
      LVDS_RES_LOW  : out std_logic;    -- AA9 , bank 13, -> U11, pulling down negative input of the LVDS repeaters. Initial state can be defined. Should be hi-Z during operation and 0 during reset
      LVDS_RES_HIGH : out std_logic;    -- AA8 , bank 13, -> U11, pulling up positive input of the LVDS repeaters. Initial state can be defined. Should be hi-Z during operation and 1 during reset
      DD2L_P        : out std_logic;    -- A16 , bank 35, U10 -> J1.132, busy signal
      DD2L_N        : out std_logic;    -- A17 , bank 35, U10 -> J1.134, busy signal
      SPD2L_P       : out std_logic;    -- A18 , bank 35, U10 -> J1.128, data from DIF
      SPD2L_N       : out std_logic;    -- A19 , bank 35, U10 -> J1.130, data from DIF
      SPL2D_P       : in  std_logic;    -- C17 , bank 35, J1.124 -> U11, validation of trigger
      SPL2D_N       : in  std_logic;    -- C18 , bank 35, J1.126 -> U11, validation of trigger
      DL2D_P        : in  std_logic;    -- D18 , bank 35, J1.136 -> U12, data + commands from LDA
      DL2D_N        : in  std_logic;    -- C19 , bank 35, J1.138 -> U12, data + commands from LDA
      HDMI_CLK_P    : in  std_logic;    -- B19 , bank 35, J1.135 -> U13, clock from LDA
      HDMI_CLK_N    : in  std_logic;    -- B20 , bank 35, J1.137 -> U13, clock from LDA

      -- SPIROC control signals
      RESETN          : out std_logic;  -- U4  , bank 13, U3 -> J1.50, global reset of KLauS(active high).
      RESET_BCID      : out std_logic;  -- T4  , bank 13, U2 -> J1.49, Not used at the moment
      -- SPIROC ACQ control
      START_ACQT      : out std_logic;  -- AA4 , bank 13, U2 -> J1.21, SPIROC Start & maintain acquisition on analogue memory (StartAcq), active High
      START_CONV_DAQB : out std_logic;  -- R6  , bank 13, U2 -> J1.23, SPIROC Start conversion signal (start_convb), active Low
      SCA_SAT         : in  std_logic;  -- P22 , bank 34, U27 -> J1.22, all memory cells filled. Open collector, '0'=full, 
      VAL_EVT         : out std_logic;  -- U11 , bank 13, U4 -> J1.65, enables the internal SPIROC trigger. "Disable discriminator output signal" (Val_Evt_p/n)
      NOTRIG          : out std_logic;  -- U12 , bank 13, U4 -> J1.63, clear 1 event from analog memory (Raz_Chn_p/n)

      --slow control signals
      SC_RESET     : out std_logic;     -- Y5  , bank 13, U1 -> J1.12, SPIROC Selected Register Reset (sr_rstb), active high
      SC_LOAD      : out std_logic;     -- AA7 , bank 13, U1 -> J1.14, SPIROC Slow Control Register Load Signal (sr_load), active on rising
      SC_SELECT    : out std_logic;     -- AA6 , bank 13, U1 -> J1.16, SPIROC Select Slow Control Register (1) or Probe Register (0) (select)
      SC_CLK1      : out std_logic;     -- AB6 , bank 13, U2 -> J1.15, SLAB 1: SPIROC Selected Register Clock (sr_ck)
      SC_CLK2      : out std_logic;     -- AB2 , bank 13, U1 -> J1.18, SLAB 2: SPIROC Selected Register Clock (sr_ck)
      SC_CLK3      : out std_logic;     -- Y4  , bank 13, U2 -> J1.17, SLAB 3: SPIROC Selected Register Clock (sr_ck)
      SC_SRIN      : out std_logic;     -- Y6  , bank 13, U1 -> J1.8,  SPIROC Selected Register Input (sr_in)
      SC_SRIN_BYP  : out std_logic;     -- AB7 , bank 13, U2 -> J1.7, bypass register for slowcontrol input. Not used yet.
      SC_SROUT1    : in  std_logic;     -- N20 , bank 34, J1 -> J1.10, SLAB 1: SPIROC Selected Register Output (sr_out)
      SC_SROUT2    : in  std_logic;     -- N19 , bank 34, J1 -> J1.11, SLAB 2: SPIROC Selected Register Output (sr_out)
      SC_SROUT3    : in  std_logic;     -- M20 , bank 34, J1 -> J1.13, SLAB 3: SPIROC Selected Register Output (sr_out)
      SC_SROUT_BYP : in  std_logic;     -- M19 , bank 34, J1 -> J1.9, not connected.

      -- interface for selecting the (mostly) analog debug pin output of SPIROC
      SROUT_READ       : in  std_logic;  -- M22 , bank 34, J1.92, Read Register Output (srout_read)
      --CLK_READ         : out std_logic;  -- U10 , bank 13, U4 -> J1.95, Read Register Clock (clk_read)
      --SRIN_READ        : out std_logic;  -- U5  , bank 13, U3 -> J1.94, SPIROC Read Register Input (srin_read)
      CLK_READ         : inout std_logic;  -- U10 , bank 13, U4 -> J1.95, Read Register Clock (clk_read)
      SRIN_READ        : inout std_logic;  -- U5  , bank 13, U3 -> J1.94, SPIROC Read Register Input (srin_read)
      RESET_READ_PROBE : out std_logic;  -- V5  , bank 13, U3 -> J1.52, resets the read register (only the read register, not probe)

      -- interface for selecting the (mostly) digital debug pin 
      -- probe register is reset when sc_select is set to 0 and SC_RESET is applied
      --CLK_PROBE   : out std_logic;      -- V7 , bank 13, U3 -> J1.96, Removed from SPIROC 2B ? clock is maybe taken from SC_CLK*
      --SRIN_PROBE  : out std_logic;      -- U9 , bank 13, U4 -> J1.97, Removed from SPIROC 2B ? uses probably SC_SRIN instead
      CLK_PROBE   : inout std_logic;      -- V7  , bank 13, U3 -> J1.96, 
      SRIN_PROBE  : inout std_logic;      -- U9  , bank 13, U4 -> J1.97, 
      SROUT_PROBE : in  std_logic;      -- M21 , bank 34, J1 -> J1.99, Removed, probably uses SC_SROUT* ?

      --preparation for SPIROC3
      ERROR_SCb       : inout std_logic;  -- U19  , bank 33, J1.104, Not used. Preparation for SPIROC 3
      SELECT_I2C_SRb  : inout std_logic;  -- T21  , bank 33, J1.4, Not used. Preparation for SPIROC 3
      SC_RW_RDb       : inout std_logic;  -- U21  , bank 33, J1.3,  Not used. Preparation for SPIROC 3
      SELECT_I2C_Port : inout std_logic;  -- T22  , bank 33, J1.103, Not used. Preparation for SPIROC 3
      I2C_DATA2       : inout std_logic;  -- U22  , bank 33, J1.105, Not used. Preparation for SPIROC 3
      I2C_CLK2        : inout std_logic;  -- V22  , bank 33, J1.107, Not used. Preparation for SPIROC 3
      I2C_DATA1       : out   std_logic;  -- W22  , bank 33, U3 -> J1.106, Not used. Preparation for SPIROC 3
      I2C_CLK1        : out   std_logic;  -- W20  , bank 33, U3 -> J1.108, Not used. Preparation for SPIROC 3

      --debug signals
      LED_DRIVE : out std_logic;        -- W21  , bank 33, U5 -> LED_TEST, controls the LED_TEST LED
      DEBUG1    : out std_logic;        -- H15 , bank 34, J5.1 
      DEBUG2    : out std_logic;        -- J15 , bank 34, J5.2
      DEBUG3    : out std_logic;        -- K15 , bank 34, J5.3
      DEBUG4    : out std_logic;        -- J16 , bank 34, J5.4
      DEBUG5    : out std_logic;        -- J17 , bank 34, J5.5
      DEBUG6    : out std_logic;        -- L16 , bank 34, J5.6
      DEBUG7    : out std_logic;        -- L17 , bank 34, J5.7

      -- SPIROC readout signals
      START_READOUT      : out std_logic;  -- AB1 , bank 13, U1 -> J1.24, SPIROC Digital RAM start reading signal (start_readout1/2)
      START_READOUT_BYP1 : out std_logic;  -- AB5 , bank 13, U1 -> J1.26, Start readout bypass signal for slab 1
      START_READOUT_BYP2 : out std_logic;  -- T6  , bank 13, U2 -> J1.25, Start readout bypass signal for slab 1
      START_READOUT_BYP3 : out std_logic;  -- AB4 , bank 13, U1 -> J1.28, Start readout bypass signal for slab 1
      END_READOUT1       : in  std_logic;  -- M17 , bank 34, J1.41, Readout chain 1, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      END_READOUT2       : in  std_logic;  -- N17 , bank 34, J1.43, Readout chain 2, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      END_READOUT3       : in  std_logic;  -- N18 , bank 34, J1.45, Readout chain 3, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      END_READOUT4       : in  std_logic;  -- M15 , bank 34, J1.42, Readout chain 4, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      END_READOUT5       : in  std_logic;  -- M16 , bank 34, J1.44, Readout chain 5, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      END_READOUT6       : in  std_logic;  -- J18 , bank 34, J1.46, Readout chain 6, SPIROC "end_readout" signal DIGITAL RAM end reading signal
      TRANSMITON1        : in  std_logic;  -- R20 , bank 34, U28 -> J1.27, Readout chain 1, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      TRANSMITON2        : in  std_logic;  -- R21 , bank 34, U29 -> J1.29, Readout chain 2, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      TRANSMITON3        : in  std_logic;  -- P20 , bank 34, U30 -> J1.31, Readout chain 3, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      TRANSMITON4        : in  std_logic;  -- P21 , bank 34, U31 -> J1.33, Readout chain 4, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      TRANSMITON5        : in  std_logic;  -- N15 , bank 34, U32 -> J1.35, Readout chain 5, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      TRANSMITON6        : in  std_logic;  -- P15 , bank 34, U33 -> J1.37, Readout chain 6, SPIROC "TransmitOn*b" signal: Active data readout, active Low
      DOUT1              : in  std_logic;  -- P17 , bank 34, U34 -> J1.30, Readout chain 1, SPIROC data serial output
      DOUT2              : in  std_logic;  -- P18 , bank 34, U35 -> J1.32, Readout chain 2, SPIROC data serial output
      DOUT3              : in  std_logic;  -- T16 , bank 34, U36 -> J1.34, Readout chain 3, SPIROC data serial output
      DOUT4              : in  std_logic;  -- T17 , bank 34, U37 -> J1.36, Readout chain 4, SPIROC data serial output
      DOUT5              : in  std_logic;  -- R19 , bank 34, U38 -> J1.38, Readout chain 5, SPIROC data serial output
      DOUT6              : in  std_logic;  -- T19 , bank 34, U39 -> J1.40, Readout chain 6, SPIROC data serial output

      -- USB communication
      USB_D     : inout std_logic_vector(7 downto 0);  -- Data for the USB chip FT245RQ
      --USB_D0       : inout std_logic;   -- K18 , bank 34, FT245RQ D0 - bidirect
      --USB_D1       : inout std_logic;   -- J21 , bank 34, FT245RQ D1 - bidirect
      --USB_D2       : inout std_logic;   -- J22 , bank 34, FT245RQ D2 - bidirect
      --USB_D3       : inout std_logic;   -- J20 , bank 34, FT245RQ D3 - bidirect
      --USB_D4       : inout std_logic;   -- K21 , bank 34, FT245RQ D4 - bidirect
      --USB_D5       : inout std_logic;   -- L21 , bank 34, FT245RQ D5 - bidirect
      --USB_D6       : inout std_logic;   -- L22 , bank 34, FT245RQ D6 - bidirect
      --USB_D7       : inout std_logic;   -- K19 , bank 34, FT245RQ D7 - bidirect
      USB_RXF_Q : in    std_logic;  -- K20 , bank 34, FT245RQ RXF#. When high, do not read data from the FIFO. When low, there is data available in the FIFO which can be read by strobing RD# low, then high again.
      USB_TXE_Q : in    std_logic;  -- L18 , bank 34, FT245RQ TXE#.  When high, do not write data into the FIFO. When low, data can be written into the FIFO by strobing WR high, then low. During reset this signal pin is tri-state. 
      USB_PWEN  : in    std_logic;      -- L19 , bank 34, FT245RQ PWREN#. Goes low after the device is configured by USB, then high during USB suspend. 
      USB_RD_Q  : out   std_logic;  -- T18 , bank 34, FT245RQ RD#. Enables the current FIFO data byte on D0...D7 when low. Fetched the next FIFO data byte (if available) from the receive FIFO buffer when RD# goes from high to low. 
      USB_WR    : out   std_logic;      -- P16 , bank 34, FT245RQ WR. Writes the data byte on the D0...D7 pins into the transmit FIFO buffer when WR goes from high to low. 
      USB_RES_Q : out   std_logic;      -- R16 , bank 34, FT245RQ reset#. Active low reset pin

      --external trigger (goes to 3:1 multiplexer on the calib board before going to HBU)
      TRIG_EXT_P_IN : out std_logic;    -- D16 , bank 35, U6 -> J1.64,
      TRIG_EXT_N_IN : out std_logic;    -- D17 , bank 35, U6 -> J1.66,

      --bank 35
      CLK_5MHZ_0_P  : out std_logic;    -- F16 , bank 35, U6 -> J1.68,
      CLK_5MHZ_0_N  : out std_logic;    -- E16 , bank 35, U6 -> J1.70,
      CLK_5MHZ_1_P  : out std_logic;    -- E15 , bank 35, U7 -> J1.67,
      CLK_5MHZ_1_N  : out std_logic;    -- D15 , bank 35, U7 -> J1.69,
      CLK_5MHZ_2_P  : out std_logic;    -- C15 , bank 35, U9 -> J1.86,
      CLK_5MHZ_2_N  : out std_logic;    -- B15 , bank 35, U9 -> J1.88,
      CLK_40MHZ_0_P : out std_logic;    -- F18 , bank 35, U8 -> J1.72,
      CLK_40MHZ_0_N : out std_logic;    -- E18 , bank 35, U8 -> J1.74,
      CLK_40MHZ_1_P : out std_logic;    -- G15 , bank 35, U7 -> J1.71,
      CLK_40MHZ_1_N : out std_logic;    -- G16 , bank 35, U7 -> J1.73,
      CLK_40MHZ_2_P : out std_logic;    -- G17 , bank 35, U8 -> J1.82,
      CLK_40MHZ_2_N : out std_logic;    -- F17 , bank 35, U8 -> J1.84,

      -- PS signals
      DDR_addr          : inout std_logic_vector (14 downto 0);
      DDR_ba            : inout std_logic_vector (2 downto 0);
      DDR_cas_n         : inout std_logic;
      DDR_ck_n          : inout std_logic;
      DDR_ck_p          : inout std_logic;
      DDR_cke           : inout std_logic;
      DDR_cs_n          : inout std_logic;
      DDR_dm            : inout std_logic_vector (3 downto 0);
      DDR_dq            : inout std_logic_vector (31 downto 0);
      DDR_dqs_n         : inout std_logic_vector (3 downto 0);
      DDR_dqs_p         : inout std_logic_vector (3 downto 0);
      DDR_odt           : inout std_logic;
      DDR_ras_n         : inout std_logic;
      DDR_reset_n       : inout std_logic;
      DDR_we_n          : inout std_logic;
      FIXED_IO_ddr_vrn  : inout std_logic;
      FIXED_IO_ddr_vrp  : inout std_logic;
      FIXED_IO_mio      : inout std_logic_vector (53 downto 0);
      FIXED_IO_ps_clk   : inout std_logic;
      FIXED_IO_ps_porb  : inout std_logic;
      FIXED_IO_ps_srstb : inout std_logic
      );

end adif2_top;

architecture rtl of adif2_top is
   attribute mark_debug : string;
   attribute ASYNC_REG  : string;

   signal rst      : std_logic;         --global reset (POR and user reset combined)
   signal rst_roc  : std_logic;         --reset-on-Configuration. For simulation only?
   --signal srst_5_syn  : std_logic_vector(2 downto 0);  -- synchronisation register into the 5MHz clock domain
   --signal srst_40_syn : std_logic_vector(2 downto 0);  -- synchronisation register into the 40MHz clock domain
   --signal srst_5      : std_logic;                     --synchronous reset in the 5MHz clock domain
   signal srst_40  : std_logic;         --synchronous reset in the 40MHz clock domain
   signal srst_100 : std_logic;         --synchronous reset in the 100MHz clock domain

   signal reset_por_100          : std_logic;  --POWER-ON reset, 100 MHz domain
   signal reset_por_100_ce       : std_logic;  --count enable for power-on-reset shiftregister. Should be reasonably slow: one pulse every 10 ms or so
   signal reset_por_100_cnt_prev : std_logic;  --signal to remember the counter previous state. Needed for detection of change of the state

   signal clk_board_100_buf : std_logic;  -- clk for testing, 100 MHz. driven by bufg
   signal clk_cnt           : unsigned(31 downto 0) := (others => '0');
   --signal second_tick       : std_logic;  --roughly each second 1 impulse

   signal FCLK_CLK0 : std_logic;                                        --clock generated from PS. Only useful in certain cases
   signal BUF_OEQ   : std_logic_vector(3 downto 0) := (others => '1');  -- buffer output enables BUF3_OEQ..BUF1_OEQ

   signal hdmi_busy     : std_logic;    -- busy signal, that gouse to hdmi
   --signal hdmi_serial_tx : std_logic;   -- serial data to be transmitted to the LDA via HDMI
   --signal hdmi_serial_rx : std_logic;   -- serial data from LDA
   signal validate_hdmi : std_logic;    -- validation signal from HDMI
   signal hdmi_clk      : std_logic;    -- 40 MHz clock from LDA
   signal uc_clk        : std_logic;    -- 
   signal trig_ext      : std_logic;    -- external trigger signal to be passed to HBU

   signal lvds_reset_en        : std_logic := '0';  -- resets the LVDS receivers to default state. Active high
   signal lvds_reset_en_n      : std_logic := '1';  -- inverted signal lvds_reset_en
   constant lvds_default_state : std_logic := '1';  -- to which state the are the lvds receivers configured

   signal clk_syn     : std_logic := '0';  --synchronize the 5 MHz / 250 kHz clock. Synchronous with fclk clock domain
   signal clk_sel     : std_logic;         -- not used. Was an output port in DIF1
   signal clk_duty    : std_logic;         -- not used. was an output port in DIF1
   signal phase_det   : std_logic;         -- not used. was an output port in DIF1
   signal acq_clk_sel : std_logic := '0';  -- clock selector from acq_start

   signal clk_acq         : std_logic;  -- clock from clk_ctrl to acq_start
   signal clk_acq_rising  : std_logic;  --pre-warning signal for the rising edge the clk_acq (fclk domain)
   signal clk_acq_falling : std_logic;  --pre-warning signal for the falling edge the clk_acq (fclk domain)

   signal clk_readout : std_logic := '0';  -- clock generated by ro_ctrl, used for readout when ro_clk_sel is 1. Drives then clk_acq
   signal ro_clk_sel  : std_logic := '0';  -- selector of clk_acq
   signal clk         : std_logic;         -- 5 MHz for user logic
   signal fclk        : std_logic;         -- 40 MHz fast clock
   signal fclk_160    : std_logic;         -- 160 MHz clock hopefully in sync with fclk
   signal fclk_320    : std_logic;         -- 320 MHz clock hopefully in sync with fclk

   signal clk_strobe_5 : std_logic;                     --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
   signal clk_phase    : std_logic_vector(7 downto 0);  --clock cycle number (in fclk domain). according to the acquisition length it goes from 159 down to 0 or from 7 downto 0

   signal clk_40M_inhibit_sc  : std_logic := '0';  --Slowcontrol module requires to shut down 40 MHz clk0
   signal clk_40M_inhibit_acq : std_logic := '0';  --SAcquisition module requires to shut down 40 MHz clk0
   signal clk_40M_inhibit     : std_logic := '0';  --result of all 40MHz clock inhibits

   signal LED_inhibit_sync                 : std_logic_vector(3 downto 0);  --synchronize the LED inhibit to 100 MHz clock domain (needed by the LED)
   attribute ASYNC_REG of LED_inhibit_sync : signal is "TRUE";              --clock domain crossed here.

   --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   -- copy of old signals from adif_top
   --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   signal data_ready : std_logic;

   signal hdmi_clk_buf    : std_logic;  --HDMI clk
   signal hdmi_clk_dcm    : std_logic;  --DCM output for hdmi clk
   signal hdmi_clk_locked : std_logic;  --hdmi clk is stable

   signal ack_wr_req : std_logic;
   signal ack_data   : std_logic_vector(7 downto 0);
   signal acq_debug  : std_logic_vector (7 downto 0);

   signal acq_start_cmd : std_logic;
   signal acq_stop_rx   : std_logic;
   signal acq_stop_done : std_logic;

   signal dout_all        : std_logic_vector(6 downto 1);  -- individual signals merged to a vector containing all 6 signals
   signal transmiton_all  : std_logic_vector(6 downto 1);  -- individual signals merged to a vector containing all 6 signals
   signal end_readout_all : std_logic_vector(6 downto 1);  -- individual signals merged to a vector containing all 6 signals
   signal sda_all	  : std_logic_vector(6 downto 1);
   signal scl_all	  : std_logic_vector(6 downto 1);

   signal dout        : std_logic_vector (C_NUM_ROW downto 1);  -- Bank 0 Page 3
   signal transmit_on : std_logic_vector (C_NUM_ROW downto 1);  -- Bank 0 Page 3
   signal end_readout : std_logic_vector (C_NUM_ROW downto 1);  -- Bank 0 Page 3
   signal sda         : std_logic_vector (C_NUM_ROW downto 1);  -- Bank 0 Page 3
   signal scl         : std_logic_vector (C_NUM_ROW downto 1);  -- Bank 0 Page 3


   signal reset_slab_std   : std_logic;  -- reset of the slab from the standard command           
   signal reset_slab_ro    : std_logic;  --reset the slab from the readout module. active low.
   signal reset_slab_acq_n : std_logic;  --control of the resetb from acquisition module. Purpose: reset POD module

   signal cal_wr_req   : std_logic;
   signal cal_data_out : std_logic_vector(7 downto 0);

   signal ro_wr_req       : std_logic;
   signal ro_data_out     : std_logic_vector(7 downto 0);
   signal end_readout_out : std_logic;
   signal sc_wr_req       : std_logic;
   signal sc_data_out     : std_logic_vector(7 downto 0);

   signal stat_wr_req   : std_logic;
   signal stat_data_out : std_logic_vector(7 downto 0);

   signal cal_charge_etrig : std_logic;
   signal cal_charge_itrig : std_logic;
   signal cal_light_etrig  : std_logic;
   signal cal_light_itrig  : std_logic;

   signal start_acq_etrig : std_logic;
   signal start_acq_itrig : std_logic;
   signal start_acq       : std_logic;
   signal stop_acq        : std_logic;
   signal stop_ro         : std_logic;
   signal stop_ro_cont    : std_logic;

   signal clk0_40M_en : std_logic;
   signal clk0_5M_en  : std_logic;
   signal clk1_40M_en : std_logic;
   signal clk1_5M_en  : std_logic;
   signal clk2_40M_en : std_logic;
   signal clk2_5M_en  : std_logic;

   signal spiroc_type    : std_logic;
   signal klaus_type     : std_logic;
   signal no_trig_enable : std_logic;
   signal slab_num_asic  : std_logic_vector (15 downto 0);  -- bit 0 => spiroc version, 5..1=>#ASICs in slab1 (left), 10..6=>#ASICs in slab1 (middle), 15..11=>#ASICs in slab1 (left)
   signal set_ready      : std_logic;

   signal acq_cmd_done  : std_logic;
   signal cal_cmd_done  : std_logic;
   signal fast_cmd_done : std_logic;
   signal ro_cmd_done   : std_logic;
   signal sc_cmd_done   : std_logic;
   signal stat_cmd_done : std_logic;
   signal std_cmd_done  : std_logic;
   signal dc_cmd_done   : std_logic;    --dutycycle controller done

--   signal reset_dif : std_logic;
--   signal reset_all : std_logic;
--   signal reset_cal : std_logic;

   --signal buf0_oe_n_int : std_logic;
   --signal buf1_oe_n_int : std_logic;
   --signal buf2_oe_n_int : std_logic;
   --signal buf3_oe_n_int : std_logic;
   --signal buf4_oe_n_int : std_logic;

   -- power control
   signal pp_mandatory         : std_logic;  --powerpulsing mode is mandatory
   signal pwr_ana_pul          : std_logic;
   signal pwr_dig_pul          : std_logic;
   signal pwr_sca_pul          : std_logic;
   signal pwr_adc_pul          : std_logic;
   signal pwr_dac_pul          : std_logic;
   signal pwr_ana_int          : std_logic;
   signal pwr_dig_int          : std_logic;
   signal pwr_sca_int          : std_logic;
   signal pwr_adc_int          : std_logic;
   signal pwr_dac_int          : std_logic;
   signal dly_reg              : std_logic_vector(15 downto 0);
   signal ro_time              : std_logic_vector(15 downto 0);
   signal power_puling_enabled : std_logic;

   -- timing configuration
   signal timing_cfg : std_logic_vector(15 downto 0);  --length of the bxid on bits 7..0

   -- USB Interface
   signal usb_wr_req    : std_logic;
   signal usb_writing   : std_logic;
   signal usb_tx_data   : std_logic_vector(7 downto 0);
   signal usb_strobe    : std_logic;
   signal usb_rx_data   : std_logic_vector(7 downto 0);
   signal usb_if_enable : std_logic;

   -- Raw USB signals (for debugging)
   signal USB_RD_Q_int : std_logic;
   signal USB_WR_int   : std_logic;


   -- HDMI data interface
   signal hdmi_writing        : std_logic;
   signal hdmi_strobe         : std_logic;
   signal hdmi_rx_data        : std_logic_vector(7 downto 0);
   signal hdmi_tx_data        : std_logic_vector(7 downto 0);
   signal hdmi_txen           : std_logic;
   signal hdmi_wr_req         : std_logic;
   signal hdmi_data_available : std_logic;
   signal hdmi_if_enable      : std_logic;
   signal hdmi_debug          : std_logic_vector(3 downto 0);
   signal cmd_idle            : std_logic;
   signal hdmi_turbo          : std_logic;
   signal roc_increment       : std_logic;

   -- common interface
   signal txen    : std_logic;
   signal wr_req  : std_logic;
   signal writing : std_logic;
   signal tx_data : std_logic_vector(7 downto 0);
   signal strobe  : std_logic;
   signal rx_data : std_logic_vector(7 downto 0);


   -- command reicever
   signal reg_ready : std_logic;
   signal cmd_ready : std_logic;
   signal cmd_end   : std_logic;
   signal cmd_done  : std_logic;

   signal SIG_CMD_REC : T_CMD_REC;      -- command record
   signal cmd12       : std_logic_vector(15 downto 0);
   signal cmd34       : std_logic_vector(15 downto 0);
   signal cmd56       : std_logic_vector(15 downto 0);
   signal cmd78       : std_logic_vector(15 downto 0);

   signal power_spare_int : std_logic;
   signal power_fe_en     : std_logic;
   signal sig_fe_en       : std_logic;

   signal start_readout_int : std_logic := '0';
   signal tb_busy_int       : std_logic := '0';
   signal tb_busy_delayed   : std_logic := '0';  --the busy is delayed until the cmd_rx goes to idle
--  SIGNAL tb_spill         : std_logic;
   signal validate          : std_logic;         --selected validation signal
   signal validate_lemo     : std_logic;         --validate signal from the lemo connector
   --signal trig_ext_t        : std_logic;
--  SIGNAL hold_ext         : std_logic;
   signal ro_debug          : std_logic_vector (7 downto 0);

   signal trig_cfg    : std_logic_vector(15 downto 0);
   signal bxid_length : std_logic_vector(7 downto 0);  --the length of the bxid
   signal dif_id      : std_logic_vector(15 downto 0);

   signal ack_end : std_logic;
   signal ack_dbg : std_logic;
   signal cal_dbg : std_logic;
   signal std_dbg : std_logic;
   signal ro_dbg  : std_logic;

   signal cmd_rx_dbg    : std_logic;
   signal acq_start_dbg : std_logic;
   signal acq_stop_dbg  : std_logic;

   signal ms_tick : std_logic;          --in clk clock domain (5MHz)

   signal led_rom_out : std_logic;      --ROM output for shifting a messages over LED

   attribute mark_debug of ms_tick             : signal is "true";
   attribute mark_debug of clk_strobe_5        : signal is "true";
   attribute mark_debug of hdmi_busy           : signal is "true";
   attribute mark_debug of validate_hdmi       : signal is "true";
   attribute mark_debug of trig_ext            : signal is "true";
   attribute mark_debug of clk_syn             : signal is "true";
   attribute mark_debug of clk_acq             : signal is "true";
   attribute mark_debug of clk_40M_inhibit_acq : signal is "true";
   attribute mark_debug of clk_40M_inhibit_sc  : signal is "true";
   attribute mark_debug of txen                : signal is "true";
   attribute mark_debug of wr_req              : signal is "true";
   attribute mark_debug of writing             : signal is "true";
   attribute mark_debug of tx_data             : signal is "true";
   attribute mark_debug of strobe              : signal is "true";
   attribute mark_debug of rx_data             : signal is "true";
   attribute mark_debug of validate            : signal is "true";
   attribute mark_debug of dif_id              : signal is "true";
--   attribute mark_debug of  : signal is "true";

   signal sc_debug		: std_logic;
   signal hdmi_locked		: std_logic;

   signal ro_cycle : std_logic_vector(7 downto 0);  --readout cycle number, synchronized with fclk

   signal dc_busy_extend  : std_logic;  --busy extend requested by the dutycycle governors
   signal dc_stop_request : std_logic;  --immediate stop of acquisition is requested bu the dutycycle governor 

   signal start_acq_int   : std_logic := '0';  --start of acquisition
   signal START_CONV_DAQB_int : std_logic := '0';
begin
   --order of slabs is defined here. Lines 3 and 4 come from middle slab (main one), Lines 5 and 6 come from right slab (was used for 2x2 HBU layers). Lines 1 and 2 are from left slab
   dout_all        <= (6 => DOUT2, 5 => DOUT1, 4 => DOUT6, 3 => DOUT5, 2 => DOUT4, 1 => DOUT3);
   dout            <= dout_all(C_NUM_ROW downto 1);
   transmiton_all  <= (6 => TRANSMITON2, 5 => TRANSMITON1, 4 => TRANSMITON6, 3 => TRANSMITON5, 2 => TRANSMITON4, 1 => TRANSMITON3);
   transmit_on     <= transmiton_all(C_NUM_ROW downto 1);
   end_readout_all <= (6 => END_READOUT2, 5 => END_READOUT1, 4 => END_READOUT6, 3 => END_READOUT5, 2 => END_READOUT4, 1 => END_READOUT3);
   end_readout     <= end_readout_all(C_NUM_ROW downto 1);

   -- i2cA-scl --> clk_read,    i2cA-sda --> srin_read
   -- i2cB-scl --> clk_probe,   i2cB-sda --> srin_probe 
   sda_all	<=(6 => '0', 5 => '0', 4 => '0', 3 => '0', 2 => SRIN_PROBE, 1 => SRIN_READ);
   --sda_all	<=(6 => '0', 5 => '0', 4 => '0', 3 => '0', 2 => SRIN_READ, 1 => SRIN_PROBE);
   sda		<= sda_all(C_NUM_ROW downto 1);
   scl_all	<=(6 => '0', 5 => '0', 4 => '0', 3 => '0', 2 => CLK_PROBE,  1 =>  CLK_READ);
   --scl_all	<=(6 => '0', 5 => '0', 4 => '0', 3 => '0', 2 => CLK_READ,  1 =>  CLK_PROBE);
   scl		<= scl_all(C_NUM_ROW downto 1);

   tb_busy_delayed_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         -- if rst = '1' then
         --    tb_busy_delayed <= '0';
         -- else
         tb_busy_delayed <= tb_busy_int or                           --busy due to memory cell full / unfinished readout
                            (tb_busy_delayed and (not cmd_idle)) or  -- busy is fired defined by the ro_cmd and is kept until cleared AND until cmd_rx finishes sending acknowledge
                            (tb_busy_delayed and dc_busy_extend);    -- busy is kept high due to dutycycle limits
      -- end if;  --rst
      end if;  --clk
   end process;
   hdmi_turbo <= tb_busy_int and hdmi_if_enable;

   acq_start_inst : entity work.acq_start
      port map (
         -- clk                 => clk,
         ms_tick             => ms_tick,
         fclk                => fclk,
         fclk_160            => fclk_160,
         fclk_320            => fclk_320,
         clk_strobe_5        => clk_strobe_5,
         clk_phase           => clk_phase,
         rst                 => '0',    --not used anymore --srst_5,
         frst                => srst_40,
         acq_start_cmd       => acq_start_cmd,
         acq_stop_rx         => acq_stop_rx,
         cmd_end             => cmd_end,
         start_acqs          => start_acq_int ,
         start_conv_n        => START_CONV_DAQB_int,
         trig_ext_n          => TRIG_EXT_N_IN,
         trig_ext_p          => TRIG_EXT_P_IN,
         trig_ext_t          => trig_ext,
         end_readout         => end_readout_out,
         ram_full            => SCA_SAT,
         data_ready          => data_ready,
         validate_hdmi       => validate_hdmi,
         validate_lemo       => validate_lemo,
         no_trig_enable      => no_trig_enable,
         no_trig             => notrig,
         val_evt             => val_evt,
         acq_clk_sel         => acq_clk_sel,
         clk_acq             => clk_acq,
         clk_acq_rising      => clk_acq_rising,
         clk_acq_falling     => clk_acq_falling,
         clk_40M_inhibit_acq => clk_40M_inhibit_acq,
         pwr_ana_pul         => pwr_ana_pul,
         pwr_dig_pul         => pwr_dig_pul,
         pwr_sca_pul         => pwr_sca_pul,
         pwr_adc_pul         => pwr_adc_pul,
         pwr_dac_pul         => pwr_dac_pul,
         trig_cfg            => trig_cfg,
         dly_reg             => dly_reg,
         ro_time             => ro_time,
         timing_cfg          => timing_cfg,
         reset_slab_acq_n    => reset_slab_acq_n,
         dc_stop_request     => dc_stop_request,
         acq_start_dbg       => acq_start_dbg,
         acq_debug           => acq_debug,
         acq_cmd_done        => acq_cmd_done
         );
   START_ACQT <= start_acq_int ;
   START_CONV_DAQB <= START_CONV_DAQB_int;

   acq_stop_inst : entity work.acq_stop
      port map (
         -- clk           => clk,
         fclk          => fclk,
         clk_strobe_5  => clk_strobe_5,
         rst           => srst_40,
         usb_strobe    => strobe,
         rx_data       => rx_data,
         acq_stop_rx   => acq_stop_rx,
         acq_stop_done => acq_stop_done,
         acq_cmd_done  => acq_cmd_done,
         acq_stop_dbg  => acq_stop_dbg,
         cmd_end       => cmd_end
         );

   cal_ctrl_inst : entity work.cal_ctrl
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => srst_40,
         cmd_ready    => cmd_ready,
         cmd_end      => cmd_end,
         cmd12        => cmd12,
         cmd34        => cmd34,
         cmd56        => cmd56,
         cmd78        => cmd78,
         cal_dbg      => cal_dbg,
         miso         => MISO0,
         mosi         => MOSI0,
         mclk         => SCK0,
         ssel         => SSEL,
         dif_id       => dif_id,
         usb_txen     => txen,
         writing      => writing,
         cal_boot     => BOOT,
         cal_wr_req   => cal_wr_req,
         cal_data_out => cal_data_out,
         cal_cmd_done => cal_cmd_done
         );

   cmd_ack_inst : entity work.cmd_ack
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => srst_40,
         ack_end      => ack_end,
         P_I_CMD_REC  => SIG_CMD_REC,   -- command record
         usb_txen     => txen,
         writing      => writing,
         ack_wr_req   => ack_wr_req,
         ack_data     => ack_data,
         ack_dbg      => ack_dbg,
         cmd_done     => cmd_done
         );

   data_mux_inst : entity work.data_mux
      port map (
         -- clk           => clk,
         fclk          => fclk,
         clk_strobe_5  => clk_strobe_5,
         rst           => srst_40,
         ack_wr_req    => ack_wr_req,
         ack_data      => ack_data,
         cal_wr_req    => cal_wr_req,
         cal_data_out  => cal_data_out,
         ro_wr_req     => ro_wr_req,
         ro_data_out   => ro_data_out,
         sc_wr_req     => sc_wr_req,
         sc_data_out   => sc_data_out,
         stat_wr_req   => stat_wr_req,
         stat_data_out => stat_data_out,
         wr_req        => wr_req,
         tx_data       => tx_data
         );

   fast_cmd_dec_inst : entity work.fast_cmd_dec
      port map (
         -- clk              => clk,
         fclk             => fclk,
         clk_strobe_5     => clk_strobe_5,
         rst              => srst_40,
         cmd_ready        => cmd_ready,
         cmd_end          => cmd_end,
         cmd12            => cmd12,
         reset_bcid       => reset_bcid,
         cal_charge_etrig => cal_charge_etrig,
         cal_charge_itrig => cal_charge_itrig,
         cal_light_etrig  => cal_light_etrig,
         cal_light_itrig  => cal_light_itrig,
         start_acq        => start_acq,
         start_acq_etrig  => start_acq_etrig,
         start_acq_itrig  => start_acq_itrig,
         stop_acq         => stop_acq,
         stop_ro          => stop_ro,
         stop_ro_cont     => stop_ro_cont,
         fast_cmd_done    => fast_cmd_done
         );

--   ro_ctrl_inst : entity work.ro_ctrl
--      port map (
--         -- clk             => clk,
--         fclk            => fclk,
--         clk_strobe_5    => clk_strobe_5,
--         ms_tick         => ms_tick,
--         rst             => srst_40,
--         cmd_ready       => cmd_ready,
--         cmd_end         => cmd_end,
--         cmd12           => cmd12,
--         cmd34           => cmd34,
--         cmd56           => cmd56,
--         ro_dbg          => ro_dbg,
--         start_readout   => start_readout_int,
--         end_readout_out => end_readout_out,
--         dout            => dout,
--         transmit_on     => transmit_on,
--         end_readout     => end_readout,
--         dif_id          => dif_id,
--         ro_cycle        => ro_cycle,
--         ro_clk_sel      => ro_clk_sel,
--         clk_readout     => clk_readout,
--         usb_txen        => txen,
--         writing         => writing,
--         ro_wr_req       => ro_wr_req,
--         ro_data_out     => ro_data_out,
--         turbo           => hdmi_turbo,
--         data_ready      => data_ready,
--         tb_busy         => tb_busy_int,
--         ro_debug        => ro_debug,
--         ro_cmd_done     => ro_cmd_done,
--         reset_slab_ro   => reset_slab_ro
--         );
   ro_ctrl_inst : entity work.ro_i2c_ctrl
      port map (
         -- clk             => clk,
         fclk            => fclk,
         clk_strobe_5    => clk_strobe_5,
         ms_tick         => ms_tick,
         rst             => srst_40,
         cmd_ready       => cmd_ready,
         cmd_end         => cmd_end,
         cmd12           => cmd12,
         cmd34           => cmd34,
         cmd56           => cmd56,
         ro_dbg          => ro_dbg,
         start_readout   => start_readout_int,
         end_readout_out => end_readout_out,
         --dout            => dout,
         --transmit_on     => transmit_on,
         --end_readout     => end_readout,
         sda		     => sda,
         scl		     => scl,
         dif_id          => dif_id,
         ro_cycle        => ro_cycle,
         ro_clk_sel      => ro_clk_sel,
         clk_readout     => clk_readout,
         usb_txen        => txen,
         writing         => writing,
         ro_wr_req       => ro_wr_req,
         ro_data_out     => ro_data_out,
         turbo           => hdmi_turbo,
         data_ready      => data_ready,
         tb_busy         => tb_busy_int,
         ro_debug        => ro_debug,
         ro_cmd_done     => ro_cmd_done,
         reset_slab_ro   => reset_slab_ro
         );

	klaus_type <= '0';

   sc_inst : entity work.sc
      port map (
         -- clk             => clk,
         fclk            => fclk,
         clk_strobe_5    => clk_strobe_5,
         rst             => srst_40,
         cmd_ready       => cmd_ready,
         cmd_end         => cmd_end,
         cmd12           => cmd12,
         cmd34           => cmd34,
         cmd56           => cmd56,
         usb_strobe      => strobe,
         rx_data         => rx_data,
         sc_clk1         => sc_clk1,
         sc_clk2         => sc_clk2,
         sc_clk3         => sc_clk3,
         clk_40M_inhibit => clk_40M_inhibit_sc,
         sc_load         => sc_load,
         sc_srin         => sc_srin,
         usb_txen        => txen,
         writing         => writing,
         sc_wr_req       => sc_wr_req,
         sc_data_out     => sc_data_out,
         spiroc_type     => klaus_type,	-- fixed to KLauS-5
         slab_num_asic   => slab_num_asic,
         sc_cmd_done     => sc_cmd_done,
         sc_debug	 => sc_debug
         );

   stat_inst : entity work.stat
      port map (
         -- clk => clk,
         fclk          => fclk,
         clk_strobe_5  => clk_strobe_5,
         rst           => srst_40,
         cmd_ready     => cmd_ready,
         cmd_end       => cmd_end,
         cmd12         => cmd12,
         cmd34         => cmd34,
         usb_txen      => txen,
         writing       => writing,
         stat_wr_req   => stat_wr_req,
         stat_data_out => stat_data_out,
         tb_trig       => validate,
         tb_busy       => tb_busy_int,
         start_readout => start_readout_int,
         spiroc_type   => spiroc_type,
         stat_cmd_done => stat_cmd_done
         );

   std_cmd_inst : entity work.std_cmd
      port map (
         -- clk                  => clk,
         fclk                 => fclk,
         clk_strobe_5         => clk_strobe_5,
         rst                  => srst_40,
         cmd_ready            => cmd_ready,
         cmd_end              => cmd_end,
         cmd12                => cmd12,
         cmd34                => cmd34,
         cmd56                => cmd56,
         std_dbg              => std_dbg,
         pp_mandatory         => pp_mandatory,
         pwr_adc_pul          => pwr_adc_pul,
         pwr_ana_pul          => pwr_ana_pul,
         pwr_dac_pul          => pwr_dac_pul,
         pwr_dig_pul          => pwr_dig_pul,
         pwr_sca_pul          => pwr_sca_pul,
         pwr_ana              => pwr_ana_int,
         pwr_dig              => pwr_dig_int,
         pwr_sca              => pwr_sca_int,
         pwr_adc              => pwr_adc_int,
         pwr_dac              => pwr_dac_int,
         power_puling_enabled => power_puling_enabled,

         PWR_ENABLE_GR1 => PWR_ENABLE_GR1,
         PWR_ENABLE_GR2 => PWR_ENABLE_GR2,
         PWR_ENABLE_GR3 => PWR_ENABLE_GR3,
         PWR_ENABLE_GR4 => PWR_ENABLE_GR4,
         OC_ENABLE      => OC_ENABLE,

         sig_fe_en        => sig_fe_en,
         trig_cfg         => trig_cfg,
         dly_reg          => dly_reg,
         ro_time          => ro_time,
         dif_id           => dif_id,
         timing_cfg       => timing_cfg,
         reset_dif        => open,      --reset_dif,
         reset_slab       => reset_slab_std,
         reset_all        => open,      --reset_all,
         reset_sc         => SC_RESET,  --reset_sc,
         reset_read_probe => RESET_READ_PROBE,
         reset_cal        => open,      --reset_cal,
         clk0_40M_en      => clk0_40M_en,
         clk0_5M_en       => clk0_5M_en,
         clk1_40M_en      => clk1_40M_en,
         clk1_5M_en       => clk1_5M_en,
         clk2_40M_en      => clk2_40M_en,
         clk2_5M_en       => clk2_5M_en,
         --clk_read         => CLK_READ,
         --srin_read        => SRIN_READ,
         --clk_probe        => CLK_PROBE,
         --srin_probe       => SRIN_PROBE,
         clk_read         => open,
         srin_read        => open,
         clk_probe        => open,
         srin_probe       => open,
         spiroc_type      => spiroc_type,
         no_trig_enable   => no_trig_enable,
         slab_num_asic    => slab_num_asic,
         set_ready        => set_ready,
         sc_select        => SC_SELECT,
         std_cmd_done     => std_cmd_done
         );

      -- reset_slab_std:	reset slab according to the command
      -- reset_slab_ro :	reset slab after readout
      -- reset_slab_acq:	reset slab before acq_start
      -- sc_cmd_done    :   reset slab when slow control is uploaded into the ASIC.
    --RESETN <= not (reset_slab_std and reset_slab_ro and (reset_slab_acq_n or (not power_puling_enabled)));  
    --RESETN <= (not (reset_slab_std or reset_slab_ro or reset_slab_acq_n)) and sc_cmd_done;  
    RESETN <= not (reset_slab_std and reset_slab_ro and reset_slab_acq_n);  

   if_sw_inst : entity work.if_switch
      port map (
         -- clk                 => clk,
         fclk                => fclk,
         clk_strobe_5        => clk_strobe_5,
         rst                 => srst_40,
         cmd_idle            => cmd_idle,
         usb_if_enable       => usb_if_enable,
         hdmi_if_enable      => hdmi_if_enable,
         --usb signals
         usb_strobe          => usb_strobe,
         usb_rxfn            => USB_RXF_Q,
         usb_rx_data         => usb_rx_data,
         usb_wr_req          => usb_wr_req,
         usb_tx_data         => usb_tx_data,
         usb_writing         => usb_writing,
         usb_txen            => USB_TXE_Q,
         --hdmi signals
         hdmi_strobe         => hdmi_strobe,
         hdmi_rx_data        => hdmi_rx_data,
         hdmi_wr_req         => hdmi_wr_req,
         hdmi_writing        => hdmi_writing,
         hdmi_tx_data        => hdmi_tx_data,
         hdmi_txen           => hdmi_txen,
         hdmi_data_available => hdmi_data_available,
         --single interface to the dif
         strobe              => strobe,
         rx_data             => rx_data,
         txen                => txen,
         wr_req              => wr_req,
         writing             => writing,
         tx_data             => tx_data
         );


   usb_if_inst : entity work.usb_if
      port map (
         -- USB interface
         usb_rxfn     => USB_RXF_Q,
         usb_txen     => USB_TXE_Q,
         usb_rdn      => USB_RD_Q_int,
         usb_wr       => USB_WR_int,
         usb_data     => USB_D,
         -- system interface
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => srst_40,
         -- DIF to USB data
         wr_req       => usb_wr_req,
         writing      => usb_writing,
         tx_data      => usb_tx_data,
         -- USB to DIF data
         usb_strobe   => usb_strobe,
         rx_data      => usb_rx_data,
         if_enable    => usb_if_enable
         );

   hdmi_if_inst : entity work.hdmi_if
      port map(
         -- clk            => clk,
         clk_strobe_5   => clk_strobe_5,
         fclk           => fclk,
         srst_5         => '0',         --srst_5 not used anymore,
         srst_40        => srst_40,
         ms_tick        => ms_tick,
         cmd_idle       => cmd_idle,
         debug          => hdmi_debug,
         --DIF2LDA
         wr_req         => hdmi_wr_req,
         turbo          => hdmi_turbo,
         writing        => hdmi_writing,
         tx_data        => hdmi_tx_data,
         hdmi_txen      => hdmi_txen,
         --LDA2DIF              
         rx_data        => hdmi_rx_data,
         hdmi_strobe    => hdmi_strobe,
         rx_error       => open,
         clk_syn        => clk_syn,
         roc_increment  => roc_increment,
         --tx,rx pins
         tx_pin_p       => SPD2L_P,
         tx_pin_n       => SPD2L_N,
         rx_pin_p       => DL2D_P,
         rx_pin_n       => DL2D_N,
         if_enable      => hdmi_if_enable,
         data_available => hdmi_data_available
         );

   cmd_rx_inst : entity work.cmd_rx
      port map (
         -- clk           => clk,
         fclk          => fclk,
         clk_strobe_5  => clk_strobe_5,
         rst           => srst_40,
         usb_strobe    => strobe,
         rx_data       => rx_data,
         reg_ready     => reg_ready,
         cmd_ready     => cmd_ready,
         acq_start_cmd => acq_start_cmd,
         acq_stop_rx   => acq_stop_rx,
         acq_cmd_done  => acq_cmd_done,
         cal_cmd_done  => cal_cmd_done,
         fast_cmd_done => fast_cmd_done,
         ro_cmd_done   => ro_cmd_done,
         sc_cmd_done   => sc_cmd_done,
         stat_cmd_done => stat_cmd_done,
         std_cmd_done  => std_cmd_done,
         dc_cmd_done   => dc_cmd_done,
         cmd_done      => cmd_done,
         ro_cycle      => ro_cycle,
         cmd_rx_dbg    => cmd_rx_dbg,
         ack_end       => ack_end,
         cmd_end       => cmd_end,
         P_O_CMD_REC   => SIG_CMD_REC,  -- command record
         cmd12         => cmd12,
         cmd34         => cmd34,
         cmd56         => cmd56,
         cmd78         => cmd78,
         cmd_idle      => cmd_idle
         );

   dutycycle_1 : entity work.dutycycle
      port map (
         fclk            => fclk,
         clk_strobe_5    => clk_strobe_5,
         ms_tick         => ms_tick,
         srst            => srst_40,
         P_I_CMD_REC     => SIG_CMD_REC,
         cmd_ready       => cmd_ready,
         cmd_end         => cmd_end,
         dc_cmd_done     => dc_cmd_done,
         pwr_ana         => pwr_ana_int,
         pwr_dig         => pwr_dig_int,
         pwr_sca         => pwr_sca_int,
         pwr_adc         => pwr_adc_int,
         pwr_dac         => pwr_dac_int,
         pp_mandatory    => pp_mandatory,
         dc_busy_extend  => dc_busy_extend,
         dc_stop_request => dc_stop_request);

   validate_lemo <= '0';                             --we don't have lemo input in DIF2
   validate      <= validate_hdmi or validate_lemo;  --validate_lemo;

--roc_inst : roc port map (O => rst_roc);
   rst_roc       <= '0';
   rst           <= rst_roc or reset_por_100;  --rst_ext_in;
   start_readout <= start_readout_int;

   --rst_usb_n  <= '1';
   NOTBOOT  <= '1';
   NOTRESIN <= '1';

   lvds_en <= '1';

   --buf0_oe_n_int <= '0';
   --buf1_oe_n_int <= '0';
   --buf2_oe_n_int <= '0';
   --buf3_oe_n_int <= '0';
   --buf4_oe_n_int <= '0';

   BUF0_OEQ <= BUF_OEQ(0);
   BUF1_OEQ <= BUF_OEQ(1);
   BUF2_OEQ <= BUF_OEQ(2);
   BUF3_OEQ <= BUF_OEQ(3);
   BUF_OEQ  <= (others => '0') when (sig_fe_en = '1') else
              (others => '1');
   --buf0_oe_n <= (not sig_fe_en) or buf0_oe_n_int;
   --buf1_oe_n <= (not sig_fe_en) or buf1_oe_n_int;
   --buf2_oe_n <= (not sig_fe_en) or buf2_oe_n_int;
   --buf3_oe_n <= (not sig_fe_en) or buf3_oe_n_int;
   --buf4_oe_n <= (not sig_fe_en) or buf4_oe_n_int;

   PWR_ANALOG  <= pwr_ana_int;
   PWR_DIGITAL <= pwr_dig_int;
   PWR_SCA     <= pwr_sca_int;
   PWR_ADC     <= pwr_adc_int;
   PWR_DAC     <= pwr_dac_int;

   --power_spare <= power_spare_int;
   --srst_5_syn_proc : process(clk)
   --begin
   --   if rising_edge(clk) then
   --      srst_5_syn <= srst_5_syn(1 downto 0) & srst_100;
   --      srst_5     <= srst_5_syn(2);
   --   end if;  --clk
   --end process;

   --reset is now generated in the clk_ctrl module
   --srst_40_syn_proc : process(clk)
   --begin
   --   if rising_edge(clk) then
   --      srst_40_syn <= srst_40_syn(1 downto 0) & srst_100;
   --      srst_40     <= srst_40_syn(2);
   --   end if;  --clk
   --end process;

   srst_100 <= reset_por_100;

   reset_por_100_SRL16E_inst : SRL16E generic map (INIT => X"FFFF")
      port map (Q => reset_por_100, A0 => '1', A1 => '1', A2 => '1', A3 => '1', CE => reset_por_100_ce, CLK => clk_board_100_buf, D => '0');


   reset_por_100_ce_proc : process(clk_board_100_buf)
   begin
      if (rising_edge(clk_board_100_buf)) then                                       --100Mhz clock does not interrupt
         if simulation = false then
            reset_por_100_ce       <= clk_cnt(19) and (not reset_por_100_cnt_prev);  --rising edge detector
            reset_por_100_cnt_prev <= clk_cnt(19);
         else
            reset_por_100_ce       <= clk_cnt(5) and (not reset_por_100_cnt_prev);   --rising edge detector
            reset_por_100_cnt_prev <= clk_cnt(5);
         end if;  --simulation
      end if;
   end process;

   clk_40M_inhibit <= clk_40M_inhibit_sc or clk_40M_inhibit_acq;

   clk_ctrl_inst : entity work.clk_ctrl
      port map (
         rst               => reset_por_100,
         clk_board_100_buf => clk_board_100_buf,
         hdmi_clk_in       => hdmi_clk,
         hdmi_clk_dcm_buf  => open,
         hdmi_locked       => hdmi_locked,
         clk_syn           => clk_syn,
         clk_sel           => clk_sel,
         clk_duty          => clk_duty,
         phase_det         => phase_det,
         acq_clk_sel       => acq_clk_sel,
         bxid_length       => timing_cfg(7 downto 0),
         clk_acq           => clk_acq,
         clk_acq_rising    => clk_acq_rising,
         clk_acq_falling   => clk_acq_falling,
         clk_readout       => clk_readout,
         ro_clk_sel        => ro_clk_sel,
         -- clk               => clk,
         fclk              => fclk,
         fclk_160          => fclk_160,
         fclk_320          => fclk_320,
         clk_strobe_5      => clk_strobe_5,
         clk_phase         => clk_phase,
         srst_40           => srst_40,
         ms_tick           => ms_tick,
         --clk_40M_inhibit   => clk_40M_inhibit,
         clk_40M_inhibit   => '0',
         clk_5M_inhibit    => clk_40M_inhibit_sc,
         clk0_40M_n        => CLK_40MHZ_0_N,
         clk0_40M_p        => CLK_40MHZ_0_P,
         clk0_5M_n         => CLK_5MHZ_0_N,
         clk0_5M_p         => CLK_5MHZ_0_P,
         clk1_40M_n        => CLK_40MHZ_1_N,
         clk1_40M_p        => CLK_40MHZ_1_P,
         clk1_5M_n         => CLK_5MHZ_1_N,
         clk1_5M_p         => CLK_5MHZ_1_P,
         clk2_40M_n        => CLK_40MHZ_2_N,
         clk2_40M_p        => CLK_40MHZ_2_P,
         clk2_5M_n         => CLK_5MHZ_2_N,
         clk2_5M_p         => CLK_5MHZ_2_P
         );

   --------------------------------------------------------------------
   -- first test assignments. to be replaced
   --------------------------------------------------------------------
   debug_clk_inst : BUFG port map (O => clk_board_100_buf, I => GCLK);
   uc_clk <= '0';

   clk_cnt_proc : process(clk_board_100_buf)
   begin
      if rising_edge(clk_board_100_buf) then
         clk_cnt <= clk_cnt + 1;
      end if;  --clk
   end process;

   LED_out_proc : process(clk_board_100_buf)
   begin
      if rising_edge(clk_board_100_buf) then
         LED_DRIVE        <= led_rom_out and (not LED_inhibit_sync(LED_inhibit_sync'high));
         LED_inhibit_sync <= LED_inhibit_sync(LED_inhibit_sync'high-1 downto 0) & (clk_40M_inhibit_acq);  --synchronization to 100M clock domain
      end if;
   end process;

   --second_tick_proc : process(clk_board_100_buf)
   --begin
   --   if rising_edge(clk_board_100_buf) then
   --      if clk_cnt(26 downto 0) = ((26 downto 0 => '1')) then
   --         second_tick <= '1';
   --      else
   --         second_tick <= '0';
   --      end if;  --cnt roughly 1 s
   --   end if;  --clk
   --end process;

  
   ODDR_inst : ODDR
   generic map(
      DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
   port map (
      Q => DEBUG1,   -- 1-bit DDR output
      C => fclk,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input
      D1 => '1',  -- 1-bit data input (positive edge)
      D2 => '0',  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
      );


    DEBUG2  <= clk_acq; 
    DEBUG3  <= ro_clk_sel;
    DEBUG4  <= ro_debug(2);
    DEBUG5  <= ro_debug(3);
    DEBUG6  <= ro_debug(5);
    DEBUG7 <= ro_cmd_done;

   ROM128X1_inst : ROM128X1
      generic map (
         INIT => F_MORSE_NUM_ENCODE(DATE)
         )
      port map (
         O  => led_rom_out,             -- ROM output
         A0 => clk_cnt(24),             -- ROM address[0]
         A1 => clk_cnt(25),             -- ROM address[1]
         A2 => clk_cnt(26),             -- ROM address[2]
         A3 => clk_cnt(27),             -- ROM address[3]
         A4 => clk_cnt(28),             -- ROM address[4]
         A5 => clk_cnt(29),             -- ROM address[5]
         A6 => clk_cnt(30)              -- ROM address[6]
         );

   hdmi_busy <= tb_busy_delayed;
   hdmi_busy_OBUFDS_inst : OBUFDS generic map (IOSTANDARD => "DEFAULT", SLEW => "SLOW")
      port map (O => DD2L_P, OB => DD2L_N, I => hdmi_busy);
   --hdmi_serial_tx_OBUFDS_inst : OBUFDS generic map (IOSTANDARD => "DEFAULT", SLEW => "SLOW")
   --   port map (O => SPD2L_P, OB => SPD2L_N, I => hdmi_serial_tx);

   hdmi_validation_IBUFDS_inst : IBUFDS generic map (IOSTANDARD => "DEFAULT", DIFF_TERM => false, IBUF_LOW_PWR => false)
      port map (O => validate_hdmi, I => SPL2D_P, IB => SPL2D_N);
   --hdmi_serial_rx_IBUFDS_inst : IBUFDS generic map (IOSTANDARD => "DEFAULT", DIFF_TERM => false, IBUF_LOW_PWR => true)
   --   port map (O => hdmi_serial_rx, I => DL2D_P, IB => DL2D_N);
   hdmi_clk_IBUFDS_inst : IBUFGDS generic map (IOSTANDARD => "DEFAULT", DIFF_TERM => false, IBUF_LOW_PWR => false)
      port map (O => hdmi_clk, I => HDMI_CLK_P, IB => HDMI_CLK_N);

   uc_clk_OBUFDS_inst : OBUFDS generic map (IOSTANDARD => "DEFAULT", SLEW => "SLOW")
      port map (O => UC_CLK_P, OB => UC_CLK_N, I => uc_clk);

   --trig_ext_OBUFDS_inst : OBUFDS generic map (IOSTANDARD => "DEFAULT", SLEW => "SLOW")
   --   port map (O => TRIG_EXT_P_IN, OB => TRIG_EXT_N_IN, I => trig_ext);

   lvds_reset_en   <= reset_por_100;
   lvds_reset_en_n <= not lvds_reset_en;
   --T=='1' means 'Z', T==0 means driving
   LVDS_RES_LOW_OBUFT_inst : OBUFT generic map (DRIVE => 12, IOSTANDARD => "DEFAULT", SLEW => "SLOW")
      port map (O => LVDS_RES_LOW, I => not lvds_default_state, T => lvds_reset_en_n);
   LVDS_RES_HIGH_OBUFT_inst : OBUFT generic map (DRIVE => 12, IOSTANDARD => "DEFAULT", SLEW => "SLOW")
      port map (O => LVDS_RES_HIGH, I => lvds_default_state, T => lvds_reset_en_n);

--    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--    -- debug first assignments
--    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--    lvds_reset_en      <= '0';
--    hdmi_busy          <= '0';
--    hdmi_serial_tx     <= '1';
--    trig_ext           <= '0';
-- --   BOOT               <= '0';
--    NOTBOOT            <= '1';           --according to old DIF
--    NOTRESIN           <= '1';           --according to old DIF
   UART_RX            <= '1';
--    SCK0               <= '0';
--    MOSI0              <= '0';
--    SSEL               <= '0';
--    CALIB_SPARE        <= '0';
--    PWR_ENABLE_GR1     <= '1';
--    PWR_ENABLE_GR2     <= '1';
--    PWR_ENABLE_GR3     <= '1';
--    PWR_ENABLE_GR4     <= '1';
--    BUF0_OEQ           <= '0';
--    BUF1_OEQ           <= '0';
--    BUF2_OEQ           <= '0';
--    BUF3_OEQ           <= '0';
--    LVDS_EN            <= '1';
--    OC_ENABLE          <= '1';
--    PWR_ANALOG         <= '0';
--    PWR_DIGITAL        <= '0';
--    PWR_ADC            <= '0';
--    PWR_DAC            <= '0';
--    PWR_SCA            <= '0';
--    RESETN             <= '0';
--    RESET_BCID         <= '0';
--    START_ACQT         <= '0';
--    START_CONV_DAQB    <= '0';
--    VAL_EVT            <= '0';
--    NOTRIG             <= '0';
--    SC_RESET           <= '0';
--    SC_LOAD            <= '0';
--    SC_SELECT          <= '0';
--    SC_CLK1            <= '0';
--    SC_CLK2            <= '0';
--    SC_CLK3            <= '0';
--    SC_SRIN            <= '0';
   SC_SRIN_BYP        <= '0';
--    CLK_READ           <= '0';
--    SRIN_READ          <= '0';
--    RESET_READ_PROBE   <= '0';
--    CLK_PROBE          <= '0';
--    SRIN_PROBE         <= '0';
--    ERROR_SCb          <= '0';
--    SELECT_I2C_SRb     <= '0';
--    SC_RW_RDb          <= '0';
--    SELECT_I2C_Port    <= '0';
   I2C_DATA2          <= '0';
   I2C_CLK2           <= '0';
   I2C_DATA1          <= '0';
   I2C_CLK1           <= '0';
--    START_READOUT      <= '0';
   START_READOUT_BYP1 <= '0';
   START_READOUT_BYP2 <= '0';
   START_READOUT_BYP3 <= '0';
--    USB_D              <= (others => '0');
--    USB_RD_Q           <= '1';
--    USB_WR             <= '0';
   USB_RES_Q          <= '1';
   USB_RD_Q           <= USB_RD_Q_int;
   USB_WR             <= USB_WR_int;
-- 
   roc_cnt_1 : entity work.roc_cnt
      generic map (
         RO_CYCLE_BITS => 8)
      port map (
         fclk          => fclk,
         clk_strobe_5  => clk_strobe_5,  --not really needed
         srst_40       => srst_40,
         clk_syn       => clk_syn,
         roc_increment => roc_increment,
         ro_cycle      => ro_cycle);
   -----------------------------------------------
   zynq_gen : if SIMULATION = false generate
      zynq_ps_i : entity work.zynq_ps
         port map (
            DDR_addr(14 downto 0)     => DDR_addr(14 downto 0),
            DDR_ba(2 downto 0)        => DDR_ba(2 downto 0),
            DDR_cas_n                 => DDR_cas_n,
            DDR_ck_n                  => DDR_ck_n,
            DDR_ck_p                  => DDR_ck_p,
            DDR_cke                   => DDR_cke,
            DDR_cs_n                  => DDR_cs_n,
            DDR_dm(3 downto 0)        => DDR_dm(3 downto 0),
            DDR_dq(31 downto 0)       => DDR_dq(31 downto 0),
            DDR_dqs_n(3 downto 0)     => DDR_dqs_n(3 downto 0),
            DDR_dqs_p(3 downto 0)     => DDR_dqs_p(3 downto 0),
            DDR_odt                   => DDR_odt,
            DDR_ras_n                 => DDR_ras_n,
            DDR_reset_n               => DDR_reset_n,
            DDR_we_n                  => DDR_we_n,
            FCLK_CLK0                 => FCLK_CLK0,
            FIXED_IO_ddr_vrn          => FIXED_IO_ddr_vrn,
            FIXED_IO_ddr_vrp          => FIXED_IO_ddr_vrp,
            FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
            FIXED_IO_ps_clk           => FIXED_IO_ps_clk,
            FIXED_IO_ps_porb          => FIXED_IO_ps_porb,
            FIXED_IO_ps_srstb         => FIXED_IO_ps_srstb
            );
   end generate;
-- 
--    --create a boot pulse ~ 1 second long
--    debug_boot_generator_SRL16E_inst : SRL16E generic map (INIT => X"0080")
--       port map (Q => BOOT, A0 => '1', A1 => '1', A2 => '1', A3 => '1', CE => second_tick, CLK => clk_board_100_buf, D => '0');
-- 
--    -- End of SRL16E_inst instantiation
-- 

end rtl;
