
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity std_cmd is
   port (
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;
      cmd_ready    : in std_logic;
      cmd_end      : in std_logic;
      cmd12        : in std_logic_vector (15 downto 0);
      cmd34        : in std_logic_vector (15 downto 0);
      cmd56        : in std_logic_vector (15 downto 0);

      std_dbg              : out std_logic;
      -- power control
      pp_mandatory         : in  std_logic;  --powerpulsing mode is mandatory (overrides received commands)
      pwr_ana_pul          : in  std_logic;
      pwr_dig_pul          : in  std_logic;
      pwr_sca_pul          : in  std_logic;
      pwr_adc_pul          : in  std_logic;
      pwr_dac_pul          : in  std_logic;
      pwr_ana              : out std_logic;
      pwr_dig              : out std_logic;
      pwr_sca              : out std_logic;
      pwr_adc              : out std_logic;
      pwr_dac              : out std_logic;
      power_puling_enabled : out std_logic;

      PWR_ENABLE_GR1 : out std_logic;   --power group enable
      PWR_ENABLE_GR2 : out std_logic;   --power group enable
      PWR_ENABLE_GR3 : out std_logic;   --power group enable
      PWR_ENABLE_GR4 : out std_logic;   --power group enable
      OC_ENABLE      : out std_logic;   --open colector enabled

      sig_fe_en : out std_logic;

      trig_cfg         : out std_logic_vector (15 downto 0);
      dly_reg          : out std_logic_vector (15 downto 0);
      ro_time          : out std_logic_vector (15 downto 0);
      dif_id           : out std_logic_vector (15 downto 0);
      timing_cfg       : out std_logic_vector (15 downto 0);
      -- reset control
      reset_dif        : out std_logic;
      reset_slab       : out std_logic;
      reset_all        : out std_logic;
      reset_sc         : out std_logic;
      reset_read_probe : out std_logic;
      --reset_read   : OUT    std_logic;
      --reset_probe  : OUT    std_logic;
      reset_cal        : out std_logic;

      clk0_40M_en    : out std_logic;
      clk0_5M_en     : out std_logic;
      clk1_40M_en    : out std_logic;
      clk1_5M_en     : out std_logic;
      clk2_40M_en    : out std_logic;
      clk2_5M_en     : out std_logic;
      clk_read       : out std_logic;
      srin_read      : out std_logic;
      clk_probe      : out std_logic;
      srin_probe     : out std_logic;
      spiroc_type    : out std_logic;
      no_trig_enable : out std_logic;
      slab_num_asic  : out std_logic_vector (15 downto 0);
      set_ready      : out std_logic;
      sc_select      : out std_logic;
      std_cmd_done   : out std_logic
      );
end std_cmd;

architecture struct of std_cmd is

   signal set_sleep  : std_logic;
   signal power_off  : std_logic;
   signal power_on   : std_logic;
   signal power_auto : std_logic;
   signal power_sel  : std_logic;
   signal power_ana  : std_logic;
   signal power_dig  : std_logic;
   signal power_sca  : std_logic;
   signal power_adc  : std_logic;
   signal power_dac  : std_logic;
   signal power_all  : std_logic;

begin

   pwr_ctrl_inst : entity work.pwr_ctrl
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => rst,
         set_sleep    => set_sleep,

         power_off    => power_off,
         power_on     => power_on,
         power_auto   => power_auto,
         power_sel    => power_sel,
         power_ana    => power_ana,
         power_dig    => power_dig,
         power_sca    => power_sca,
         power_adc    => power_adc,
         power_dac    => power_dac,
         power_all    => power_all,
         pp_mandatory => pp_mandatory,

         pwr_ana_pul => pwr_ana_pul,
         pwr_dig_pul => pwr_dig_pul,
         pwr_sca_pul => pwr_sca_pul,
         pwr_adc_pul => pwr_adc_pul,
         pwr_dac_pul => pwr_dac_pul,

         power_puling_enabled => power_puling_enabled,

         pwr_ana => pwr_ana,
         pwr_dig => pwr_dig,
         pwr_sca => pwr_sca,
         pwr_adc => pwr_adc,
         pwr_dac => pwr_dac
         );

   std_cmd_dec_inst : entity work.std_cmd_dec
      port map (
         -- system interface
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => rst,
         -- command reg and control
         cmd_ready    => cmd_ready,
         cmd_end      => cmd_end,
         cmd12        => cmd12,
         cmd34        => cmd34,
         cmd56        => cmd56,

         std_dbg    => std_dbg,
         -- power control
         power_off  => power_off,
         power_on   => power_on,
         power_auto => power_auto,
         power_sel  => power_sel,
         power_ana  => power_ana,
         power_dig  => power_dig,
         power_sca  => power_sca,
         power_adc  => power_adc,
         power_dac  => power_dac,
         power_all  => power_all,

         PWR_ENABLE_GR1 => PWR_ENABLE_GR1,
         PWR_ENABLE_GR2 => PWR_ENABLE_GR2,
         PWR_ENABLE_GR3 => PWR_ENABLE_GR3,
         PWR_ENABLE_GR4 => PWR_ENABLE_GR4,
         OC_ENABLE      => OC_ENABLE,
         sig_fe_en      => sig_fe_en,

         trig_cfg         => trig_cfg,
         timing_cfg       => timing_cfg,
         dly_reg          => dly_reg,
         ro_time          => ro_time,
         dif_id           => dif_id,
         -- reset control
         reset_dif        => reset_dif,
         reset_slab       => reset_slab,
         reset_all        => reset_all,
         reset_sc         => reset_sc,
         reset_read_probe => reset_read_probe,
         --reset_read   => reset_read,
         --reset_probe  => reset_probe,
         reset_cal        => reset_cal,
         -- clock control and misc status and control
         clk0_5M_en       => clk0_5M_en,
         clk0_40M_en      => clk0_40M_en,
         clk1_5M_en       => clk1_5M_en,
         clk1_40M_en      => clk1_40M_en,
         clk2_5M_en       => clk2_5M_en,
         clk2_40M_en      => clk2_40M_en,
         clk_read         => clk_read,
         srin_read        => srin_read,
         clk_probe        => clk_probe,
         srin_probe       => srin_probe,
         set_sleep        => set_sleep,
         spiroc_type      => spiroc_type,
         no_trig_enable   => no_trig_enable,
         slab_num_asic    => slab_num_asic,
         set_ready        => set_ready,
         sc_select        => sc_select,
         std_cmd_done     => std_cmd_done
         );

end struct;
