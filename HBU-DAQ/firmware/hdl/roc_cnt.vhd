-------------------------------------------------------------------------------
-- Title      : Readout cycle counter
-- Project    : 
-------------------------------------------------------------------------------
-- File       : roc_cnt.vhd
-- Author     : Jiri Kvasnicka (jiri.kvasnicka@desy.de), (kvas@fzu.cz)
-- Company    : DESY / Institute of Physics ASCR
-- Created    : 2018-04-10
-- Last update: 2018-04-12
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: counts the readout cycle on every "stop acquisition" fast
-- command. 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 DESY / Institute of Physics ASCR
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-04-10  1.0      kvas    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity roc_cnt is
   generic (
      RO_CYCLE_BITS : natural range 8 to 32 := 8
      );
   port (
      fclk          : in  std_logic;                                  -- 40 MHz clock
      clk_strobe_5  : in  std_logic;                                  -- pulse in fclk domain every 200 ns
      srst_40       : in  std_logic;                                  -- reset in fclk domain, active high
      clk_syn       : in  std_logic;                                  -- clk_sync command used for resetting the counter. 1 clk long in fclk domain
      roc_increment : in  std_logic;                                  -- stop acquisition recieved. a veeeery long pulse - using only rising edge to increment the ROC
      ro_cycle      : out std_logic_vector(RO_CYCLE_BITS-1 downto 0)  -- actual readout cycle
      );
end entity roc_cnt;

architecture rtl of roc_cnt is
   signal roc_increment_rising : std_logic := '0';  --rising edge of the roc_increment
   signal roc_increment_prev   : std_logic := '0';  --roc_increment value in the previous cycle

   signal ro_cycle_int : unsigned(RO_CYCLE_BITS-1 downto 0) := (others => '0');
begin
   roc_increment_prev_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         roc_increment_prev <= roc_increment;
      end if;  --clk
   end process;

   roc_increment_rising_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         roc_increment_rising <= '0';
         if (roc_increment = '1') and (roc_increment_prev = '0') then
            roc_increment_rising <= '1';
         end if;  --risin edge condition
      end if;  --clk
   end process;

   ro_cycle_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if (srst_40 = '1') or (clk_syn = '1') then
            ro_cycle_int <= (others => '0');
         else
            if roc_increment_rising = '1' then
               ro_cycle_int <= ro_cycle_int + 1;
            end if;  --increment condition
         end if;  --reset condition
      end if;  --clk
   end process;

   ro_cycle <= std_logic_vector(ro_cycle_int);
end architecture rtl;
