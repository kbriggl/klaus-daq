--
-- VHDL Architecture LDA_lib.word_transmitter3.rtl
--
-- Created:
--          by - kvas.UNKNOWN (FLCKVASWL)
--          at - 11:12:23 18.07.2014
--
-- using Mentor Graphics HDL Designer(TM) 2012.2b (Build 5)
--
-- for VHDL-87 
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use ieee.math_real.all;


entity word_transmitter2 is
   generic(
      WORD_SIZE    : integer := 16;
      SCALE_FACTOR : integer := 4
      );
   port(
      CLK      : in  std_logic;
      RESET    : in  std_logic;
      TX_DATA  : in  std_logic_vector (WORD_SIZE-1 downto 0);
      TX_START : in  std_logic;
      TX_BUSY  : out std_logic;
      TX_PIN   : out std_logic
      );
end word_transmitter2;

--
architecture rtl of word_transmitter2 is
   constant counter_bits : integer := integer(ceil(log2(real(WORD_SIZE))));  -- number of bits needed for the complete multiplexing to and from all the ports


   type state_type is
      (idle, start, bits, stop1, stop2, sneww);
   
   signal prescaler   : std_logic_vector (SCALE_FACTOR-1 downto 0);
   signal CS, ns      : state_type;     --current state, next state
   signal bit_counter : std_logic_vector(counter_bits-1 downto 0);
   signal cout        : std_logic;      --carry out of the bit_counter
   signal shiftreg    : std_logic_vector(WORD_SIZE-1 downto 0);

   signal sum                                          : std_logic_vector(bit_counter'high downto 0);
   signal shift, pre_count, count, inject, clear, copy : std_logic;
   signal TX_BUSY_int                                  : std_logic;
   signal TX_PIN_int                                   : std_logic;
   signal TX_PIN_WE                                    : std_logic;
   
begin
   
   clk_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            CS      <= idle;
            TX_BUSY <= '0';
         else
            CS      <= ns;
            TX_BUSY <= TX_BUSY_int;
         end if;  -- reset
      end if;  --clk
   end process;


   next_state : process(CS, TX_START, prescaler, cout)
   begin
      case CS is
         when idle =>
            if TX_START = '1' then
               ns <= start;
            else
               ns <= idle;
            end if;
         when start =>
            if prescaler(prescaler'high) = '1' then
               ns <= bits;
            else
               ns <= start;
            end if;
         when bits =>
            if cout = '1' then
               ns <= stop1;
            else
               ns <= bits;
            end if;
         when stop1 =>
            if prescaler(prescaler'high) = '1' then
               ns <= stop2;
            else
               ns <= stop1;
            end if;
         when stop2 =>
            if TX_START = '1' then
               if prescaler(prescaler'high) = '1' then
                  ns <= start;
               else
                  ns <= sneww;
               end if;
            else
               if prescaler(prescaler'high) = '1' then
                  ns <= idle;
               else
                  ns <= stop2;
               end if;
            end if;
         when sneww =>
            if prescaler(prescaler'high) = '1' then
               ns <= start;
            else
               ns <= sneww;
            end if;
         when others =>
            ns <= idle;
      end case;
   end process;

   outputs : process(CS, TX_START, prescaler)
   begin
      --defaults
      inject      <= '0';
      shift       <= '0';
      pre_count   <= '0';
      clear       <= '0';
      TX_BUSY_int <= '0';
      copy        <= '0';
      count       <= '0';
      TX_PIN_WE   <= '0';

      case CS is
         when idle =>
            TX_PIN_WE <= '1';           --'1' is assigned in idle
            if TX_START = '1' then
               TX_BUSY_int <= '1';
               inject      <= '1';
               pre_count   <= '1';
               copy        <= '1';
               shift       <= '1';
               TX_PIN_WE   <= '1';
            else
               clear <= '1';
            end if;
         when start =>
            if prescaler(prescaler'high) = '1' then
               TX_PIN_WE <= '1';
            end if;
            pre_count   <= '1';
            TX_BUSY_int <= '1';
         when bits =>
            pre_count   <= '1';
            TX_BUSY_int <= '1';
            shift       <= '1';
            count       <= '1';
            TX_PIN_WE   <= '1';
         when stop1 =>
            shift     <= '1';
            pre_count <= '1';
            if prescaler(prescaler'high) = '1' then
               TX_BUSY_int <= '0';
            else
               TX_BUSY_int <= '1';
            end if;
         when stop2 =>
            if TX_START = '1' then
               TX_PIN_WE   <= '1';
               pre_count   <= '1';
               TX_BUSY_int <= '1';
               copy        <= '1';
               shift       <= '1';
            else
               if prescaler(prescaler'high) = '0' then
                  pre_count <= '1';
               else
               end if;
            end if;
         when sneww =>
            pre_count   <= '1';
            TX_BUSY_int <= '1';
            TX_PIN_WE   <= '1';
         when others =>
      end case;
   end process;

   sum  <= std_logic_vector(unsigned(bit_counter) + to_unsigned(1, bit_counter'length)) when prescaler(prescaler'high) = '1'                                                                              else bit_counter;
   cout <= '1'                                                                          when (sum = std_logic_vector(to_unsigned(WORD_SIZE, sum'length))) and (bit_counter /= (bit_counter'range => '0')) else '0';


   bit_counter_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            bit_counter <= (others => '0');
         else
            if count = '1' then
               bit_counter <= sum (bit_counter'range);
            else
               bit_counter <= (others => '0');
            end if;  --clear
         end if;  --reset
      end if;  --clk
   end process;

   TX_PIN_int <= '0' when ns = start else
                 '1' when (ns = stop1) or (ns = stop2) or (ns = sneww) or (ns = idle) else
                 shiftreg(0);
   
   TX_PIN_proc : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            TX_PIN <= '1';
         elsif TX_PIN_WE = '1' then
            TX_PIN <= TX_PIN_int;
         end if;
      end if;  --clk
   end process;

   prescaler_proc : process(clk)
      variable last_pre_high : std_logic;
   begin
      if rising_edge(clk) then
         if reset = '1' then
            prescaler <= (others => '0');
         else
            if clear = '1' then
               prescaler <= (others => '0');
            else
               last_pre_high := prescaler(prescaler'high);
               for i in prescaler'high downto 1 loop
                  prescaler(i) <= prescaler (i-1);
               end loop;
               prescaler(0) <= inject or (last_pre_high and pre_count);
            end if;  --clear
         end if;  --reset
      end if;  --clk
   end process;

   shiftreg_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            shiftreg <= (others => '0');
         else
            if copy = '1' then
               shiftreg <= TX_DATA;
            elsif (shift = '1') and (prescaler(prescaler'high-1) = '1') then
               for i in shiftreg'high-1 downto 0 loop
                  shiftreg(i) <= shiftreg (i+1);
               end loop;
               shiftreg(shiftreg'high) <= '1';
            end if;  --save and prescaler
         end if;  --reset
      end if;  --clk
   end process;
   
end architecture rtl;
