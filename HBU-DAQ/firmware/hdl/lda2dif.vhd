--
-- VHDL Architecture LDA_lib.lda2dif.rtl
--
-- Created:
--          by - kvas.UNKNOWN (FLCKVASWL)
--          at - 15:49:57 24.07.2014
--
-- using Mentor Graphics HDL Designer(TM) 2012.2b (Build 5)
--
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use ieee.math_real.all;

entity lda2dif is
   generic (
      SCALE_FACTOR : positive := 4
      );
   port (
      -- clk          : in std_logic;      --5 MHz
      fclk         : in std_logic;      --40 MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      srst_5       : in std_logic;
      srst_40      : in std_logic;
      ms_tick      : in std_logic;      --ms tick in clk domain (5MHz)
      rx_pin       : in std_logic;

      clk_syn          : out std_logic;  -- synchronization pulse, registered by fclk
      cmd_idle         : in  std_logic;  --no command being processed
      flow_d2l_suspend : out std_logic;  -- no data should be transmitted to LDA if '1': the buffer in LDA is full
      roc_increment      : out std_logic;  --stop of acquisition increments the ROC number. Set to 1 for whole clk_strobe_5 period.

      rx_data     : out std_logic_vector (7 downto 0);
      hdmi_strobe : out std_logic;
      rx_error    : out std_logic;      --error in the receiver (missing proper stop bits)
      debug1      : out std_logic;
      debug2      : out std_logic;

      if_enable      : in  std_logic;
      data_available : out std_logic
      );
end entity lda2dif;

--
architecture rtl of lda2dif is
   constant UART_FCMD_FLOW_STOP     : std_logic_vector(15 downto 0) := X"E519";  -- stop sending data
   constant UART_FCMD_FLOW_CONTINUE : std_logic_vector(15 downto 0) := X"E51A";  -- stop sending data
   constant UART_FCMD_STOP_ACQ      : std_logic_vector(15 downto 0) := X"E313";  -- stop acquisition

   signal word : std_logic_vector(15 downto 0);  --registered
   signal done : std_logic;                      -- new word available, registered

   component word_receiver2
      generic (
         WORD_SIZE    : positive := 16;
         SCALE_FACTOR : positive := 4
         );
      port (
         CLK    : in  std_logic;                                -- Input clock
         RESET  : in  std_logic;                                -- External rerset signal
         Rx_PIN : in  std_logic;                                -- Data reception pin
         WORD   : out std_logic_vector (WORD_SIZE-1 downto 0);  -- A register to hold the received word
         DONE   : out std_logic;
         error  : out std_logic                                 --there was an error during the receival (no 1 between 1st and 2nd stop bits)
         );
   end component word_receiver2;
   
   type states is (INIT,
                   S01, S02, S03, S04, S05, S06, S07, S08, S09, S10,
                   S11, S12, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24, S25
                   );
   signal state, next_state : states;
   --signal save_high, save : std_logic; -- write enables to the downsizes data register
   signal hdmi_strobe_int   : std_logic;
   -- fifo signals
   signal wr_en             : std_logic;
   signal rd_en, rd_en_int  : std_logic;
   signal din               : std_logic_vector(15 downto 0);
   signal dout              : std_logic_vector(15 downto 0);
   signal full              : std_logic;  --should be treated as an error. Fifo should never fill completely
   signal empty             : std_logic;
   signal valid             : std_logic;  -- data from the fifo is valid
   signal receiver_error    : std_logic;
   signal arst              : std_logic;  -- async reset needed for reseting clk_domain (the clk is not working during the srst='1')

   signal save_high : std_logic;        -- saves the high part of the word to the rx_data register
   signal save_low  : std_logic;        -- saves the low part of the word to the rx_data register

   signal clk_syn_int : std_logic;      --synchronization signal before it is clocked;

   component hdmi_rx_fifo
      port (
         rst    : in  std_logic;
         wr_clk : in  std_logic;
         rd_clk : in  std_logic;
         din    : in  std_logic_vector(15 downto 0);
         wr_en  : in  std_logic;
         rd_en  : in  std_logic;
         dout   : out std_logic_vector(15 downto 0);
         full   : out std_logic;
         empty  : out std_logic;
         valid  : out std_logic
         );
--    PORT (
--      rst : IN STD_LOGIC;
--      wr_clk : IN STD_LOGIC;
--      rd_clk : IN STD_LOGIC;
--      din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
--      wr_en : IN STD_LOGIC;
--      rd_en : IN STD_LOGIC;
--      dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
--      full : OUT STD_LOGIC;
--      empty : OUT STD_LOGIC;
--      valid : OUT STD_LOGIC
--    );
   end component;
   signal flow_d2l_suspend_int : std_logic;                                --signal whether to suspend the data traffic
   signal flow_d2l_suspend_cnt : unsigned(7 downto 0) := (others => '0');  --downcounting number of milisecond tics
begin
   debug1 <= clk_syn_int;
   debug2 <= hdmi_strobe_int;

   wr_en <= '1' when (done = '1') and (clk_syn_int = '0') else '0';  --do not save the synchronisation fast command to the cmd_rx;
   hdmi_rx_fifo_inst : hdmi_rx_fifo
      port map (
         rst    => srst_40,
         wr_clk => fclk,
         rd_clk => fclk,
         din    => din,
         wr_en  => wr_en,
         rd_en  => rd_en,
         dout   => dout,
         full   => full,
         empty  => empty,
         valid  => valid
         );

   word_receiver_inst : word_receiver2
      generic map (
         WORD_SIZE    => 16,
         SCALE_FACTOR => SCALE_FACTOR
         )
      port map (
         CLK    => fclk,
         RESET  => srst_40,
         Rx_PIN => rx_pin,
         WORD   => word,
         DONE   => done,
         error  => receiver_error
         );

   clk_syn_int <= '1' when (if_enable = '1')       --interface must be enabled
                  and (cmd_idle = '1')             --command receiver must be in idle state (otherwise some random SC data migh match the fast command packet
                  and (done = '1')                 -- the command was just received (will stay active only 1 clock cycle)
                  and (word(15 downto 8) = X"E0")  --K28.0 = syncmd
                  --and (word(7 downto 0) = X"00)   --second byte can be specified
                  else '0';
   
   
   sync_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         clk_syn <= clk_syn_int;
      end if;  --clk
   end process;

   -- for 18bit BRAM downto 9b mapping
--   din(7 downto 0)  <= word(7 downto 0);
--   din(8)           <= '0';
--   din(16 downto 9) <= word(15 downto 8);
--   din(17)          <= '0';
   din <= word;                         --assignment for distributed ram

   rx_error <= receiver_error;

   --arst <= srst;                        --arst will always meet the timing, since the slow 5HMz clock sill start 4 cycles after the rst release

   clk_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if (srst_40 = '1') then
            state       <= INIT;
            hdmi_strobe <= '0';
            rd_en       <= '0';
         elsif (clk_strobe_5 = '1') then
            state          <= next_state;
            hdmi_strobe    <= hdmi_strobe_int;
            rd_en          <= rd_en_int;
            data_available <= not empty;
         else
            rd_en <= '0';               --since rd clock was changed to 40 MHz, the rd_en has to be active only 1 pulse (not the whole 5MHz clock period
         end if;
      end if;  --clk
   end process;

   next_process : process(state, valid, empty, if_enable)
   begin
      case state is
         when INIT =>                   -- wait for data in the fifo
            if (valid = '1') and (if_enable = '1') then
               next_state <= S01;       --fifo is not empty anymore
            else
               next_state <= INIT;
            end if;
         when S01 =>                    -- high byte of the word saved, strobe set in next clockcycle
            next_state <= S02;
         when S02 =>                    -- wait 
            next_state <= S03;
         when S03 =>                    -- wait 
            next_state <= S04;
         when S04 =>                    -- wait 
            next_state <= S05;
         when S05 =>                    -- wait 
            next_state <= S06;
         when S06 =>                    -- wait 
            next_state <= S07;
         when S07 =>                    -- wait 
            next_state <= S08;
         when S08 =>                    -- wait 
            next_state <= S09;
         when S09 =>                    -- wait 
            next_state <= S10;
         when S10 =>                    -- wait 
            next_state <= S11;
         when S11 =>                    -- low byte of the word saved, strobe set in next clockcycle
            next_state <= S12;
         when S12 =>                    -- FIFO word is fully read and has to be shifted away by the rd_en signal
            next_state <= S13;
         when S13 =>                    -- wait 
            next_state <= S14;
         when S14 =>                    -- wait 
            next_state <= S15;
         when S15 =>                    -- wait 
            next_state <= S16;
         when S16 =>                    -- wait 
            next_state <= S17;
         when S17 =>                    -- wait 
            next_state <= S18;
         when S18 =>                    -- wait 
            next_state <= S19;
         when S19 =>                    -- wait 
            next_state <= S20;
         when S20 =>                    -- wait 
            next_state <= S21;
         when S21 =>                    -- wait 
            next_state <= S22;
         when S22 =>                    -- wait 
            next_state <= S23;
         when S23 =>                    -- wait 
            next_state <= S24;
         when S24 =>                    -- wait 
            next_state <= S25;
         when S25 =>                    -- wait 
            next_state <= init;
         when others =>
            next_state <= INIT;
      end case;
   end process;

   outputs_process : process(state, valid)
   begin
      --default values
      rd_en_int       <= '0';
      hdmi_strobe_int <= '0';
      save_high       <= '0';
      save_low        <= '0';
      case state is
         when INIT => null;
         when S01 =>
            save_high       <= '1';
            hdmi_strobe_int <= '1';
         when S11 =>
            save_low        <= '1';
            hdmi_strobe_int <= '1';
         when S12 =>
            rd_en_int <= '1';           --Read from the fifo (fifo is FWFT)
         when others => null;
      end case;
   end process;

   datareg_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if (clk_strobe_5 = '1') then
            --         if srst = '1' then
--            rx_data <= (others => '0');
--         else
            if save_high = '1' then
               rx_data <= dout(15 downto 8);
            elsif save_low = '1' then
               rx_data <= dout(7 downto 0);
            end if;  --valid
--         end if; --reset
         end if;  --strobe
      end if;  --clk
   end process;

   flow_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if (ms_tick = '1') and (flow_d2l_suspend_cnt /= 0) then                        --count down to 0
            flow_d2l_suspend_cnt <= flow_d2l_suspend_cnt - 1;
         end if;
         if (state = S01) then                                                          --hook on the fifo output at the first reading state.
            case dout is
               when UART_FCMD_FLOW_STOP =>
                  flow_d2l_suspend_cnt <= to_unsigned(3, flow_d2l_suspend_cnt'length);  --wait 2-3 ms and then consider suspension not valid anymore
               when UART_FCMD_FLOW_CONTINUE =>
                  flow_d2l_suspend_cnt <= to_unsigned(0, flow_d2l_suspend_cnt'length);  --resume communication immediately
               when others => null;
            end case;
         end if;
         flow_d2l_suspend <= flow_d2l_suspend_int;
      end if;  --clk
   end process;
   flow_d2l_suspend_int <= '1' when (flow_d2l_suspend_cnt /= 0) else '0';

   roc_increment_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if clk_strobe_5 = '1' then
            if (state = S01) and (dout = UART_FCMD_STOP_ACQ) then
               roc_increment <= '1';
            else
               roc_increment <= '0';
            end if;  --fcmd_stop
         end if;  --strobe
      end if;  --fclk
   end process;
   
end architecture rtl;















