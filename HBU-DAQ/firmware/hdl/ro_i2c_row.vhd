library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

use work.adif2_types.all;

entity ro_i2c_row is
    generic(        -- i2c readout speed in Hz
        divider     :   integer := 24;   -- divider of 10MHz clock
        C_MAX_EVT   :   integer := 511
    );
    port(
		fclk         : in std_logic;      --clk 40MHz
		clk_strobe_5 : in std_logic;      
		rst          : in std_logic;

	    -- i2c interface
        i2c_sda_i   : in  std_logic;
        i2c_sda_o   : out std_logic;
        i2c_sda_oen : out std_logic;
        i2c_scl_i   : in  std_logic;
        i2c_scl_o   : out std_logic;
        i2c_scl_oen : out std_logic;

		ro_rx_start : in  std_logic;
		ro_rx_end   : out std_logic;
		
		ro_tx_start : in  std_logic;
		ro_tx_end   : out std_logic;
		
		asic_ix : out unsigned(3 downto 0);	-- record maximum number of ASIC recorded
		addr_pt : out POINTER;

        --debug   : out std_logic;
		
		ro_rd_addr : in  std_logic_vector (14 downto 0);
		ro_mem_out : out std_logic_vector (7 downto 0);
		
		ro_done : in std_logic
    );
end ro_i2c_row;

architecture rtl of ro_i2c_row is
    component ro_row_mem_6slab is
        port (
            clka  : in  std_logic;
            ena   : in  std_logic;
            wea   : in  std_logic_vector(0 downto 0);
            addra : in  std_logic_vector(14 downto 0);
            dina  : in  std_logic_vector(7 downto 0);
            clkb  : in  std_logic;
            enb   : in  std_logic;
            addrb : in  std_logic_vector(14 downto 0);
            doutb : out std_logic_vector(7 downto 0)
            );
    end component;

    -- parameter for the clock division of the i2c readout
    signal  clk_cnt     : integer range 0 to 4*divider;
    signal  bit_cnt     : integer range 0 to 7;
    signal  byte_cnt    : integer range 0 to 5;
    signal  evt_cnt     : integer range 0 to 512;
    signal  count       : std_logic_vector(7 downto 0);
    --constant    CNT1    : integer := divider;           -- could be reduced
    --constant    CNT2    : integer := CNT1 + divider;    -- could be reduced
    constant    CNT1    : integer := 4; -- could be reduced
    constant    CNT2    : integer := 8; -- could be reduced
    constant    CNT3    : integer := CNT2 + divider;
    constant    CNT4    : integer := CNT3 + divider;

    type STATE_TYPE is (    FS_IDLE,
                            FS_ADDR,
                            FS_I2C_START,   
                            FS_I2C_ADDR,
                            FS_I2C_WAIT_SLAVE_ACK,
                            FS_I2C_SLAVE_NACK,
                            FS_I2C_READ_BYTE,
                            FS_I2C_MASTER_ACK,
                            FS_I2C_MASTER_NACK,
                            FS_I2C_STOP,
                            FS_I2C_FINISH,
                            s27, s28, s29,
                            s30, s31, s32, s33
                        );

    signal  current_state   : STATE_TYPE;
    signal  next_state      : STATE_TYPE;

    -- i2c 
    signal  s_i2c_data_rx   :   std_logic_vector(7 downto 0);
    signal  s_i2c_data_tx   :   std_logic_vector(7 downto 0);
    signal  s_sda_clk       :   std_logic;
    signal  s_scl_clk       :   std_logic;
    signal  sda_int         :   std_logic := '1';
    signal  slave_nack      :   std_logic := '0';

    -- memory related signals
    signal  ro_wr_en        :   std_logic_vector( 0 downto 0);
    signal  ro_wr_addr      :   std_logic_vector(14 downto 0);
    signal  ro_mem_in       :   std_logic_vector( 7 downto 0);
    signal  mem_rst         :   std_logic;
    signal  wr_index        :   unsigned(14 downto 0);

    signal  s_last_evt      :   std_logic;
    signal  asic_index      :   unsigned(3 downto 0);

begin

    -- MEMORY to store the events
    ro_row_mem_inst : ro_row_mem_6slab
    port map (
            clka  => fclk,                  -- use the scl_clk as the write clock
            ena   => '1',                   -- always 1
            wea   => ro_wr_en,
            addra => ro_wr_addr,
            dina  => ro_mem_in,
            clkb  => fclk,
            enb   => clk_strobe_5,         --'1'
            addrb => ro_rd_addr,
            doutb => ro_mem_out
            );

    -- i2c interface
    i2c_scl_o   <= '0';
    i2c_scl_oen <= s_scl_clk;
    i2c_sda_o   <= '0';
    i2c_sda_oen <= sda_int;


    -- FSM machine for control
    clocked_proc : process (fclk)
    begin
            if rising_edge(fclk) then
                    if (rst = '1') then
                            current_state <= FS_IDLE;
                    elsif (clk_strobe_5 = '1') then
                            current_state <= next_state;
                    end if;  --strobe
            end if;  --clk
    end process clocked_proc;
    
    nextstate_proc : process (current_state, ro_done, ro_rx_start, ro_tx_start, clk_cnt, slave_nack, 
                                bit_cnt, byte_cnt, evt_cnt, count, wr_index )
    begin
        case current_state is
            when FS_IDLE =>
                if ro_rx_start = '1' then
                    next_state  <= FS_ADDR;
                else
                    next_state  <= FS_IDLE;
                end if;

            when FS_ADDR =>     -- setup the I2C slave ADDR
                next_state      <= FS_I2C_START;

            when FS_I2C_START =>    -- i2c start
                if clk_cnt = CNT4 - 1 then
                    next_state  <= FS_I2C_ADDR;
                else
                    next_state  <= FS_I2C_START;
                end if;

            when FS_I2C_ADDR =>     -- write addr and r/w bit to slave
                if bit_cnt = 0 and clk_cnt = CNT4 -1 then   -- finish writing 8 bits to slave
                    next_state  <= FS_I2C_WAIT_SLAVE_ACK;
                else
                    next_state  <= FS_I2C_ADDR;
                end if;

            when FS_I2C_WAIT_SLAVE_ACK =>   -- waiting for the slave ack
                if clk_cnt = CNT4 -1 then
                    if slave_nack = '1' then
                        next_state  <= FS_I2C_SLAVE_NACK;
                    else
                        next_state  <= FS_I2C_READ_BYTE;
                    end if;
                else
                    next_state  <= FS_I2C_WAIT_SLAVE_ACK;
                end if;

            when FS_I2C_SLAVE_NACK =>
                next_state  <= FS_I2C_STOP;

            when FS_I2C_READ_BYTE =>
                if bit_cnt = 0 and clk_cnt = CNT4 -1 then       -- finish writing 8 bits to slave
                    if s_last_evt = '1' and byte_cnt = 3 then   -- NACK/ACK
                        next_state  <= FS_I2C_MASTER_NACK;
                    else
                        next_state  <= FS_I2C_MASTER_ACK;
                    end if;
                else
                    next_state <= FS_I2C_READ_BYTE;
                end if;

            when FS_I2C_MASTER_ACK =>
                if clk_cnt = CNT4 - 1 then
                    --if byte_cnt = 1 and (s_i2c_data_rx = x"FE" or evt_cnt = C_MAX_EVT) then
                    if byte_cnt = 4 and s_last_evt = '1' then
                        next_state  <= FS_I2C_STOP;
                    else
                        next_state  <= FS_I2C_READ_BYTE;
                    end if;
                else
                    next_state  <= FS_I2C_MASTER_ACK;
                end if;

            when FS_I2C_STOP =>
                if clk_cnt = CNT4 - 1 then
                    next_state  <= FS_I2C_FINISH;
                else
                    next_state  <= FS_I2C_STOP;
                end if;

            when FS_I2C_FINISH =>
                if count < x"FF" then   -- increase the waiting time for next ASIC
                    next_state  <= FS_I2C_FINISH;
                else
                    next_state  <= s27;
                end if;

            when FS_I2C_MASTER_NACK =>
                if clk_cnt = CNT4 -1 then
                    next_state  <= FS_I2C_STOP;
                else
                    next_state  <= FS_I2C_MASTER_NACK;
                end if;

            when s27 =>     -- record 
                if asic_index < C_ASIC_NUM_MAX then
                    next_state  <= FS_ADDR;
                else
                    next_state  <= s28;
                end if;

            when s28 =>
                next_state      <= s29;
    
            when s29 =>
                if ro_tx_start = '1' then
                    next_state <= s30;
                else
                    next_state <= s29;
                end if;
            
            when s30 =>
                next_state <= s31;

            when s31 =>
                if ro_done = '1' then
                    next_state <= s32;
                else
                    next_state <= s31;
                end if;

            when s32 =>
                next_state <= s33;

            when s33 =>
                next_state <= FS_IDLE;

            when others =>
                next_state  <= FS_IDLE;
            
        end case;
    end process nextstate_proc;
    
    -----------------------------------------------------------------
    output_proc : process (fclk)
    -----------------------------------------------------------------
    begin
        if rising_edge(fclk) then
            if rst = '1' then
                ro_wr_en    <= "0";
                asic_index  <= (others => '0');
                asic_ix     <= (others => '0');
                ro_rx_end   <= '0';
                ro_tx_end   <= '0';
                mem_rst     <= '1';
                bit_cnt     <= 7;
                byte_cnt    <= 0;
                evt_cnt     <= 0;
                for i in 0 to (C_ASIC_NUM_MAX) loop
                   addr_pt(i) <= (others => '0');
                end loop;
                s_sda_clk   <= '1';
                s_scl_clk   <= '1';
                slave_nack  <= '0';
                s_i2c_data_rx   <= (others => '0');
                s_i2c_data_tx   <= (others => '0');
                s_last_evt  <= '0';
                ro_wr_addr  <= (others => '0');
                wr_index    <= (others => '0');
                ro_mem_in   <= (others => '0');

            --elsif clk_strobe_5 = '1' then
            else
                case current_state is
                    when FS_IDLE =>
                        ro_wr_en    <= "0";
                        asic_index  <= (others => '0');
                        asic_ix     <= (others => '0');
                        ro_rx_end   <= '0';
                        ro_tx_end   <= '0';
                        mem_rst     <= '1';
                        bit_cnt     <= 7;
                        byte_cnt    <= 0;
                        evt_cnt     <= CNT2;
                        for i in 0 to (C_ASIC_NUM_MAX) loop
                           addr_pt(i) <= (others => '0');
                        end loop;
                        s_sda_clk   <= '1';
                        s_scl_clk   <= '1';
                        s_i2c_data_rx   <= (others => '0');
                        s_i2c_data_tx   <= (others => '0');
                        slave_nack  <= '0';
                        s_last_evt  <= '0';
                        ro_wr_addr  <= (others => '0');
                        wr_index    <= (others => '0');
                        ro_mem_in   <= (others => '0');

                    when FS_ADDR =>
                        ro_wr_en    <= "1";
                        clk_cnt     <= CNT2;
                        bit_cnt     <= 7;
                        byte_cnt    <= 0;
                        evt_cnt     <= 0;
                        count       <= (others => '0');
                        s_sda_clk   <= '1';
                        s_scl_clk   <= '1';
                        s_i2c_data_tx(7 downto 1)   <= i2c_addr_calc(asic_index);
                        s_i2c_data_tx(0)            <= '1';
                        sda_int     <= '1';
                        slave_nack  <= '0';
                        s_last_evt  <= '0';

                    when FS_I2C_START =>
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt = CNT3 - 1 then
                            s_sda_clk   <= '0';
                            sda_int     <= '0';
                        end if;
                        s_scl_clk   <= '1';         -- always 1

                    when FS_I2C_ADDR =>
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                            if bit_cnt > 0 then
                                bit_cnt <= bit_cnt - 1;     -- 
                            end if;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt < CNT3 then  -- sda_clk
                            s_sda_clk   <= '1';
                            sda_int     <= s_i2c_data_tx(bit_cnt);  -- master load data to sda line
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if clk_cnt < CNT2 then
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;
                        
                    when FS_I2C_WAIT_SLAVE_ACK =>
                        bit_cnt     <= 7;
                        sda_int     <= '1';             -- master release sda line imediately
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt = CNT4 -2 then  -- slave nack
                            slave_nack  <= i2c_sda_i;
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt < CNT3 then  -- sda_clk
                            s_sda_clk   <= '1';
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if clk_cnt < CNT2 then    -- scl_clk
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;

                    when FS_I2C_SLAVE_NACK =>

                    when FS_I2C_READ_BYTE =>
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                            if bit_cnt > 0 then
                                bit_cnt <= bit_cnt - 1;     -- 
                            end if;
                            s_i2c_data_rx(bit_cnt)  <= i2c_sda_i;
                            --ro_mem_in(bit_cnt)      <= i2c_sda_i;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt < CNT3 then  -- sda_clk
                            s_sda_clk   <= '1';
                            sda_int     <= '1';
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if clk_cnt < CNT2 then
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;

                    when FS_I2C_MASTER_NACK =>
                        bit_cnt     <= 7;
                        byte_cnt    <= 4;
                        if clk_cnt = CNT4 -1 then   -- clk_counter
                            clk_cnt <= 0;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt < CNT2 then      -- scl clock
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt < CNT3 then  -- sda_clk
                            s_sda_clk   <= '1';
                            sda_int     <= '0';
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if clk_cnt >= CNT1 then     -- NACK
                            sda_int     <= '1';
                        end if;
                        if clk_cnt = CNT2 then
                            ro_mem_in   <= s_i2c_data_rx;
                        end if;
                        if clk_cnt = CNT3 then
                            ro_wr_addr  <= std_logic_vector(unsigned(ro_wr_addr) + 1);
                            wr_index    <= wr_index + 1;
                        end if;

                    when FS_I2C_MASTER_ACK =>
                        bit_cnt     <= 7;
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                            if byte_cnt = 4 then        -- byte counter
                                evt_cnt     <= evt_cnt + 1;
                                byte_cnt    <= 0;
                            else
                                byte_cnt    <= byte_cnt + 1;
                            end if;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt < CNT2 then
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt < CNT3 then  -- sda_clk
                            s_sda_clk   <= '1';
                            sda_int     <= '0';
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if byte_cnt = 0 and ((s_i2c_data_rx = x"FE" and evt_cnt > 0)or evt_cnt = C_MAX_EVT) then
                            s_last_evt  <= '1';
                        end if;
                        if clk_cnt = CNT2 then
                            ro_mem_in   <= s_i2c_data_rx;
                        end if;
                        if clk_cnt = CNT3 then
                            ro_wr_addr  <= std_logic_vector(unsigned(ro_wr_addr) + 1);
                            wr_index    <= wr_index + 1;
                        end if;

                    when FS_I2C_STOP =>
                        count       <= (others => '0');
                        if clk_cnt = CNT4 -1 then  -- clk_counter
                            clk_cnt <= 0;
                            --s_i2c_data_rx(bit_cnt)  <= i2c_sda_i;
                        else
                            clk_cnt <= clk_cnt + 1;
                        end if;
                        if clk_cnt < CNT2 then
                            s_scl_clk   <= '0';
                        else
                            s_scl_clk   <= '1';
                        end if;
                        if clk_cnt >= CNT1 then  -- sda_clk
                            s_sda_clk   <= '1';
                        else
                            s_sda_clk   <= '0';
                        end if;
                        if clk_cnt >= CNT1 and clk_cnt <= CNT3 then
                            sda_int     <= '0';
                        else
                            sda_int     <= '1';
                        end if;

                    when FS_I2C_FINISH =>
                        sda_int     <= '1';
                        count       <= std_logic_vector(unsigned(count)+1);

                    when s27 =>
                        if clk_strobe_5 = '1' then
                            addr_pt(to_integer(asic_index)) <= wr_index;
                            asic_index                      <= asic_index + 1;
                            s_last_evt  <= '0';
                            -- starting address prepared for the next ASIC if exist
                            ro_wr_addr <= std_logic_vector(unsigned(ro_wr_addr) + 1);
                            wr_index   <= wr_index + 1;
                        end if;

                    when s28 =>
                        if clk_strobe_5 = '1' then
                            ro_rx_end   <= '1';
                        end if;

                    when s29 =>
                        if clk_strobe_5 = '1' then
                            ro_wr_en    <= "0";
                            asic_ix     <= asic_index;
                        end if;

                    when s30 =>
                        if clk_strobe_5 = '1' then
                            ro_rx_end   <= '0';
                        end if;
                    when s31 =>
                    when s32 =>
                        if clk_strobe_5 = '1' then
                            ro_tx_end   <= '1';
                        end if;
                    when s33 =>
                        if clk_strobe_5 = '1' then
                            ro_tx_end   <= '0';
                        end if;
                    when others =>

                end case;
            end if;
        end if; -- clk
    end process output_proc;

end architecture rtl;




