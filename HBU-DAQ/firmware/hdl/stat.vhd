
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stat is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;

      cmd_ready : in std_logic;
      cmd_end   : in std_logic;
      cmd12     : in std_logic_vector (15 downto 0);
      cmd34     : in std_logic_vector (15 downto 0);

      usb_txen      : in  std_logic;
      writing       : in  std_logic;
      stat_wr_req   : out std_logic;
      stat_data_out : out std_logic_vector (7 downto 0);

--    tb_spill      : IN     std_logic;
      tb_trig       : in std_logic;     -- =validate
      tb_busy       : in std_logic;
      start_readout : in std_logic;

      spiroc_type   : in  std_logic;
      stat_cmd_done : out std_logic
      );
end stat;

architecture ctrl of stat is

   type STATE_TYPE is (s0, s01,
                       s21, s22, s23, s24, s25,
                       s26, s27, s28, s29, s30,
                       s90, s91, s92, s93);

   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;
   signal trig_int      : std_logic;

   signal tb_spill : std_logic;

begin
   
   tb_spill <= '0';

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (cmd12, cmd34, cmd_end, cmd_ready, current_state, usb_txen, writing)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            -- wait for command
            if cmd_ready = '1' then
               next_state <= s01;
            else
               next_state <= s0;
            end if;
         when s01 =>
--      IF  cmd12 = x"CC84" AND cmd34 = x"0001" THEN
            if cmd12 = x"0084" and cmd34 = x"0000" then
               next_state <= s21;
            else
               next_state <= s0;        -- not cal command
            end if;

         when s21 =>                    -- send 1st status byte 
            next_state <= s22;
         when s22 =>
            next_state <= s23;
         when s23 =>
            if writing = '1' then
               next_state <= s24;
            else
               next_state <= s23;
            end if;
         when s24 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= s25;
            else
               next_state <= s24;
            end if;
         when s25 =>
            next_state <= s26;

         when s26 =>                    -- send 2nd status byte 
            next_state <= s27;
         when s27 =>
            next_state <= s28;
         when s28 =>
            if writing = '1' then
               next_state <= s29;
            else
               next_state <= s28;
            end if;
         when s29 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= s30;
            else
               next_state <= s29;
            end if;
         when s30 =>
            next_state <= s90;


         when s90 =>
            next_state <= s91;

         when s91 =>
            next_state <= s92;
         when s92 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s93;
            else
               next_state <= s92;
            end if;
         when s93 =>
            next_state <= s0;

      -- WHEN OTHERS =>
      -- next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            stat_cmd_done <= '0';
            stat_wr_req   <= '0';
            stat_data_out <= (others => '0');
         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  stat_cmd_done <= '0';
                  stat_wr_req   <= '0';
                  stat_data_out <= (others => '0');

               when s01 =>

               when s21 =>
                  stat_data_out <= "000000" & trig_int & tb_busy;
               when s22 =>
                  stat_wr_req <= '1';
               when s23 =>
                  stat_wr_req <= '0';
               when s24 =>
               when s25 =>

               when s26 =>
                  stat_data_out <= "0000000" & spiroc_type;
               when s27 =>
                  stat_wr_req <= '1';
               when s28 =>
                  stat_wr_req <= '0';
               when s29 =>
               when s30 =>

               when s90 =>
                  stat_wr_req <= '0';
               when s91 =>
                  stat_cmd_done <= '1';
               when s92 =>
               when s93 =>
                  stat_cmd_done <= '0';

            --WHEN OTHERS =>
            -- NULL;
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;


   -----------------------------------------------------------------
   stat_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            trig_int <= '0';
         elsif (clk_strobe_5 = '1') then
            if start_readout = '1' then
               trig_int <= '0';
            elsif (tb_spill = '1' and tb_trig = '1') then
               trig_int <= '1';
            else
               trig_int <= trig_int;
            end if;
         end if;  --strobe
      end if;  --clk
   end process stat_proc;

end architecture ctrl;

