-------------------------------------------------------------------------------
-- Title      : Duty cycle controller
-- Project    : 
-------------------------------------------------------------------------------
-- File       : dutycycle.vhd
-- Author     : Jiri Kvasnicka (jiri.kvasnicka@desy.de), (kvas@fzu.cz)
-- Company    : DESY / Institute of Physics ASCR
-- Created    : 2018-11-08
-- Last update: 2019-04-30
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This module has following functionalities:
--   * receives the configuration commands
--   * guards two duty cycles
--   * evaluates conditions for stopping and extending of busy
-------------------------------------------------------------------------------
-- Copyright (c) 2018 DESY / Institute of Physics ASCR
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-11-08  1.0      kvas    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.adif2_types.all;

entity dutycycle is
   port (
      fclk            : in  std_logic;  -- main clock
      clk_strobe_5    : in  std_logic;
      ms_tick         : in  std_logic;  --1 pulse in slow clock domain (8 fclk tics)
      srst            : in  std_logic;  --synchronous reset
      P_I_CMD_REC     : in  T_CMD_REC;  -- full command record
      cmd_ready       : in  std_logic;
      cmd_end         : in  std_logic;
      dc_cmd_done     : out std_logic;  --dutycycle command done
      --powerpulsing info signals
      pwr_ana         : in  std_logic;  --power status signals from acquisition module
      pwr_dig         : in  std_logic;  --power status signals from acquisition module
      pwr_sca         : in  std_logic;  --power status signals from acquisition module
      pwr_adc         : in  std_logic;  --power status signals from acquisition module
      pwr_dac         : in  std_logic;  --power status signals from acquisition module
      pp_mandatory    : out std_logic;  -- whether powerpulsing is obligatory
      --duty cycle results
      dc_busy_extend  : out std_logic;  -- busy signal should be extended until this signal goes down
      dc_stop_request : out std_logic   -- duty cycle is violated, need to stop now
      );
end entity dutycycle;

architecture rtl of dutycycle is
   --first duty cycle governor: checks, that ACQuisition period is shorter, than discharge time of the powerpulsing capacitors   
   signal DC1_cost0 : signed(15 downto 0);  --must be a negative number. Counted when everything is off (powerpulsed)
   --three different costs represent different cunsumption levels. At the moment only cost1 is implemented
   signal DC1_cost1 : signed(15 downto 0) ;  -- constant, that will be counted during active time
   signal DC1_cost2 : signed(15 downto 0) ;  -- reserved for other states (conversion)
   signal DC1_cost3 : signed(15 downto 0) ;  -- reserved for other states (readout)
   signal DC1_limit : signed(47 downto 0);                      --decimal=225000;  --5 ms acquisition with cost 9
   signal DC1_zero  : std_logic           := '0';               --counter is at 0
   signal DC1_over  : std_logic           := '0';               --counter is over the limit
   --second duty cycle governor: eliinates long-term heat production on POWERBOARD
   signal DC2_cost0 : signed(15 downto 0);
   signal DC2_cost1 : signed(15 downto 0);
   signal DC2_cost2 : signed(15 downto 0);
   signal DC2_cost3 : signed(15 downto 0);
   signal DC2_limit : signed(47 downto 0);                      --decimal=450000000, ~10 s of continuous operation --TO_SIGNED(450000000, 48)
   signal DC2_zero  : std_logic           := '0';               --counter is at 0
   signal DC2_over  : std_logic           := '0';               --counter is over the limit

   signal mode_select : unsigned(1 downto 0) := (others => '0');  --selects the cost mode in both dutycycle governor

   signal s_pp_mandatory : std_logic;   -- dutycycle conditions are set in a way, that the powerpulsing is mandatory
   signal s_busy_extend  : std_logic;   -- busy signal should be extended until this signal goes down
   signal s_stop_request : std_logic;   -- duty cycle is violated, need to stop now
begin  -- architecture rtl
   dc_ctrl_inst : entity work.dutycycle_ctrl
      port map (
         fclk         => fclk,
         srst         => srst,
         clk_strobe_5 => clk_strobe_5,
         ms_tick      => ms_tick,
         P_I_CMD_REC  => P_I_CMD_REC,
         cmd_ready    => cmd_ready,
         cmd_end      => cmd_end,
         dc_cmd_done  => dc_cmd_done,
         DC1_cost0    => DC1_cost0,
         DC1_cost1    => DC1_cost1,
         DC1_cost2    => DC1_cost2,
         DC1_cost3    => DC1_cost3,
         DC1_limit    => DC1_limit,
         DC2_cost0    => DC2_cost0,
         DC2_cost1    => DC2_cost1,
         DC2_cost2    => DC2_cost2,
         DC2_cost3    => DC2_cost3,
         DC2_limit    => DC2_limit);

   dc_monitor_dsp_1 : entity work.dutycycle_monitor_dsp
      port map (
         clk           => fclk,
         ce            => clk_strobe_5,
         srst          => srst,
         cost_mode0    => DC1_cost0,
         cost_mode1    => DC1_cost1,
         cost_mode2    => DC1_cost2,
         cost_mode3    => DC1_cost3,
         cost_limit    => DC1_limit,
         mode_select   => mode_select,
         reached_limit => DC1_over,
         reached_zero  => DC1_zero,
         counter       => open);

   dc_monitor_dsp_2 : entity work.dutycycle_monitor_dsp
      port map (
         clk           => fclk,
         ce            => clk_strobe_5,
         srst          => srst,
         cost_mode0    => DC2_cost0,
         cost_mode1    => DC2_cost1,
         cost_mode2    => DC2_cost2,
         cost_mode3    => DC2_cost3,
         cost_limit    => DC2_limit,
         mode_select   => mode_select,
         reached_limit => DC2_over,
         reached_zero  => DC2_zero,
         counter       => open);

   pp_mandatory_process : process(fclk)
   begin
      if rising_edge(fclk) then
         if (clk_strobe_5) = '1' then
            if (DC1_cost1 = (DC1_cost1'range => '0')) and
               (DC1_cost2 = (DC1_cost2'range => '0')) and
               (DC1_cost3 = (DC1_cost3'range => '0')) and
               (DC2_cost1 = (DC2_cost1'range => '0')) and
               (DC2_cost2 = (DC2_cost2'range => '0')) and
               (DC2_cost3 = (DC2_cost3'range => '0'))
            then
               s_pp_mandatory <= '0';
            else
               s_pp_mandatory <= '1';
            end if;
         end if;  --strobe
      end if;  --clk
   end process;
   pp_mandatory <= s_pp_mandatory;

   busy_extend_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if clk_strobe_5 = '1' then
            if (DC1_zero = '0') or      --first counter not yet returned to -1
               (DC2_over = '1')         --second counter is over the limit
            then
               s_busy_extend <= '1';
            else
               s_busy_extend <= '0';
            end if;
         end if;  --strobe
      end if;  --clk
   end process;
   dc_busy_extend <= s_busy_extend;

   stop_reqest_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if clk_strobe_5 = '1' then
            if DC1_over = '1' then      --first counter is over the limit
               s_stop_request <= '1';
            else
               s_stop_request <= '0';
            end if;
         end if;  --clk_strobe
      end if;  --clk
   end process;
   dc_stop_request <= s_stop_request;

   mode_select_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if clk_strobe_5 = '1' then
            mode_select <= "00";
            if (pwr_dac = '1') or
               (pwr_adc = '1') or
               (pwr_sca = '1') or
               (pwr_dig = '1') or
               (pwr_ana = '1')
            then
               mode_select <= "01";
            end if;  --or of pwr_*
         --TODO more levels of powerpulsing
         end if;  --clk_strobe
      end if;  --clk
   end process;
end architecture rtl;
