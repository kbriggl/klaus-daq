--
-- VHDL Architecture adif_lib.usb_if.rtl
--
-- Created:
--          by - krivan.UNKNOWN (FEBPCX3865)
--          at - 15:09:19 17.02.2009
--
-- using Mentor Graphics HDL Designer(TM) 2005.3 (Build 75)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

entity usb_if is
   port(
      -- USB interface
      usb_rxfn     : in    std_logic;
      usb_txen     : in    std_logic;
      usb_rdn      : out   std_logic;
      usb_wr       : out   std_logic;
      usb_data     : inout std_logic_vector (7 downto 0);
      -- system interface
      -- clk          : in    std_logic;
      fclk         : in    std_logic;   --clk 40MHz
      clk_strobe_5 : in    std_logic;   --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in    std_logic;
      -- DIF to USB data
      wr_req       : in    std_logic;
      writing      : out   std_logic;
      tx_data      : in    std_logic_vector (7 downto 0);
      -- USB to DIF data
      usb_strobe   : out   std_logic;
      rx_data      : out   std_logic_vector (7 downto 0);
      if_enable    : in    std_logic    --interface is enabled    
      );
end usb_if;

architecture rtl of usb_if is

   component usb2dif                                        -- data transfer from USB FIFO to DIF FPGA register
      port (
         -- clk          : in  std_logic;                      -- external clock
         fclk         : in  std_logic;                      --clk 40MHz
         clk_strobe_5 : in  std_logic;                      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
         rst          : in  std_logic;                      -- global reset
         usb_rxfn     : in  std_logic;                      -- external USB FIFO signal
         usb_rdn      : out std_logic;                      -- data transfer cycle
         usb_data     : in  std_logic_vector (7 downto 0);  -- USB FIFO data
         usb_strobe   : out std_logic;                      -- data strobe to FPGA register
         rx_data      : out std_logic_vector (7 downto 0);  -- FPGA register data (in)
         if_enable    : in  std_logic                       --interface is enabled
         );
   end component;

   component dif2usb                                        -- data transfer from DIF FPGA register to USB FIFO
      port (
         -- clk          : in  std_logic;                      -- external clock
         fclk         : in  std_logic;                      --clk 40MHz
         clk_strobe_5 : in  std_logic;                      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
         rst          : in  std_logic;                      -- global reset
         usb_txen     : in  std_logic;                      -- external USB FIFO signal
         usb_wr       : out std_logic;                      -- data transfer cycle ( FPGA -> USB )
         usb_data     : out std_logic_vector (7 downto 0);  -- USB FIFO data
         wr_req       : in  std_logic;                      -- request to transfer data from FPGA to FIFO
         writing      : out std_logic;                      -- data transfer cycle ( FPGA -> USB )
         tx_data      : in  std_logic_vector (7 downto 0);  -- FPGA register data (out)
         if_enable    : in  std_logic                       --interface is enabled
         );
   end component;

begin

   usb2dif_inst : usb2dif               -- data transfer from USB FIFO to DIF FPGA register
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => rst,
         usb_rxfn     => usb_rxfn,
         usb_rdn      => usb_rdn,
         usb_data     => usb_data,
         usb_strobe   => usb_strobe,
         rx_data      => rx_data,
         if_enable    => if_enable
         );

   dif2usb_inst : dif2usb               -- data transfer from DIF FPGA register to USB FIFO
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         rst          => rst,
         usb_txen     => usb_txen,
         usb_wr       => usb_wr,
         usb_data     => usb_data,
         wr_req       => wr_req,
         writing      => writing,
         tx_data      => tx_data,
         if_enable    => if_enable
         );

end architecture rtl;
