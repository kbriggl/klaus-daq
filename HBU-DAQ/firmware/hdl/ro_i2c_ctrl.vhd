
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

library unisim;
use unisim.vcomponents.all;

use work.adif2_types.all;

entity ro_i2c_ctrl is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      ms_tick      : in std_logic;      --clk domain

      rst       : in std_logic;
      cmd_ready : in std_logic;
      cmd_end   : in std_logic;
      cmd12     : in std_logic_vector (15 downto 0);
      cmd34     : in std_logic_vector (15 downto 0);
      cmd56     : in std_logic_vector (15 downto 0);    -- NC

      ro_dbg : out std_logic;

      start_readout   : out std_logic;                                  -- indicating readout ASIC is started
      end_readout_out : out std_logic;                                  -- indicating readout ASIC is ended
      --dout            : in  std_logic_vector (C_NUM_ROW downto 1);      -- every slab has two row
      --transmit_on     : in  std_logic_vector (C_NUM_ROW downto 1);      -- every slab has two
      --end_readout     : in  std_logic_vector (C_NUM_ROW downto 1);      -- every slab has two

      -- i2c interface
      sda       : inout std_logic_vector(C_NUM_ROW downto 1);
      scl       : inout std_logic_vector(C_NUM_ROW downto 1);

      dif_id : in std_logic_vector (15 downto 0);
      ro_cycle : in std_logic_vector (7 downto 0);

      ro_clk_sel  : out std_logic;      -- always 0 in KLauS
      clk_readout : out std_logic;      -- always 0 in KLauS

      usb_txen    : in  std_logic;
      writing     : in  std_logic;
      ro_wr_req   : out std_logic;
      ro_data_out : out std_logic_vector (7 downto 0);
      turbo       : in  std_logic;      --faster communicatino protocol primarly designed for hdmi interface (txen handshare acts as full/busy signal). Added by Jiri


      data_ready : in  std_logic;
      tb_busy    : out std_logic;

      ro_debug : out std_logic_vector(7 downto 0);

      ro_cmd_done   : out std_logic;
      reset_slab_ro : out std_logic     --added by jiri to perform an automated slab reset after the end of each cycle
      );

end ro_i2c_ctrl;

architecture rtl of ro_i2c_ctrl is

    component ro_i2c_row is
    generic(        -- i2c readout speed in Hz
        divider     :   integer := 24;   -- divider of 10MHz clock
        C_MAX_EVT   :   integer := 511
    );
    port(
            fclk         : in std_logic;      --clk 40MHz
            clk_strobe_5 : in std_logic;      
            rst          : in std_logic;
            
            -- i2c interface
            i2c_sda_i	: in	std_logic;
            i2c_sda_o	: out	std_logic;
            i2c_sda_oen	: out	std_logic;
            i2c_scl_i	: in	std_logic;
            i2c_scl_o	: out	std_logic;
            i2c_scl_oen	: out	std_logic;
            
            ro_rx_start : in  std_logic;
            ro_rx_end   : out std_logic;
            
            ro_tx_start : in  std_logic;
            ro_tx_end   : out std_logic;
            
            asic_ix : out unsigned(3 downto 0);
            addr_pt : out POINTER;
            
    
            ro_rd_addr : in  std_logic_vector (14 downto 0);
            ro_mem_out : out std_logic_vector (7 downto 0);
            
            ro_done : in std_logic
    );
    end component ro_i2c_row;
    
    
    type STATE_TYPE is (s0, s01, s02, s03, s04, s05, s06, s07,
                        s10, s11, s12, s13, s14, s17, s18,
                        s28, s29, s31, s32,
                        s41, s42, s43, s44, s45, s46, s47, s48,
                        s50a, s51a, s52a, s53a, s54a, s55a, s56a, s57a,	-- states to send the chip ID
                        s50, s51, s52, s53, s54, s55, s56, s57,
                        s60, s61, s62, s63,
                        s80, S84,
                        s81, s82, s83, s85, s86, s87, s88, s89,
                        s91a, s91b, s91c, s91d,
                        s90, s91, s92);
    
    signal current_state : STATE_TYPE;
    signal next_state    : STATE_TYPE;
    
    signal pheader : HEADER;
    
    signal ro_asic_index : ASIC_DB;         -- e.g. 2 ASIC in row 1, then ro_asic_index(1) = 2
    signal ro_addr_pt    : ADDR_POINTER;    -- top address 
    signal ro_pt         : POINTER;
    signal ro_dout       : DATA_OUT;
    
    signal ro_rd_addr : std_logic_vector (14 downto 0); -- always point to the address of next byte to be read
    
    signal rd_index   : unsigned(14 downto 0);
    signal tx_index   : unsigned(14 downto 0);          -- maximum data address, included
    signal head_index : integer range 0 to 15;
    signal pack_index : unsigned(9 downto 0);
    signal asic       : unsigned(3 downto 0);
    
    signal row : integer range 1 to C_NUM_ROW;  --UNSIGNED(2 DOWNTO 0);
    
    signal timeoutcnt   : unsigned(9 downto 0);  --counts ms_tick for up to 1 s
    signal count        : unsigned(15 downto 0);
    signal slab_rst_cnt : unsigned(7 downto 0);  --4 bits used only. A reserve was left for expansion if needed
     signal pack1        : std_logic;					-- 1: first packet for this chip
    signal asic_ro      : std_logic;
    
    signal ro_rx_start : std_logic_vector (C_NUM_ROW downto 1);
    signal ro_rx_end   : std_logic_vector (C_NUM_ROW downto 1);
    
    signal ro_tx_start : std_logic_vector (C_NUM_ROW downto 1);
    signal ro_tx_end   : std_logic_vector (C_NUM_ROW downto 1);
    
    signal sda_i         : std_logic_vector(C_NUM_ROW downto 1);
    signal sda_o         : std_logic_vector(C_NUM_ROW downto 1);
    signal sda_oen       : std_logic_vector(C_NUM_ROW downto 1);
    signal scl_i         : std_logic_vector(C_NUM_ROW downto 1);
    signal scl_o         : std_logic_vector(C_NUM_ROW downto 1);
    signal scl_oen       : std_logic_vector(C_NUM_ROW downto 1);
    
    type RO_ROW_DEBUG is array(1 to C_NUM_ROW) of std_logic_vector(7 downto 0);
    
    signal ro_done : std_logic;
	signal	byte_ready	:	std_logic;
    signal  dbg_start_asic  : std_logic;
    signal  dbg_start_row   : std_logic;
    signal  evt_byte_cnt    : std_logic_vector(3 downto 0);

   attribute mark_debug               : string;
   attribute mark_debug of timeoutcnt : signal is "true";
begin

--  ro_debug <= ro_rd_addr(7 downto 0);
-- ro_debug(0) <= start_readout_int;
-- ro_debug(1) <= end_readout_int;
-- ro_debug(2) <= ro_clk_sel_int;
-- ro_debug(3) <= clk_readout_int;
-- ro_debug(4) <= ro_wr_req_int;
-- ro_debug(5) <= dtest(1);
-- ro_debug(6) <= dtest(2);
-- ro_debug(7) <= ro_cmd_done_int;	
	ro_debug(0) <= ro_rx_start(1);
	ro_debug(1) <= ro_rx_end(1);
	ro_debug(2) <= ro_tx_start(1);
	ro_debug(3) <= ro_tx_end(1);
    ro_debug(7 downto 4) <= (others => '0');

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (asic, asic_ro, cmd12, cmd34, cmd_end, cmd_ready, count, 
       current_state, head_index, pack_index, rd_index, ro_asic_index, ro_rx_end, row, 
       slab_rst_cnt, timeoutcnt,turbo, tx_index, usb_txen, writing)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            --wait for usb_command
            if cmd_ready = '1' then
               next_state <= s01;
            else
               next_state <= s0;
            end if;
         when s01 =>
--      IF cmd12 = x"CC0E" AND cmd34 = x"0001" THEN
            if cmd12 = x"000E" and cmd34 = x"0000" then
               next_state <= s02;                                                                                 -- start readout command
            else
               next_state <= s0;
            end if;
         when s02 =>
            next_state <= s03;
         when s03 =>
            next_state <= s04;
         when s04 =>
            next_state <= s05;
         when s05 =>
            next_state <= s06;
         when s06 =>
            next_state <= s07;
         when s07 =>
            next_state <= s10;
         when s10 =>                                                                                              --clk_readout is '1' in this state only
            next_state <= s14;                                                                                    --s11;
         when s11 =>
            next_state <= s14;                                                                                    --s12;
         when s12 =>                                                                                              --skipped
            next_state <= s13;
         when s13 =>                                                                                              --skipped
            next_state <= s14;
         when s14 =>
            if (ro_rx_end = (ro_rx_end'range => '1')) --if the readout ended in all ro-modules, or readout too long
               or (timeoutcnt > to_unsigned(500, timeoutcnt'length))  
            then
               next_state <= s18;
            else
               next_state <= s17;
            end if;
         when s17 =>
            next_state <= s11;                                                                                    --s10;
         when s18 =>
            next_state <= s28;

         when s28 =>
            if asic_ro = '1' then
               next_state <= s18;
            else
               next_state <= s29;
            end if;
         when s29 =>
            next_state <= s31;

         when s31 =>
            if asic_ro = '1' then
               if asic < ro_asic_index(row) then
                  next_state <= s32;
               else
                  next_state <= s62;
               end if;
            else
               next_state <= s41;
            end if;
         when s32 =>
            next_state <= s41;

         when s41 =>
            next_state <= s42;
         when s42 =>
            next_state <= s43;
         when s43 =>                    --prepare the data
            next_state <= s44;
         when s44 =>
            if turbo = '1' then
               if (usb_txen = '0') then
                  next_state <= s47;    --skip then handshaking protocol in turbomode
               else
                  next_state <= s44;    --we do not send the wr_req until the usb_txen is available.
               end if;  --usb_txen=0
            else
               next_state <= s45;
            end if;  --turbo
         when s45 =>
            if writing = '1' then
               next_state <= s46;
            elsif count > x"0005" then
               next_state <= s46;
            else
               next_state <= s45;
            end if;
         when s46 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= s47;
            else
               next_state <= s46;
            end if;
         when s47 =>
            next_state <= s48;
        when s48 =>
            if head_index < C_NUM_WORD_HEAD then
                if turbo = '1' then
                   next_state <= s42;
                else
                   next_state <= s42;
                end if;
            else
                next_state <= s50;
            end if;

			-- data readout loop
        when s50 =>
            --if turbo = '1' then
            --   next_state <= s53;       --skip the idle states in turbo mode
            --else
            --   next_state <= s51;
            --end if;
            next_state  <= s51a;        -- write down the chip ID



        when s50a =>
        when s51a =>
            next_state <= s52a;
        when s52a =>
            next_state <= s53a;
        when s53a =>
            next_state <= s54a;
        when s54a =>
            if turbo = '1' then
                if (usb_txen = '0') then
                    next_state <= s57a;    --turbo jump skipping the handshakes
                else
                    next_state <= s54a;    --wait until we can save the data
                end if;
            else
                next_state <= s55a;
            end if;
        when s55a =>
            if writing = '1' then
                next_state <= s56a;
            elsif count > x"0005" then
                next_state <= s56a;
            else
                next_state <= s55a;
            end if;
        when s56a =>
            if (writing = '0' and usb_txen = '0') then
                next_state <= s57a;
            else
                next_state <= s56a;
            end if;
            when s57a =>
           	    next_state <= s53;

        when s51 =>
            next_state <= s52;
        when s52 =>
            next_state <= s53;
        when s53 =>
            next_state <= s54;
        when s54 =>
            if turbo = '1' then
                if (usb_txen = '0') then
                    next_state <= s57;    --turbo jump skipping the handshakes
                else
                    next_state <= s54;    --wait until we can save the data
                end if;
            else
                next_state <= s55;
            end if;
        when s55 =>
            if writing = '1' then
                next_state <= s56;
            elsif count > x"0005" then
                next_state <= s56;
            else
                next_state <= s55;
            end if;
        when s56 =>
            if (writing = '0' and usb_txen = '0') then
                next_state <= s57;
            else
                next_state <= s56;
            end if;
           
        when s57 =>
            --if (rd_index >= tx_index) or (pack_index = x"64") then
            --if (rd_index >= tx_index) or (pack_index = x"3FC") then	-- 1020 bytes, in total 170 evt
            if (rd_index > tx_index) or (pack_index = x"3FC") then	-- 1020 bytes, in total 170 evt
                if turbo = '1' then
                    next_state <= S80;
                else
                    next_state <= s81;
                end if;
            else
                if evt_byte_cnt = "0101" then   -- new event
                    next_state  <= s53a;        -- add the new chip ID to the event
                else
                    next_state  <= s53;         -- loop over event
                end if;
            end if;



         when s60 =>
            next_state <= s61;
            
         when s61 =>
            if asic < ro_asic_index(row) then
               next_state <= s32;
            else
               next_state <= s62;
            end if;
         when s62 =>
            if row = C_NUM_ROW then
--          next_state <= s81;
               next_state <= s90;
            else
               next_state <= s63;
            end if;
         when s63 =>
            next_state <= s31;
            
         when S80 =>                    --prepare the CRC data and wait for available txen
            if usb_txen = '0' then
               next_state <= S81;
            else
               next_state <= S80;       --wait until the data can be written
            end if;

         -- CRC
         when S81 =>
            if turbo = '1' then
               next_state <= S84;
            else
               next_state <= S82;
            end if;
         when S82 =>
            if writing = '1' then
               next_state <= S83;
            else
               next_state <= S82;
            end if;
         when S83 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S85;
            else
               next_state <= S83;
            end if;
            
         when S84 =>
            if usb_txen = '0' then
               next_state <= S85;       --we can directly proceed
            else
               next_state <= S84;       -- we have to wait until the data can be sent out.
            end if;
         when S85 =>
            if turbo = '1' then
               next_state <= S88;
            else
               next_state <= S86;
            end if;
         when S86 =>
            if writing = '1' then
               next_state <= S87;
            else
               next_state <= S86;
            end if;
         when S87 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S88;
            else
               next_state <= S87;
            end if;
         when S88 =>
            next_state <= s89;
            
         when s89 =>
            --if rd_index >= tx_index then
            if rd_index > tx_index then
               next_state <= s60;
            else
               next_state <= s41;
            end if;
         when s90 =>
            next_state <= s91;
         when s91 =>
            if cmd_end = '1' then
               next_state <= s91a;      --s92;
            else
               next_state <= s91;
            end if;

         when s91a =>                   -- slab reset is initiated
            next_state <= s91b;
         when s91b =>                   --check if we counted for long enough
            if slab_rst_cnt < x"0F" then
               next_state <= s91a;
            else
               next_state <= s91c;
            end if;
         when s91c =>                   -- empty state (reserved) 
            next_state <= s91d;
         when s91d =>                   -- clear back the reset
            next_state <= s92;

         when s92 =>
            next_state <= s0;

      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk, rst)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            timeoutcnt      <= (others => '0');
            reset_slab_ro   <= '1';     --default. reset is active 0
            slab_rst_cnt    <= (others => '0');
            start_readout   <= '0';
            clk_readout     <= '0';
            ro_clk_sel      <= '0';
            end_readout_out <= '0';
            ro_wr_req       <= '0';
            ro_done         <= '0';
            ro_cmd_done     <= '0';
            tb_busy         <= '0';
            ro_data_out     <= (others => '0');
            ro_rd_addr      <= (others => '0');
            rd_index        <= (others => '0');
            pack_index      <= (others => '0');
            tx_index        <= (others => '0');
            asic            <= (others => '0');
            row             <= 1;
            count           <= (others => '0');
            head_index      <= 0;
            pack1           <= '0';
            asic_ro         <= '0';

            ro_dbg <= '0';
            dbg_start_asic  <= '0';
            dbg_start_row   <= '0';

            ro_rx_start <= (others => '0');
            ro_tx_start <= (others => '0');
            evt_byte_cnt    <= (others => '0');

--      for i in 0 to C_NUM_WORD_HEAD loop
--          pheader(i) <= (OTHERS => '0');
--        end loop;

            pheader(0)  <= x"43";
            pheader(1)  <= x"41";
            pheader(2)  <= x"00";
            pheader(3)  <= x"00";       --packet ID
            pheader(4)  <= x"41";
            pheader(5)  <= x"48";
            pheader(6)  <= x"00";
            pheader(7)  <= x"36";       --Jiri: changed from 34. Length now consistent with daq manual and other readout packets
            pheader(8)  <= x"46";       --???
            pheader(9)  <= x"45";       --???
            pheader(10) <= x"00";
            pheader(11) <= x"00";
            pheader(12) <= x"00";
            pheader(13) <= x"00";
            pheader(14) <= x"EE";

         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  pheader(0)  <= x"43";
                  pheader(1)  <= x"41";
                  pheader(2)  <= unsigned(ro_cycle);--x"00";
                  pheader(4)  <= x"41";
                  pheader(5)  <= x"48";
                  pheader(6)  <= x"00";
                  --pheader(7)  <= x"36";          --Jiri: changed from 34. Length now consistent with daq manual and other readout packets
                  --pheader(8)  <= x"46";
                  --pheader(9)  <= x"45";
                  --pheader(10) <= x"00";
                  --pheader(11) <= x"00";
                  --pheader(12) <= x"00";
                  --pheader(13) <= x"00";
                  pheader(14) <= x"EE";

                  slab_rst_cnt    <= (others => '0');
                  reset_slab_ro   <= '1';
                  start_readout   <= '0';
                  clk_readout     <= '0';
                  ro_clk_sel      <= '0';
                  end_readout_out <= '0';
                  ro_wr_req       <= '0';
                  ro_done         <= '0';
                  ro_cmd_done     <= '0';
                  if data_ready = '1' then  --the acquisition phase ended
                     tb_busy <= '1';
--        ELSE 
--                       tb_busy <= '0'; --set the busy default to '0'
                  end if;
                  ro_data_out <= (others => '0');
                  ro_rd_addr  <= (others => '0');
                  rd_index    <= (others => '0');
                  pack_index  <= (others => '0');
                  tx_index    <= (others => '0');
                  asic        <= (others => '0');
                  row         <= 1;
                  count       <= (others => '0');
                  head_index  <= 0;
                  pack1       <= '1';
                  asic_ro     <= '0';

                  ro_dbg            <= '0';
                  dbg_start_row     <= '0';
                  dbg_start_asic    <= '0';

                  ro_rx_start <= (others => '0');
                  ro_tx_start <= (others => '0');
                  evt_byte_cnt    <= (others => '0');
                  
               when s01 =>
                  ro_dbg <= '0';
                  
               when s02 =>
                  ro_clk_sel    <= '1';
                  clk_readout   <= '1';
                  start_readout <= '1';
                  ro_dbg        <= '1';
               when s03 =>
               when s04 =>
               when s05 =>
                  clk_readout <= '0';
               when s06 =>
                  ro_rx_start <= (others => '1');
               when s07 =>
                  clk_readout <= '1';
               when s10 =>
                  timeoutcnt    <= (others => '0');
                  start_readout <= '0';                                                         --investigate the effect of shorter start_readout pulse to POD functionality
                  clk_readout   <= '0';                                                         --'1';
                  ro_dbg        <= '0';
                  ro_rx_start   <= (others => '0');
               when s11 =>
                  timeoutcnt  <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  clk_readout <= '0';                                                           --'1';
               when s12 =>
                  timeoutcnt  <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  clk_readout <= '0';                                                           --'1';
               when s13 =>
                  timeoutcnt  <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  clk_readout <= '0';
               when s14 =>
                  timeoutcnt  <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  clk_readout <= '0';
                  ro_dbg      <= '0';
               when s17 =>
                  timeoutcnt  <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  clk_readout <= '1';                                                           --'0';
               when s18 =>
                  clk_readout     <= '0';                                                       --clk_readout <= '1';
                  asic_ro         <= '0';
                  end_readout_out <= '1';
                  ro_dbg          <= '0';
               when s28 =>
                  start_readout <= '0';
                  clk_readout   <= '1';                                                         --'0';
                  ro_dbg        <= '0';
               when s29 =>
                  clk_readout <= '0';
                  ro_rd_addr  <= (others => '0');
                  asic_ro     <= '1';
                  ro_tx_start <= (others => '1');
                  ro_dbg      <= '0';

               when s31 =>
                  ro_clk_sel <= '0';
                  ro_pt      <= ro_addr_pt(row);
                  ro_dbg     <= '0';
                  dbg_start_row <= '1';

               when s32 =>
                  tx_index <= ro_pt(to_integer(asic));
                  pack1    <= '1';
                  asic_ro  <= '0';
                  ro_dbg   <= '0';
                  dbg_start_asic    <= '1';

               when s41 =>
                  head_index  <= 0;
                  pheader(3)  <= pheader(3) + 1;     --packet ID
                  pheader(2) <= unsigned(ro_cycle);  --readout cycle number as a upper part of the packet ID
                  pheader(8)  <= pheader(6) + row;   --readout chain
                  pheader(9)  <= pheader(6) + asic;  -- spiroc in RO chain
                  pheader(11) <= pheader(11) + 1;
                  pheader(12) <= unsigned(dif_id(15 downto 8));
                  pheader(13) <= unsigned(dif_id(7 downto 0));

                  if pack1 = '1' then
                     pheader(10) <= x"B0";                                      --first packet
                     pheader(7)  <= x"36";                                      --added by Jiri
                  --elsif (tx_index - rd_index) < x"65" then
                  elsif (tx_index - rd_index) < x"3FD" then
                     pheader(10) <= x"E0";                                      --last packet
                     pheader(7)  <= shift_right((tx_index - rd_index + 8), 1);  --Added by Jiri. the remaining length  + 6 bytes of the header and 2 bytes of CRC. length is in words => result is divided by 2 by shr
                  else
                     pheader(10) <= x"FF";  --x"00"; --middle packet
                     pheader(7)  <= x"36";                                      --added by Jiri
                  end if;
               when s42 =>
                  pack1  <= '0';
                  ro_dbg <= '1';
                  
               when s43 =>
                  ro_data_out <= std_logic_vector(pheader(head_index));

               when s44 =>
                  if (turbo = '1') then
                     if (usb_txen = '0') then
                        ro_wr_req <= '1';  
                     end if;
                  else
                     ro_wr_req <= '1';
                  end if;  --turbo        
                  count <= (others => '0');
               when s45 =>
                  ro_wr_req <= '0';
                  count     <= count + 1;
                  ro_dbg    <= '0';
               when s46 =>
                  ro_dbg <= '0';

               when s47 =>
                  ro_wr_req  <= '0';    --Added by jiri for direct turbo jump from S44
                  head_index <= head_index + 1;
                  ro_dbg     <= '0';

               when s48 =>

					-- added for send out ASIC ID
               when s50a =>
               when s51a =>
               when s52a =>
               when s53a =>
                    ro_data_out   <= '0' & i2c_addr_calc(asic);	-- ASIC ID
                    evt_byte_cnt  <= (others => '0');
               when s54a =>
                  if turbo = '1' then          --in turbo mode we care about the txen as in s44
                     if (usb_txen = '0') then  --free to proceed
                        ro_wr_req  <= '1';
                     end if;
                  else
                     ro_wr_req  <= '1';
                  end if;
                  count <= (others => '0');
               when s55a =>
                  ro_wr_req <= '0';
                  count     <= count + 1;
               when s56a =>
               when s57a =>
                  ro_wr_req <= '0';            --added for the turbojump from s54, which skips assignment in s55


               when s50 =>
                  pack_index <= (others => '0');
                  ro_dbg     <= '0';

               when s51 =>

               when s52 =>

               when s53 =>
                  ro_data_out <= ro_dout(row);
                  evt_byte_cnt  <= std_logic_vector(unsigned(evt_byte_cnt)+1);

               when s54 =>
                  if turbo = '1' then          --in turbo mode we care about the txen as in s44
                     if (usb_txen = '0') then  --free to proceed
                        ro_wr_req  <= '1';
                        ro_rd_addr <= std_logic_vector(unsigned(ro_rd_addr) + 1);
                        rd_index   <= rd_index + 1;
                        pack_index <= pack_index + 1;
                     end if;
                  else
                     ro_wr_req  <= '1';
                     ro_rd_addr <= std_logic_vector(unsigned(ro_rd_addr) + 1);
                     rd_index   <= rd_index + 1;
                     pack_index <= pack_index + 1;
                  end if;
                  count <= (others => '0');
               when s55 =>
                  ro_wr_req <= '0';
                  count     <= count + 1;
                  ro_dbg    <= '0';
               when s56 =>
                  ro_dbg <= '0';
               when s57 =>
                  ro_wr_req <= '0';            --added for the turbojump from s54, which skips assignment in s55
                  ro_dbg    <= '0';

               when s60 =>
                  asic_ro <= '1';
                  asic    <= asic + 1;
                  ro_dbg  <= '0';
                  dbg_start_asic    <= '0';

               when s61 =>
                  ro_dbg <= '0';

               when s62 =>
                  ro_dbg <= '0';
               when s63 =>
                  row        <= row + 1;
                  asic       <= (others => '0');
                  ro_rd_addr <= (others => '0');
                  rd_index   <= (others => '0');
                  ro_dbg     <= '0';
                  dbg_start_row <= '0';
                  
               when S80 =>
                  ro_data_out <= x"AB";
                  
               when S81 =>
                  ro_wr_req   <= '1';
                  ro_data_out <= x"AB";
                  ro_dbg      <= '1';
               when S82 =>
                  ro_wr_req <= '0';
               when S83 =>
                  ro_wr_req <= '0';
                  ro_dbg    <= '0';
               when S84 =>
                  ro_wr_req <= '0';
               when S85 =>
                  ro_wr_req   <= '1';
                  ro_data_out <= x"AB";
                  ro_dbg      <= '1';
               when S86 =>
                  ro_wr_req <= '0';
               when S87 =>
                  ro_wr_req <= '0';
                  ro_dbg    <= '0';
               when S88 =>
                  ro_wr_req <= '0';
               when S89 =>
               when s90 =>
                  ro_done       <= '1';
                  ro_cmd_done   <= '1';
                  start_readout <= '0';
                  count         <= (others => '1');
                  ro_dbg        <= '1';
               when s91 =>
                  count        <= (others => '0');
                  slab_rst_cnt <= (others => '0');
                  ro_dbg       <= '0';
               when s91a =>
                  reset_slab_ro <= '0';  --reset slab        
               when s91b =>     
                  slab_rst_cnt <= slab_rst_cnt + 1;
               when s91c => null;
               when s91d =>
                  reset_slab_ro <= '1';  --return the reset to the inactive position 
               when s92 =>
                  ro_done     <= '0';
                  ro_cmd_done <= '0';
                  tb_busy     <= '0';
                  ro_dbg      <= '1';

            --WHEN OTHERS =>
            -- NULL;
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;


   -- divider can be set as low as 12, to be verified with the bit rate error
   gen_ro_i2c_row : for i in 1 to C_NUM_ROW generate
        ro_row_i2c_inst : ro_i2c_row
        generic map (
                divider     => 24,      
                C_MAX_EVT   => 512            
        )
        port map (
                fclk         => fclk,
                clk_strobe_5 => clk_strobe_5,
                rst          => rst,
                
                i2c_sda_i       => sda_i(i),
                i2c_sda_o       => sda_o(i),
                i2c_sda_oen     => sda_oen(i),
                i2c_scl_i       => scl_i(i),
                i2c_scl_o       => scl_o(i),
                i2c_scl_oen     => scl_oen(i),
                
                ro_rx_start => ro_rx_start(i),
                ro_rx_end   => ro_rx_end(i),
                
                ro_tx_start => ro_tx_start(i),
                ro_tx_end   => ro_tx_end(i),
                
                asic_ix	 	=> ro_asic_index(i),
                addr_pt 	=> ro_addr_pt(i),

                
                ro_rd_addr => ro_rd_addr,
                ro_mem_out => ro_dout(i),
                
                ro_done => ro_done
        );

        inst_sda_iobuf : IOBUF
--        generic map(
--                DRIVE   => 12,
--                IOSTANDARD => "LVCMOS33",
--                SLEW    => "FAST")
        port map(
                O       =>      sda_i(i),
                IO      =>      sda(i),
                I       =>      sda_o(i),
                T       =>      sda_oen(i)
        );

        inst_scl_iobuf : IOBUF
--        generic map(
--                DRIVE   => 12,
--                IOSTANDARD => "LVCMOS33",
--                SLEW    => "FAST")
        port map(
                O       =>      scl_i(i),
                IO      =>      scl(i),
                I       =>      scl_o(i),
                T       =>      scl_oen(i)
        );

   end generate gen_ro_i2c_row;

end architecture rtl;
