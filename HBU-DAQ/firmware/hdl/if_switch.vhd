----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:50:44 08/22/2014 
-- Design Name: 
-- Module Name:    if_switch - rtl 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity if_switch is
   port (
      -- clk          : in std_logic;      -- slow 5MHz clock
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;

      cmd_idle : in std_logic;          --dif cmd_rx is in idle

      usb_if_enable       : out std_logic;  --enable for usb interface
      hdmi_if_enable      : out std_logic;  --enable for hdmi interface
      --usb signals
      usb_strobe          : in  std_logic;
      usb_rxfn            : in  std_logic;  -- 0 = data available on USB
      usb_rx_data         : in  std_logic_vector(7 downto 0);
      usb_wr_req          : out std_logic;
      usb_tx_data         : out std_logic_vector(7 downto 0);
      usb_writing         : in  std_logic;
      usb_txen            : in  std_logic;
      --hdmi signals
      hdmi_strobe         : in  std_logic;
      hdmi_rx_data        : in  std_logic_vector(7 downto 0);
      hdmi_wr_req         : out std_logic;
      hdmi_writing        : in  std_logic;
      hdmi_tx_data        : out std_logic_vector (7 downto 0);
      hdmi_txen           : in  std_logic;
      hdmi_data_available : in  std_logic;
      --single interface to the dif
      strobe              : out std_logic;
      rx_data             : out std_logic_vector(7 downto 0);
      txen                : out std_logic;
      wr_req              : in  std_logic;
      writing             : out std_logic;
      tx_data             : in  std_logic_vector(7 downto 0)
      );
end if_switch;

architecture rtl of if_switch is
   type states is (usb, s01, hdmi, s11);
   signal state, next_state : states;
   signal if_selector       : std_logic;             --'0' = USB, '1' = HDMI
   signal global_en         : std_logic;             --enables the selector to drive the *_if_enables signals
   signal hdmi_da_reg1      : std_logic;             -- data available delayed signal
   signal hdmi_da_reg2      : std_logic;             -- data available delayed signal
   signal cnt               : unsigned(3 downto 0);  -- counter for delaying the switch decision by few clock cycles because of data_available registers
   signal cnt_inc, cnt_clr  : std_logic;

--  signal usb_if_enable_int  : std_logic;
--  signal hdmi_if_enable_int : std_logic;
   
begin
   usb_if_enable  <= (not if_selector) and global_en;
   hdmi_if_enable <= if_selector and global_en;

   usb_tx_data  <= tx_data;
   hdmi_tx_data <= tx_data;
   
   muxes_proc : process(
      if_selector,
      usb_strobe,
      usb_rx_data,
      usb_txen,
      usb_writing,
      wr_req,
      hdmi_strobe,
      hdmi_rx_data,
      hdmi_txen,
      hdmi_writing
      )
   begin
      if (if_selector = '0') then
         strobe      <= usb_strobe;
         rx_data     <= usb_rx_data;
         txen        <= usb_txen;
         writing     <= usb_writing;
         usb_wr_req  <= wr_req;
         hdmi_wr_req <= '0';
      else
         strobe      <= hdmi_strobe;
         rx_data     <= hdmi_rx_data;
         txen        <= hdmi_txen;
         writing     <= hdmi_writing;
         usb_wr_req  <= '0';
         hdmi_wr_req <= wr_req;
      end if;
   end process;

   ---------------------------------------------------------------------------
   clk_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            state <= usb;
         elsif (clk_strobe_5 = '1') then
            state        <= next_state;
            hdmi_da_reg2 <= hdmi_da_reg1;
            hdmi_da_reg1 <= hdmi_data_available;
            if (cnt_clr = '1') then
               cnt <= (others => '0');
            else
               if (cnt_inc = '1') then
                  cnt <= cnt + 1;
               end if;  --cnt_inc
            end if;  --cnt_clr
         end if;  --strobe
      end if;  --clk
   end process;

   next_state_proc : process(state, cmd_idle, usb_rxfn, hdmi_da_reg2, cnt, usb_strobe, hdmi_strobe)
   begin
      case state is
         when usb =>
            if (cmd_idle = '1') and (usb_rxfn = '1') and (hdmi_da_reg2 = '1') and (usb_strobe = '0') then
               next_state <= s01;
            else
               next_state <= usb;
            end if;
         when s01 =>
            if (cmd_idle = '1') and (usb_rxfn = '1') and (hdmi_da_reg2 = '1') and (usb_strobe = '0') then
               if (cnt = (cnt'range => '1')) then  --counter reached top value
                  next_state <= hdmi;              --
               else
                  next_state <= s01;
               end if;
            else
               next_state <= usb;
            end if;
         when hdmi =>
            if (cmd_idle = '1') and (hdmi_da_reg2 = '0') and (usb_rxfn = '0') and (hdmi_strobe = '0') then
               next_state <= s11;
            else
               next_state <= hdmi;
            end if;
         when s11 =>
            if (cmd_idle = '1') and (hdmi_da_reg2 = '0') and (usb_rxfn = '0') and (hdmi_strobe = '0') then
               if (cnt = (cnt'range => '1')) then  --counter reached top value
                  next_state <= usb;               --
               else
                  next_state <= s11;
               end if;
            else
               next_state <= hdmi;
            end if;
         when others =>
            next_state <= usb;
      end case;
   end process;

   output_proc : process(state)
   begin
      cnt_inc     <= '0';
      cnt_clr     <= '0';
      if_selector <= '0';               --USB default
      global_en   <= '1';
--    usb_if_enable_int <= '0';
--    hdmi_if_enable_int <= '0';

      case state is
         when usb =>
            cnt_clr     <= '1';
            if_selector <= '0';
            global_en   <= '1';
         when s01 =>
            cnt_inc     <= '1';
            if_selector <= '0';
            global_en   <= '0';
         when hdmi =>
            cnt_clr     <= '1';
            if_selector <= '1';
            global_en   <= '1';
         when s11 =>
            cnt_inc     <= '1';
            if_selector <= '1';
            global_en   <= '0';
         when others =>
      end case;
   end process;

end rtl;

