
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.adif2_types.all;

entity cal_ctrl is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;

      cmd_ready : in std_logic;
      cmd_end   : in std_logic;
      cmd12     : in std_logic_vector (15 downto 0);
      cmd34     : in std_logic_vector (15 downto 0);
      cmd56     : in std_logic_vector (15 downto 0);
      cmd78     : in std_logic_vector (15 downto 0);

      cal_dbg : out std_logic;

      miso : in  std_logic;
      mosi : out std_logic;
      mclk : out std_logic;
      ssel : out std_logic;

      dif_id : in std_logic_vector (15 downto 0);

      usb_txen : in std_logic;
      writing  : in std_logic;

      cal_boot     : out std_logic;
      cal_wr_req   : out std_logic;
      cal_data_out : out std_logic_vector (7 downto 0);

      cal_cmd_done : out std_logic
      );
end cal_ctrl;

architecture rtl of cal_ctrl is

   type STATE_TYPE is (s0, s01, s10, s11, s12,
                       s20, s21, s22, s23, s24, s25, s26, s27, s28, s29,
                       s30, s31, s32, s40, s41,
                       s50, s51, s52, s53, s54, s55, s56, s57, s58, s59, s5A,
                       s61, s62, s63, s64, s65, s66, s67, s68,
                       s71, s72, s73, s75, s76, s77,
                       s81, s82, s83, s85, s86, s87,
                       s90, s91, s92, s93);

   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;

   signal cal_cmd_end : std_logic;
   signal cal_data    : std_logic_vector (7 downto 0);
   -- SIGNAL cal_data_end :  std_logic;

   signal pheader   : HEADER;
   signal cali_data : CALIB_DATA;

   signal head_index : integer range 0 to 15;
   signal data_index : integer range 0 to 3;
   signal byte_index : unsigned(2 downto 0);
   signal bit_index  : unsigned(2 downto 0);
   signal count      : unsigned(15 downto 0);

begin

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk)then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --ckl
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (bit_index, cal_cmd_end, cmd12, cmd34, cmd_end, cmd_ready, count, current_state, data_index, head_index, usb_txen, writing)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            -- wait for command
            if cmd_ready = '1' then
               next_state <= s01;
            else
               next_state <= s0;
            end if;
         when s01 =>
--      IF cmd12(15 DOWNTO 4) = x"CC7" AND cmd34 = x"0002" THEN
            if cmd12(15 downto 4) = x"007" and cmd34 = x"0001" then
               next_state <= s10;
            else
               next_state <= s0;        -- not cal command
            end if;


         when s10 =>                    -- sending command to calib board
            next_state <= s11;
         when s11 =>
            next_state <= s12;
         when s12 =>
            if cal_cmd_end = '1' then
               next_state <= s30;
            else
               next_state <= s20;
            end if;

         when s20 =>                    -- sending command to calib board
            if count < x"0024" then
               next_state <= s20;
            else
               next_state <= s21;
            end if;
         when s21 =>
            next_state <= s22;
         when s22 =>
            next_state <= s23;
         when s23 =>
            next_state <= s24;
         when s24 =>
            if bit_index = "111" then
               next_state <= s26;
            else
               next_state <= s25;
            end if;
         when s25 =>
            next_state <= s22;
         when s26 =>
            next_state <= s27;
         when s27 =>
            if count < x"0024" then
               next_state <= s27;
            else
               next_state <= s28;
            end if;
         when s28 =>
            next_state <= s29;
         when s29 =>
            if count < x"00700" then
               next_state <= s28;
            else
               next_state <= s10;
            end if;

         when s30 =>
            next_state <= s31;
         when s31 =>
            next_state <= s32;
         when s32 =>
            if count < x"0100" then
               next_state <= s31;
            else
               next_state <= s40;
            end if;

         when s40 =>                    -- receiving data bytes from calib board
            next_state <= s41;
         when s41 =>
            --IF data_index = "011" THEN
            if data_index < C_NUM_CALIB_BYTES then
               next_state <= s50;
            else
               next_state <= s61;
            end if;

         when s50 =>
            if count < x"0024" then
               next_state <= s50;
            else
               next_state <= s51;
            end if;
         when s51 =>
            next_state <= s52;
         when s52 =>
            next_state <= s53;
         when s53 =>
            next_state <= s54;
         when s54 =>
            if bit_index = "111" then
               next_state <= s56;
            else
               next_state <= s55;
            end if;
         when s55 =>
            next_state <= s52;
         when s56 =>
            next_state <= s57;
         when s57 =>
            next_state <= s58;
         when s58 =>
            next_state <= s59;
         when s59 =>
            if count < x"0024" then
               next_state <= s59;
            else
               next_state <= s5A;
            end if;
         when s5A =>
            if count < x"0700" then
               next_state <= s5A;
            else
               next_state <= s40;
            end if;

         when s61 =>
            next_state <= s62;
         when s62 =>
            next_state <= s63;
         when s63 =>
            next_state <= s64;
         when s64 =>
            next_state <= s65;
         when s65 =>
            if writing = '1' then
               next_state <= s66;
            elsif count > x"0005" then
               next_state <= s66;
            else
               next_state <= s65;
            end if;
         when s66 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= s67;
            else
               next_state <= s66;
            end if;
         when s67 =>
            next_state <= s68;
         when s68 =>
            if head_index < C_NUM_WORD_HEAD then
               next_state <= s62;
            else
               next_state <= s71;
            end if;

         -- DATA
         when S71 =>
            next_state <= S72;
         when S72 =>
            if writing = '1' then
               next_state <= S73;
            else
               next_state <= S72;
            end if;
         when S73 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S75;
            else
               next_state <= S73;
            end if;

         when S75 =>
            next_state <= S76;
         when S76 =>
            if writing = '1' then
               next_state <= S77;
            else
               next_state <= S76;
            end if;
         when S77 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S81;
            else
               next_state <= S77;
            end if;

         -- CRC
         when S81 =>
            next_state <= S82;
         when S82 =>
            if writing = '1' then
               next_state <= S83;
            else
               next_state <= S82;
            end if;
         when S83 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S85;
            else
               next_state <= S83;
            end if;

         when S85 =>
            next_state <= S86;
         when S86 =>
            if writing = '1' then
               next_state <= S87;
            else
               next_state <= S86;
            end if;
         when S87 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S90;
            else
               next_state <= S87;
            end if;

         when s90 =>
            next_state <= s91;

         when s91 =>
            next_state <= s92;
         when s92 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s93;
            else
               next_state <= s92;
            end if;
         when s93 =>
            next_state <= s0;

      -- WHEN OTHERS =>
      -- next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk, rst)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            cal_cmd_done <= '0';
            ssel         <= '1';
            mclk         <= '0';
            mosi         <= '0';
            cal_wr_req   <= '0';
            cal_cmd_end  <= '0';
--      cal_data_end <= '0';
            cal_data     <= (others => '0');
            cal_data_out <= (others => '0');
            byte_index   <= (others => '0');
            data_index   <= 0;
            bit_index    <= "000";
            count        <= (others => '0');
            cal_boot     <= '1';
            cal_dbg      <= '0';

            pheader(0)  <= x"43";
            pheader(1)  <= x"41";
            pheader(2)  <= x"00";
            pheader(3)  <= x"00";
            pheader(4)  <= x"00";
            pheader(5)  <= x"00";
            pheader(6)  <= x"00";
            pheader(7)  <= x"00";
            pheader(8)  <= x"00";
            pheader(9)  <= x"00";
            pheader(10) <= x"00";
            pheader(11) <= x"00";
            pheader(12) <= x"00";
            pheader(13) <= x"00";
            pheader(14) <= x"EE";

         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  cal_cmd_done <= '0';
                  ssel         <= '1';
                  mclk         <= '0';
                  mosi         <= '0';
                  cal_wr_req   <= '0';
                  cal_cmd_end  <= '0';
--        cal_data_end <= '0';
                  cal_data     <= (others => '0');
                  cal_data_out <= (others => '0');
                  byte_index   <= (others => '0');
                  data_index   <= 0;
                  bit_index    <= "000";
                  count        <= (others => '0');
                  cal_boot     <= '0';
                  cal_dbg      <= '0';

                  pheader(0)  <= x"43";
                  pheader(1)  <= x"41";
                  pheader(2)  <= x"00";
                  pheader(3)  <= x"00";
                  pheader(4)  <= x"00";
                  pheader(5)  <= x"00";
                  pheader(6)  <= x"00";
                  pheader(7)  <= x"00";
                  pheader(8)  <= x"00";
                  pheader(9)  <= x"00";
                  pheader(10) <= x"00";
                  pheader(11) <= x"00";
                  pheader(12) <= x"00";
                  pheader(13) <= x"00";
                  pheader(14) <= x"EE";

               when s01 =>

               when s10 =>
                  ssel  <= '1';
                  mclk  <= '0';
                  count <= (others => '0');
               when s11 =>
                  if byte_index = "000" then
                     cal_data <= cmd56(15 downto 8);
                  elsif byte_index = "001" then
                     cal_data <= cmd78(15 downto 8);
                  elsif byte_index = "010" then
                     cal_data <= cmd78(7 downto 0);
                  else
                     cal_cmd_end <= '1';
                  end if;
                  bit_index <= "000";
               when s12 =>
                  mclk    <= '0';
                  cal_dbg <= '1';

               when s20 =>
                  mclk  <= '0';
                  ssel  <= '0';
                  count <= count + 1;
               when s21 =>
                  bit_index <= "000";
                  count     <= (others => '0');
               when s22 =>
                  mclk     <= '0';
--        mosi <= cal_data(0);
--        cal_data <= '0' & cal_data(7 DOWNTO 1);
                  mosi     <= cal_data(7);
                  cal_data <= cal_data(6 downto 0) & '0';
               when s23 =>
               when s24 =>
                  mclk <= '1';
               when s25 =>
                  bit_index <= bit_index + 1;
               when s26 =>
                  byte_index <= byte_index + 1;
               when s27 =>
                  mosi  <= '0';
                  mclk  <= '0';
                  count <= count + 1;
               when s28 =>
                  ssel  <= '1';
                  count <= count + 1;
               when s29 =>

               when s30 =>
                  count <= (others => '0');
               when s31 =>
                  ssel  <= '1';
                  count <= count + 1;
               when s32 =>


               when s40 =>
                  ssel       <= '1';
                  mclk       <= '0';
                  mosi       <= '0';
                  bit_index  <= "000";
                  count      <= (others => '0');
                  cal_wr_req <= '0';
               when s41 =>

               when s50 =>
                  ssel  <= '0';
                  count <= count + 1;
               when s51 =>
                  count <= (others => '0');
               when s52 =>
                  mclk <= '0';
               when s53 =>
               when s54 =>
                  mclk      <= '1';
                  cal_data  <= cal_data(6 downto 0) & miso;
                  bit_index <= bit_index + 1;
               when s55 =>

               when s56 =>
                  cal_data_out          <= cal_data;
                  cali_data(data_index) <= cal_data;

--        cal_wr_req <= '1';
               when s57 =>
                  mclk       <= '0';
                  data_index <= data_index + 1;
               when s58 =>
--        cal_wr_req <= '0';
               when s59 =>
                  count <= count + 1;
               when s5A =>
                  ssel  <= '1';
                  count <= count + 1;

               when s61 =>
                  head_index  <= 0;
                  pheader(3)  <= pheader(3) + 1;
                  pheader(4)  <= unsigned(cmd12(15 downto 8));
                  pheader(5)  <= unsigned(cmd12(7 downto 0));
                  pheader(7)  <= x"05";
                  pheader(8)  <= unsigned(cmd56(15 downto 8));
                  pheader(9)  <= unsigned(cmd56(7 downto 0));
                  pheader(10) <= x"E0";
                  pheader(11) <= pheader(11) + 1;
                  pheader(12) <= unsigned(dif_id(15 downto 8));
                  pheader(13) <= unsigned(dif_id(7 downto 0));

               when s62 =>
                  cal_dbg <= '1';
                  
               when s63 =>
                  cal_data_out <= std_logic_vector(pheader(head_index));

               when s64 =>
                  cal_wr_req <= '1';
                  count      <= (others => '0');
               when s65 =>
                  cal_wr_req <= '0';
                  count      <= count + 1;
                  cal_dbg    <= '0';
               when s66 =>
                  cal_dbg <= '0';

               when s67 =>
                  head_index <= head_index + 1;
                  cal_dbg    <= '0';

               when s68 =>

               when S71 =>
                  cal_wr_req   <= '1';
                  --ack_data <= P_I_CMD_REC.DATA(15 downto 8);
                  cal_data_out <= cali_data(1);
                  cal_dbg      <= '1';
               when S72 =>
                  cal_wr_req <= '0';
               when S73 =>
                  cal_wr_req <= '0';
                  cal_dbg    <= '0';
               when S75 =>
                  cal_wr_req   <= '1';
                  --ack_data   <= P_I_CMD_REC.DATA(7 downto 0);
                  cal_data_out <= cali_data(2);
                  cal_dbg      <= '1';
               when S76 =>
                  cal_wr_req <= '0';
               when S77 =>
                  cal_wr_req <= '0';
                  cal_dbg    <= '0';

               when S81 =>
                  cal_wr_req   <= '1';
                  --cal_data_out   <= P_I_CMD_REC.CRC(15 downto 8);
                  cal_data_out <= x"AB";
                  cal_dbg      <= '1';
               when S82 =>
                  cal_wr_req <= '0';
               when S83 =>
                  cal_wr_req <= '0';
                  cal_dbg    <= '0';
               when S85 =>
                  cal_wr_req   <= '1';
                  --cal_data_out   <= P_I_CMD_REC.CRC(7 downto 0);
                  cal_data_out <= x"AB";
                  cal_dbg      <= '1';
               when S86 =>
                  cal_wr_req <= '0';
               when S87 =>
                  cal_wr_req <= '0';
                  cal_dbg    <= '0';

               when s90 =>
                  mclk       <= '0';
                  ssel       <= '1';
                  cal_wr_req <= '0';

               when s91 =>
                  ssel         <= '1';
                  mclk         <= '0';
                  mosi         <= '0';
                  cal_wr_req   <= '0';
                  cal_cmd_done <= '1';
               when s92 =>
               when s93 =>
                  cal_cmd_done <= '0';

            --WHEN OTHERS =>
            -- NULL;
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;
   
end architecture rtl;
