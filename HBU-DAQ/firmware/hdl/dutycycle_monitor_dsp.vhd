-------------------------------------------------------------------------------
-- Title      : dutycycle_monitor_dsp.vhd
-- Project    : 
-------------------------------------------------------------------------------
-- File       : dutycycle_monitor_dsp.vhd
-- Author     : Jiri Kvasnicka (jiri.kvasnicka@desy.de), (kvas@fzu.cz)
-- Company    : DESY / Institute of Physics ASCR
-- Created    : 2018-11-07
-- Last update: 2018-11-07
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Checks, whether the duty cycle is not violated. It is designed to
-- check 1:N duty cycle, but can do also M:N duty cycle.
--
-- Operation principle: internal 48-bit counter adds the "cost" values and accumulates
-- them internally. Once the accumulated value reaches "cost_limit" value,
-- "reached_limit" signal is set high (and countinues counting).
-- On idle (mode 0) it adds a cost, which is expect to be a negative number. This
-- wayt it returns to zero. Once it turn a negative number (starts with "11..."),
-- it stops counting down (only allows to count up). "reached_zero" signal
-- is set in this stats.
-- example for 1:3 (cost_mode0=-1, cost_mode1=3)
--
--        +3__-1
--         |  |__-1
--  - - - -|- - -|-_-1- - - - - - - - cost_limit
--     +3__|        |__-1
--      |              |__-1
--      |                 |__-1
--  +3__|                    |__-1
--   |                          |__-1
--   |                             |__-1
--___|                                |_____________counter (idle at -1)
--m0 mode1       mode0                      (still mode 0) 
--____                                  ______________
--    \________________________________/   (reached_zero)
--           _____
--__________/     \________________________(reached_limit)         
--
-------------------------------------------------------------------------------
-- Copyright (c) 2018 DESY / Institute of Physics ASCR
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-11-07  1.0      kvas    Created
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;

entity dutycycle_monitor_dsp is
   port (clk           : in  std_logic;             -- system clock
         ce            : in  std_logic;             -- count enable: designed for reducing the speed of the counter (from 40 MHz to 5 MHz)
         srst          : in  std_logic;             -- synchronous reset: reset the counter value
         cost_mode0    : in  signed(15 downto 0);   -- must be a negative number. Use TO_SIGNED(-1,16) function, which converts -1 to 16-bit signed integer. Internally, 16th bit is forced to 1
         cost_mode1    : in  signed(15 downto 0);   -- "cost" of this mode. This value will be added to the accumulator when this mode is selected
         cost_mode2    : in  signed(15 downto 0);   -- "cost" of this mode. This value will be added to the accumulator when this mode is selected
         cost_mode3    : in  signed(15 downto 0);   -- "cost" of this mode. This value will be added to the accumulator when this mode is selected
         cost_limit    : in  signed(47 downto 0);   -- counter threshold after which "reached_limit" is raised.
         mode_select   : in  unsigned(1 downto 0);  -- selects which mode to use (and therefore which cost to add to the accumulator)
         reached_limit : out std_logic;             -- the counter is over the cost_limit
         reached_zero  : out std_logic;             -- the counter is at the bottom (and not counting down anymore)
         counter       : out signed(47 downto 0)    -- output of the counter
         );
end dutycycle_monitor_dsp;

architecture RTL of dutycycle_monitor_dsp is
   --signal counter_int   : signed(47 downto 0);            --internal counter value
   signal cost_selected : signed(15 downto 0);

   --first DSP: count and check for return to zero
   signal PCOUT         : std_logic_vector(47 downto 0);  -- 48-bit output: Cascade output
   signal PATTERNDETECT : std_logic;                      -- 1-bit output: Pattern detect output
   signal P             : std_logic_vector(47 downto 0);  -- 48-bit output: Primary data output
   signal C             : std_logic_vector(47 downto 0);  -- 48-bit input: C data input

   --second dsp: check the high limit
   signal PATTERNDETECT2 : std_logic;                      -- 1-bit output: Pattern detect output
   signal P2             : std_logic_vector(47 downto 0);  -- 48-bit output: Primary data output
   signal C2             : std_logic_vector(47 downto 0);  -- 48-bit input: C data input

begin
   --selects cost according to the mode
   cost_selected_process : process(PATTERNDETECT, cost_mode0, cost_mode1, cost_mode2, cost_mode3, mode_select)
      variable selected         : signed(15 downto 0);
      variable cost_mode0_fixed : signed(15 downto 0);
      variable cost_mode1_fixed : signed(15 downto 0);
      variable cost_mode2_fixed : signed(15 downto 0);
      variable cost_mode3_fixed : signed(15 downto 0);
   begin
      cost_mode3_fixed := cost_mode3;
      cost_mode2_fixed := cost_mode2;
      cost_mode1_fixed := cost_mode1;
      cost_mode0_fixed := cost_mode0;

      cost_mode3_fixed(cost_mode3_fixed'high) := '0';  --ensure positive
      cost_mode2_fixed(cost_mode3_fixed'high) := '0';  --ensure positive
      cost_mode1_fixed(cost_mode3_fixed'high) := '0';  --ensure positive
      cost_mode0_fixed(cost_mode3_fixed'high) := '1';  --ensure negative      

      case TO_INTEGER(mode_select) is
         when 3      => selected := cost_mode3_fixed;
         when 2      => selected := cost_mode2_fixed;
         when 1      => selected := cost_mode1_fixed;
         when others => selected := cost_mode0_fixed;
      end case;
      --if (TO_INTEGER(mode_select) = 0)
      if (selected(selected'high) = '1')               --counting down
                     and ((PATTERNDETECT = '1')) then  --counter already reached the bottom
         selected := TO_SIGNED(0, selected'length);
      end if;
      cost_selected <= selected;
   end process;

   C(cost_selected'high downto 0)            <= std_logic_vector(cost_selected);
   C(C'high downto (cost_selected'high + 1)) <= (others => cost_selected(cost_selected'high));

   C2(C2'high-1 downto 0) <= std_logic_vector(cost_limit(cost_limit'high -1 downto 0));  --use bits 46..0
   C2(C2'high)            <= '0';                                                        --ensure the cost_limit is a positive number by forcing bit 47 to '0'

   reached_zero <= PATTERNDETECT;       --reached when two most significant bits of the counter are '1' (= negative, but will tollerate slight overflow over 2^47)

   --reached_limit_process : process(clk)
   --begin
   --   if rising_edge(clk) then
   --   end if;  --clk
   --end process;
   reached_limit <= not PATTERNDETECT2;  --patterndetect2 checks for negative result of the (counter - limit). When the result is negative, limit is not reached

   counter <= signed(P);

   main_counter_inst : DSP48E1
      generic map (
         -- Feature Control Attributes: Data Path Selection
         A_INPUT            => "DIRECT",         -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
         B_INPUT            => "DIRECT",         -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
         USE_DPORT          => false,            -- Select D port usage (TRUE or FALSE)
         USE_MULT           => "NONE",           -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
         USE_SIMD           => "ONE48",          -- SIMD selection ("ONE48", "TWO24", "FOUR12")
         -- Pattern Detector Attributes: Pattern Detection Configuration
         AUTORESET_PATDET   => "NO_RESET",       -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
         --pattern and mask for detecting non-negative numbers
         MASK               => X"3FFFFFFFFFFF",  -- 48-bit mask value for pattern detect (1=ignore)
         PATTERN            => X"FFFFFFFFFFFF",  -- 48-bit pattern match for pattern detect
         SEL_MASK           => "MASK",           -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
         SEL_PATTERN        => "PATTERN",        -- Select pattern value ("PATTERN" or "C")
         USE_PATTERN_DETECT => "PATDET",         -- Enable pattern detect ("PATDET" or "NO_PATDET")
         -- Register Control Attributes: Pipeline Register Configuration
         ACASCREG           => 0,                -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
         ADREG              => 0,                -- Number of pipeline stages for pre-adder (0 or 1)
         ALUMODEREG         => 0,                -- Number of pipeline stages for ALUMODE (0 or 1)
         AREG               => 0,                -- Number of pipeline stages for A (0, 1 or 2)
         BCASCREG           => 0,                -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
         BREG               => 0,                -- Number of pipeline stages for B (0, 1 or 2)
         CARRYINREG         => 0,                -- Number of pipeline stages for CARRYIN (0 or 1)
         CARRYINSELREG      => 0,                -- Number of pipeline stages for CARRYINSEL (0 or 1)
         CREG               => 1,                -- Number of pipeline stages for C (0 or 1)
         DREG               => 0,                -- Number of pipeline stages for D (0 or 1)
         INMODEREG          => 0,                -- Number of pipeline stages for INMODE (0 or 1)
         MREG               => 0,                -- Number of multiplier pipeline stages (0 or 1)
         OPMODEREG          => 0,                -- Number of pipeline stages for OPMODE (0 or 1)
         PREG               => 1                 -- Number of pipeline stages for P (0 or 1)
         )
      port map (
         -- Cascade: 30-bit (each) output: Cascade Ports
         ACOUT          => open,                 -- 30-bit output: A port cascade output
         BCOUT          => open,                 -- 18-bit output: B port cascade output
         CARRYCASCOUT   => open,                 -- 1-bit output: Cascade carry output
         MULTSIGNOUT    => open,                 -- 1-bit output: Multiplier sign cascade output
         PCOUT          => PCOUT,                -- 48-bit output: Cascade output
         -- Control: 1-bit (each) output: Control Inputs/Status Bits
         OVERFLOW       => open,                 -- 1-bit output: Overflow in add/acc output
         PATTERNBDETECT => open,                 -- 1-bit output: Pattern bar detect output
         PATTERNDETECT  => PATTERNDETECT,        -- 1-bit output: Pattern detect output
         UNDERFLOW      => open,                 -- 1-bit output: Underflow in add/acc output
         -- Data: 4-bit (each) output: Data Ports
         CARRYOUT       => open,                 -- 4-bit output: Carry output
         P              => P,                    -- 48-bit output: Primary data output
         -- Cascade: 30-bit (each) input: Cascade Ports
         ACIN           => (others => '0'),      -- 30-bit input: A cascade data input
         BCIN           => (others => '0'),      -- 18-bit input: B cascade input
         CARRYCASCIN    => '0',                  -- 1-bit input: Cascade carry input
         MULTSIGNIN     => '0',                  -- 1-bit input: Multiplier sign input
         PCIN           => (others => '0'),      -- 48-bit input: P cascade input
         -- Control: 4-bit (each) input: Control Inputs/Status Bits
         ALUMODE        => "0000",               -- 4-bit input: ALU control input       "0000" = Z+X+Y+CIN
         CARRYINSEL     => "000",                -- 3-bit input: Carry select input     "000" = fabric CIN
         CLK            => clk,                  -- 1-bit input: Clock input
         INMODE         => "0" & "0010",         -- 5-bit input: INMODE control input output register     AMULT=0, BMULT=B2
         OPMODE         => "010"&"11"&"00",      -- 7-bit input: Operation mode input         Z=P, Y=C   X=0, 
         -- Data: 30-bit (each) input: Data Ports
         A              => (others => '0'),      -- 30-bit input: A data input
         B              => (others => '0'),      -- 18-bit input: B data input
         C              => C,                    -- 48-bit input: C data input
         CARRYIN        => '0',                  -- 1-bit input: Carry input signal
         D              => (others => '0'),      -- 25-bit input: D data input
         -- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
         CEA1           => '0',                  -- 1-bit input: Clock enable input for 1st stage AREG
         CEA2           => '0',                  -- 1-bit input: Clock enable input for 2nd stage AREG
         CEAD           => '0',                  -- 1-bit input: Clock enable input for ADREG
         CEALUMODE      => '1',                  -- 1-bit input: Clock enable input for ALUMODE
         CEB1           => '0',                  -- 1-bit input: Clock enable input for 1st stage BREG
         CEB2           => '0',                  -- 1-bit input: Clock enable input for 2nd stage BREG
         CEC            => '1',                  -- 1-bit input: Clock enable input for CREG
         CECARRYIN      => '1',                  -- 1-bit input: Clock enable input for CARRYINREG
         CECTRL         => '1',                  -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
         CED            => '0',                  -- 1-bit input: Clock enable input for DREG
         CEINMODE       => '1',                  -- 1-bit input: Clock enable input for INMODEREG
         CEM            => '0',                  -- 1-bit input: Clock enable input for MREG
         CEP            => ce,                   -- 1-bit input: Clock enable input for PREG
         RSTA           => '0',                  -- 1-bit input: Reset input for AREG
         RSTALLCARRYIN  => '0',                  -- 1-bit input: Reset input for CARRYINREG
         RSTALUMODE     => '0',                  -- 1-bit input: Reset input for ALUMODEREG
         RSTB           => '0',                  -- 1-bit input: Reset input for BREG
         RSTC           => '0',                  -- 1-bit input: Reset input for CREG
         RSTCTRL        => '0',                  -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
         RSTD           => '0',                  -- 1-bit input: Reset input for DREG and ADREG
         RSTINMODE      => '0',                  -- 1-bit input: Reset input for INMODEREG
         RSTM           => '0',                  -- 1-bit input: Reset input for MREG
         RSTP           => srst                  -- 1-bit input: Reset input for PREG
         );

   limit_comparator_inst : DSP48E1
      generic map (
         -- Feature Control Attributes: Data Path Selection
         A_INPUT            => "DIRECT",         -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
         B_INPUT            => "DIRECT",         -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
         USE_DPORT          => false,            -- Select D port usage (TRUE or FALSE)
         USE_MULT           => "NONE",           -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
         USE_SIMD           => "ONE48",          -- SIMD selection ("ONE48", "TWO24", "FOUR12")
         -- Pattern Detector Attributes: Pattern Detection Configuration
         AUTORESET_PATDET   => "NO_RESET",       -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
         MASK               => X"3FFFFFFFFFFF",  -- 48-bit mask value for pattern detect (1=ignore)
         PATTERN            => X"F00000000000",  -- 48-bit pattern match for pattern detect
         SEL_MASK           => "MASK",           -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
         SEL_PATTERN        => "PATTERN",        -- Select pattern value ("PATTERN" or "C")
         USE_PATTERN_DETECT => "PATDET",         -- Enable pattern detect ("PATDET" or "NO_PATDET")
         -- Register Control Attributes: Pipeline Register Configuration
         ACASCREG           => 0,                -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
         ADREG              => 0,                -- Number of pipeline stages for pre-adder (0 or 1)
         ALUMODEREG         => 0,                -- Number of pipeline stages for ALUMODE (0 or 1)
         AREG               => 0,                -- Number of pipeline stages for A (0, 1 or 2)
         BCASCREG           => 0,                -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
         BREG               => 0,                -- Number of pipeline stages for B (0, 1 or 2)
         CARRYINREG         => 0,                -- Number of pipeline stages for CARRYIN (0 or 1)
         CARRYINSELREG      => 0,                -- Number of pipeline stages for CARRYINSEL (0 or 1)
         CREG               => 1,                -- Number of pipeline stages for C (0 or 1)
         DREG               => 0,                -- Number of pipeline stages for D (0 or 1)
         INMODEREG          => 0,                -- Number of pipeline stages for INMODE (0 or 1)
         MREG               => 0,                -- Number of multiplier pipeline stages (0 or 1)
         OPMODEREG          => 0,                -- Number of pipeline stages for OPMODE (0 or 1)
         PREG               => 1                 -- Number of pipeline stages for P (0 or 1)
         )
      port map (
         -- Cascade: 30-bit (each) output: Cascade Ports
         ACOUT          => open,                 -- 30-bit output: A port cascade output
         BCOUT          => open,                 -- 18-bit output: B port cascade output
         CARRYCASCOUT   => open,                 -- 1-bit output: Cascade carry output
         MULTSIGNOUT    => open,                 -- 1-bit output: Multiplier sign cascade output
         PCOUT          => open,                 -- 48-bit output: Cascade output
         -- Control: 1-bit (each) output: Control Inputs/Status Bits
         OVERFLOW       => open,                 -- 1-bit output: Overflow in add/acc output
         PATTERNBDETECT => open,                 -- 1-bit output: Pattern bar detect output
         PATTERNDETECT  => PATTERNDETECT2,       -- 1-bit output: Pattern detect output
         UNDERFLOW      => open,                 -- 1-bit output: Underflow in add/acc output
         -- Data: 4-bit (each) output: Data Ports
         CARRYOUT       => open,                 -- 4-bit output: Carry output
         P              => P2,                   -- 48-bit output: Primary data output
         -- Cascade: 30-bit (each) input: Cascade Ports
         ACIN           => (others => '0'),      -- 30-bit input: A cascade data input
         BCIN           => (others => '0'),      -- 18-bit input: B cascade input
         CARRYCASCIN    => '0',                  -- 1-bit input: Cascade carry input
         MULTSIGNIN     => '0',                  -- 1-bit input: Multiplier sign input
         PCIN           => PCOUT,                -- 48-bit input: P cascade input
         -- Control: 4-bit (each) input: Control Inputs/Status Bits
         ALUMODE        => "0011",               -- 4-bit input: ALU control input        "0011" = Z-(X+Y+CIN)
         CARRYINSEL     => "000",                -- 3-bit input: Carry select input       "000" = fabric carryin  
         CLK            => clk,                  -- 1-bit input: Clock input
         INMODE         => "0" & "0010",         -- 5-bit input: INMODE control input
         OPMODE         => "001"&"11"&"00",      -- 7-bit input: Operation mode input         Z=PCIN, Y=C   X=0, 
         -- Data: 30-bit (each) input: Data Ports
         A              => (others => '0'),      -- 30-bit input: A data input
         B              => (others => '0'),      -- 18-bit input: B data input
         C              => C2,                   -- 48-bit input: C data input
         CARRYIN        => '0',                  -- 1-bit input: Carry input signal
         D              => (others => '0'),      -- 25-bit input: D data input
         -- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
         CEA1           => '0',                  -- 1-bit input: Clock enable input for 1st stage AREG
         CEA2           => '0',                  -- 1-bit input: Clock enable input for 2nd stage AREG
         CEAD           => '0',                  -- 1-bit input: Clock enable input for ADREG
         CEALUMODE      => '1',                  -- 1-bit input: Clock enable input for ALUMODE
         CEB1           => '0',                  -- 1-bit input: Clock enable input for 1st stage BREG
         CEB2           => '0',                  -- 1-bit input: Clock enable input for 2nd stage BREG
         CEC            => '1',                  -- 1-bit input: Clock enable input for CREG
         CECARRYIN      => '1',                  -- 1-bit input: Clock enable input for CARRYINREG
         CECTRL         => '1',                  -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
         CED            => '0',                  -- 1-bit input: Clock enable input for DREG
         CEINMODE       => '1',                  -- 1-bit input: Clock enable input for INMODEREG
         CEM            => '0',                  -- 1-bit input: Clock enable input for MREG
         CEP            => ce,                   -- 1-bit input: Clock enable input for PREG
         RSTA           => '0',                  -- 1-bit input: Reset input for AREG
         RSTALLCARRYIN  => '0',                  -- 1-bit input: Reset input for CARRYINREG
         RSTALUMODE     => '0',                  -- 1-bit input: Reset input for ALUMODEREG
         RSTB           => '0',                  -- 1-bit input: Reset input for BREG
         RSTC           => '0',                  -- 1-bit input: Reset input for CREG
         RSTCTRL        => '0',                  -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
         RSTD           => '0',                  -- 1-bit input: Reset input for DREG and ADREG
         RSTINMODE      => '0',                  -- 1-bit input: Reset input for INMODEREG
         RSTM           => '0',                  -- 1-bit input: Reset input for MREG
         RSTP           => srst                  -- 1-bit input: Reset input for PREG
         );
end RTL;
