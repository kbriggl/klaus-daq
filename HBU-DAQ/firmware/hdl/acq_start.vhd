
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.adif2_types.all;

entity acq_start is
   port(
      -- clk          : in std_logic;
      ms_tick      : in std_logic;                     --in clk domain
      fclk         : in std_logic;
      fclk_160     : in std_logic;                     --for synchronization of ram_full. in defined phase with fclk.
      fclk_320     : in std_logic;
      clk_strobe_5 : in std_logic;                     --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      clk_phase    : in std_logic_vector(7 downto 0);  --the acq_clk phase (clock phase within bxid - direct output of the clock counter). counts down to 0. clk_acq is 1 fclk delayd behind.

      rst           : in std_logic;     --reset in clk clock domain. Not used
      frst          : in std_logic;     --reset in fclk clock domain
      acq_start_cmd : in std_logic;
      acq_stop_rx   : in std_logic;
      cmd_end       : in std_logic;

      start_acqs   : out std_logic;
      start_conv_n : out std_logic;

      trig_ext_n : out std_logic;
      trig_ext_p : out std_logic;
      trig_ext_t : out std_logic;

      end_readout : in  std_logic;	-- not used.
      ram_full    : in  std_logic;      --0 means full, 1 means not full
      data_ready  : out std_logic;      -- signal to ro_ctrl. will contribute to busy signal

--    tb_spill       : IN     std_logic;
      validate_hdmi : in std_logic;     -- not synchronized to any clock
      validate_lemo : in std_logic;     -- not synchronized to any clock
--    hold_ext       : OUT    std_logic;

      no_trig_enable : in  std_logic;
      no_trig        : out std_logic;
      val_evt        : out std_logic;

      acq_clk_sel         : out std_logic;
      clk_acq             : in  std_logic;
      clk_acq_rising      : in  std_logic;  -- when 1, the clk_acq will go from 0 -> 1 in the next fclk clock cycle. Inhibit is ignored for this signal. fclk domain
      clk_acq_falling     : in  std_logic;  -- when 1, the clk_acq will go from 1 -> 0 in the next fclk clock cycle. Inhibit is ignored for this signal. fclk domain
      clk_40M_inhibit_acq : out std_logic;  --shut down 40 MHz in order to surpress noise


      -- power control
      pwr_ana_pul : out std_logic;
      pwr_dig_pul : out std_logic;
      pwr_sca_pul : out std_logic;
      pwr_adc_pul : out std_logic;
      pwr_dac_pul : out std_logic;
      trig_cfg    : in  std_logic_vector (15 downto 0);
      dly_reg     : in  std_logic_vector (15 downto 0);
      ro_time     : in  std_logic_vector (15 downto 0);
      timing_cfg  : in  std_logic_vector(15 downto 0);  --timing config. bits 7-0: bxid width (hot used here)
                                                        --bit 8: use faster clock (200 ns) for conversion
                                                        --bit 9: stopping parity (in what bxid parity to stop)
                                                        --bit 10: use 40MHz clock during acq (1=use,0=dont)
                                                        --bits 15..11: not used




      reset_slab_acq_n : out std_logic;  --resetb of POD. Active low. Needed for reset fo POD module

      dc_stop_request : in std_logic;   -- duty cycle is violated, need to stop now

      acq_start_dbg : out std_logic;
      acq_debug     : out std_logic_vector (7 downto 0);

      acq_cmd_done : out std_logic
      );
end acq_start;

architecture struct of acq_start is

   type STATE_TYPE is (s0, s01, s02, s03, s04, s05,
                       s10, s11, s11b, s12, s13, s14, s15, s16, s17, s18, s19,
                       s21,
                       s30, s32, s33, s34, s34a,
                       s40,
                       s90, s91, s92);

   -- Declare current and next state signals
   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;

--  SIGNAL hold_ext_i   :  std_logic;
   signal trig_ext     : std_logic;
   signal trig_ext_all : std_logic;
   signal trig_type    : std_logic;
   signal trig_mode    : std_logic;
   signal trig_dbg     : std_logic;
   signal trig_active  : std_logic;

--  SIGNAL ntrig        :  std_logic;
--  SIGNAL no_trigd     :  std_logic;
--  SIGNAL fntrig       :  std_logic;
   signal val_evt_int : std_logic;

--  SIGNAL cnt_rst      :  std_logic;
--  SIGNAL cnt_en       :  std_logic;
--  SIGNAL fcnt_rst     :  std_logic;
--  SIGNAL fcnt_en      :  std_logic;

   signal trig_set  : std_logic_vector (6 downto 0);
   signal trig_time : std_logic_vector (15 downto 0);

   signal ro_time_int : std_logic_vector (31 downto 0);
   signal start_dly   : std_logic_vector (15 downto 0);

   signal s17_dbg : std_logic;
   signal s29_dbg : std_logic;

   signal count      : unsigned(15 downto 0);  --general purpose counter in the state machine.
   signal trig_count : unsigned(6 downto 0);

   signal timeoutcnt        : unsigned(9 downto 0);  --counts ms_tick for up to 1 s
   signal clk_acq_phase_cnt : unsigned(7 downto 0);  --counting the phase of acq_clk. for rising edge, counter reset to 0. Then counts up (1,2,...). Oposite counting to the clk_phase

--  SIGNAL ecount     : UNSIGNED( 4 DOWNTO 0);

   signal bxid_cnt : unsigned(15 downto 0);  --counts the bxids in the acquisitions

   --cross-clock synchronization registers, that prevents from entering into an undefined state because of timing hazards
   signal val_hdmi_syn    : std_logic_vector(2 downto 0);  --two registers for synchronisation, one (3rd) for edge detection. Edge detection is needed, because the validate signal can span over 2 clock cycles
   signal val_lemo_syn    : std_logic_vector(2 downto 0);  --two registers for synchronisation, one (3rd) for edge detection. Edge detection is needed, because the validate signal can span over 2 clock cycles
   signal val_evt_syn     : std_logic_vector(1 downto 0);  -- two synchronization registers
   signal ram_full_syn    : std_logic_vector(3 downto 0) := (others => '0');  --synchronization of the ram_full in the 320 MHz clock
   -- changed: trig_ext_all does not need a clock synchronization
   --    SIGNAL trig_ext_syn   : std_logic_vector(1 to 2); --two synchronization registers. edge detect is not necessary, wince the trigger ext is well defined within the acq_clk period and does not span across two clock cycles
   -- rising edge is detected synchronously to the fclk and stay high for 1 fclk period
   signal val_hdmi_rising : std_logic;  --rising edge of the validation signal
   signal val_lemo_rising : std_logic;  --rising edge of the validation signal
   signal val_occured     : std_logic;  --retention register for the rising edge of the validate signal. 

   attribute mark_debug                : string;
   attribute mark_debug of timeoutcnt  : signal is "true";
   attribute ASYNC_REG                 : string;
   attribute ASYNC_REG of ram_full_syn : signal is "true";
begin

   fast_sync : process(fclk_160)        --faster synchronization
   begin
      if rising_edge(fclk_160) then
         ram_full_syn <= ram_full_syn(ram_full_syn'high-1 downto 0) & ram_full;
         val_hdmi_syn <= val_hdmi_syn(val_hdmi_syn'high-1 downto 0) & validate_hdmi;
         val_lemo_syn <= val_lemo_syn(val_lemo_syn'high-1 downto 0) & validate_lemo;
      end if;  --clk0
   end process;

   -----------------------------------------------------------------
   sync_reg_proc : process (fclk)       --all signals, that are not synchronized in the fclk domain are synchronized here
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         val_evt_syn <= val_evt_syn(0) & val_evt_int;
--      trig_ext_syn <= trig_ext_all & trig_ext_syn(1);
      end if;  --clk
   end process;

   val_hdmi_rising <= (not val_hdmi_syn(2)) and val_hdmi_syn(1);  --fclk rising edge detector for val_hdmi_syn(2)
   val_lemo_rising <= (not val_lemo_syn(2)) and val_lemo_syn(1);  --fclk rising edge detector for val_lemo_syn(2)


   -----------------------------------------------------------------
   val_occured_process : process(fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (trig_ext_all = '1')                       --internal trigger
                           or (val_hdmi_rising = '1')  --validation from HDMI
                           or (val_lemo_rising = '1')  --validation from lemo (not on DIF2)
         then                                          --if the trigger ext is active or rising edge of the validate is registered
            val_occured <= '1';
         end if;  --sreset and CE of val_occured
         if clk_phase = (clk_phase'range => '0') then
            val_occured <= '0';
         end if;
      end if;  --fclk
   end process;

   no_trig_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         case clk_phase is
            when C_TIM_VAL_RISE_PHASE =>
               no_trig <= (not (val_occured or val_hdmi_rising or val_lemo_rising)) and no_trig_enable;
            when C_TIM_VAL_FALL_PHASE =>
               no_trig <= '0';
            when others => null;        --keep
         end case;
      end if;  --clk
   end process;

   trig_ext_t <= trig_ext_all;
   val_evt    <= val_evt_int;

   acq_debug(0) <= trig_type;
   acq_debug(1) <= trig_ext;
   acq_debug(2) <= ram_full_syn(ram_full_syn'high);
   acq_debug(3) <= val_evt_int;
   acq_debug(4) <= val_occured;
   acq_debug(5) <= trig_dbg;
   acq_debug(6) <= s17_dbg;
   acq_debug(7) <= s29_dbg;

   trig_ext_obufds_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25")
      port map (
         O  => trig_ext_p,
         OB => trig_ext_n,
         I  => trig_ext_all
         );



   -----------------------------------------------------------------
--  trig_mul_proc : PROCESS ( trig_mode, tb_spill, tb_trig, trig_ext)
   trig_mul_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then         --without the clock problems with synthesis
         if (trig_mode = '1') then
--      trig_ext_all <= tb_spill and tb_trig;
--      trig_ext_all <= tb_trig;
            trig_ext_all <= '0';
         else
            trig_ext_all <= trig_ext;
         end if;
      end if;  --clk
   end process trig_mul_proc;


   -- trig_active_proc : process(fclk)                                   --moved from asynchronous design
   --    variable next_trig_ext : std_logic := '0';                      --what will be the next trig_ext value
   -- begin
   --    if rising_edge(fclk) then
   --       if (tcount_rst <= '1') then
   --          trig_active <= '0';
   --       else
   --          next_trig_ext := '0';
   --          if (trig_mode = '0') then
   --             next_trig_ext := trig_ext;
   --          end if;
   --          if ((next_trig_ext = '1') and (trig_ext_all = '0')) then  --rising edge sensitive
   --             trig_active <= '1';
   --          end if;
   --       end if;
   --    end if;  --clk
   -- end process;


   --counting of the clock tics from the rising edge of the acquisition
   --counts in oposite direction then the clk_phase: 0 to {7, 159, ...}
   clk_acq_phase_cnt_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if clk_acq_rising = '1' then
            clk_acq_phase_cnt <= (others => '0');
         else
            clk_acq_phase_cnt <= clk_acq_phase_cnt + 1;
         end if;
      end if;  --clk
   end process;

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if (fclk'event and fclk = '1') then
         if (frst = '1') then
            current_state <= s0;
         else
            -- if (clk_strobe_5 = '1') then
            current_state <= next_state;
         -- end if;
         end if;  --rst
      end if;
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (acq_start_cmd, acq_stop_rx, bxid_cnt, clk_acq_falling, clk_acq_phase_cnt, clk_acq_rising, clk_phase(C_TIM_ACQ_RESET_RELEASE_PHASE'range), cmd_end, count, current_state,
                             dc_stop_request, ram_full_syn, ro_time, start_dly, timeoutcnt, trig_count, trig_set, trig_time, trig_type)
   -----------------------------------------------------------------
   begin
      case current_state is

         when s0 =>
            --wait for usb_command
            if acq_start_cmd = '1' then  -- check if next command is fast acq start
               next_state <= s01;
            else
               next_state <= s0;         -- command is not ready
            end if;

         when s01 =>                             -- start with POD reset
            if count < C_TIM_ACQ_RESET_LOW then  -- number of fclk cycles
               next_state <= s01;
            else
               next_state <= s02;
            end if;

         when s02 =>                                                                                   --start the pwr_*_pul 
            if count < C_TIM_ACQ_RESET_PUL then                                                        --fclk cycles
               next_state <= s02;
            else
               if clk_phase(C_TIM_ACQ_RESET_RELEASE_PHASE'range) = C_TIM_ACQ_RESET_RELEASE_PHASE then  --lets synchronize the release on the defined clock phase
                  next_state <= s03;                                                                   --transition only in defined clk phase
               else
                  next_state <= s02;
               end if;
            end if;

         when s03 =>                              --minimum delay after reset is released
            if count < C_TIM_ACQ_RESET_HIGH then  --slow clock cycles (should be 200 ns)
               next_state <= s03;
            else
               next_state <= s04;
            end if;

         when s04 =>                         --startup delay (long counter, 200 ns steps)
            if count < unsigned(start_dly) then
               next_state <= s04;
            else
               next_state <= s05;
            end if;
            if (dc_stop_request = '1') then  --don't wait if we are requested to stop ASAP
               next_state <= s05;
            end if;

         when s05 =>                    --synchronized jump to s10
            if clk_acq_rising = '1' then
               next_state <= s10;
            else
               next_state <= s05;
            end if;

         when s10 =>                    -- setup trigger type and acquisition timming
            if clk_acq_falling = '1' then
               next_state <= s11;
            else
               next_state <= s10;
            end if;

         when s11 =>                    -- start acquisition aligned with falling edge of clk_acq
            if clk_acq_rising = '1' then
               next_state <= s11b;
            else
               next_state <= s11;
            end if;

         when s11b =>                   --adding 1 clock cycle (clk_acq). aligned with rising edge of clk_acq
            if clk_acq_rising = '1' then
               next_state <= s12;
            else
               next_state <= s11b;
            end if;

         when s12 =>                    -- this should be the BXID 0
            if count < C_TIM_ACQ_TRIGEXT_DELAY then
               next_state <= s12;
            else
               next_state <= s13;
            end if;

         when s13 =>                    -- external trigger
            if count < C_TIM_ACQ_TRIGEXT_LENGTH then
               next_state <= s13;
            else
               next_state <= s14;
            end if;

         when s14 =>                    -- finish external trigger and hold
            next_state <= s15;

         when s15 =>
            if count < unsigned(trig_time) then            -- after trigger delay (delay between triggers)
               next_state <= s15;
            else
               next_state <= s16;
            end if;
            if (ram_full_syn(ram_full_syn'high) = '0')     --don't wait if ram is full
               or (not (trig_count < unsigned(trig_set)))  --do not wait after the last trigger
               or (dc_stop_request = '1')                  --don't wait if we are requested to stop ASAP
            then
               if clk_acq_rising = '1' then                --otherwise 
                  next_state <= s16;
               end if;
            end if;

         when s16 =>
            if (ram_full_syn(ram_full_syn'high) = '0')  --ram is full?
               or (dc_stop_request = '1')
            then
               next_state <= s17;
            else
               if trig_count < unsigned(trig_set) then  -- check number of the external triggers
                  next_state <= s11b;
               else
                  if trig_type = '1' then
                     next_state <= s17;                 -- LAB trigger setting. No autotrigger branch
                  else
                     next_state <= s21;                 -- Test Beam trigger setting (autotrigger)
                  end if;
               end if;
            end if;

         when s17 =>                                             -- wait for the proper stopping condition (bxid parity, phase)
            next_state <= s17;                                   -- wait by default
--            if std_match(C_TIM_ACQ_STOP_BXID_PATT, std_logic_vector(bxid_cnt)) then  -- stop in defined bxid parity only
            if bxid_cnt(0) = timing_cfg(9) then                  -- stop in defined bxid parity only
               if clk_acq_phase_cnt = C_TIM_ACQ_STOP_DELAY then  -- stop on a defined phase only
                  next_state <= s18;
               end if;
            end if;

         when s18 =>                    --stop acquisition
            if clk_acq_rising = '1' then
               next_state <= s19;
            else
               next_state <= s18;
            end if;

         when s19 =>                    --restart 40 MHz clock
            next_state <= s30;          --start conversion

         when s21 =>                                    --autotrigger: just wait for the end condition
            if (ram_full_syn(ram_full_syn'high) = '0')  --memory full
               or (bxid_cnt >= unsigned(ro_time))       --timeout
               or (acq_stop_rx = '1')                   --recieved command to stop
            then
               next_state <= s17;                       --end of acq
            else
               next_state <= s21;                       --keep taking data
            end if;

         when s30 =>                        --wait state for stabilizing ADC in powerpulse mode
            if count < C_TIM_CONV_DLY then  --wait this amount of cycles for stabilization
               next_state <= s30;
            else
               if clk_acq_falling = '1' then
                  next_state <= s32;
               else
                  next_state <= s30;
               end if;
            end if;

         when s32 =>                    --start conversion
            next_state <= s33;

         when s33 =>
            if count < C_TIM_CONV_WIDTH then  --wait 16 clock cycles
               next_state <= s33;
            else
               next_state <= s34;
            end if;

         when s34 =>                                                     --release conversion and wait for end
            if ram_full_syn(ram_full_syn'high) = '1' then                --ram_full is active low
               -- the conversion is finished when the ram_full is deactivated (it is active low),
               next_state <= s34a;
            else
               if (timeoutcnt > to_unsigned(5, timeoutcnt'length)) then  --5 ms timeout for conversion
                  next_state <= s34a;
               else
                  --still converting...
                  next_state <= s34;
               end if;
            end if;

         when s34a =>
            if count < x"0064" then     --fix the TminSatPwr delay for POD
               next_state <= s34a;
            else
               next_state <= s40;
            end if;

         when s40 =>
            next_state <= s90;

         when s90 =>
            next_state <= s91;

         when s91 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s92;
            else
               next_state <= s91;
            end if;

         when s92 =>
            next_state <= s0;
      end case;

   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if frst = '1' then
            timeoutcnt          <= (others => '0');
            clk_40M_inhibit_acq <= '0';
            acq_cmd_done        <= '0';
            start_acqs          <= '0';
            start_conv_n        <= '1';
            trig_dbg            <= '0';
            trig_ext            <= '1';
            val_evt_int         <= '0';
            data_ready          <= '0';
            trig_type           <= '1';
            trig_mode           <= '0';
--      hold_ext_i   <= '1';
            s17_dbg             <= '0';
            s29_dbg             <= '0';
            acq_clk_sel         <= '0';
            pwr_adc_pul         <= '1';
            pwr_ana_pul         <= '1';
            pwr_dac_pul         <= '1';
            pwr_dig_pul         <= '1';
            pwr_sca_pul         <= '1';
            count               <= (others => '0');
            trig_set            <= (others => '0');
            start_dly           <= (others => '0');
            trig_time           <= "0000000000011111";
            trig_count          <= (others => '0');
            acq_start_dbg       <= '0';
            reset_slab_acq_n    <= '1';
         else                             --if frst
            if clk_acq_rising = '1' then  --count bxids by default in every state
               bxid_cnt <= bxid_cnt + 1;
            end if;

            case current_state is
               when s0 =>
                  timeoutcnt          <= (others => '0');
                  clk_40M_inhibit_acq <= '0';
                  acq_cmd_done        <= '0';
                  start_acqs          <= '0';
                  start_conv_n        <= '1';
                  trig_dbg            <= '0';
                  trig_ext            <= '1';
                  val_evt_int         <= '0';
                  data_ready          <= '0';
                  trig_type           <= '1';
                  trig_mode           <= '0';
--        hold_ext_i   <= '1';
                  s17_dbg             <= '0';
                  s29_dbg             <= '0';
                  acq_clk_sel         <= '0';
                  pwr_adc_pul         <= '1';
                  pwr_dac_pul         <= '1';
                  pwr_ana_pul         <= '1';
                  pwr_dig_pul         <= '1';
                  pwr_sca_pul         <= '1';
                  count               <= (others => '0');
                  trig_set            <= (others => '0');
                  start_dly           <= (others => '0');
                  ro_time_int         <= x"00000080";
                  trig_time           <= "0000000000011111";
                  trig_count          <= (others => '0');
                  acq_start_dbg       <= '0';
                  reset_slab_acq_n    <= '1';

               when s01 =>
                  reset_slab_acq_n <= '0';
                  if count < C_TIM_ACQ_RESET_LOW then
                     count <= count+1;
                  else
                     count <= (others => '0');
                  end if;
                  start_dly <= dly_reg;

               when s02 =>              --switch power on
                  start_acqs  <= '1';
                  pwr_ana_pul <= '1';
                  pwr_adc_pul <= '1';   
                  pwr_dac_pul <= '1';
                  pwr_dig_pul <= '1';
                  pwr_sca_pul <= '1';

                  if count < C_TIM_ACQ_RESET_PUL then
                     count <= count+1;
                  else
                     if clk_phase(C_TIM_ACQ_RESET_RELEASE_PHASE'range) = C_TIM_ACQ_RESET_RELEASE_PHASE then  --lets synchronize the release on the defined clock phase
                        count <= (others => '0');
                     end if;
                  end if;

               when s03 =>
                  reset_slab_acq_n <= '1';         --end the slab reset
                  if count < C_TIM_ACQ_RESET_HIGH then
                     if clk_acq_rising = '1' then  --counting slow clocks
                        count <= count+1;
                     end if;
                  else
                     count <= (others => '0');
                  end if;

               when s04 =>
                  acq_clk_sel <= '1';     --switch to slower clock
                  if clk_phase(2 downto 0) = "000" then
                     count <= count + 1;  --count every 200 ns
                  end if;

               when s05 =>
                  count <= (others => '0');

               when s10 =>
                  if timing_cfg(10) = '0' then
                     clk_40M_inhibit_acq <= '1';
                  end if;
                  trig_type   <= trig_cfg(7);
                  trig_set    <= trig_cfg(6 downto 0);
                  ro_time_int <= "000000000" & ro_time & "0000001";
                  if trig_cfg(7) = '1' then  --lab mode
                     trig_time <= "000" & trig_cfg(15 downto 8) & "11111";
                  else                       --TB mode
                     trig_time <= "0000000000011111";
                  end if;
                  count <= (others => '0');

               when s11 =>
                  start_acqs  <= '1';
                  val_evt_int <= '1';
                  bxid_cnt    <= (others => '1');

               when s11b =>
                  --bxid_cnt <= (others => '0'); --no: every trigger would reset the bxid counter

               when s12 =>
                  if count < C_TIM_ACQ_TRIGEXT_DELAY then
                     count <= count + 1;
                  else
                     count <= (count'high downto 1 => '0', 0 => '1');  --init to 1 for correct length counting
                  end if;

               when s13 =>
                  trig_ext <= '0';
                  count    <= count + 1;

               when s14 =>
                  trig_ext   <= '1';
                  trig_count <= trig_count + 1;
                  count      <= (others => '0');

               when s15 =>
                  if clk_phase(2 downto 0) = "000" then  --count every 200 ns
                     count <= count + 1;
                  end if;

               when s16 =>
                  count <= (others => '0');

               when s17 =>
                  if clk_acq_rising = '1' then
                     count <= (others => '0');
                  else
                     count <= count + 1;
                  end if;
                  trig_mode  <= '0';
                  data_ready <= '1';    -- in addition to state s29, the busy is set in labview mode

               when s18 =>
                  count       <= (others => '0');
                  start_acqs  <= '0';
                  val_evt_int <= '0';
                  trig_count  <= (others => '0');
                  if timing_cfg(8) = '1' then
                     acq_clk_sel <= '0';  --switch the slow clock to 5 MHz clock
                  end if;

               when s19 =>
                  count               <= (others => '0');
                  clk_40M_inhibit_acq <= '0';  --restart 40MHz clock

               when s21 =>
                  val_evt_int <= '1';
                  trig_mode   <= '1';
                  --TODO treatment of val_evt_int after ram_full ???

               when s30 =>
                  --pwr_adc_pul   <= '1';
                  if clk_phase(2 downto 0) = "000" then
                     count <= count + 1;
                  end if;

               when s32 =>
                  start_conv_n <= '0';  --activate start conversion
                  count        <= (others => '0');

               when s33 =>
                  if clk_phase(2 downto 0) = "000" then
                     count <= count + 1;
                  end if;
                  timeoutcnt <= (others => '0');

               when s34 =>
                  timeoutcnt   <= timeoutcnt + (timeoutcnt'high downto 1 => '0', 0 => ms_tick);  --count ms ticks
                  start_conv_n <= '1';                                                           --deactivate the start conversion signal
                  count        <= (others                                => '0');

               when s34a =>
                  count <= count+1;

               when s40 =>
                  pwr_adc_pul   <= '1';
                  pwr_dac_pul   <= '1';
                  pwr_sca_pul   <= '1';
                  pwr_ana_pul   <= '1';
                  pwr_dig_pul   <= '1';  --can power off digital, POD module will wake up spiroc automatically 
                  acq_start_dbg <= '0';

               when s90 =>
                  acq_clk_sel  <= '0';
                  acq_cmd_done <= '1';

               when s91 =>

               when s92 =>
                  acq_cmd_done  <= '0';
                  acq_start_dbg <= '1';
            end case;
         end if;
      end if;
   end process output_proc;

end architecture struct;
