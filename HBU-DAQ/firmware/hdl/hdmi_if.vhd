--
-- VHDL Architecture LDA_lib.hdmi_if.rtl
--
-- Created:
--          by - kvas @ DESY
--          at - 15:02:23 24.07.2014
--
-- using Mentor Graphics HDL Designer(TM) 2012.2b (Build 5)
--
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity hdmi_if is
   generic (
      SCALE_FACTOR : positive := 4
      );
   port (
      -- clk          : in std_logic;
      fclk         : in std_logic;
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      srst_5       : in std_logic;
      srst_40      : in std_logic;
      ms_tick      : in std_logic;

      clk_syn       : out std_logic;    -- synchronization pulse, registered by fclk
      cmd_idle      : in  std_logic;    --no command being processed
      roc_increment : out std_logic;

      debug : out std_logic_vector(3 downto 0);

      wr_req : in std_logic;
      turbo  : in std_logic;            --faster mode of sending data out

      hdmi_txen : out std_logic;
      writing   : out std_logic;
      tx_data   : in  std_logic_vector (7 downto 0);

      rx_data     : out std_logic_vector (7 downto 0);
      hdmi_strobe : out std_logic;
      rx_error    : out std_logic;      --error in the receiver (missing proper stop bits)

      tx_pin_p : out std_logic;
      tx_pin_n : out std_logic;

      rx_pin_p : in std_logic;
      rx_pin_n : in std_logic;

      if_enable      : in  std_logic;
      data_available : out std_logic
      );
end entity hdmi_if;

--
architecture rtl of hdmi_if is
   signal tx_pin           : std_logic;
   signal rx_pin           : std_logic;
   signal hdmi_strobe_int  : std_logic;
--   signal rx_pin_reg1, rx_pin_reg2 : std_logic; --two registers
   signal flow_d2l_suspend : std_logic;

   component dif2lda is
      generic (
         SCALE_FACTOR : positive);
      port (
         fclk             : in  std_logic;
         clk_strobe_5     : in  std_logic;  --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
         srst_5           : in  std_logic;
         srst_40          : in  std_logic;
         tx_pin           : out std_logic;
         wr_req           : in  std_logic;
         tx_data          : in  std_logic_vector (7 downto 0);
         writing          : out std_logic;
         txen             : out std_logic;
         turbo            : in  std_logic;
         flow_d2l_suspend : in  std_logic;
         debug1           : out std_logic;
         debug2           : out std_logic;
         if_enable        : in  std_logic);
   end component dif2lda;

   component lda2dif is
      generic (
         SCALE_FACTOR : positive);
      port (
         fclk             : in  std_logic;
         clk_strobe_5     : in  std_logic;  --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
         srst_5           : in  std_logic;
         srst_40          : in  std_logic;
         ms_tick          : in  std_logic;
         rx_pin           : in  std_logic;
         clk_syn          : out std_logic;
         cmd_idle         : in  std_logic;
         flow_d2l_suspend : out std_logic;
         roc_increment    : out std_logic;
         rx_data          : out std_logic_vector (7 downto 0);
         hdmi_strobe      : out std_logic;
         rx_error         : out std_logic;
         debug1           : out std_logic;
         debug2           : out std_logic;
         if_enable        : in  std_logic;
         data_available   : out std_logic);
   end component lda2dif;
   
begin
   debug(0) <= rx_pin;
   debug(1) <= tx_pin;
   debug(2) <= hdmi_strobe_int;
   debug(3) <= clk_strobe_5;



--   rx_pin_reg: process(fclk)
--   begin
--      if rising_edge(fclk) then
--         if srst = '1' then
--            rx_pin_reg1 <= '1';
--            rx_pin_reg2 <= '1';
--         else
--            rx_pin_reg2 <= rx_pin_reg1;
--            rx_pin_reg1 <= rx_pin;
--      end if; --clk
--   end process;

   hdmi_strobe <= hdmi_strobe_int;
   lda2dif_inst : lda2dif
      generic map (
         SCALE_FACTOR => SCALE_FACTOR
         )
      port map (
         -- clk          => clk,
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         srst_5       => srst_5,
         srst_40      => srst_40,
         ms_tick      => ms_tick,
         rx_pin       => rx_pin,        --rx_pin_reg2,

         clk_syn          => clk_syn,
         cmd_idle         => cmd_idle,
         flow_d2l_suspend => flow_d2l_suspend,
         roc_increment    => roc_increment,

         rx_data        => rx_data,
         hdmi_strobe    => hdmi_strobe_int,
         rx_error       => rx_error,
         debug1         => open,
         debug2         => open,
         if_enable      => if_enable,
         data_available => data_available

         );
   dif2lda_inst : dif2lda
      generic map (
         SCALE_FACTOR => SCALE_FACTOR
         )
      port map (
         -- clk              => clk,
         fclk             => fclk,
         clk_strobe_5     => clk_strobe_5,
         srst_5           => srst_5,
         srst_40          => srst_40,
         tx_pin           => tx_pin,
         wr_req           => wr_req,
         tx_data          => tx_data,
         writing          => writing,
         txen             => hdmi_txen,
         turbo            => turbo,
         flow_d2l_suspend => flow_d2l_suspend,
         debug1           => open,
         debug2           => open,
         if_enable        => if_enable
         );

   rx_pin_ibufds_inst : IBUFDS
      generic map (
         IOSTANDARD => "LVDS_25",
         DIFF_TERM  => true
         )
      port map (
         O  => rx_pin,
         I  => rx_pin_p,
         IB => rx_pin_n
         );

   tx_pin_OBUFDS_inst : OBUFDS
      generic map (
         IOSTANDARD => "LVDS_25"
         ) 
      port map (
         O  => tx_pin_p,
         OB => tx_pin_n,
         I  => tx_pin
         );

end architecture rtl;









