library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

library work;
use work.adif2_types.all;

entity acq_stop is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock

      rst           : in  std_logic;
      usb_strobe    : in  std_logic;
      rx_data       : in  std_logic_vector (7 downto 0);
      acq_stop_rx   : out std_logic;
      acq_stop_done : out std_logic;
      acq_cmd_done  : in  std_logic;
      acq_stop_dbg  : out std_logic;
      cmd_end       : in  std_logic

      );
end acq_stop;

architecture rtl of acq_stop is

   type states is (INIT, S11, S12, S20, S21, S22,
                   S80, S81, S90, S91, S92);
   signal state, next_state : states;

   signal rx : T_ARRAY_8BIT(C_NUM_BYTES-1 downto 0);

begin
   P0 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            state <= INIT;
         elsif (clk_strobe_5 = '1') then
            state <= next_state;
         end if;
      end if;
   end process P0;

   P1 : process (state, usb_strobe, rx, cmd_end, acq_cmd_done)
   begin
      case state is
         when INIT =>
            if usb_strobe = '1' then    -- wait for strobe signal
               next_state <= S11;       -- go to check byte #0
            else
               next_state <= INIT;
            end if;

         when S11 =>                    -- save byte #0 of the command - packet type byte 0
            next_state <= S12;
            
         when S12 =>                    -- test command identifier
            if rx(0) = x"E3" then
               next_state <= S20;       -- it is a command 
            else
               next_state <= INIT;      -- back to init
            end if;
            
         when S20 =>
            if usb_strobe = '1' then    -- wait for strobe signal
               next_state <= S21;       -- go to save byte #1
            else
               next_state <= S20;       -- still wait for strobe
            end if;
            
         when S21 =>                    -- save byte #1 of the command - packet type byte 1
            next_state <= S22;          -- check if is it fast acq stop command
            
         when S22 =>
            if rx(1) = x"13" then       -- test if is it fast acq stop command 
               next_state <= S80;       -- it is fast acq stop command
            else
               next_state <= INIT;      -- back to init 
            end if;
            
         when S80 =>                    -- set fast acq stop received
            next_state <= S81;

         when S81 =>                    -- check if fast acq finish
            if (acq_cmd_done = '1') then
               next_state <= S90;
            else
               next_state <= S81;
            end if;

         when S90 =>                    -- set cmd_done to activate command ack
            next_state <= S91;

         when S91 =>                    -- command ready deactivated
            if cmd_end = '1' then       -- waiting up to command is finished
               next_state <= S92;
            else
               next_state <= S91;
            end if;

         when S92 =>                    -- command ready deactivated
            next_state <= INIT;

      end case;
   end process P1;

   P2 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            acq_stop_rx   <= '0';
            acq_stop_done <= '0';
            rx            <= (others => (others => '0'));
            acq_stop_dbg  <= '0';
         elsif (clk_strobe_5 = '1') then
            case state is
               when INIT =>
                  acq_stop_rx   <= '0';
                  acq_stop_done <= '0';
                  rx            <= (others => (others => '0'));
                  acq_stop_dbg  <= '0';

               when S11 =>
                  rx(0) <= rx_data;     -- save byte #0 of the command - packet type byte 0

               when S12 =>

               when S20 =>

               when S21 =>
                  rx(1) <= rx_data;     -- save byte #1 of the command - packet type byte 1
                  
               when S22 =>

               when S80 =>
                  acq_stop_rx  <= '1';  -- set fast acq stop command received
                  acq_stop_dbg <= '1';

               when S81 =>

               when S90 =>
                  acq_stop_done <= '1';  -- set fast acq stop command done
                  acq_stop_dbg  <= '0';

               when S91 =>

               when S92 =>               -- waiting up to fast acq start command is finished
                  acq_stop_rx   <= '0';  -- deactivated fast acq stop command received
                  acq_stop_done <= '0';  -- deactivated command done
                  acq_stop_dbg  <= '1';

            end case;
         end if;
      end if;
   end process P2;

end architecture rtl;
