--
-- VHDL Architecture LDA_lib.dif2lda.rtl
--
-- Created:
--          by - kvas.UNKNOWN (FLCKVASWL)
--          at - 18:06:15 24.07.2014
--
-- using Mentor Graphics HDL Designer(TM) 2012.2b (Build 5)
--
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity dif2lda is
   generic (
      SCALE_FACTOR : positive := 4
      );
   port (
      -- clk          : in  std_logic;
      fclk         : in  std_logic;
      clk_strobe_5 : in  std_logic;     --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      srst_5       : in  std_logic;
      srst_40      : in  std_logic;
      tx_pin       : out std_logic;

      wr_req           : in  std_logic;
      tx_data          : in  std_logic_vector (7 downto 0);
      writing          : out std_logic;
      txen             : out std_logic;
      turbo            : in  std_logic;  --when set, the FSM does only listen to wr_req and provides then txen signal with the "full" logic
      flow_d2l_suspend : in  std_logic;  -- no data should be transmitted to LDA if '1': the buffer in LDA is full

      debug1    : out std_logic;
      debug2    : out std_logic;
      if_enable : in  std_logic
      );
end entity dif2lda;

--
architecture rtl of dif2lda is
   signal word16                 : std_logic_vector(15 downto 0);  --register for the expanded word from 2 byte
   signal tx_start, tx_start_int : std_logic;
   signal tx_busy                : std_logic;

   component word_transmitter2
      generic(
         WORD_SIZE    : integer := 16;
         SCALE_FACTOR : integer := 4
         );
      port(
         CLK      : in  std_logic;
         RESET    : in  std_logic;
         TX_DATA  : in  std_logic_vector (WORD_SIZE-1 downto 0);
         TX_START : in  std_logic;
         TX_BUSY  : out std_logic;
         TX_PIN   : out std_logic
         );
   end component word_transmitter2;
   
   type states is (
      INIT,
      S01, S02, S03, S04, S05,
      S10, S11, S12, S13, S14, S15
      );
   signal state, next_state : states;
   signal writing_int       : std_logic;
   signal txen_int          : std_logic;
   signal txen_reg          : std_logic;
   signal save_low          : std_logic;
   signal save_low_int      : std_logic;
   signal save_high         : std_logic;
   signal save_high_int     : std_logic;


   component hdmi_tx_fifo
      port (
         rst       : in  std_logic;
         wr_clk    : in  std_logic;
         rd_clk    : in  std_logic;
         din       : in  std_logic_vector(15 downto 0);
         wr_en     : in  std_logic;
         rd_en     : in  std_logic;
         dout      : out std_logic_vector(15 downto 0);
         full      : out std_logic;
         empty     : out std_logic;
         valid     : out std_logic;
         prog_full : out std_logic
         );
   end component;

   -- cross clock domain FIFO signals
   signal wr_en     : std_logic;
   signal wr_en_reg : std_logic;
   signal wr_en_int : std_logic;
   signal full      : std_logic;

   signal rd_en, rd_en_int : std_logic;
   signal dout             : std_logic_vector (word16'range);
   signal empty            : std_logic;
   signal valid            : std_logic;
   
   type fstates is (
      FINIT,
      FS01, FS02
      );
   signal fstate, fnext_state : fstates;


--   signal tx_start_1,tx_start_2,tx_start_3,tx_start_f : std_logic;
--   signal tx_busy_f, tx_busy_1   : std_logic;
--   signal word_16_f : std_logic_vector (word16'range);
   
begin
   debug1 <= wr_en;
   debug2 <= rd_en;
   ---------------------------------------------------------------------------
   -- clk domain of the transmitter (40 MHz)
   ---------------------------------------------------------------------------
   -- word16 vector does not need special clock cross-clock-domain care 
   -- (metastability is gone when patched tx_start signal is active_
   -- tx_start requires a cross-clock-domain care
   -- tx_busy requires a cross-clock-domain care
   word_transmitter_inst : word_transmitter2
      generic map (
         WORD_SIZE    => 16,
         SCALE_FACTOR => SCALE_FACTOR
         )
      port map (
         CLK      => fclk,
         RESET    => srst_40,
         TX_DATA  => dout,
         TX_START => tx_start,
         TX_BUSY  => tx_busy,
         TX_PIN   => tx_pin
         );

   hdmi_tx_fifo_inst : hdmi_tx_fifo
      port map (
         rst       => srst_40,
         wr_clk    => fclk,
         rd_clk    => fclk,
         din       => word16,
         wr_en     => wr_en,
         rd_en     => rd_en,
         dout      => dout,
         full      => open,
         empty     => empty,
         valid     => valid,
         prog_full => full
         );
   ----------------------------------------------------------------------------
   -- state machine
   -- clk_domain fclk (40MHz)
   ----------------------------------------------------------------------------
   fclk_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if srst_40 = '1' then
            fstate   <= FINIT;
            tx_start <= '0';
            rd_en    <= '0';
         else
            fstate   <= fnext_state;
            tx_start <= tx_start_int;
            rd_en    <= rd_en_int;
         end if;
      end if;
   end process;

   fnext_proc : process(empty, flow_d2l_suspend, fstate, tx_busy, valid)
   begin
      case fstate is
         when FINIT =>
            if (empty = '0') and (valid = '1') and (tx_busy = '0') and (flow_d2l_suspend = '0') then
               fnext_state <= FS01;
            else
               fnext_state <= FINIT;
            end if;
         when FS01 =>
            fnext_state <= FS02;
         when FS02 =>
            fnext_state <= FINIT;
         when others =>
            fnext_state <= FINIT;
      end case;
   end process;

   fout_proc : process(empty, flow_d2l_suspend, fstate, tx_busy, valid)
   begin
      rd_en_int    <= '0';
      tx_start_int <= '0';
      case fstate is
         when FINIT =>
            if (empty = '0') and (valid = '1') and (tx_busy = '0') and (flow_d2l_suspend = '0') then
               rd_en_int    <= '1';
               tx_start_int <= '1';
            end if;
         when FS01 =>
         when FS02 =>
         when others =>
      end case;
   end process;

   ----------------------------------------------------------------------------
   -- state machine
   -- clk_domain clk (5MHz)
   ----------------------------------------------------------------------------
   clk_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if srst_40 = '1' then
            state     <= INIT;
            writing   <= '0';
            txen_reg  <= '0';
            wr_en_reg <= '0';
            save_high <= '0';
            save_low  <= '0';
         elsif (clk_strobe_5 = '1') then
            state     <= next_state;
            writing   <= writing_int;
            txen_reg  <= txen_int or full;
            wr_en_reg <= wr_en_int;
            save_high <= save_high_int;
            save_low  <= save_low_int;
         else
            wr_en_reg <= '0';           --wr_en_int is 8 cycles long -> must be shortened to 1 fclk pulse when not strobe='1'
         end if;  -- strobe/reset
      end if;  --clk
   end process;

   txen  <= (txen_int or txen_reg or full) when turbo = '1' else txen_reg;  --in turbo mode we don't use the register version of txen and we have to propagate the full signal as soon as possible
   wr_en <= wr_en_reg;                                                      -- wr_en_int when turbo = '1' else wr_en_reg;


   next_proc : process(full, if_enable, state, turbo, wr_req)
   begin
      case state is
         when INIT =>
            if (wr_req = '1') and (if_enable = '1') then  -- request for writing a byte
               if turbo = '1' then
                  next_state <= S10;                      --in turbo mode we save directly and we are ready to recieve a next word immediatly
               else
                  next_state <= S01;
               end if;
            else
               next_state <= INIT;
            end if;
         when S01 =>                                      -- saved to bits 7..0, writing signal raised and waiting
            if turbo = '1' then
               next_state <= S10;
            else
               next_state <= S02;
            end if;
         when S02 =>                                      -- writing signal raised and waiting
            next_state <= S03;
         when S03 =>                                      -- txen raised (confirmation) and waiting
            next_state <= S04;
         when S04 =>                                      -- waiting with txen = '1'
            next_state <= S05;
         when S05 =>                                      -- waiting with txen = '1'
            next_state <= S10;
         when S10 =>                                      -- waiting for then other half of the data
            if wr_req = '1' then                          -- request for writing a byte (second half of the word)
               if turbo = '1' then
                  next_state <= S12;
               else
                  next_state <= S11;
               end if;  --turbo
            else
               next_state <= S10;
            end if;
         when S11 =>
            next_state <= S12;
         when S12 =>                                      -- saved bits 15 downto 8, writing signal raised
            if full = '1' then
               next_state <= S12;                         --waiting for previous data to be serialized and transmitted
            else
               if turbo = '1' then
                  next_state <= INIT;
               else
                  next_state <= S13;                      -- transmitter is empty, we can transmitt a new word16 
               end if;
            end if;
         when S13 =>
            next_state <= S14;
         when S14 =>
            next_state <= S15;
         when S15 =>
            next_state <= INIT;
         when others =>
            next_state <= INIT;
      end case;
   end process;

   output_proc : process (full, if_enable, state, turbo, wr_req)
   begin
      save_low_int  <= '0';
      save_high_int <= '0';
      writing_int   <= '0';
      wr_en_int     <= '0';
      txen_int      <= '0';
      case state is
         when INIT =>
            if (wr_req = '1') and (if_enable = '1') then  -- request for writing a byte
               writing_int <= '1';
               if turbo = '1' then
                  save_high_int <= '1';                   --in turbo mode we write directly
               end if;
            end if;
         when S01 =>
            save_high_int <= '1';
            writing_int   <= '1';
            txen_int      <= '1';
         when S02 =>
            writing_int <= '1';
            txen_int    <= '1';
         when S03 =>
            txen_int <= '1';
         when S04 =>
            txen_int <= '1';
         when S05 =>
            txen_int <= '1';
         when S10 =>
            if wr_req = '1' then                          -- request for writing a byte
               writing_int <= '1';
               if turbo = '1' then
                  save_low_int <= '1';                    --in turbo mode we write in s10 directly
               end if;
            end if;
         when S11 =>
            writing_int  <= '1';
            save_low_int <= '1';
            txen_int     <= '1';
         when S12 =>
            writing_int <= '1';
            txen_int    <= '1';
            if full = '0' then
               wr_en_int <= '1';
--            txen_int <= '1';
            end if;
         when S13 =>
            writing_int <= '1';
            txen_int    <= '1';
         when S14 =>
            writing_int <= '1';
            txen_int    <= '1';
         when S15 =>
            txen_int <= '1';
         when others =>
      end case;
   end process;

   tx_data_reg_proc : process(fclk)
   begin
      if rising_edge(fclk) then
         if srst_40 = '1' then
            word16 <= (others => '0');
         else
            if (clk_strobe_5 = '1') then
               --if turbo = '1' then
               --   if save_low_int = '1' then
               --      word16(7 downto 0) <= tx_data;
               --   end if;  --save low
               --   if save_high_int = '1' then
               --      word16(15 downto 8) <= tx_data;
               --   end if;  --save high
               --else
               if save_low = '1' then
                  word16(7 downto 0) <= tx_data;
               end if;  --save low
               if save_high = '1' then
                  word16(15 downto 8) <= tx_data;
               end if;  --save high
--                              end if;
            end if;  --strobe
         end if;  --reset
      end if;  --clk
   end process;



end architecture rtl;


