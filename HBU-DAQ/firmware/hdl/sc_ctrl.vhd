
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sc_ctrl is
   port(
      -- clk             : in  std_logic;
      fclk            : in  std_logic;  --clk 40MHz
      clk_strobe_5    : in  std_logic;  --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst             : in  std_logic;
      usb_strobe      : in  std_logic;
      rx_data         : in  std_logic_vector (7 downto 0);
      mem_data_out    : in  std_logic_vector (7 downto 0);
      mem_data_in     : out std_logic_vector (7 downto 0);
      mem_addr        : out std_logic_vector (14 downto 0);
      mem_clk         : out std_logic;
      mem_we          : out std_logic;
      sc_cmd_done     : out std_logic;
      sc_clk1         : out std_logic;
      sc_clk2         : out std_logic;
      sc_clk3         : out std_logic;
      clk_40M_inhibit : out std_logic;  --stop the 40 MHz clock in order to surpress noise 
      sc_load         : out std_logic;
      sc_srin         : out std_logic;
      cmd12           : in  std_logic_vector (15 downto 0);
      cmd34           : in  std_logic_vector (15 downto 0);
      cmd56           : in  std_logic_vector (15 downto 0);
      cmd_ready       : in  std_logic;
      sc_wr_req       : out std_logic;
      usb_txen        : in  std_logic;
      writing         : in  std_logic;
      sc_data_out     : out std_logic_vector (7 downto 0);
      cmd_end         : in  std_logic;
      spiroc_type     : in  std_logic;
      sc_debug 	      : out std_logic;
      slab_num_asic   : in  std_logic_vector (15 downto 0)
--      sw_1V5to3V3  : OUT    std_logic
      );
end sc_ctrl;

architecture rtl of sc_ctrl is

   type STATE_TYPE is (s0, s01,
                       s100, s10, s11, s12, s13,
                       s200, s20, s21, s22, s23, s24, s25,
                       s300, s301, s302, s303, s305,
                       s310, s311, s312, s315, s320, s321, s322, s325,
                       s330, s331, s332, s335, s340, s341, s342, s345,
                       s350, s351, s352, s360, s361, s362, s363,
                       s90, s91, s92);

   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;

   signal sc_clk         : std_logic := '0';
   signal sc_data_loaded : std_logic;
   signal sc_data        : std_logic_vector (7 downto 0);
   signal mem_addr_int   : std_logic_vector (14 downto 0);

   signal slab : std_logic_vector (1 downto 0);
   signal asic : std_logic_vector (4 downto 0);
   signal set  : std_logic;

   signal num_asic   : std_logic_vector (4 downto 0) := (others => '0');
   signal asic_index : unsigned(4 downto 0);

   signal count      : unsigned(23 downto 0);
   signal byte_index : unsigned(8 downto 0);
   signal bit_index  : unsigned(2 downto 0);

   -- purpose: calculates the base address for storing the slowcontrol files. The goal is to fit 24 slowcontrols into the memory, which has 12 bits. 
   function mem_addr_calc (
      slab       : std_logic_vector(1 downto 0);  -- the slab number
      asic_index : unsigned(4 downto 0))          -- the asic index for which the address needs to be calculated
      return std_logic_vector is                  --returns the complete address vector
      variable addr : unsigned(6 downto 0);
   begin  -- mem_addr_calc
      --originally:  slab & std_logic_vector(asic_index + 1) & "0000000";
      --The idea is to do a multiplication of the chip SC size (=319) with the asic index.
      --For simplification we will multiply by 320, which is binary 1 0100 0000.
      --So it is enough just to add the asic_index to (asic_index shl 2)
      addr := asic_index & "00";                  --asic_index shifted by 2 left saved to addr
      addr := addr + asic_index;                  --asic_index added to addr. in the address is 5x asic_index at the moment.
      --and add last 5 zeroes from the right and slab index from left:
      return slab & std_logic_vector(addr) & "000000";
   end mem_addr_calc;
   
begin
   
   mem_clk  <= fclk;
   mem_addr <= mem_addr_int;

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (bit_index, byte_index, cmd12, cmd34, cmd_end, cmd_ready, count, current_state, sc_data_loaded, spiroc_type, usb_strobe, usb_txen, writing)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            -- wait for usb command
            if cmd_ready = '1' then
               next_state <= s01;
            else
               next_state <= s0;
            end if;
         when s01 =>
            --check for the typemodifyer and datalength
            if cmd12 = x"000A" and cmd34 = x"002C" then          --in words 2C / in bytes 58
               next_state <= s100;                               -- sc data transfer to DIF
            elsif cmd12 = x"000A" and cmd34 = x"002D" then       --in words 2D / in bytes 5A
               next_state <= s100;                               -- sc data transfer to DIF
            elsif cmd12 = x"000A" and cmd34 = x"003B" then       -- KLauS-5
               next_state <= s100;                               -- sc data transfer to DIF
            elsif cmd12 = x"000A" and cmd34 = x"00A0" then       -- SPIROC 2D / in bytes 149, 1186 bits
               next_state <= s100;                               -- sc data transfer to DIF
            elsif cmd12 = x"0016" and cmd34 = x"0000" then
               next_state <= s200;                               -- sc data read back from DIF
            elsif cmd12 = x"000C" and cmd34 = x"0000" then
               next_state <= s300;                               -- load sc data from DIF to ASIC
            else
               next_state <= s0;                                 -- not sc command
            end if;
         when s100 =>
            next_state <= s10;
         when s10 =>                                             -- sc data transfer to DIF - waiting for sc data
            if usb_strobe = '1' then
               next_state <= s11;                                -- save next sc byte in memory 
            else
               next_state <= s10;
            end if;
         when s11 =>
            if spiroc_type = '0' and byte_index < x"13F" then	-- Write from PC to DIF from bytes 0 to 319, in total 320 bytes
               next_state <= s12;
            elsif spiroc_type = '1' and byte_index < x"134" then   --for klaus-6: 308 bytes = 0x134, 
               next_state <= s12;
            else
               next_state <= s13;
            end if;
         when s12 =>
            next_state <= s10;
         when s13 =>
            next_state <= s90;
            
         when s200 =>
            next_state <= s20;
         when s20 =>                    -- sc data read back from DIF
            next_state <= s21;
         when s21 =>
            next_state <= s22;
         when s22 =>
            next_state <= s23;
         when s23 =>
            if writing = '1' then
               next_state <= s24;
            else
               next_state <= s23;
            end if;
            
         when s24 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= s25;
            else
               next_state <= s24;
            end if;
            
         when s25 =>
            if spiroc_type = '0' and byte_index < x"13E" then	-- from 0...318 in total 319 bytes write back to the DIF
               next_state <= s20;
            elsif spiroc_type = '1' and byte_index < x"133" then
               next_state <= s20;
            else
               next_state <= s90;
            end if;
            
         when s300 =>                   -- load sc data from DIF to ASIC
            next_state <= s301;
         when s301 =>
            --IF count < x"000300" THEN
            if count < x"0A0000" then
               next_state <= s301;
            else
               next_state <= s302;
            end if;
         when s302 =>
            next_state <= s303;
         when s303 =>
            if count < x"000008" then
               next_state <= s303;
            else
               next_state <= s305;
            end if;
         when s305 =>
            next_state <= s310;

         when s310 =>
            next_state <= s311;
         when s311 =>
            next_state <= s312;
         when s312 =>
            if count < x"000008" then
               next_state <= s311;
            else
               next_state <= s315;
            end if;
         when s315 =>
            if sc_data_loaded = '1' then
               next_state <= s350;
            else
               next_state <= s320;
            end if;

         when s340 =>
            next_state <= s341;
         when s341 =>
            next_state <= s342;
         when s342 =>
            if count < x"000008" then
               next_state <= s341;
            else
               next_state <= s345;
            end if;
         when s345 =>
            if sc_data_loaded = '1' then
               next_state <= s350;
            else
               next_state <= s320;
            end if;

         when s320 =>
            next_state <= s321;
         when s321 =>
            next_state <= s322;
         when s322 =>
            if count < x"000008" then
               next_state <= s321;
            else
               next_state <= s325;
            end if;
         when s325 =>
            if spiroc_type = '0' and byte_index = x"13E" and bit_index = "001" then  --modified for KLauS-5 (318 bytu + 1 bity). 
               next_state <= s310;
            elsif spiroc_type = '1' and byte_index = x"134" and bit_index = "111" then -- KLauS-6 (307 bytes + 7 bits)
               next_state <= s310;
            elsif bit_index = "111" then
               next_state <= s340;
            else
               next_state <= s330;
            end if;

         when s330 =>
            next_state <= s331;
         when s331 =>
            next_state <= s332;
         when s332 =>
            if count < x"000008" then
               next_state <= s331;
            else
               next_state <= s335;
            end if;
         when s335 =>
            next_state <= s320;

         when s350 =>
            if count < x"000100" then
               next_state <= s350;
            else
               next_state <= s351;
            end if;
         when s351 =>
            next_state <= s352;
         when s352 =>
            if count < x"001000" then
               next_state <= s352;
            else
               next_state <= s360;
            end if;

         when s360 =>
            next_state <= s361;
         when s361 =>
            --IF count < x"0000A0" THEN
            if count < x"004000" then
               next_state <= s361;
            else
               next_state <= s362;
            end if;
         when s362 =>
            next_state <= s363;
         when s363 =>
            if count < x"000300" then
               next_state <= s363;
            else
               next_state <= s90;
            end if;
            
         when s90 =>
            next_state <= s91;
         when s91 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s92;
            else
               next_state <= s91;
            end if;
         when s92 =>
            next_state <= s0;

      -- WHEN OTHERS =>
      -- next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            clk_40M_inhibit <= '0';
            sc_data_loaded  <= '0';
            sc_cmd_done     <= '0';
            sc_clk          <= '0';
            sc_load         <= '1';
            mem_we          <= '0';
            sc_srin         <= '0';
            sc_wr_req       <= '0';
--      sw_1V5to3V3 <= '1';
            sc_data         <= (others => '0');
            sc_data_out     <= (others => '0');
            mem_data_in     <= (others => '0');
            mem_addr_int    <= (others => '0');
            count           <= (others => '0');
            byte_index      <= (others => '0');
            bit_index       <= "000";
            slab            <= "00";
            asic            <= "00000";
            set             <= '0';
            asic_index      <= "00000";
	    sc_debug		<= '0';
         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  clk_40M_inhibit <= '0';
                  sc_data_loaded  <= '0';
                  sc_cmd_done     <= '0';
                  sc_clk          <= '0';
                  sc_load         <= '1';
                  mem_we          <= '0';
                  sc_srin         <= '0';
                  sc_wr_req       <= '0';
--        sw_1V5to3V3 <= '1';
                  sc_data         <= (others => '0');
                  sc_data_out     <= (others => '0');
                  mem_data_in     <= (others => '0');
--        mem_addr_int <= (others => '0');
                  count           <= (others => '0');
                  byte_index      <= (others => '0');
                  bit_index       <= "000";
                  slab            <= "00";
                  asic            <= "00000";
                  set             <= '0';
                  asic_index      <= "00000";
		  sc_debug	  <= '0';

               when s01 =>
                  slab       <= cmd56(7 downto 6);
                  asic       <= cmd56(5 downto 1);
                  set        <= cmd56(0);
                  asic_index <= unsigned(asic);

               when s100 =>
                  mem_addr_int 	<= mem_addr_calc(slab, unsigned(asic));
               when s10 =>
                  mem_we <= '1';
               when s11 =>
                  mem_data_in <= rx_data;
               when s12 =>
                  mem_addr_int <= std_logic_vector(unsigned(mem_addr_int) + 1);
                  byte_index   <= byte_index + 1;
               when s13 =>
                  mem_we <= '0';
                  
               when s200 =>
                  mem_we       <= '0';
                  mem_addr_int <= mem_addr_calc(slab, unsigned(asic));
               when s20 =>
               when s21 =>
                  sc_data_out <= mem_data_out;
               when s22 =>
                  sc_wr_req <= '1';
               when s23 =>
                  sc_wr_req <= '0';
               when s24 =>
               when s25 =>
                  mem_addr_int <= std_logic_vector(unsigned(mem_addr_int) + 1);
                  byte_index   <= byte_index + 1;
                  sc_wr_req    <= '0';
                  
               when s300 =>
		  sc_debug	<= '1';
                  clk_40M_inhibit <= '1';    --silence starts
                  sc_srin         <= '0';
                  sc_data         <= (others => '0');
                  mem_addr_int    <= (others => '0');
                  byte_index      <= (others => '0');
                  bit_index       <= "000";  -- bit_index <= "001";
                  asic            <= "00000";
                  set             <= '0';
                  asic_index      <= "00000";
                  --mem_addr_int <= slab & std_logic_vector(asic_index) & "0000000";
                  mem_addr_int    <= mem_addr_calc(slab, asic_index);
               when s301 =>
                  count <= count + 1;
               when s302 =>
                  count <= (others => '0');
               when s303 =>
                  count <= count + 1;
                  sc_load   <= '0';
               when s305 =>
                  count <= (others => '0');

               when s310 =>
                  sc_data    <= mem_data_out;
                  bit_index  <= "000";  -- bit_index <= "001";
                  byte_index <= (others => '0');
               when s311 =>
                  count <= count + 1;
               when s312 =>
               when s315 =>
                  --        sc_srin <= '0';
                  count <= (others => '0');
               when s340 =>
                  sc_clk    <= '1';
                  sc_data   <= mem_data_out;
                  bit_index <= "000";
                  count     <= (others => '0');
               when s341 =>
                  count <= count + 1;
               when s342 =>
               when s345 =>
                  --        sc_srin <= '0';
                  count <= (others => '0');
               when s320 =>
                  sc_clk  <= '0';
                  sc_srin <= sc_data(0);
                  sc_data <= '0' & sc_data(7 downto 1);
                  count   <= (others => '0');
               when s321 =>
                  count <= count + 1;
               when s322 =>
               when s325 =>
                  count <= (others => '0');

               when s330 =>
                  sc_clk <= '1';
                  if bit_index = "001" then
                     mem_addr_int <= std_logic_vector(unsigned(mem_addr_int) + 1);
                     byte_index   <= byte_index + 1;
                  end if;

		  -- byte[318] is under shifting, its bit 0 is already shifted, which means the last bit already shifted.
                  if spiroc_type = '0' and byte_index = x"13E" and bit_index = "000" then  --last bit of SPIROC 2D bitstream
                     asic_index   <= asic_index + 1;
                     mem_addr_int <= mem_addr_calc(slab, asic_index + 1);
                  end if;

                  if spiroc_type = '1' and byte_index = x"133" and bit_index = "110" then
                     mem_addr_int <= mem_addr_calc(slab, asic_index + 1);
                     asic_index   <= asic_index + 1;
                  end if;
                  count     <= (others => '0');
	       	  bit_index <= bit_index + 1;
               when s331 =>
                  count <= count + 1;
               when s332 =>
               when s335 =>
                  if asic_index = unsigned(num_asic) then
                     sc_data_loaded <= '1';
                  end if;

               when s350 =>
                  sc_srin <= '0';
                  sc_clk  <= '0';
                  count   <= count + 1;
               when s351 =>
                  sc_load <= '1';
                  count   <= (others => '0');
               when s352 =>
                  count <= count + 1;
               when s360 =>
                  sc_load <= '1';
                  count   <= (others => '0');
               when s361 =>
                  count <= count + 1;
               when s362 =>
--        sw_1V5to3V3 <= '1';
                  count           <= (others => '0');
                  clk_40M_inhibit <= '0';  --start the clock again
               when s363 =>
                  count <= count + 1;
                  
               when s90 =>
                  sc_cmd_done 	<= '1';
		  sc_debug 	<= '0';
               when s91 =>
               when s92 =>
                  sc_cmd_done <= '0';

            --WHEN OTHERS =>
            -- NULL;
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;

   -----------------------------------------------------------------
   clk_out_proc : process (sc_clk, slab, slab_num_asic)
   -----------------------------------------------------------------
   begin
      --if (rst = '1') then
      --   sc_clk1  <= '0';
      --   sc_clk2  <= '0';
      --   sc_clk3  <= '0';
      --   num_asic <= slab_num_asic(5 downto 1);
      --else
      if slab = "00" then
         sc_clk1  <= sc_clk;
         sc_clk2  <= '0';
         sc_clk3  <= '0';
         num_asic <= slab_num_asic(5 downto 1);
      elsif slab = "01" then
         sc_clk1  <= '0';
         sc_clk2  <= sc_clk;
         sc_clk3  <= '0';
         num_asic <= slab_num_asic(10 downto 6);
      elsif slab = "10" then
         sc_clk1  <= '0';
         sc_clk2  <= '0';
         sc_clk3  <= sc_clk;
         num_asic <= slab_num_asic(15 downto 11);
      else
         sc_clk1  <= sc_clk;
         sc_clk2  <= sc_clk;
         sc_clk3  <= sc_clk;
         num_asic <= "00010";
      end if;
   --end if;
   end process clk_out_proc;
   
end architecture rtl;
