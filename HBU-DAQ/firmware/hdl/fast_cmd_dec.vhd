
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fast_cmd_dec is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in std_logic;
      cmd_ready    : in std_logic;
      cmd_end      : in std_logic;
      cmd12        : in std_logic_vector (15 downto 0);

      reset_bcid : out std_logic;

      cal_charge_etrig : out std_logic;
      cal_charge_itrig : out std_logic;
      cal_light_etrig  : out std_logic;
      cal_light_itrig  : out std_logic;

      start_acq       : out std_logic;
      start_acq_etrig : out std_logic;
      start_acq_itrig : out std_logic;
      stop_acq        : out std_logic;
      stop_ro         : out std_logic;
      stop_ro_cont    : out std_logic;

      fast_cmd_done : out std_logic
      );
end fast_cmd_dec;

architecture rtl of fast_cmd_dec is

   type STATE_TYPE is (s0, s10, s11, s20, s21, s22,
                       s91, s92, s93, s94);

   signal current_state : STATE_TYPE;
   signal next_state    : STATE_TYPE;

   signal end_fast_cmd : std_logic := '0';
   signal cmd_ok       : std_logic := '0';

   signal count : unsigned(15 downto 0);

begin

   -----------------------------------------------------------------
   clocked_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if (rst = '1') then
            current_state <= s0;
         elsif (clk_strobe_5 = '1') then
            current_state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process clocked_proc;

   -----------------------------------------------------------------
   nextstate_proc : process (cmd12, cmd_end, cmd_ok, cmd_ready, count, current_state, end_fast_cmd)
   -----------------------------------------------------------------
   begin
      case current_state is
         when s0 =>
            --wait for usb_command
            if cmd_ready = '1' then
               next_state <= s10;
            else
               next_state <= s0;
            end if;
         when s10 =>
            if (cmd12(15 downto 8) = x"E3") then
               next_state <= s11;       -- fast commands
            else
               next_state <= s0;
            end if;
         when s11 =>
            next_state <= s20;

         when s20 =>
            next_state <= s21;
         when s21 =>
            if count < x"000F" then
               next_state <= s20;
            else
               next_state <= s22;
            end if;
         when s22 =>
            next_state <= s91;

         when s91 =>
            if cmd_ok = '1' then
               next_state <= s92;
            else
               next_state <= s0;
            end if;
         when s92 =>
            --wait for command completion
            if end_fast_cmd = '1' then
               next_state <= s93;
            else
               next_state <= s92;
            end if;
         when s93 =>
            --wait for command acknowledge
            if cmd_end = '1' then
               next_state <= s94;
            else
               next_state <= s93;
            end if;
         when s94 =>
            next_state <= s0;
      end case;
   end process nextstate_proc;

   -----------------------------------------------------------------
   output_proc : process (fclk)
   -----------------------------------------------------------------
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            end_fast_cmd <= '0';
            cmd_ok       <= '0';

            reset_bcid       <= '0';
            start_acq_itrig  <= '0';
            start_acq_etrig  <= '0';
            start_acq        <= '0';
            stop_acq         <= '0';
            stop_ro          <= '0';
            stop_ro_cont     <= '0';
            cal_light_itrig  <= '0';
            cal_charge_itrig <= '0';
            cal_light_etrig  <= '0';
            cal_charge_etrig <= '0';
            count            <= (others => '0');
--      data_reg  <= (others => '0');
         elsif (clk_strobe_5 = '1') then
            case current_state is
               when s0 =>
                  end_fast_cmd <= '0';
                  cmd_ok       <= '1';

                  reset_bcid       <= '0';
                  start_acq_itrig  <= '0';
                  start_acq_etrig  <= '0';
                  start_acq        <= '0';
                  stop_acq         <= '0';
                  stop_ro          <= '0';
                  stop_ro_cont     <= '0';
                  cal_light_itrig  <= '0';
                  cal_charge_itrig <= '0';
                  cal_light_etrig  <= '0';
                  cal_charge_etrig <= '0';
                  count            <= (others => '0');
--        data_reg <= (others => '0');
               when s10 =>
               when s11 =>
                  if cmd12 = x"E309" then reset_bcid <= '1';
                  elsif cmd12 = x"E311" then
                     start_acq_itrig <= '1';
                     start_acq       <= '1';
                  elsif cmd12 = x"E312" then
                     start_acq_etrig <= '1';
                     start_acq       <= '1';
                  elsif cmd12 = x"E313" then stop_acq         <= '1';
                  elsif cmd12 = x"E319" then stop_ro          <= '1';
                  elsif cmd12 = x"E31A" then stop_ro_cont     <= '1';
                  elsif cmd12 = x"E359" then cal_light_itrig  <= '1';
                  elsif cmd12 = x"E35A" then cal_charge_itrig <= '1';
                  elsif cmd12 = x"E35B" then cal_light_etrig  <= '1';
                  elsif cmd12 = x"E35C" then cal_charge_etrig <= '1';
                  else
                     cmd_ok <= '0';
                  end if;

               when s20 =>
                  count <= count + 1;
               when s21 =>
               when s22 =>
                  reset_bcid       <= '0';
                  start_acq_itrig  <= '0';
                  start_acq_etrig  <= '0';
                  start_acq        <= '0';
                  stop_acq         <= '0';
                  stop_ro          <= '0';
                  stop_ro_cont     <= '0';
                  cal_light_itrig  <= '0';
                  cal_charge_itrig <= '0';
                  cal_light_etrig  <= '0';
                  cal_charge_etrig <= '0';

               when s91 =>
               when s92 =>
                  end_fast_cmd <= '1';
               when s93 =>
                  end_fast_cmd <= '0';
               when s94 =>
            end case;
         end if;  --strobe
      end if;  --clk
   end process output_proc;


   fast_cmd_done <= end_fast_cmd;
   -------------------------------------------------------------------
   --done_proc : process (end_fast_cmd, rst)
   -------------------------------------------------------------------
   --begin
   --   if rst = '1' then
   --      fast_cmd_done <= '0';
   --   else
   --      fast_cmd_done <= end_fast_cmd;
   --   end if;
   --end process done_proc;

end architecture rtl;

