----------------------------------------------------------------------------------
-- Company: DESY
-- Developer: jiri.kvasnicka@desy.de
-- 
-- Create Date: 07/17/2014
-- Design Name: 
-- Module Name: word receiver - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
use ieee.math_real.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity word_receiver2 is
   generic (WORD_SIZE    : positive := 16;
            SCALE_FACTOR : positive := 4);

   port (CLK    : in  std_logic;                                -- Input clock
         RESET  : in  std_logic;                                -- External rerset signal
         Rx_PIN : in  std_logic;                                -- Data reception pin
         WORD   : out std_logic_vector (WORD_SIZE-1 downto 0);  -- A register to hold the received word
         DONE   : out std_logic;
         error  : out std_logic                                 --there was an error during the receival (no 1 between 1st and 2nd stop bits)
         );
end word_receiver2;

architecture Behavioral of word_receiver2 is
   constant counter_bits : integer := integer(ceil(log2(real(WORD_SIZE))));  -- number of bits needed for the complete multiplexing to and from all the ports
   constant save_index   : integer := integer(round(real(SCALE_FACTOR)/2.0-1.0));  --index of prescaler, which will be used for saving of the data

   type state_type is
      (idle, start, bits, stop);
   
   signal prescaler   : std_logic_vector (SCALE_FACTOR-1 downto 0);
   signal CS, ns      : state_type;     --current state, next state
   signal bit_counter : std_logic_vector(counter_bits-1 downto 0);
   signal cout        : std_logic;      --carry out of the bit_counter
   signal shiftreg    : std_logic_vector(WORD_SIZE-1 downto 0);

   signal sum : std_logic_vector(bit_counter'high +1 downto 0);
--   constant zerox : std_logic_vector(bit_counter'high +1 downto 0) := (others => '0');
--   signal outreg        : std_logic_vector(shiftreg'range);

   signal save, pre_count, inject, clear, copy, error_stop : std_logic;
   signal operand1, operand2                               : unsigned(sum'range);  --sum operands

   signal rx_pin_reg1 : std_logic;      -- input pin clock synchronisation
   signal rx_pin_reg2 : std_logic;

begin
   error <= error_stop;

   clk_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            CS          <= idle;
            rx_pin_reg1 <= '1';
            rx_pin_reg2 <= '1';
         else
            CS          <= ns;
            rx_pin_reg2 <= rx_pin_reg1;
            rx_pin_reg1 <= Rx_PIN;
         end if;  -- reset
      end if;  --clk
   end process;

   next_state : process(CS, rx_pin_reg2, prescaler, cout)
   begin
      case CS is
         when idle =>
            if rx_pin_reg2 = '0' then
               ns <= start;
            else
               ns <= idle;
            end if;
         when start =>
            if prescaler(prescaler'high) = '1' then
               ns <= bits;
            else
               ns <= start;
            end if;
         when bits =>
            if cout = '1' then
               ns <= stop;
            else
               ns <= bits;
            end if;
         when stop =>
            if prescaler(prescaler'high) = '1' then
               ns <= idle;
            else
               ns <= stop;
            end if;
         when others =>
            ns <= idle;
      end case;
   end process;

   outputs : process(CS, rx_pin_reg2, prescaler, cout)
   begin
      --defaults
      inject     <= '0';
      save       <= '0';
      pre_count  <= '0';
      clear      <= '0';
      copy       <= '0';
      error_stop <= '0';

      case CS is
         when idle =>
            if rx_pin_reg2 = '0' then
               inject    <= '1';
               pre_count <= '1';
            else
               clear <= '1';
            end if;
         when start =>
            pre_count <= '1';
         when bits =>
            pre_count <= '1';
            save      <= '1';
         when stop =>
            if prescaler(prescaler'high) = '1' then
               if rx_pin_reg2 = '1' then
                  copy <= '1';
               else
                  error_stop <= '1';
               end if;  --Rx_pin
            end if;  -- prescaler'high='1'
         when others =>
      end case;
   end process;

   operand1(operand1'high)          <= '0';
   operand1(bit_counter'range)      <= unsigned(bit_counter);
   operand2(0)                      <= prescaler(prescaler'high);
   operand2(operand2'high downto 1) <= (others => '0');

   sum  <= std_logic_vector(operand1 + operand2);
   cout <= sum(sum'high);

   bit_counter_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            bit_counter <= (others => '0');
         else
            if clear = '1' then
               bit_counter <= (others => '0');
            elsif save = '1' then
               bit_counter <= sum (bit_counter'range);
               -- std_logic_vector(unsigned(bit_counter) + shiftreg(shiftreg'high)); --adds the shiftreg bit as a carry in (cin)
            end if;  --clear
         end if;  --reset
      end if;  --clk
   end process;

   prescaler_process : process(clk)
      variable last_pre_high : std_logic;
   begin
      if rising_edge(clk) then
         if reset = '1' then
            prescaler <= (others => '0');
         else
            if clear = '1' then
               prescaler <= (others => '0');
            else
               last_pre_high := prescaler(prescaler'high);
               for i in prescaler'high downto 1 loop
                  prescaler(i) <= prescaler (i-1);
               end loop;
               prescaler(0) <= inject or (last_pre_high and pre_count);
            end if;  --clear
         end if;  --reset
      end if;  --clk
   end process;

   shiftreg_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            shiftreg <= (others => '0');
         else
            if (save = '1') and (prescaler(save_index) = '1') then
               for i in shiftreg'high-1 downto 0 loop
                  shiftreg(i) <= shiftreg (i+1);
               end loop;
               shiftreg(shiftreg'high) <= Rx_PIN_reg2;
            end if;  --save and prescaler
         end if;  --reset
      end if;  --clk
   end process;

   output_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            WORD <= (others => '0');
         else
            if (copy = '1') then
               WORD <= shiftreg;
            end if;  --copy
         end if;  --reset
      end if;  --clk
   end process;

   done_process : process(clk)
   begin
      if rising_edge(clk) then
         if reset = '1' then
            DONE <= '0';
         else
            if (copy = '1') then
               DONE <= '1';
            else
               DONE <= '0';
            end if;  --copy
         end if;  --reset
      end if;  --clk
   end process;
   
end Behavioral;
