
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

library work;
use work.adif2_types.all;

entity cmd_rx is
   port(
      -- clk          : in  std_logic;
      fclk         : in  std_logic;     --clk 40MHz
      clk_strobe_5 : in  std_logic;     --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in  std_logic;
      usb_strobe   : in  std_logic;
      rx_data      : in  std_logic_vector (7 downto 0);
      reg_ready    : out std_logic;
      cmd_ready    : out std_logic;

      acq_start_cmd : out std_logic;
      acq_stop_rx   : in  std_logic;
      acq_cmd_done  : in  std_logic;
      cal_cmd_done  : in  std_logic;
      fast_cmd_done : in  std_logic;
      ro_cmd_done   : in  std_logic;
      sc_cmd_done   : in  std_logic;
      stat_cmd_done : in  std_logic;
      std_cmd_done  : in  std_logic;
      dc_cmd_done   : in  std_logic;    --duty cycle controller
      cmd_done      : out std_logic;
      ro_cycle      : in  std_logic_vector(7 downto 0);

      cmd_rx_dbg  : out std_logic;
      ack_end     : in  std_logic;
      cmd_end     : out std_logic;
      P_O_CMD_REC : out T_CMD_REC;                       -- command record
      cmd12       : out std_logic_vector (15 downto 0);  --type_modifier
      cmd34       : out std_logic_vector (15 downto 0);  --data_length
      cmd56       : out std_logic_vector (15 downto 0);  --specifier
      cmd78       : out std_logic_vector (15 downto 0);  --first_data
      cmd_idle    : out std_logic
      );
end cmd_rx;

architecture rtl of cmd_rx is

   type states is (INIT, S11, S12, S20, S21, S211, S22, S23,
                   S24, S25, S26, S27, S28, S29,
                   S30, S31, S32, S33, S34, S35, S36, S37, S38,
                   S40, S41, S42, S43, S44, S71, S73, S74, S75,
                   S80, S81, S82, S83, S84, S86, S90, S91, S92, S93, S94,
                   SB01, SB02, SB03, SB10, SB11);
   signal state, next_state : states;
   signal rx_end            : std_logic := '0';

   signal rx : T_ARRAY_8BIT(C_NUM_BYTES-1 downto 0) := (others => (others => '0'));
--  signal SIG_CMD_REC  : T_CMD_REC; -- command record

begin
   P0 : process (fclk)
   begin
      if rising_edge (fclk) then
         if rst = '1' then
            state <= INIT;
         elsif (clk_strobe_5 = '1') then
            state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process P0;

   P1 : process (ack_end, acq_cmd_done, cal_cmd_done, dc_cmd_done, fast_cmd_done, ro_cmd_done, rx, sc_cmd_done, stat_cmd_done, state, std_cmd_done,
                 usb_strobe)
   begin
      case state is
         when INIT =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S11;            -- go to check byte #0
            else
               next_state <= INIT;
            end if;
         when S11 =>                         -- save byte #0 of the command - packet type byte 0
            next_state <= S12;
         when S12 =>                         -- test command identifier
            if rx(0) = x"CC" or rx(0) = x"E3" then
               next_state <= S20;            -- it is a command 
            else
               next_state <= INIT;           -- back to init
            end if;
         when S20 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S21;            -- go to save byte #1
            else
               next_state <= S20;            -- still wait for strobe
            end if;
         when S21 =>                         -- save byte #1 of the command - packet type byte 1
            if rx(0) = x"E3" then            -- test if is it fast command - K28.3 mark 
               next_state <= S211;
            else
               next_state <= S22;            --treat as a command
            end if;
         when S211 =>
            if rx(1) = x"11" then
               next_state <= S86;            -- it is a start acquisition fast command
            else
               next_state <= INIT;           -- it is other fast command
            end if;
         when S22 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S23;            -- go to save byte #2
            else
               next_state <= S22;            -- still wait for strobe
            end if;
         when S23 =>                         -- save byte #2 of the command - packet id byte 0
            next_state <= S24;
         when S24 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S25;            -- go to save byte #3
            else
               next_state <= S24;            -- still wait for strobe
            end if;
         when S25 =>                         -- save byte #3 of the command - packet id byte 1
            next_state <= S26;
         when S26 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S27;            -- go to save byte #4
            else
               next_state <= S26;            -- still wait for strobe
            end if;
         when S27 =>                         -- save byte #4 of the command - type modifier byte 0
            next_state <= S28;
         when S28 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S29;            -- go to save byte #5
            else
               next_state <= S28;            -- still wait for strobe
            end if;
         when S29 =>                         -- save byte #5 of the command - type modifier byte 1
            next_state <= S30;
         when S30 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S31;            -- go to save byte #6
            else
               next_state <= S30;            -- still wait for strobe
            end if;
         when S31 =>                         -- save byte #6 of the command - specifier byte 0
            next_state <= S32;
         when S32 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S33;            -- go to save byte #7
            else
               next_state <= S32;            -- still wait for strobe
            end if;
         when S33 =>                         -- save byte #7 of the command - specifier byte 1
            next_state <= S34;
         when S34 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S35;            -- go to save byte #8
            else
               next_state <= S34;            -- still wait for strobe
            end if;
         when S35 =>                         -- save byte #8 of the command - data length byte 0
            next_state <= S36;
         when S36 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S37;            -- go to save byte #9
            else
               next_state <= S36;            -- still wait for strobe
            end if;
         when S37 =>                         -- save byte #9 of the command - data length byte 1
            next_state <= S38;
         when S38 =>
            if (rx(5)(7 downto 4) = x"7")    -- cal command
               or ((rx(4)&rx(5)) = X"0009")  --acquisition duty cycle
            then
               next_state <= S40;            -- go to save byte #10
            else
               next_state <= S71;            -- go to registers ready
            end if;
         when S40 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S41;            -- go to save byte #10
            else
               next_state <= S40;            -- still wait for strobe
            end if;
         when S41 =>                         -- save byte #10 of the command - data byte 0
            next_state <= S42;
         when S42 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S43;            -- go to save byte #11
            else
               next_state <= S42;            -- still wait for strobe
            end if;
         when S43 =>                         -- save byte #11 of the command - data byte 1
--      next_state <= S80;
            next_state <= S44;
         when S44 =>                         -- save byte #11 of the command - data byte 1
            next_state <= S71;
         when S71 =>                         -- command receive end activated
            next_state <= S73;
--    when S72 => 
--      next_state <= S73;
         when S73 =>                         -- command receive end deactivated and command ready activated
            next_state <= S74;
         when S74 =>
            if rx(5) = x"0A" then            -- sc transfer command
               next_state <= S75;            -- go to read sc data
            else
               next_state <= S80;            -- go to CRC
            end if;
         when S75 =>
            if (sc_cmd_done = '1' or ro_cmd_done = '1' or cal_cmd_done = '1') then
               next_state <= S80;            -- go to CRC
            else
               next_state <= S75;            -- wait for  sc data transfer
            end if;
         when S80 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S81;            -- go to save byte #12
            else
               next_state <= S80;            -- still wait for strobe
            end if;
         when S81 =>                         -- save byte #12 of the command - CRC byte 0
            next_state <= S82;
         when S82 =>
            if usb_strobe = '1' then         -- wait for strobe signal
               next_state <= S83;            -- go to save byte #13
            else
               next_state <= S82;            -- still wait for strobe
            end if;
         when S83 =>                         -- save byte #13 of the command - CRC byte 1
            next_state <= S84;
         when S84 =>
            if (std_cmd_done = '1' or stat_cmd_done = '1' or fast_cmd_done = '1' or sc_cmd_done = '1'
                or cal_cmd_done = '1' or acq_cmd_done = '1' or ro_cmd_done = '1' or (dc_cmd_done = '1')) then
               next_state <= S90;
            else
               next_state <= S84;
            end if;
         when S86 =>
            -- requires stop fast command to be received:       if (acq_cmd_done = '1' and acq_stop_rx = '1') then
            -- removed 8.10.2015. The acq_stop_rx can still stop the acquisition.
            if (acq_cmd_done = '1') then
               next_state <= SB01;           --autosend results without receiving any command
            -- next_state <= S92; --no autosending
            else
               next_state <= S86;
            end if;
         when S90 =>                         -- set cmd_done to activate command ack
            next_state <= S91;
         when S91 =>                         -- command ready deactivated
            if ack_end = '1' then            -- waiting up to ack is finished
               next_state <= S92;
            else
               next_state <= S91;
            end if;
         when S92 =>                         -- command ready deactivated
            next_state <= S93;
         when S93 =>                         -- command ready deactivated
            next_state <= S94;
         when S94 =>                         -- command ready deactivated
            next_state <= INIT;
         --SB01 starts a branch, when the acquisition is followed by an automated data sending
         -- - replaces S92
         when SB01 => next_state <= SB02;    --finish the acquisition
         when SB02 => next_state <= SB03;
         when SB03 => next_state <= SB10;    --prepare the packet for readout
         when SB10 => next_state <= SB11;
         when SB11 => next_state <= S84;     -- go to execution of the prepared readout command via a standard loop
      end case;
   end process P1;

   P2 : process (fclk)
   begin
      if rising_edge (fclk) then
         if rst = '1' then
            cmd_ready     <= '0';
            rx_end        <= '0';
            cmd_done      <= '0';
            cmd_end       <= '0';
            cmd_rx_dbg    <= '0';
            rx            <= (others => (others => '0'));
            acq_start_cmd <= '0';
            P_O_CMD_REC   <= C_CMD_REC;
            cmd12         <= (others => '0');
            cmd34         <= (others => '0');
            cmd56         <= (others => '0');
            cmd78         <= (others => '0');
            cmd_idle      <= '0';
         elsif (clk_strobe_5 = '1') then
            case state is
               when INIT =>
                  cmd_ready     <= '0';
                  rx_end        <= '0';
                  cmd_done      <= '0';
                  cmd_end       <= '0';
                  cmd_rx_dbg    <= '1';
                  rx            <= (others => (others => '0'));
                  acq_start_cmd <= '0';
                  P_O_CMD_REC   <= C_CMD_REC;
                  cmd12         <= (others => '0');
                  cmd34         <= (others => '0');
                  cmd56         <= (others => '0');
                  cmd78         <= (others => '0');
                  cmd_idle      <= '1';
               when S11 =>
                  rx(0)    <= rx_data;               -- save byte #0 of the command - packet type byte 0
                  cmd_idle <= '0';
               when S12 => null;
               when S20 =>
                  cmd_rx_dbg <= '1';
               when S21 =>
                  rx(1)      <= rx_data;             -- save byte #1 of the command - packet type byte 1
                  cmd_rx_dbg <= '0';
               when S211 => null;
               when S22  => null;
               when S23 =>
                  rx(2) <= rx_data;                  -- save byte #2 of the command - packet id byte 0
               when S24 => null;
               when S25 =>
                  rx(3) <= rx_data;                  -- save byte #3 of the command - packet id byte 1
               when S26 => null;
               when S27 =>
                  rx(4) <= rx_data;                  -- save byte #4 of the command - type modifier byte 0
               when S28 => null;
               when S29 =>
                  rx(5) <= rx_data;                  -- save byte #5 of the command - type modifier byte 1
               when S30 => null;
               when S31 =>
                  rx(6) <= rx_data;                  -- save byte #6 of the command - specifier byte 0
               when S32 => null;
               when S33 =>
                  rx(7) <= rx_data;                  -- save byte #7 of the command - specifier byte 1
               when S34 => null;
               when S35 =>
                  rx(8) <= rx_data;                  -- save byte #8 of the command - data length byte 0
               when S36 => null;
               when S37 =>
                  rx(9) <= rx_data;                  -- save byte #9 of the command - data length byte 1
               when S38 =>
                  P_O_CMD_REC.PACKET_TYPE   <= rx(0) & rx(1);
                  P_O_CMD_REC.PACKET_ID     <= rx(2) & rx(3);
                  P_O_CMD_REC.TYPE_MODIFIER <= rx(4) & rx(5);
                  P_O_CMD_REC.SPECIFIER     <= rx(6) & rx(7);
                  P_O_CMD_REC.DATA_LENGTH   <= rx(8) & rx(9);
                  cmd12                     <= rx(4) & rx(5);
                  cmd34                     <= rx(8) & rx(9);
                  cmd56                     <= rx(6) & rx(7);
               when S40 => null;
               when S41 =>
                  rx(10) <= rx_data;                 -- save byte #10 of the command - data byte 0
               when S42 => null;
               when S43 =>
                  rx(11) <= rx_data;                 -- save byte #11 of the command - data byte 1
               when S44 =>
                  P_O_CMD_REC.DATA <= rx(10) & rx(11);
                  cmd78            <= rx(10) & rx(11);
               when S71 =>
                  rx_end <= '1';                     -- command receive end activated
--      when S72 =>
               when S73 =>
                  rx_end    <= '0';                  -- command receive end deactivated
                  cmd_ready <= '1';                  -- command ready activated
               when S74 => null;
               when S75 => null;
               when S80 => null;
               when S81 =>
                  rx(12) <= rx_data;                 -- save byte #12 of the command - CRC byte 0
               when S82 => null;
               when S83 =>
                  rx(13) <= rx_data;                 -- save byte #13 of the command - CRC byte 1
                  rx_end <= '1';
               when S84 =>
                  rx_end          <= '0';
                  cmd_ready       <= '0';            -- command ready deactivated
                  P_O_CMD_REC.CRC <= rx(12) & rx(13);
--                P_O_CMD_REC.CRC           <= x"ABAB";
               when S86 =>
                  acq_start_cmd <= '1';
                  cmd_rx_dbg    <= '1';
               when S90 =>
                  cmd_done  <= '1';                  -- set command done to activate command ack
                  cmd_ready <= '0';                  -- command ready deactivated
               when S91 => null;
               when S92 =>                           -- waiting up to command is finished
                  cmd_done      <= '0';              -- deactivated command done
                  cmd_end       <= '1';              -- activated command end
                  acq_start_cmd <= '0';
                  cmd_rx_dbg    <= '0';
               when S93 => null;
               when S94 =>                           -- waiting up to command is finished
                  cmd_end     <= '0';                -- deactivated command end
                  cmd_rx_dbg  <= '1';
                  P_O_CMD_REC <= C_CMD_REC;
                  cmd12       <= (others => '0');
                  cmd34       <= (others => '0');
                  cmd56       <= (others => '0');
                  cmd78       <= (others => '0');
               when SB01 =>                          -- waiting up to command is finished
                  cmd_done      <= '0';              -- deactivated command done
                  cmd_end       <= '1';              -- activated command end
                  acq_start_cmd <= '0';
                  cmd_rx_dbg    <= '0';
               when SB02 => null;
               when SB03 =>
                  cmd_end                   <= '0';  -- deactivated command end
                  cmd_rx_dbg                <= '1';
                  --prepare a readout command
                  P_O_CMD_REC.PACKET_TYPE   <= X"CC02";
                  P_O_CMD_REC.PACKET_ID     <= ro_cycle & X"0F";
                  P_O_CMD_REC.TYPE_MODIFIER <= X"000E";
                  P_O_CMD_REC.SPECIFIER     <= X"0003";
                  P_O_CMD_REC.DATA_LENGTH   <= X"0000";
                  P_O_CMD_REC.CRC           <= X"ABAB";
                  rx(12)                    <= X"AB";
                  rx(13)                    <= X"AB";
                  cmd12                     <= X"000E";
                  cmd34                     <= X"0000";
                  cmd56                     <= X"0003";
                  cmd78                     <= X"ABAB";
               when SB10 =>
                  cmd_ready <= '1';                  -- command ready activated
               when SB11 => null;
            end case;
         end if;  --strobe
      end if;  --clk
   end process P2;

--  P3 : process (clk, rst, state, rx_end, rx)
--  begin
--    if rst='1' then
--         P_O_CMD_REC <= C_CMD_REC;
--      cmd12 <= (others => '0');
--      cmd34 <= (others => '0');
--      cmd56 <= (others => '0');
--      cmd78 <= (others => '0');
--    elsif rising_edge (clk) then
--      if (state = INIT) then
--                P_O_CMD_REC <= C_CMD_REC;
--        cmd12 <= (others => '0');
--        cmd34 <= (others => '0');
--        cmd56 <= (others => '0');
--        cmd78 <= (others => '0');
--      end if;    
--      if (rx_end = '1') then
--                P_O_CMD_REC.PACKET_TYPE   <= rx(0)  & rx(1);
--                P_O_CMD_REC.PACKET_ID     <= rx(2)  & rx(3);
--                P_O_CMD_REC.TYPE_MODIFIER <= rx(4)  & rx(5);
--                P_O_CMD_REC.SPECIFIER     <= rx(6)  & rx(7);
--                P_O_CMD_REC.DATA_LENGTH   <= rx(8)  & rx(9);
--                P_O_CMD_REC.DATA          <= rx(10) & rx(11);
--                P_O_CMD_REC.CRC           <= rx(12) & rx(13);
--        cmd12 <= rx(4) & rx(5);
--        cmd34 <= rx(8) & rx(9);
--        cmd56 <= rx(6) & rx(7);
--        cmd78 <= rx(10) & rx(11);
--      end if;
--    end if;
--  end process P3;

   reg_ready <= rx_end;
   --P4 : process (rst, rx_end)
   --begin
   --   if rst = '1' then
   --      reg_ready <= '0';
   --   else
   --      reg_ready <= rx_end;
   --   end if;
   --end process P4;
   
end architecture rtl;
