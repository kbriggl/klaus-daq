library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.adif2_types.all;

entity cmd_ack is
   port(
      -- clk          : in std_logic;
      fclk         : in std_logic;      --clk 40MHz
      clk_strobe_5 : in std_logic;      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock

      rst : in std_logic;

      ack_end     : out std_logic;
      P_I_CMD_REC : in  T_CMD_REC;      -- command record
--    cmd12         : IN     std_logic_vector (15 DOWNTO 0);
--    cmd34         : IN     std_logic_vector (15 DOWNTO 0);
--    cmd56         : IN     std_logic_vector (15 DOWNTO 0);

      usb_txen   : in  std_logic;
      writing    : in  std_logic;
      ack_wr_req : out std_logic;
      ack_data   : out std_logic_vector (7 downto 0);

      ack_dbg  : out std_logic;
      cmd_done : in  std_logic
--    acq_cmd_done  : IN     std_logic;
--    cal_cmd_done  : IN     std_logic;
--    fast_cmd_done : IN     std_logic;
--    ro_cmd_done   : IN     std_logic;
--    sc_cmd_done   : IN     std_logic;
--    stat_cmd_done : IN     std_logic;
--    std_cmd_done  : IN     std_logic
      );
end cmd_ack;

architecture rtl of cmd_ack is

   type states is (INIT,
                   S01, S02, S03, S05, S06, S07,
                   S11, S12, S13, S15, S16, S17,
                   S21, S22, S23, S25, S26, S27,
                   S31, S32, S33, S35, S36, S37,
                   S41, S42, S43, S45, S46, S47, S48,
                   S51, S52, S53, S55, S56, S57,
                   S61, S62, S63, S65, S66, S67,
                   S91, S92, S93, S94);
   signal state, next_state : states;

begin

   P0 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            state <= INIT;
         elsif (clk_strobe_5 = '1') then
            state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process P0;

   P1 : process (state, P_I_CMD_REC, writing, usb_txen, cmd_done)
--                  std_cmd_done, stat_cmd_done, fast_cmd_done, sc_cmd_done, 
--                  cal_cmd_done, acq_cmd_done, ro_cmd_done ) 
   begin
      case state is
         when INIT =>
--          if (std_cmd_done = '1' OR stat_cmd_done = '1' OR fast_cmd_done = '1' OR sc_cmd_done = '1' 
--              OR cal_cmd_done = '1' OR acq_cmd_done = '1' OR ro_cmd_done = '1') then
            if cmd_done = '1' then
               next_state <= S01;
            else
               next_state <= INIT;
            end if;

         -- PACKET_TYPE 
         when S01 =>
            next_state <= S02;
         when S02 =>
            if writing = '1' then
               next_state <= S03;
            else
               next_state <= S02;
            end if;
         when S03 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S05;
            else
               next_state <= S03;
            end if;
            
         when S05 =>
            next_state <= S06;
         when S06 =>
            if writing = '1' then
               next_state <= S07;
            else
               next_state <= S06;
            end if;
         when S07 =>
            if P_I_CMD_REC.PACKET_TYPE(15 downto 8) = x"E3" then  -- K28.3 mark fast command
               next_state <= S91;
            elsif (writing = '0' and usb_txen = '0') then
               next_state <= S11;
            else
               next_state <= S07;
            end if;

         -- PACKET_ID
         when S11 =>
            next_state <= S12;
         when S12 =>
            if writing = '1' then
               next_state <= S13;
            else
               next_state <= S12;
            end if;
         when S13 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S15;
            else
               next_state <= S13;
            end if;

         when S15 =>
            next_state <= S16;
         when S16 =>
            if writing = '1' then
               next_state <= S17;
            else
               next_state <= S16;
            end if;
         when S17 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S21;
            else
               next_state <= S17;
            end if;

         -- TYPE_MODIFIER
         when S21 =>
            next_state <= S22;
         when S22 =>
            if writing = '1' then
               next_state <= S23;
            else
               next_state <= S22;
            end if;
         when S23 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S25;
            else
               next_state <= S23;
            end if;

         when S25 =>
            next_state <= S26;
         when S26 =>
            if writing = '1' then
               next_state <= S27;
            else
               next_state <= S26;
            end if;
         when S27 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S31;
            else
               next_state <= S27;
            end if;

         -- SPECIFIER
         when S31 =>
            next_state <= S32;
         when S32 =>
            if writing = '1' then
               next_state <= S33;
            else
               next_state <= S32;
            end if;
         when S33 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S35;
            else
               next_state <= S33;
            end if;

         when S35 =>
            next_state <= S36;
         when S36 =>
            if writing = '1' then
               next_state <= S37;
            else
               next_state <= S36;
            end if;
         when S37 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S41;
            else
               next_state <= S37;
            end if;

         -- DATA_LENGTH
         when S41 =>
            next_state <= S42;
         when S42 =>
            if writing = '1' then
               next_state <= S43;
            else
               next_state <= S42;
            end if;
         when S43 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S45;
            else
               next_state <= S43;
            end if;

         when S45 =>
            next_state <= S46;
         when S46 =>
            if writing = '1' then
               next_state <= S47;
            else
               next_state <= S46;
            end if;

         when S47 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S48;
            else
               next_state <= S47;
            end if;

         --when S48 =>
         --   next_state <= S61;
         when S48 =>
            if P_I_CMD_REC.DATA_LENGTH = X"0001" then  -- data word was received -> let's send it back
               next_state <= S51;
            else
               next_state <= S61;
            end if;

         -- DATA
         when S51=>
            next_state <= S52;
         when S52 =>
            if writing = '1' then
               next_state <= S53;
            else
               next_state <= S52;
            end if;
         when S53 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S55;
            else
               next_state <= S53;
            end if;

         when S55=>
            next_state <= S56;
         when S56 =>
            if writing = '1' then
               next_state <= S57;
            else
               next_state <= S56;
            end if;
         when S57 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S61;
            --next_state <= S91;
            else
               next_state <= S57;
            end if;

         -- CRC
         when S61 =>
            next_state <= S62;
         when S62 =>
            if writing = '1' then
               next_state <= S63;
            else
               next_state <= S62;
            end if;
         when S63 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S65;
            else
               next_state <= S63;
            end if;

         when S65 =>
            next_state <= S66;
         when S66 =>
            if writing = '1' then
               next_state <= S67;
            else
               next_state <= S66;
            end if;
         when S67 =>
            if (writing = '0' and usb_txen = '0') then
               next_state <= S91;
            else
               next_state <= S67;
            end if;

         when S91 =>
            next_state <= S92;

         when S92 =>
            if cmd_done = '0' then
               next_state <= S93;
            else
               next_state <= S92;
            end if;
            
         when S93 =>
            next_state <= S94;

         when S94 =>
            next_state <= INIT;
      end case;
   end process P1;

   P2 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            ack_wr_req <= '0';
            ack_end    <= '0';
            ack_data   <= x"00";
            ack_dbg    <= '0';
         elsif (clk_strobe_5 = '1') then
            case state is
               when INIT =>
                  ack_wr_req <= '0';
                  ack_end    <= '0';
--            ack_data <= x"CC"; 
                  ack_data   <= x"00";
                  ack_dbg    <= '0';
               when S01 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.PACKET_TYPE(15 downto 8);
                  ack_dbg    <= '1';
               when S02 =>
                  ack_wr_req <= '0';
               when S03 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S05 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.PACKET_TYPE(7 downto 0);
                  ack_dbg    <= '1';
               when S06 =>
                  ack_wr_req <= '0';
               when S07 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';

               when S11 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.PACKET_ID(15 downto 8);
                  ack_dbg    <= '1';
               when S12 =>
                  ack_wr_req <= '0';
               when S13 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S15 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.PACKET_ID(7 downto 0);
                  ack_dbg    <= '1';
               when S16 =>
                  ack_wr_req <= '0';
               when S17 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';

               when S21 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.TYPE_MODIFIER(15 downto 8);
                  ack_dbg    <= '1';
               when S22 =>
                  ack_wr_req <= '0';
               when S23 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S25 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.TYPE_MODIFIER(7 downto 0);
                  ack_dbg    <= '1';
               when S26 =>
                  ack_wr_req <= '0';
               when S27 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';

               when S31 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.SPECIFIER(15 downto 8);
                  ack_dbg    <= '1';
               when S32 =>
                  ack_wr_req <= '0';
               when S33 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S35 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.SPECIFIER(7 downto 0);
                  ack_dbg    <= '1';
               when S36 =>
                  ack_wr_req <= '0';
               when S37 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';

               when S41 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.DATA_LENGTH(15 downto 8);
                  ack_dbg    <= '1';
               when S42 =>
                  ack_wr_req <= '0';
               when S43 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S45 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.DATA_LENGTH(7 downto 0);
                  ack_dbg    <= '1';
               when S46 =>
                  ack_wr_req <= '0';
               when S47 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S48 =>

               when S51 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.DATA(15 downto 8);
                  ack_dbg    <= '1';
               when S52 =>
                  ack_wr_req <= '0';
               when S53 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S55 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.DATA(7 downto 0);
                  ack_dbg    <= '1';
               when S56 =>
                  ack_wr_req <= '0';
               when S57 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';

               when S61 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.CRC(15 downto 8);
                  ack_dbg    <= '1';
               when S62 =>
                  ack_wr_req <= '0';
               when S63 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
               when S65 =>
                  ack_wr_req <= '1';
                  ack_data   <= P_I_CMD_REC.CRC(7 downto 0);
                  ack_dbg    <= '1';
               when S66 =>
                  ack_wr_req <= '0';
               when S67 =>
                  ack_wr_req <= '0';
                  ack_dbg    <= '0';
                  
               when S91 =>
                  ack_end <= '1';
                  ack_dbg <= '1';
               when S92 =>
                  ack_dbg <= '0';
               when S93 =>
                  ack_end <= '0';
                  ack_dbg <= '1';
               when S94 =>
                  ack_dbg <= '0';
                  
            end case;
         end if;  --strobe
      end if;  --clk
   end process P2;

end architecture rtl;

