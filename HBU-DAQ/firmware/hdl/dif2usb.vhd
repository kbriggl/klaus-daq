
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

entity dif2usb is
   port (
      -- clk          : in  std_logic;                      -- external clock
      fclk         : in  std_logic;                      --clk 40MHz
      clk_strobe_5 : in  std_logic;                      --synchronous strobe to be used as a clock gate (or CE) for the fclk in order to get the 'old style' 5MHz clock
      rst          : in  std_logic;                      -- global reset
      usb_txen     : in  std_logic;                      -- external USB FIFO signal
      usb_wr       : out std_logic;                      -- data transfer cycle ( FPGA -> USB )
      usb_data     : out std_logic_vector (7 downto 0);  -- USB FIFO data
      wr_req       : in  std_logic;                      -- request to transfer data from FPGA to FIFO
      writing      : out std_logic;                      -- data transfer cycle ( FPGA -> USB )
      tx_data      : in  std_logic_vector (7 downto 0);  -- FPGA register data (out)
      if_enable    : in  std_logic                       --interface is enabled    
      );
end entity dif2usb;

architecture rtl of dif2usb is

   type states is (INIT, S1, S2, S3, S4, S5);
   signal state, next_state : states;
   signal usb_txen_int      : std_logic;

begin
   
   P0 : process (fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            state <= INIT;
         elsif (clk_strobe_5 = '1')then
            state <= next_state;
         end if;  --strobe
      end if;  --clk
   end process P0;

   P3 : process (fclk)
   begin
      if rising_edge (fclk) then
         if rst = '1' then
            usb_txen_int <= '0';
         elsif (clk_strobe_5 = '1') then
            usb_txen_int <= usb_txen;
         end if;  --strobe
      end if;
   end process P3;

   P1 : process (if_enable, state, usb_txen_int, wr_req)
   begin
      case state is
         when INIT =>
            if (wr_req = '1') and (usb_txen_int = '0') and (if_enable = '1') then
               --FPGA wants to send data to the USB FIFO and USB FIFO available
               next_state <= S1;
            else
               next_state <= INIT;
            end if;
         when S1 =>                     --start transfer cycle + data available to USB FIFO
            next_state <= S2;
         when S2 =>                     --data available one more cycle
            next_state <= S3;
         when S3 =>                     --data available one more cycle
            next_state <= S4;
         when S4 =>                     --data available one more cycle
            next_state <= S5;
         when S5 =>                     --release write and data and wait for txen realease
            if usb_txen_int = '1' then
               next_state <= INIT;
            else
               next_state <= S5;
            end if;
      end case;
   end process P1;

   P2 : process(fclk)
   begin
      if rising_edge(fclk) then
         if rst = '1' then
            usb_wr   <= '0';
            usb_data <= (others => 'Z');
            writing  <= '0';
         elsif (clk_strobe_5 = '1') then
            case state is
               when INIT =>
                  usb_wr   <= '0';
                  usb_data <= (others => 'Z');
                  writing  <= '0';
               when S1 =>
                  usb_wr   <= '1';
                  usb_data <= tx_data;
                  writing  <= '1';
               when S2 => null;
               when S3 => null;
               when S4 =>
                  usb_wr <= '0';
               when S5 =>
                  usb_data <= (others => 'Z');
            end case;
         end if;  --strobe
      end if;  --clk
   end process P2;

end architecture rtl;

