create_clock -period 10.000 -name GCLK -waveform {0.000 5.000} [get_ports GCLK]
create_clock -period 25.000 -name HDMI_CLK_P -waveform {0.000 12.500} [get_ports HDMI_CLK_P]
#IO is synth. name for ...
#MMCME2_BASE_inst_n_2 is name for ...


create_generated_clock -name pll_board_40 \
    -source [get_pins clk_ctrl_inst/PLLE2_BASE_inst/CLKIN1] \
    [get_pins clk_ctrl_inst/PLLE2_BASE_inst/CLKOUT0]\



create_generated_clock -name mmcm_ccc_fb \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN1] \ 
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKFBOUT]
    
create_generated_clock -name mmcm_board_fb \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN2] \
    -master_clock pll_board_40 \
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKFBOUT]


create_generated_clock -name mmcm_ccc_40 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN1] \ 
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT0]
    
create_generated_clock -name mmcm_board_40 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN2] \
    -master_clock pll_board_40 \
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT0]
    
create_generated_clock -name mmcm_ccc_160 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN1] \ 
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT1]
    
create_generated_clock -name mmcm_board_160 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN2] \
    -master_clock pll_board_40 \
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT1]
    
create_generated_clock -name mmcm_ccc_320 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN1] \ 
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT2]
    
create_generated_clock -name mmcm_board_320 \
    -source [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKIN2] \
    -master_clock pll_board_40 \
    [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT2]


set_clock_groups -name exclusive_mmcm_b40c40   -logically_exclusive  -group mmcm_board_40  -group mmcm_ccc_40
set_clock_groups -name exclusive_mmcm_b40c160  -logically_exclusive  -group mmcm_board_40  -group mmcm_ccc_160
set_clock_groups -name exclusive_mmcm_b40c160  -logically_exclusive  -group mmcm_board_40  -group mmcm_ccc_320

set_clock_groups -name exclusive_mmcm_b160c40  -logically_exclusive  -group mmcm_board_160 -group mmcm_ccc_40
set_clock_groups -name exclusive_mmcm_b160c160 -logically_exclusive  -group mmcm_board_160 -group mmcm_ccc_160
set_clock_groups -name exclusive_mmcm_b160c320 -logically_exclusive  -group mmcm_board_160 -group mmcm_ccc_320

set_clock_groups -name exclusive_mmcm_b320c40  -logically_exclusive  -group mmcm_board_320 -group mmcm_ccc_40
set_clock_groups -name exclusive_mmcm_b320c160 -logically_exclusive  -group mmcm_board_320 -group mmcm_ccc_160
set_clock_groups -name exclusive_mmcm_b320c320 -logically_exclusive  -group mmcm_board_320 -group mmcm_ccc_320

set_clock_groups -name exclusive_mmcm_fb -physically_exclusive -group mmcm_board_fb -group mmcm_ccc_fb

set_clock_groups -asynchronous -group [get_clocks {GCLK}] -group [get_clocks {HDMI_CLK_P}]                                

#set_clock_groups -asynchronous -group [get_clocks {mmcm_ccc_160}] -group [get_clocks {mmcm_board_40}]
#set_clock_groups -asynchronous -group [get_clocks {mmcm_ccc_40}] -group [get_clocks {pll_board_40}]


set_false_path -from [get_pins acq_start_inst/clk_40M_inhibit_acq_reg/C] -to [get_pins {LED_inhibit_sync_reg[0]/D}]
set_false_path -from [get_pins clk_ctrl_inst/presence_counter_hdmi_reg[1]/C] -to [get_pins {clk_ctrl_inst/pc_sync_reg_reg[0]/D}]

#very weird, should have been covered already by set_clock_groups -name exclusive_mmcm_b40c160  -physically_exclusive  -group mmcm_board_40  -group mmcm_ccc_160 
set_clock_groups -logically_exclusive -group \ 
  [get_clocks -of_objects [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT1] -filter {IS_GENERATED && MASTER_CLOCK == HDMI_CLK_P}] \
              -group [get_clocks -of_objects [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT0] -filter {IS_GENERATED && MASTER_CLOCK == pll_board_40}]
              
#set_false_path -from [get_clocks -of_objects [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT1] -filter {IS_GENERATED && MASTER_CLOCK == HDMI_CLK_P}] 
#               -to [get_clocks -of_objects [get_pins clk_ctrl_inst/MMCME2_ADV_inst/CLKOUT0] -filter {IS_GENERATED && MASTER_CLOCK == HDMI_CLK_P}]              
    
#    -add -master_clock MMCME2_BASE_inst_n_2 \
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/O]


#create_generated_clock -name bufg_board \
#    -source [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/I0] -divide_by 1 \ 
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/O]

#create_generated_clock -name bufg_ccc \
#    -source [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/I1] -multiply_by 1 \ 
#    -add -master_clock MMCME2_BASE_inst_n_2 \
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/O]

#create_generated_clock -name bufg_ccc \
#    -source [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/I1] -divide_by_by 1 \ 
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/O] -add

#create_generated_clock -name BUFG_board -divide_by 1 \
#    -source [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/I0] \ 
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/0]

#create_generated_clock -name BUFG_ccc -divide_by 1 \
#    -source [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/I1] \ 
#    [get_pins clk_ctrl_inst/clk_40m_BUFGMUX_inst/0] -add

#set_clock_groups -logically_exclusive -group [get_clocks bufg_board] -group [get_clocks bufg_ccc]


    
#set_false_path -from [get_clocks bufg_ccc] -to [get_clocks GCLK]
#set_false_path -from [get_clocks bufg_board] -to [get_clocks GCLK]
#set_clock_groups -name logically_exclusive -logically_exclusive -group [get_clocks -include_generated_clocks I0] -group [get_clocks -include_generated_clocks MMCME2_BASE_inst_n_2]
#set_clock_groups -name logically_exclusive -logically_exclusive \
#    -group [get_clocks [list [get_clocks -of_objects [get_pins clk_ctrl_inst/PLLE2_BASE_inst/CLKOUT0]]]] \
#    -group [get_clocks [list [get_clocks -of_objects [get_pins clk_ctrl_inst/MMCME2_BASE_inst/CLKOUT0]]]]
    
    
#set_input_jitter GCLK 1.000


