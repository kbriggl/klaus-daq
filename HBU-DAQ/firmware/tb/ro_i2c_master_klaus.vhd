library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
USE ieee.math_real.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ro_i2c_master_klaus is
        --
end ro_i2c_master_klaus;

architecture behavior   of ro_i2c_master_klaus is

    component i2c_master_klaus is
	port(
		--SYSTEM SIGNALS
		i_clk : in std_logic;						        --SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
		i_rst : in std_logic;						        --RESET SIGNAL FOR THE i2c COMPONENT
		i_i2c_data_in   : in  std_logic_vector(8 downto 0); 
		o_i2c_data_out  : out std_logic_vector(8 downto 0);

		--CONTROL SIGNALS FROM THE DAQ CONTROL
		i_start_trans 	: in  std_logic;					--START TRANSMISSION SIGNAL FROM THE DAQ CONTROL
        i_slave_addr    : in  std_logic_vector(6 downto 0); -- slave addr
        i_reg_addr      : in  std_logic_vector(7 downto 0); -- reg addr
        i_RWbit         : in  std_logic;                    -- 1: read; 2: write
		o_i2c_done 	    : out std_logic;					--END OF TRANSMISSION SIGNAL

        -- KLauS event readout related control signal
        i_evt_ro_mode   : in  std_logic;                    -- KLauS event read access
        i_evt_empty     : in  std_logic;                    -- KLauS FIFO empty
        o_byte_ready    : out std_logic;                    -- KLauS event byte is ready
        o_event_ready   : out std_logic;                    -- KLauS event ready

		-- i2c lines
		scl_pad_i     : in  std_logic;                    -- i2c clock line input
		scl_pad_o     : out std_logic;                    -- i2c clock line output
		scl_padoen_o  : out std_logic;                    -- i2c clock line output enable, active low
		sda_pad_i     : in  std_logic;                    -- i2c data line input
		sda_pad_o     : out std_logic;                    -- i2c data line output
		sda_padoen_o  : out std_logic                     -- i2c data line output enable, active low
	);
    end component i2c_master_klaus;

    signal fclk         : std_logic;
    signal srst_40      : std_logic;
    
    signal  i2c_trans_start : std_logic;
    signal  i2c_addr        : std_logic_vector(6 downto 0) := "1100000";
    signal  i2c_rw          : std_logic := '1';
    signal  i2c_busy        : std_logic;
    signal  i2c_data_wr     : std_logic_vector(7 downto 0) := "00000000";
    signal  i2c_data_rd     : std_logic_vector(7 downto 0);
    signal  i2c_ack_error   : std_logic;
    signal  i2c_sda_i       : std_logic := '1';
    signal  i2c_sda_o       : std_logic;
    signal  i2c_sda_oen     : std_logic;
    signal  i2c_scl_i       : std_logic := '1';
    signal  i2c_scl_o       : std_logic;
    signal  i2c_scl_oen     : std_logic;
    signal  i2c_sda         : std_logic;
	signal	i2c_byte_ready	: std_logic;
	signal	i2c_evt_ready	: std_logic;
	signal	i2c_slave_empty	: std_logic;

    signal  i2c_reg_addr    : std_logic_vector(7 downto 0) := "00000000";
    signal  i2c_done        : std_logic;
        
    constant fclk_period : time := 25 ns;

begin
    
    inst_i2c_master: i2c_master_klaus
    port map(
		i_clk 		    => fclk,
        i_rst           => srst_40,
		i_i2c_data_in   => "00000000",
		o_i2c_data_out  => i2c_data_rd,

		i_start_trans   => i2c_trans_start,
        i_slave_addr    => i2c_addr,
        i_reg_addr      => i2c_reg_addr,
        i_RWbit         => i2c_rw,
		o_i2c_done 	    => i2c_done,

        i_evt_ro_mode   => '1',
        i_evt_empty     => '0',
        o_byte_ready    => i2c_byte_ready,
        o_event_ready   => i2c_evt_ready,

		-- i2c lines
		scl_pad_i       => i2c_scl_i,
		scl_pad_o       => i2c_scl_o,
		scl_padoen_o    => i2c_scl_oen,
		sda_pad_i       => i2c_sda_i,
		sda_pad_o       => i2c_sda_o,
		sda_padoen_o    => i2c_sda_oen
                
    );
    
    -- clock generation
    fclk_proc : process
    begin
            fclk <= '1';
            wait for 12.5 ns;
            fclk <= '0';
            wait for 12.5 ns;
    end process fclk_proc;
    
    -- reset generation
    reset_proc: process
    begin
            i2c_trans_start <= '0';
            srst_40         <= '1';
            wait for 75 ns;
            srst_40         <= '0';
            wait for 100 ns;
            i2c_trans_start <= '1';
    
            wait for 50 us;
            i2c_trans_start <= '0';
            wait for 10 us;
    end process stim_proc;
    
    -- slave stim
    slave_proc: process
    begin
        --
    end process slave_proc;

end architecture behavior;
