----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/10/2018 05:59:37 PM
-- Design Name: 
-- Module Name: roc_cnt_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
USE ieee.math_real.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity roc_cnt_tb is
--  Port ( );
end roc_cnt_tb;

architecture Behavioral of roc_cnt_tb is
   component roc_cnt is
      generic (
         RO_CYCLE_BITS : natural range 8 to 32);
      port (
         fclk         : in  std_logic;
         clk_strobe_5 : in  std_logic;
         srst_40      : in  std_logic;
         clk_syn      : in  std_logic;
         acq_stop_rx  : in  std_logic;
         ro_cycle     : out std_logic_vector(RO_CYCLE_BITS-1 downto 0));
   end component roc_cnt;
   signal fclk         : std_logic;
   signal clk_strobe_5 : std_logic;
   signal srst_40      : std_logic;
   signal clk_syn      : std_logic;
   signal acq_stop_rx  : std_logic;
   signal ro_cycle     : std_logic_vector(7 downto 0);

   constant fclk_period : time := 25 ns;

begin
   fclk_proc : process
   begin
      fclk <= '1';
      wait for 12.5 ns;
      fclk <= '0';
      wait for 12.5 ns;
   end process;

   roc_cnt_1 : roc_cnt
      generic map (
         RO_CYCLE_BITS => 8)
      port map (
         fclk         => fclk,
         clk_strobe_5 => clk_strobe_5,
         srst_40      => srst_40,
         clk_syn      => clk_syn,
         acq_stop_rx  => acq_stop_rx,
         ro_cycle     => ro_cycle);


   srst_proc : process
   begin
      srst_40 <= '0';
      wait until rising_edge(fclk);
      srst_40 <= '1';
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      srst_40 <= '0';
      wait;
   end process;

   clk_strobe_proc : process
   begin
      clk_strobe_5 <= '0';
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      wait until rising_edge(fclk);
      clk_strobe_5 <= '1';
      wait until rising_edge(fclk);
   end process;

   roc_increment_proc : process
      variable seed1, seed2 : positive;  -- Seed values for the random generator
      variable rand         : real;      -- Random value (0 to 1.0 range)
   begin
      acq_stop_rx <= '0';
      clk_syn     <= '0';
      wait for 50000 ns;
      wait until rising_edge(fclk);
      clk_syn     <= '1';
      wait until rising_edge(fclk);
      clk_syn     <= '0';
      UNIFORM(seed1, seed2, rand);
      while rand > 0.05 loop
         UNIFORM(seed1, seed2, rand);
         wait for 1000*fclk_period*rand;
         wait until rising_edge(clk_strobe_5);
         wait until rising_edge(fclk);
         acq_stop_rx <= '1';
         wait for 100*fclk_period*rand;
         wait until rising_edge(clk_strobe_5);
         wait until rising_edge(fclk);
         acq_stop_rx <= '0';
         UNIFORM(seed1, seed2, rand);
      end loop;

      
   end process;
   
end Behavioral;
