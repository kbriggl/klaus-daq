----------------------------------------------------------------------------------
-- Company: KIP, Uni-Heidelberg
-- Engineer: Z. Yuan
-- 
-- Create Date: 08/08/2019
-- Design Name: 
-- Module Name: ro_i2c_master - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
USE ieee.math_real.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ro_i2c_master_tb is
        --
end ro_i2c_master_tb;

architecture behavior   of ro_i2c_master_tb is

	component i2c_master_klaus is
	port(
		--SYSTEM SIGNALS
		i_clk : in std_logic;						--SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
		i_rst : in std_logic;						--RESET SIGNAL FOR THE i2c COMPONENT
		i_i2c_data_in : in std_logic_vector(31 downto 0);
		o_i2c_data_out : out std_logic_vector(31 downto 0);

		--CONTROL SIGNALS FROM THE DAQ CONTROL
		i_start_trans 	: in std_logic;		--START TRANSMISSION SIGNAL FROM THE DAQ CONTROL
		o_i2c_done 	: out std_logic;	--END OF TRANSMISSION SIGNAL

		-- i2c lines
		scl_pad_i     : in  std_logic;		-- i2c clock line input
		scl_pad_o     : out std_logic;		-- i2c clock line output
		scl_padoen_o  : out std_logic;		-- i2c clock line output enable, active low
		sda_pad_i     : in  std_logic;		-- i2c data line input
		sda_pad_o     : out std_logic;		-- i2c data line output
		sda_padoen_o  : out std_logic 		-- i2c data line output enable, active low
	);
	end component i2c_master_klaus;

        signal fclk         : std_logic;
        signal srst_40      : std_logic;

        signal  i2c_trans_start : std_logic;
        signal  i2c_addr        : std_logic_vector(6 downto 0) := "1100000";
        signal  i2c_rw          : std_logic := '1';
        signal  i2c_busy        : std_logic;
        signal  i2c_data_wr     : std_logic_vector(7 downto 0) := "00000000";
        signal  i2c_data_rd     : std_logic_vector(7 downto 0);
        signal  i2c_ack_error   : std_logic;
        signal  i2c_sda_i       : std_logic := '1';
        signal  i2c_sda_o       : std_logic;
        signal  i2c_sda_oen     : std_logic;
        signal  i2c_scl_i       : std_logic := '1';
        signal  i2c_scl_o       : std_logic;
        signal  i2c_scl_oen     : std_logic;
        signal  i2c_sda         : std_logic;
	signal	i2c_byte_ready	: std_logic;
	signal	i2c_evt_ready	: std_logic;
	signal	i2c_slave_empty	: std_logic;
        
        constant fclk_period : time := 25 ns;

begin


        fclk_proc : process
        begin
                fclk <= '1';
                wait for 12.5 ns;
                fclk <= '0';
                wait for 12.5 ns;
        end process fclk_proc;


        inst_i2c_master: ro_i2c_master
        generic map(
                speed		=>  5000000)
        port map(
                fclk            => fclk,
                frst		=> srst_40,
                ena             => i2c_trans_start,
                addr            => i2c_addr,
                data    	=> i2c_data_rd,
		empty		=> i2c_slave_empty,
                ack_err         => i2c_ack_error,
                busy            => i2c_busy,
                byte_ready      => i2c_byte_ready,
                sda_i           => i2c_sda_i,
                sda_o           => i2c_sda_o,
                sda_oen         => i2c_sda_oen,
                scl_i           => i2c_scl_i,
                scl_o           => i2c_scl_o,
                scl_oen         => i2c_scl_oen
                );

        stim_proc: process
        begin
                i2c_trans_start <= '0';
                srst_40         <= '1';
                wait for 75 ns;
                srst_40         <= '0';
                wait for 100 ns;
                i2c_trans_start <= '1';

                wait until i2c_slave_empty = '1';
                wait until i2c_slave_empty = '0';
                --wait until i2c_byte_ready = '1';
                --wait until i2c_byte_ready = '1';
                --wait until i2c_byte_ready = '1';
                --wait until i2c_byte_ready = '1';
                --wait until i2c_byte_ready = '1';
                --wait until i2c_scl_o = '1';
                i2c_trans_start <= '0';
                wait for 10 us;
        end process stim_proc;

        i2c_slave: process
        begin
                i2c_sda_i       <= '1';
		i2c_scl_i	<= '1';		-- when read, the slave will not stretch the scl line

		-- ack
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';

		-- 1st byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		-- 2nd byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		-- 3rd byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		-- 4th byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		-- 5th byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		-- 1st byte
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '1';
		wait until i2c_scl_o = '0';	i2c_sda_i <= '0';
                wait until i2c_scl_o = '0';	i2c_sda_i <= '1';       -- wait for master ack
		
		wait until i2c_scl_o = '1';	i2c_sda_i <= '1';

        end process i2c_slave;


end architecture behavior;
