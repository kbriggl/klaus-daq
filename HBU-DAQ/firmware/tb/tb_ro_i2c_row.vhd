library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
USE ieee.math_real.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library work;
use work.adif2_types.all;

entity  tb_ro_i2c_row   is

end entity tb_ro_i2c_row;


architecture tb of tb_ro_i2c_row is

    component ro_i2c_row is
    port(
        fclk         : in std_logic;      --clk 40MHz
        clk_strobe_5 : in std_logic;      
        rst          : in std_logic;
        
        -- i2c interface
        i2c_sda_i	: in	std_logic;
        i2c_sda_o	: out	std_logic;
        i2c_sda_oen	: out	std_logic;
        i2c_scl_i	: in	std_logic;
        i2c_scl_o	: out	std_logic;
        i2c_scl_oen	: out	std_logic;
        
        ro_rx_start : in  std_logic;
        ro_rx_end   : out std_logic;
        
        ro_tx_start : in  std_logic;
        ro_tx_end   : out std_logic;
        
        asic_ix : out unsigned(3 downto 0);	-- record maximum number of ASIC recorded
        addr_pt : out POINTER;
        
        debug	: out	std_logic_vector(7 downto 0);
        
        ro_rd_addr : in  std_logic_vector (14 downto 0);
        ro_mem_out : out std_logic_vector (7 downto 0);
        
        ro_done : in std_logic
        );
    end component ro_i2c_row;

    signal fclk, rst, clk_strobe_5  : std_logic;
    signal ro_rx_start, ro_rx_end   : std_logic;
    signal ro_tx_start, ro_tx_end   : std_logic;
    signal ro_rd_addr   : std_logic_vector(14 downto 0);
    signal ro_done      : std_logic;
    signal  i2c_sda_i       : std_logic := '1';
    signal  i2c_sda_o       : std_logic := '0';
    signal  i2c_sda_oen     : std_logic := '1';
    signal  i2c_scl_i       : std_logic := '1';
    signal  i2c_scl_o       : std_logic := '0';
    signal  i2c_scl_oen     : std_logic := '1';
    signal  asic_ix         : unsigned(3 downto 0);
    signal  addr_pt         : POINTER;
    signal  ro_debug        : std_logic_vector(7 downto 0);
    signal  ro_mem_out      : std_logic_vector(7 downto 0);

begin

    inst_ro_i2c_row: ro_i2c_row
    port map(
        fclk            => fclk,
        clk_strobe_5    => clk_strobe_5,
        rst             => rst,
        
        i2c_sda_i           => i2c_sda_i,
        i2c_sda_o           => i2c_sda_o,
        i2c_sda_oen         => i2c_sda_oen,
        i2c_scl_i           => i2c_scl_i,
        i2c_scl_o           => i2c_scl_o,
        i2c_scl_oen         => i2c_scl_oen,
        
        ro_rx_start     => ro_rx_start,
        ro_rx_end       => ro_rx_end,
        
        ro_tx_start     => ro_tx_start,
        ro_tx_end       => ro_tx_end,
        
        asic_ix	 	    => asic_ix,
        addr_pt 	    => addr_pt,
        
        debug		    => ro_debug,
        
        ro_rd_addr      => ro_rd_addr,
        ro_mem_out      => ro_mem_out,
        
        ro_done         => ro_done
            
    );

    -- clock generation
    fclk_proc : process
    begin
            fclk <= '1';
            wait for 12.5 ns;
            fclk <= '0';
            wait for 12.5 ns;
    end process fclk_proc;
    strobe_proc : process
    begin
            clk_strobe_5 <= '0';
            wait for 175 ns;
            clk_strobe_5 <= '1';
            wait for 25 ns;
    end process strobe_proc;
    
    -- reset generation
    reset_proc: process
    begin
            ro_rx_start <= '0';
            rst         <= '1';
            wait for 75 ns;
            rst         <= '0';
            wait for 100 ns;
            ro_rx_start <= '1';
    
            wait for 400 ns;
            ro_rx_start <= '0';
            wait for 100 us;
    end process reset_proc;
    
    -- slave stim
    slave_proc: process
    begin
        i2c_sda_i <= '1';

        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);
        wait until rising_edge(i2c_scl_oen);

        -- slave ack
        wait for 150 ns;
        i2c_sda_i <= '0';
        wait until rising_edge(i2c_scl_oen);
        wait for 150 ns;

--        evt_loop: for i in 1 to 5 loop
--            -- data to be sent
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '0'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '0'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--            -- slave release SDA
--            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
--        end loop;
        empty_loop: for i in 1 to 1 loop
            -- data to be sent
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            i2c_sda_i <= '0'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
            -- slave release SDA
            i2c_sda_i <= '1'; wait until rising_edge(i2c_scl_oen); wait for 150 ns;
        end loop;

        wait until rising_edge(i2c_scl_oen);

    end process slave_proc;
end architecture tb;
