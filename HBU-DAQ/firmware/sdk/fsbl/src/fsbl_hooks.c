/******************************************************************************
 *
 * Copyright (C) 2012 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/

/*****************************************************************************
 *
 * @file fsbl_hooks.c
 *
 * This file provides functions that serve as user hooks.  The user can add the
 * additional functionality required into these routines.  This would help retain
 * the normal FSBL flow unchanged.
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date        Changes
 * ----- ---- -------- -------------------------------------------------------
 * 3.00a np   08/03/12 Initial release
 * </pre>
 *
 * @note
 *
 ******************************************************************************/

#include "fsbl.h"
#include "xstatus.h"
#include "fsbl_hooks.h"

/************************** Variable Definitions *****************************/

/************************** Function Prototypes ******************************/

/******************************************************************************
 * This function is the hook which will be called  before the bitstream download.
 * The user can add all the customized code required to be executed before the
 * bitstream download to this routine.
 *
 * @param None
 *
 * @return
 *		- XST_SUCCESS to indicate success
 *		- XST_FAILURE.to indicate failure
 *
 ****************************************************************************/
u32 FsblHookBeforeBitstreamDload(void) {
	u32 Status;

	Status = XST_SUCCESS;

	/*
	 * User logic to be added here. Errors to be stored in the status variable
	 * and returned
	 */
	fsbl_printf(DEBUG_INFO, "In FsblHookBeforeBitstreamDload function \r\n");

	return (Status);
}

/******************************************************************************
 * This function is the hook which will be called  after the bitstream download.
 * The user can add all the customized code required to be executed after the
 * bitstream download to this routine.
 *
 * @param None
 *
 * @return
 *		- XST_SUCCESS to indicate success
 *		- XST_FAILURE.to indicate failure
 *
 ****************************************************************************/

#define OPCODE_EXIT       0U
#define OPCODE_CLEAR      1U
#define OPCODE_WRITE      2U
#define OPCODE_MASKWRITE  3U
#define OPCODE_MASKPOLL   4U
#define OPCODE_MASKDELAY  5U
#define NEW_PS7_ERR_CODE 1
#define PS7_MASK_POLL_TIME 100000000


/* Encode number of arguments in last nibble */
#define EMIT_EXIT()                   ( (OPCODE_EXIT      << 4 ) | 0 )
#define EMIT_CLEAR(addr)              ( (OPCODE_CLEAR     << 4 ) | 1 ) , addr
#define EMIT_WRITE(addr,val)          ( (OPCODE_WRITE     << 4 ) | 2 ) , addr, val
#define EMIT_MASKWRITE(addr,mask,val) ( (OPCODE_MASKWRITE << 4 ) | 3 ) , addr, mask, val
#define EMIT_MASKPOLL(addr,mask)      ( (OPCODE_MASKPOLL  << 4 ) | 2 ) , addr, mask
#define EMIT_MASKDELAY(addr,mask)      ( (OPCODE_MASKDELAY << 4 ) | 2 ) , addr, mask

unsigned long step3[] = {
// 3.Enable L2 cache dynamic clock gating. Set l2cpl310.reg15_power_ctrl[dynamic_clk_gating_en]=1.//	cp15.power_control_register[dynamic_clock_gating] = 1;
//B.23 L2 Cache (L2Cpl310) Base Address 0xF8F02000 l2cache
//reg15_power_ctrl 0x00000F80 32 mixed 0x00000000 Purpose Controls the operating mode clock and power modes.	Usage constraints There are nousage constraints.
//dynamic_clk_gating_en; bit 1; rw 0x0 Dynamic clock gating enable.1 = Enabled.0 = Masked. This is the default.
//standby_mode_en; bit 0; rw 0x0 Standby mode enable.1 = Enabled.0 = Masked. This is the default
		EMIT_MASKWRITE(0xF8F02F80, 0x00000002, 0x00000002),
		EMIT_EXIT() };

unsigned long step4[] = {
//4.Enable SCU standby mode. Set mpcore.SCU_CONTROL_REGISTER[SCU_standby_enable] = 1.
//B.24 Application Processing Unit (mpcore) Base Address 0xF8F00000
// SCU_CONTROL_REGISTER, Relative Address 0x00000000, Absolute Address 0xF8F00000, Reset Value 0x00000002
// SCU_standby_enable bit 5; rw 0x0 When set, SCU CLK is turned off when all processors are in WFI mode, there is no pending request on the ACP (if	implemented), and there is no remaining activity in the SCU. When SCU CLK is off, ARREADYS, AWREADYS and WREADYS on	the ACP are forced LOW. The clock is turned on	when any processor	leaves WFI mode, or if there is a new request on the ACP.
		EMIT_MASKWRITE(0xF8F00000, 0x0000020, 0x00000020),
		EMIT_EXIT() };

unsigned long step5[] = {
//5.Enable topswitch clock stop. Set slcr.TOPSW_CLK_CTRL[CLK_DIS] = 1.
//B.28 System Level Control Registers (slcr), Base Address 0xF8000000
//TOPSW_CLK_CTRL 0x0000016C 32 rw 0x00000000 Central Interconnect Clock	Control
//Register (slcr) TOPSW_CLK_CTRL Absolute Address 0xF800016C
//CLK_DIS  bit 0; rw 0x0 Clock disable control:	0: Clock is not disabled	1: Clock can be disabled
		EMIT_MASKWRITE(0xF800016C, 0x00000001, 0x00000001) };

unsigned long go_to_sleep1[] = {
		EMIT_MASKWRITE(0xF8F02F80, 0x00000002,0x00000002),
		EMIT_MASKWRITE(0xF8F00000, 0x0000020, 0x00000020),
		EMIT_MASKWRITE(0xF800016C, 0x00000001, 0x00000001),
		EMIT_EXIT()};

unsigned long go_to_sleep2[]={
		EMIT_MASKWRITE(0xF8000100, 0x00000010, 0x00000010),
		EMIT_MASKWRITE(0xF8000104, 0x00000010, 0x00000010),
		EMIT_MASKWRITE(0xF8000108, 0x00000010, 0x00000010),
		EMIT_MASKWRITE(0xF8000100, 0x00000002, 0x00000002),
		EMIT_MASKWRITE(0xF8000104, 0x00000002, 0x00000002),
		EMIT_MASKWRITE(0xF8000108, 0x00000002, 0x00000002),
		EMIT_MASKWRITE(0xF8000120, 0x00003F00, 0x00003F00),
		EMIT_EXIT()};

u32 FsblHookAfterBitstreamDload(void) {
	u32 Status;

	Status = XST_SUCCESS;

	/*
	 * User logic to be added here.
	 * Errors to be stored in the status variable and returned
	 */
	fsbl_printf(DEBUG_INFO, "In FsblHookAfterBitstreamDload function \r\n");
	fsbl_printf(DEBUG_INFO, "Putting ARM cores to sleep\r\n");

	fsbl_printf(DEBUG_INFO, "step 1\r\n");
//1.Disable interrupts. Execute cpsid if.
	cpsidi();
	cpsidf();

	fsbl_printf(DEBUG_INFO, "step 2\r\n");
	ps7_config(go_to_sleep1);

	fsbl_printf(DEBUG_INFO, "step 3\r\n");

	fsbl_printf(DEBUG_INFO, "step 4\r\n");

	fsbl_printf(DEBUG_INFO, "step 5\r\n");

	fsbl_printf(DEBUG_INFO, "step 6\r\n");
//6.Enable Cortex-A9 dynamic clock gating. Set cp15.power_control_register[dynamic_clock_gating] =1.
	u32 data;
	asm("mrc p15,0,%0,c15,c0,0": "=r"(data));
//Read Power Control Register
	fsbl_printf(DEBUG_INFO, "data=0x%08x\r\n", data);
	data |= 0x01;
	asm("mcr p15,0,%0,c15,c0,0": "=r"(data));
//Write Power Control Register
	asm("mrc p15,0,%0,c15,c0,0": "=r"(data));
//read update register. Read Power Control Register
	fsbl_printf(DEBUG_INFO, "data after write=0x%08x\r\n", data);

	fsbl_printf(DEBUG_INFO, "nothing to do in step 7\r\n");
// 7.Put the external DDR memory into self-refresh mode. Refer to section 10.9.6 DDR Power Reduction

	fsbl_printf(DEBUG_INFO, "step 8\r\n");
	ps7_config(go_to_sleep2);
// 8.Put the PLLs into bypass mode. Set slcr.{ARM, DDR, IO}_PLL_CTRL[PLL_BYPASS_FORCE] = 1.
// B.28 System Level Control Registers (slcr), Base Address 0xF8000000 slcr
// ARM_PLL_CTRL, Relative Address 0x00000100, Absolute Address 0xF8000100
//PLL_BYPASS_FORCE bit 4; rw; ARM PLL Bypass override control:	PLL_BYPASS_QUAL = 0:	0: enabled, not bypassed.	1: bypassed.	PLL_BYPASS_QUAL = 1 (QUAL bit default value):	0: PLL mode is set based on pin strap setting.	1: PLL bypassed regardless of the pin strapping.
//	EMIT_MASKWRITE(0xF8000100, 0x00000010, 0x00000010);
// DDR_PLL_CTRL, Relative Address 0x00000104, Absolute Address 0xF8000104
// PLL_BYPASS_FORCE  bit 4 (same as ARM)
//	EMIT_MASKWRITE(0xF8000104, 0x00000010, 0x00000010);
// IO_PLL_CTRL,  Relative Address 0x00000108, Absolute Address 0xF8000108
// PLL_BYPASS_FORCE bit 4 (same as ARM)
//	EMIT_MASKWRITE(0xF8000108, 0x00000010, 0x00000010);

	fsbl_printf(DEBUG_INFO, "step 9\r\n");
// 9.Shut down the PLLs. Set slcr.{ARM, DDR, IO}_PLL_CTRL[PLL_PWRDWN] = 1.
// B.28 System Level Control Registers (slcr), Base Address 0xF8000000 slcr
// ARM_PLL_CTRL, Relative Address 0x00000100, Absolute Address 0xF8000100
// PLL_PWRDWN bit 1; rw 0x0; PLL Power-down control: 	0: PLL powered up 	1: PLL powered down
//	EMIT_MASKWRITE(0xF8000100, 0x00000002, 0x00000002);
// DDR_PLL_CTRL, Relative Address 0x00000104, Absolute Address 0xF8000104
// PLL_PWRDWN bit 1; rw 0x0; PLL Power-down control:	0: PLL powered up	1: PLL powered down
//	EMIT_MASKWRITE(0xF8000104, 0x00000002, 0x00000002);
// IO_PLL_CTRL,  Relative Address 0x00000108, Absolute Address 0xF8000108
// PLL_PWRDWN bit 1; rw 0x0; PLL Power-down control:	0: PLL powered up	1: PLL powered down
//	EMIT_MASKWRITE(0xF8000108, 0x00000002, 0x00000002);

	fsbl_printf(DEBUG_INFO, "step 10\r\n");
// 10.Increase the clock divisor to slow down the CPU clock. Set slcr.ARM_CLK_CTRL[DIVISOR] = 0x3f
// B.28 System Level Control Registers (slcr), Base Address 0xF8000000 slcr
// ARM_CLK_CTRL, Relative Address 0x00000120, Absolute Address 0xF8000120
// DIVISOR bits 13:8 rw 0x4 Frequency divisor for the CPU clock source. (When PLL is being used, 1&3 are illegal values)
//	EMIT_MASKWRITE(0xF8000120, 0x00003F00, 0x00003F00);

	fsbl_printf(DEBUG_INFO, "step 11\r\n");
// 11.Execute the wfi instruction to enter WFI mode.
	asm("wfi");
//wait for interrupt

	fsbl_printf(DEBUG_INFO, "after step 11\r\n");
	return (Status);
}

/******************************************************************************
 * This function is the hook which will be called  before the FSBL does a handoff
 * to the application. The user can add all the customized code required to be
 * executed before the handoff to this routine.
 *
 * @param None
 *
 * @return
 *		- XST_SUCCESS to indicate success
 *		- XST_FAILURE.to indicate failure
 *
 ****************************************************************************/
u32 FsblHookBeforeHandoff(void) {
	u32 Status;

	Status = XST_SUCCESS;

	/*
	 * User logic to be added here.
	 * Errors to be stored in the status variable and returned
	 */
	fsbl_printf(DEBUG_INFO, "In FsblHookBeforeHandoff function \r\n");

	return (Status);
}

/******************************************************************************
 * This function is the hook which will be called in case FSBL fall back
 *
 * @param None
 *
 * @return None
 *
 ****************************************************************************/
void FsblHookFallback(void) {
	/*
	 * User logic to be added here.
	 * Errors to be stored in the status variable and returned
	 */
	fsbl_printf(DEBUG_INFO, "In FsblHookFallback function \r\n");
	while (1)
		;
}

