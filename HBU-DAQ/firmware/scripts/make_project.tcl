#*****************************************************************************************
# Vivado (TM) v2015.4 (64-bit)
#
# This scripts creates the vivado project in current directory. Files
# are copied from the SVN directory, which has to be editted in this file.
#
# To recreate the project, call:  
#   vivado -mode tcl -source  make_project.tcl
#*****************************************************************************************

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir "."

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

variable script_file
set script_file "make_project.tcl"

variable sccalo_svn_dir
#set sccalo_svn_dir "~/git-repos/KLauS-DAQ/HBU-DAQ/firmware"
set sccalo_svn_dir ".."

variable project_name
set project_name "dif2_retiming_2015.4"

set bd_files [list \
 "[file normalize "$sccalo_svn_dir/ip/zynq_ps.bd"]"\
]

set hdl_work_files [list \
 "[file normalize "$sccalo_svn_dir/hdl/adif2_top.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/acq_start.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/acq_stop.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/adif2_types.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/cal_ctrl.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/clk_ctrl.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/cmd_ack.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/cmd_rx.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/data_mux.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/dif2lda.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/dif2usb.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/fast_cmd_dec.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/hdmi_if.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/if_switch.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/lda2dif.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/pwr_ctrl.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/ro_i2c_ctrl.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/ro_i2c_row.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/sc_ctrl.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/sc.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/stat.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/std_cmd_dec.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/std_cmd.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/usb2dif.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/usb_if.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/word_receiver2.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/word_transmitter2.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/roc_cnt.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/dutycycle_monitor_dsp.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/dutycycle.vhd"]"\
 "[file normalize "$sccalo_svn_dir/hdl/dutycycle_ctrl.vhd"]"\
]

set xci_work_files [list \
 "[file normalize "$sccalo_svn_dir/ip/hdmi_rx_fifo.xci"]"\
 "[file normalize "$sccalo_svn_dir/ip/hdmi_tx_fifo.xci"]"\
 "[file normalize "$sccalo_svn_dir/ip/ro_row_mem_6slab.xci"]"\
]
set xdc_files [list \
  "[file normalize "$sccalo_svn_dir/xdc/pins.xdc"]"\
  "[file normalize "$sccalo_svn_dir/xdc/timing.xdc"]"\
]

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
      "--sccalo_svn_dir" { incr i; set sccalo_svn_dir [lindex $::argv $i] }
      "--help"       { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/"]"

# Create project
#create_project $project_name ./$project_name
create_project $project_name .

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects $project_name]
set_property "default_lib" "work" $obj
set_property "part" "xc7z020clg484-1" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "VHDL" $obj
set_property "target_language" "VHDL" $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set 'sources_1' fileset object
set obj [get_filesets sources_1]
#add_files -norecurse -copy_to "${proj_dir}/${project_name}.srcs/sources_1/" -fileset $obj $files
import_files -norecurse -fileset $obj $bd_files
import_files -norecurse -fileset $obj $hdl_work_files
import_files -norecurse -fileset $obj $xci_work_files

set file "zynq_ps.bd"
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
if { ![get_property "is_locked" $file_obj] } {
  set_property "generate_synth_checkpoint" "0" $file_obj
}

foreach ifile $hdl_work_files {
    set ifile [file tail $ifile]
    puts "setting attributes for $ifile"
    set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$ifile"]]
    set_property "file_type" "VHDL" $file_obj
    set_property "library" "work" $file_obj
}

foreach ifil $xci_work_files {
    set ifile [file tail $ifile]
    puts "setting attributes for $ifile"
    set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$ifile"]]
    set_property "library" "work" $file_obj
}

# Set 'sources_1' fileset file properties for local files
# None

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "adif2_top" $obj

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

#import xdc_files

import_files -norecurse -fileset $obj $xdc_files

foreach ifile $xdc_files {
    set ifile [file tail $ifile]
    puts "setting attributs for $ifile"
    set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$ifile"]]
    set_property "file_type" "XDC" $file_obj
    set_property "library" "work" $file_obj
}

# Set 'constrs_1' fileset properties
set obj [get_filesets constrs_1]
set_property target_constrs_file [get_files -of_objects [get_filesets constrs_1] [list "*timing.xdc"]] $obj

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
# Empty (no sources present)

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "adif2_top" $obj
set_property "xelab.nosort" "1" $obj
set_property "xelab.unifast" "" $obj

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part xc7z020clg484-1 -flow {Vivado Synthesis 2015} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
  set_property flow "Vivado Synthesis 2015" [get_runs synth_1]
}
set obj [get_runs synth_1]
set_property "part" "xc7z020clg484-1" $obj

# set the current synth run
current_run -synthesis [get_runs synth_1]

# Create 'impl_1' run (if not found)
if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part xc7z020clg484-1 -flow {Vivado Implementation 2015} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
} else {
  set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
  set_property flow "Vivado Implementation 2015" [get_runs impl_1]
}
set obj [get_runs impl_1]
set_property "part" "xc7z020clg484-1" $obj
set_property "steps.write_bitstream.args.raw_bitfile" "1" $obj
set_property "steps.write_bitstream.args.bin_file" "1" $obj
set_property "steps.write_bitstream.args.readback_file" "0" $obj
set_property "steps.write_bitstream.args.verbose" "0" $obj

# set the current impl run
current_run -implementation [get_runs impl_1]

puts "INFO: Project created:dif2_olddif_firmware_2015.4"
update_compile_order -fileset sources_1
reset_run synth_1
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1
file mkdir $origin_dir/$project_name.sdk
file copy -force $origin_dir/$project_name.runs/impl_1/adif2_top.sysdef $origin_dir/$project_name.sdk/adif2_top.hdf
#launch_sdk -workspace $origin_dir/$project_name.sdk -hwspec $origin_dir/$project_name.sdk/adif2_top.hdf
start_gui
