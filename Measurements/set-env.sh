#!/bin/bash

export LD_LIBRARY_PATH=$(pwd)/devicelib/DeviceLibBase/install/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$(pwd)/devicelib/Agilent6614c/install/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$(pwd)/devicelib/Agilent33250a/install/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$(pwd)/devicelib/Lecroy/install/lib:$LD_LIBRARY_PATH
