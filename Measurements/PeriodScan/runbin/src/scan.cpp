#include "Klaus4Config.h"
#include "IBConfig.h"
#include "k4_config_helpers.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1F.h"

#include "Sequencer.h"
//include "SCPIoUART.h"
//#include "VPulseGen.h"
//#include "Lecroy.h"

#include "Agilent33250a.h"

#include <string>
#include <stdlib.h>
#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <iterator>
#include <iomanip>
using namespace std;

#define C_GREEN "\x1B[32m"
#define C_RESET "\x1B[0m"

#include <ctime>
#include "MeasureClass.h"

std::vector<TTree*> trees;
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=trees.begin();it!=trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}

// **************************
// Sweep options and propagated objects
struct{
	// **************************
	// Basic sweep params
	struct{
		
		std::list<unsigned short> channels	=Seq::RangeList<unsigned short>("35");
		std::list<float> periods		=Seq::RangeSize<float>(9e-6,9e-6+5e-9,2e-10);
		//std::list<unsigned short> Icps		=Seq::RangeSize<unsigned short>(0,3,1);
		//std::list<unsigned short> Ibufs		=Seq::RangeSize<unsigned short>(6,9,1);
 	} opt;
} sweep;

int main (int argc, char *argv[])
{
	if (argc<4){
		std::cerr << "Usage: scan <host> <configfile> <outputfile>\n";
		return -1;
	}
	
	//Init interface board
	TIBConfig *config_ib;
	config_ib=new VCRemoteClient<TIBConfig>(argv[1],0);
	config_ib->SetParValue("system/clkenable",1);
	config_ib->SetParValue("system/aqu_ena",1);

	//Init configuration
	char filename[256];
	setup.dev.asic=new VCRemoteClient<TKLauS4Config>(argv[1],1);
	sprintf(filename,"%s",argv[2]);
	printf("Reading file %s\n",filename);
	setup.dev.asic->ReadFromFile(filename);
	setup.dev.asic->UpdateConfig();
	Seq::Settling::Request(1000000);
	
//*	//Init Pulsegenerator
	setup.dev.pulsegen=new Agilent33250a("/dev/ttyS0");
	setup.dev.pulsegen->SetHiLevel(1.65);
	setup.dev.pulsegen->SetLoLevel(0);
	setup.dev.pulsegen->SetPulseWidth(3e-7);
//	setup.dev.pulsegen->SetPeriod(10e-6);
//	setup.dev.pulsegen->SetAmplitude(3.3);
//	setup.dev.pulsegen->SetOffset(0);
	setup.dev.pulsegen->On();
	Seq::Settling::Request(10000000);
 

	//Init DAQ
	setup.dev.daq=new HistogramDAQ(argv[1]);
	long long unsigned int addr;
	setup.dev.asic->GetParValueWR("digital/i2c_address",addr);
	setup.dev.daq->BindTo(addr);
	setup.dev.daqt=new EventListDAQ(argv[1]);
	setup.dev.daqt->RegisterQueue(5000,1);
	
	//Init ROOT
	TFile fout(argv[3],"recreate");
	TTree* treeQ=new TTree("qscan","qscan");
	treeQ->AutoSave();
	TTree* treeN=new TTree("dump","acquisitions");
	treeN->AutoSave();
	std::vector<TTree*> trees;
	trees.push_back(treeQ);
	trees.push_back(treeN);

	Seq::MeasurementCollection todo;
	todo.Add("qscan",new TDCMeasurement(treeQ,-1e4,treeN));	//read at least 1 second
	Seq::Settling::Settle();
	
	// Some configuration settings
	SetParAllChannel(setup.dev.asic,"branch_sel_config",3); // 0: Auto; 1:External; 2: LG; 3:HG
	SetParAllChannel(setup.dev.asic,"ADC_in_n_select",1); 	// 0: ADCn; 1: inner ground
	SetParAllChannel(setup.dev.asic,"ext_trigger_select",1);// 0: internal; 1: external trigger
	SetParAllChannel(setup.dev.asic,"HG_scale_select",1);// 0: MG mode ; 1: HG mode
	setup.dev.asic->SetParValue("digital/i2c_readout_enable",0);
	Seq::Sequence<unsigned short>(trees,"channel","channel/s").LoopOver(sweep.opt.channels,
	[&todo](Seq::Sequence<unsigned short>* this_seq){
		std::cout<<"\tChannel: "<<this_seq->GetObservable();
		unsigned short channel = this_seq->GetObservable();
		SetParAllChannel(setup.dev.asic,"mask",1);
		SetParForChannel(setup.dev.asic,channel,"mask",0);
		
			//Separate measurements
			todo.Loop([this_seq](Seq::MeasurementBase* job){
					Seq::Sequence<float>(this_seq->GetTrees(), "Periods", "Periods/F").LoopOver(sweep.opt.periods,
					[job](Seq::Sequence<float>* this_seq){
						double per=this_seq->GetObservable();
						setprecision(4);
						std::cout<<"Period:"<<per<<"s"<<std::endl;
						setup.dev.pulsegen->SetPeriod(per);
						Seq::Settling::Request(100000);
						setup.dev.asic->UpdateConfig();
						job->Run();

					return true;});
				});//measurements
		return true;});//periods


	printf("Finish the scan\n");
	for(auto it=trees.begin();it!=trees.end();++it){(*it)->Write(0,TObject::kOverwrite);};
//	printf("debugging print\n");
	
	fout.Close();
//	printf("debugging print 2\n");
	setup.dev.pulsegen->SetHiLevel(2.0e-3);
//	printf("debugging print 3\n");
return 0;
}

