#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TH1F.h"
#include "EventType.h"
#include "TSpectrum.h"
#include "TMath.h"
#include "TPaveStats.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "math.h"
#include "CalibrationBox.h"

#include <iostream>
#include <vector>
#include <iterator>


using namespace std;

// correct FC and MC before further processing. 
// For FC, offset=18, bit =5
// For FC, offset= 0, bit =3 //MC propably meant
int	OffsetCorrection(int TC, int offset, int bit){ return (TC+offset)%(1<<bit); }
// Merge the time. The input values are offset-corrected
// CC, MC, FC are the input raw counter values
// cCC, cMC are the aligned counter values, FC is used as a reference
// ccbit is the number of bit stored. ccbit<=18
int	MergeTime(int CC, int MC, int FC, int& cCC, int& cMC, int ccbit=13){
	cMC = MC/2;
	if(MC%2==1 && FC/8==0) cMC = (cMC + 1)%4; 	
	if(MC%2==0 && FC/8==3) cMC = (cMC - 1 + 4)%4; 	
	int R = 1<<ccbit;
	cCC = (CC%(2*R))/2;
 	if(CC%2==1 && cMC/2==0) cCC = (cCC + 1)%R;
	if(CC%2==0 && cMC/2==3) cCC = (cCC - 1 + R)%R;
	return 128*cCC+32*cMC+FC;
}

void SortXY(double *PosX, double *PosY, int NPeaks){
	double Xtemp, Ytemp;
	for(int i=0; i<NPeaks; i++){
		for(int j=i+1;j<NPeaks; j++){
			if(PosX[i]>PosX[j]){
				Xtemp=PosX[i];
				Ytemp=PosY[i];
				PosX[i]=PosX[j];
				PosY[i]=PosY[j];
				PosX[j]=Xtemp;
				PosY[j]=Ytemp;
			}
		}
	}
}

void DNLMakeHisto(TH1D *h){
	char DNLName[128];
	char BinWidthName[128];
	sprintf(DNLName, "DNL_%s",h->GetTitle());
	sprintf(BinWidthName, "Bin Width_%s",h->GetTitle());
	TH1D *DNL_Histo = new TH1D(DNLName, DNLName, h->GetNbinsX(),h->GetXaxis()->GetXmin(),h->GetXaxis()->GetXmax());
	TH1D *BinWidth_Histo = new TH1D(BinWidthName, BinWidthName, h->GetNbinsX(),h->GetXaxis()->GetXmin(),h->GetXaxis()->GetXmax());

	int Nbins = h->GetNbinsX();
	float mean = h->GetEntries()/Nbins;
	double DNL;
	double BinWidth;
	for(int i=1;i<Nbins+1;i++){
		int b_cont = h->GetBinContent(i);
		DNL = b_cont/mean-1;
		BinWidth = DNL +1;
	        BinWidth_Histo->SetBinContent(i,BinWidth);	
		DNL_Histo->SetBinContent(i,DNL);
	}	
	DNL_Histo->GetYaxis()->SetRange(-1,1);
	DNL_Histo->Write();
	BinWidth_Histo->Write();
}

int main(int argc, char* argv[]){
	if(argc<2){
	    printf("Usage: analysis <filename>");
	    return -1;
	}
	TFile* iFile = new TFile(argv[1]);
	TTree* iTree = (TTree*)iFile->Get("dump");
	klaus_acquisition* acquisitions = NULL;
	float  period,last_period;
	iTree->SetBranchAddress("acquisitions",&acquisitions);
	iTree->SetBranchAddress("Periods",&period);
	period=0;
	last_period=0;
		
	TSpectrum* sp1 = new TSpectrum(10,2);

	char ofilename[128],hdiffname[128];
	sprintf(ofilename, "analysed-%s", argv[1]);
	TFile* ofile = new TFile(ofilename,"recreate");
	TDirectory* diff = ofile->mkdir("diff");
	int moffset = 0, foffset = 18;
	int channel = 35;
	int ccbit = 13;
	TH1D* hcc 	= new TH1D("cc","cc",1<<ccbit,0,1<<ccbit);	// CC
	TH1D* hmc 	= new TH1D("mc","mc",1<<3,0,1<<3);	// MC
	TH1D* hfc 	= new TH1D("fc","fc",1<<5,0,1<<5);	// FC
	TH1I* hmmf 	= new TH1I("Mmf","Mmf",1<<7,0,1<<7);	// FC+MC
	TH1I* hrmf 	= new TH1I("Rmf","Rmf",1<<8,0,1<<8);	// raw FC+MC
	TH1I* hmcm 	= new TH1I("Mcm","Mcm",1<<7,0,1<<7);	// MC + 4-bit CC
	TH1I* hrcm 	= new TH1I("Rcm","Rcm",1<<8,0,1<<8);	// raw MC + 3-bit CC
	TH1I* hmcf 	= new TH1I("Mcmf","Mcmf",1<<10,0,1<<10);	//  3-bit CC + 2-bit MC + 5-bit FC
	TH1I* hdiff = new TH1I("tdiff","tdiff",1<<(7+ccbit),0,1<<(7+ccbit));	// 13-bit CC, 2-bit MC, 5-bit FC
	int   ptime = 1<<(7+ccbit+1)-1;
	int nEntries = iTree->GetEntries();
	double par[3*nEntries];
       	double  periods[nEntries];

	//Use Calibration Box for DNL Correction 
	CalibrationBox* calib = new CalibrationBox(36,106e6,7); //Calibration Box for FC
	uint32_t realtime;
	for(int i=0;i<iTree->GetEntries();i++){
		iTree->GetEntry(i);
		calib->AddHits(*acquisitions);
	}
	calib->RecomputeDNL();
	calib->Write(); 
	calib->GetCountsHist().Write();
	calib->GetBinWidthHist(35).Write();
	
	TH1D*  DNL_BDITH 	= new TH1D("DNL_BDITH","DNL_BDITH",1<<9,0,1<<9);//Histogram for the DNL corrected counters	
	for(int i=0;i<iTree->GetEntries();i++){
		iTree->GetEntry(i);
		for(auto hitlist: acquisitions->data){
			for(auto hit: hitlist.second){
				realtime = calib->GetTimeDNLCorrected_BDITH(hit,false);
				DNL_BDITH->Fill(realtime%(1<<9));
			}
		}	
		DNL_BDITH->Write();	
	}


	for(int i=0;i<iTree->GetEntries();i++){
		iTree->GetEntry(i);
		for(auto it=acquisitions->data.begin();it!=acquisitions->data.end();it++){
			for(auto itE=it->second.begin(); itE!=it->second.end();++itE){
				if(itE->channel != channel) continue;
				
				
				int CC = itE->T_CC;
				int MC = itE->T_MC;//OffsetCorrection(itE->T_MC, moffset, 3);
				int FC = itE->T_FC;//OffsetCorrection(itE->T_FC, foffset, 5);
				int cMC, cCC;
				int time = MergeTime(CC,MC,FC,cCC,cMC,ccbit);
				// fill the raw data
				hcc->Fill(CC); hmc->Fill(MC);
				hfc->Fill(FC);
				hrmf->Fill(32*MC+FC);   hrcm->Fill(8*(CC%32)+MC);
				// fill the merged data
				hmmf->Fill(FC+32*cMC); 
				hmcf->Fill(time%1024); 
				int td = time - ptime; ptime = time;
				if(td>0)hdiff->Fill(td);
				if(last_period!=period){
				//	printf("last period not= period\n");
					printf("at %d: old period: %.4e new period: %.4e\n",i,last_period,period);
					periods[i]=period;
					//save/use old histograms
			//		if(last_period>=0){
						sp1->Search(hdiff,2,"",0.1);
						sp1->Draw("SAME");
						double *PosX = sp1->GetPositionX();
						TF1* f1 = new TF1("Fit","gaus",*PosX-2,*PosX+2);
						hdiff->Fit(f1, "RMNQ+");
						printf("Gaus Fit\n");
						f1->GetParameters(&par[3*(i)]);
						diff->cd();
						sprintf(hdiffname,"diff_%.4e",period);
						hdiff->Write(hdiffname);
						hdiff->Reset();
						last_period=period;
					}	
				
			}
		}
		if(i==0){
			ofile->cd();
			hcc->Write();
			hmc->Write();
			hfc->Write();
			hrmf->Write();
			hrcm->Write();
			hmmf->Write();
			hmcf->Write();

		}	
	}
	ofile->cd();
	DNLMakeHisto(hfc);
	diff->cd();

	TCanvas *c1 = new TCanvas("c1", "Canvas1",200,10,1024,1024);
	gStyle->SetOptFit(1111);
	double PeaksX[nEntries];
	for(int i=0;i<nEntries;i++){
		PeaksX[i]=par[1+3*i];
	};
	auto gr = new TGraph(nEntries, periods, PeaksX);
	gr->SetTitle("External Trigger");
	gr->SetMarkerColor(4);
	gr->SetMarkerStyle(2);
	gr->SetMarkerSize(1);
	gr->GetXaxis()->SetTitle("Period ext. Trigger [s]");
	gr->GetYaxis()->SetTitle("Corresponding TDC Bin [Bin]");
	gr->SetLineWidth(0);
	
	double par2[2];
	TF1* f2 = new TF1("linear","pol1",periods[0]+0.1e-9,periods[nEntries-1]);
//	f2->FixParameter(0,0);
	gr->Fit(f2,"RMQ+");
	f2->GetParameters(&par2[0]);
	gr->Draw("ALP");
	f2->Draw("SAME");
	c1->Update();
	
	double jitter[nEntries-1];	
	for(int i=0;i<nEntries-1;i++){jitter[i]=par[2+3*i]*(1/par2[1])*1e12*1/sqrt(2);};
	
	
	TCanvas *c2 = new TCanvas("c2", "Canvas2",200,10,1024,1024);
	auto jd = new TGraph(nEntries, periods, jitter); 
	jd->SetTitle("Jitter");
	jd->SetMarkerColor(4);
	jd->SetMarkerStyle(2);
	jd->SetMarkerSize(2);
	jd->GetXaxis()->SetTitle("Period ext. Trigger [s]");
	jd->GetYaxis()->SetTitle("Jitter [ps]");
	jd->SetLineWidth(1);
	jd->Draw("ALP");
	c2->Update(); 


   //	TDirectory* counters
/*    ofile->cd();
    hcc->Write();
    hmc->Write();
    hfc->Write();
    hrmf->Write();
    hrcm->Write();
    hmmf->Write();
    hmcf->Write();*/
    diff->cd();
    gr->Write();
    jd->Write();
	
    ofile->Close();
	return 1;

}

