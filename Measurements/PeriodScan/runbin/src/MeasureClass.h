#include "MeasurementCollection.h"
#include <unistd.h>
#include "Sequencer.h"
#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "k4_config_helpers.h"
#include "Agilent33250a.h"
//#include "Keithley6487.h"
#include "HistogramDAQ_client.h"
#include "HistogrammedResults.h"
#include "EventListDAQ_client.h"
#include "EventType.h"


struct{
	struct{
		//Lecroy* scope;
		TKLauS4Config *asic;
		HistogramDAQ  *daq;
		EventListDAQ  *daqt;
		Agilent33250a *pulsegen; // may be changed
                //Keithley6487  *Keithley;
	} dev;
} setup;

class TDCMeasurement: public Seq::MeasurementBase{
	protected:
		TTree* treeN;
		TTree* treeDump;
		std::vector<TTree*> f_trees;

		struct{
			klaus_acquisition* curr_acqu=NULL;
		} res;	
		int f_readTime;
		unsigned long long prev_threshold;
	public:


	TDCMeasurement(TTree* _tree, int readTime, TTree* _treeDump=NULL){
		treeN=_tree;
		treeN->Branch(TString::Format("histos_chip%d",setup.dev.daq->GetBound()),setup.dev.daq->GetResultsPtr());
		f_trees.push_back(treeN);
		treeDump=_treeDump;
		if(treeDump!=NULL){
			f_trees.push_back(treeDump);
			TBranch* br=treeDump->Branch("acquisitions",&(res.curr_acqu));
		}
		f_readTime=readTime; 	//readTime < 0: read at least readTime events
					//readTime > 0: read at least readTime seconds
	}
	~TDCMeasurement(){
		if(treeDump!=NULL)
			treeDump->SetBranchAddress("acquisitions",NULL);
	}

	void Prepare(){
		//TDC MEASUREMENT
		std::cout<<"\tTDC  measurement: preparations..."<<std::endl;
	}

	void Cleanup(){
		std::cout<<"  TDC measurement: cleanup..."<<std::endl;
	}


	void Run(){
		Seq::Settling::Settle();
		setup.dev.daq->FlushFIFO(0);
		setup.dev.daq->ResetResults();

		Seq::Settling::Request(10000);
		Seq::Settling::Settle();

		setup.dev.daq->FlushFIFO();
		setup.dev.daq->ResetResults();
		setup.dev.daqt->ResetResults();

		if(f_readTime<=0){
			printf("Trying to get %d events\n",-f_readTime);
			setup.dev.daq->ReadChipUntilEmpty(-f_readTime);
		}
		else{
			printf("Trying to read for %d us\n",f_readTime);
			setup.dev.daq->ReadChipAsyncStart(0,0);
			sleep(f_readTime);
			setup.dev.daq->ReadChipAsyncStop();
		}
		
		//Histogram mode
		//setup.dev.daq->FetchResults();
		//Save results to TreeQ
		//treeN->Fill();

		//Tree mode
		if(treeDump!=NULL){
			TList* li=setup.dev.daqt->FetchResults();
			if(li!=NULL){
				TObjLink* curr_aqu_entry=li->FirstLink();
				while(curr_aqu_entry){
					res.curr_acqu=(klaus_acquisition*)curr_aqu_entry->GetObject();
					treeDump->Fill();
					curr_aqu_entry=curr_aqu_entry->Next();
				}
			}
		}
	}
};

