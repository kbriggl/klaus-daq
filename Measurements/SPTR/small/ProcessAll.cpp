#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TF1.h"
#include "TH1F.h"

#include <iostream>
#include <vector>
#include <iterator>
using namespace std;

struct scope{
	float	average;
	float	high;
	float	low;
	float	last;
	float	sigma;
	float	sweeps;
};

std::vector<std::vector<double> > getMeasurement(const char* fname);
void    spsSort(double* a, int n);
double  ExtractGainByFFT(TH1F* sps);
TGraph* ExtractGainAndThreshold(TH1F* sps, TF1* f2, double& gain);

void    ProcessAll(){
    //auto data0 = getMeasurement("hv60-t15-ch30th3-long.root");
    auto data0 = getMeasurement("hv60-t15-ch30th3.root");
    //auto data1 = getMeasurement("hv60-t15-ch30th3-mid.root");

    std::vector<std::vector<double>> data;
    data.insert(data.end(),data0.begin(),data0.end());
    //data.insert(data.end(),data1.begin(),data1.end());

    double gain;
    TH1F* sps = new TH1F("sps","",1000,0,1000);
    for(auto i=0;i<data.size();i++){ sps->Fill(data[i][0]); }
    TF1* f2 = new TF1("f2","pol1");
    TGraph* fit = ExtractGainAndThreshold(sps,f2,gain); fit->SetName("fit");
    double p0 = f2->GetParameter(0);
    double p1 = f2->GetParameter(1);

    //data.insert(data.end(),data2.begin(),data2.end());
    TGraph* tw = new TGraph(); tw->SetName("tw");
    for(auto i=0;i<data.size();i++){ tw->SetPoint(tw->GetN(),data[i][0],data[i][1]); }


    TCanvas* c3 = new TCanvas("c3","",1500,0,600,500);
    TF1* f3 = new TF1("f3","[0]+[1]/TMath::Sqrt(x)+[2]/x+[3]/x/x",50,1000);
    tw->Fit(f3,"R");
    tw->Draw();
    double p30 = f3->GetParameter(0);
    double p31 = f3->GetParameter(1);
    double p32 = f3->GetParameter(2);
    double p33 = f3->GetParameter(3);

    std::vector<TH1F*> histos;
    for(int i=0;i<80;i++) histos.push_back(new TH1F(Form("ht%d",i),"",400,-10,10));
    for(int i=0;i<data.size();i++){
        int npe10 = (int)(10*(data[i][0]-p0)/p1);
        //if(npe10%10>2 || npe10%10<8) continue;
        int npe = round(npe10/10.0); if(npe<=0) continue;
        double v = data[i][0]; //if(t<=45) continue;
        double tfit = p30+p31/TMath::Sqrt(v)+p32/v+p33/v/v;
        if(i%20==0)
        printf("%d,\tx=%.4f\t,t=%.4f,\tnpe=%d,\ttfit=%.4f,\tnpe10=%d\n",i,data[i][0],data[i][1],npe,tfit,npe10);
        histos[npe]->Fill(data[i][1]-tfit);
    }
    
    // get the jitter
    TGraphErrors* jitter = new TGraphErrors(); jitter->SetName("jitter");
    for(int i=0;i<80;i++){
        if(histos[i]->GetEntries()<50) continue;
        double rms = histos[i]->GetRMS();
        double err = histos[i]->GetRMSError();
            TF1* fg = new TF1("fg","gaus",
                        histos[i]->GetMinimumBin()-50,
                        histos[i]->GetMinimumBin()+50);
            sps->Fit(fg, "RQ");
            if((int)(1000*fg->GetParameter(2))<(int)(rms*1000)){// ps
                rms = 1000*fg->GetParameter(2);
                err = 1000*fg->GetParError(2);
            }
            jitter->SetPoint(jitter->GetN(),i,rms);
            jitter->SetPointError(jitter->GetN()-1,0,err);

    }

    TFile* ofile = new TFile("results.root","recreate");
    sps->Write();
    tw->Write();
    fit->Write();
    jitter->Write();

    TDirectory* hdir = ofile->mkdir("hdir"); hdir->cd();
    for(auto h : histos) if(h->GetEntries()>20) h->Write();

    ofile->Close();

}

std::vector<std::vector<double> > getMeasurement(const char* fname){

	TFile* ifile = new TFile(fname);
	TTree* sptr = (TTree*)ifile->Get("sptr");

	// datas
	scope M0, T0, M1, M2;
	sptr->SetBranchAddress("scope_T0",&T0);
	sptr->SetBranchAddress("scope_M0",&M0);
	sptr->SetBranchAddress("scope_M1",&M1);
	sptr->SetBranchAddress("scope_M2",&M2);
        int  val1, val2,val3,val4;

        std::vector<std::vector<double> > data;
        for(int i=0;i<sptr->GetEntries();i++){
            sptr->GetEntry(i);
            if(M0.sweeps == 1 && T0.sweeps ==1 && M1.sweeps == 1 && M2.sweeps == 1){
                std::vector<double> single; single.resize(0);
                val2= (int)(1.e9*T0.average);   if(val2<40 || val2>60) continue;
                val4= (int)(1000*M2.average);   if(val4>0) continue;
                val3= (int)(1000*M1.average);   if(val3>600 || val3<580) continue;
                single.push_back(1.0e3*(M0.average-M1.average));
                single.push_back(1.0e9* T0.average);
                data.push_back(single);
            }
        }

        return data;
}

void    spsSort(double* a, int n){
    int     index;
    double  temp;
    for(auto i=0;i<n-1;i++){
        index = i;
        for(auto j=i+1;j<n;j++){
            if (a[j]<a[index]) index = j;
        }
        temp = a[i];    a[i] = a[index];    a[index] = temp;
    }
}

double  ExtractGainByFFT(TH1F* sps){
    TCanvas* c1 = new TCanvas("c1","",0,0,600,500);
    double gain = -1;
    TH1*    hm = NULL;
    TVirtualFFT::SetTransform(0);
    hm = sps->FFT(hm, "MAG");              

    TSpectrum* spec = new TSpectrum(1);
    hm->GetXaxis()->SetRange(10,30);
    int nPeaks = 0; nPeaks = spec->Search(hm);
    if(nPeaks){
        double* xPeaks = spec->GetPositionX();
        gain = sps->GetNbinsX()/xPeaks[0];
    }
    return gain;
}


TGraph* ExtractGainAndThreshold(TH1F* sps, TF1* f2, double& gain){
        TCanvas* c2 = new TCanvas("c2","",700,0,600,500);
        std::vector<double> pos;
        TGraph* gr = new TGraph();
        TSpectrum* spec = new TSpectrum(10);
        int     nPeaks = spec->Search(sps,2);
        double* xPeaks = spec->GetPositionX();
        if(nPeaks<=1){ 
            printf("ExtractGainByTSpectrum() Failed\n"); 
            return gr; 
        }
        // Estimate the gain
        spsSort(xPeaks,nPeaks);   // sort the peaks
        double estimateGain = (xPeaks[nPeaks-1]-xPeaks[0])/(nPeaks-1);
        int fitrange = (int) estimateGain/2.0-1;
        //fitrange = 4;
        double gain_fft = ExtractGainByFFT(sps);
        if((int)gain_fft >10){ estimateGain = gain_fft;   }
        for(auto i=0;i<nPeaks;i++){
            TF1* f1 = new TF1("f1","gaus",xPeaks[i]-fitrange,xPeaks[i]+fitrange);
            sps->Fit(f1, "RNQ+");
            pos.push_back(f1->GetParameter(1));
            //printf("%d, %.2f\n",i,f1->GetParameter(1));
        }
        // linear fit of the peaks
        int npe0 = (int)ceil((pos[0])/estimateGain);
        for(int i=0;i<nPeaks;i++){ 
                int npe = npe0 + (int)round((pos[i]-pos[0])/estimateGain);
                gr->SetPoint(gr->GetN(),npe,pos[i]); 
                printf("%d->%d,%.2f\n",i,npe,pos[i]);
        }
        gr->Fit(f2,"");
        gain = f2->GetParameter(1);
        if(gain-estimateGain>1){
            for(int i=0;i<nPeaks;i++){
                int npe = (int)round((pos[i])/estimateGain);
                printf("est=%.2f,fit=%.2f,%d->%d,%.2f\n",estimateGain, gain,i,npe,pos[i]);
            }
        }

        return gr;
}

