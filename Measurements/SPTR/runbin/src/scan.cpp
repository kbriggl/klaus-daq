
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraphErrors.h"

#include "Sequencer.h"
#include "SCPIoUART.h"
#include "VPulseGen.h"
#include "Lecroy.h"
#include "Agilent6614c.h"

#include <string>
#include <stdlib.h>
#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <iterator>
using namespace std;

#define C_GREEN "\x1B[32m"
#define C_RESET "\x1B[0m"

#include <ctime>
#include "MeasureClass.h"

std::vector<TTree*> trees;
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=trees.begin();it!=trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}

int main (int argc, char *argv[])
{
	if (argc<2){
		std::cerr << "Usage: scan <outputfile>\n";
		return -1;
	}

	//Init ROOT
	char ofname[64];
	TFile fout(argv[1],"recreate");
	TTree* treeS=new TTree("sptr","sptr");
	treeS->AutoSave();
	//std::vector<TTree*> trees;
	trees.push_back(treeS);

	//Init SCOPE
        printf("initialized the lecroy\n");
	setup.dev.scope=new Lecroy();
	setup.dev.scope->OpenDevice("lecroy610zi");  //SMALL SCOPE
        printf("initialized the lecroy\n");

        setup.dev.hv = new Agilent6614c("/dev/ttyS1");
        setup.dev.hv->On();
        std::list<float> hvs;
        hvs.push_back(44.0);        // 1315CS
        //hvs.push_back(59.25);
        //hvs.push_back(58.5);    // Vov = 5V for 15degree
        //hvs.push_back(56.5);    // Vov = 3V for 15degree
        //for(int i=0;i<8;i++) hvs.push_back(58.0+0.5*i);
        //for(int i=0;i<8;i++) hvs.push_back(57.0+0.5*i); // low gain
        //hvs.push_back(60);

	Seq::MeasurementCollection todo;
	todo.Add("sptr",new SPTRMeasurement(treeS,5e4));
	Seq::Settling::Settle();

	Seq::Sequence<float>(trees,"hv","hv/F").LoopOver(hvs,
	[&todo](Seq::Sequence<float>* this_seq){
		std::cout<<" HV #"<<this_seq->GetObservable()<<":"<<std::endl;
                setup.dev.hv->SetVolt(this_seq->GetObservable());
		//Separate measurements
		todo.Loop([this_seq](Seq::MeasurementBase* job){
				job->Run();
		});//measurements
	return true;});

	treeS->Write();

        setup.dev.hv->Off();

	return 0;
}

