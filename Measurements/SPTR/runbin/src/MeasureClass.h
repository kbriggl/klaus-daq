#include "MeasurementCollection.h"
#include <unistd.h>

struct{
	struct{
		Lecroy* scope;
                Agilent6614c*   hv;

	} dev;
} setup;


class SPTRMeasurement: public Seq::MeasurementBase{
	public:
		TTree* treeQ;
		std::list<int> sample_list;
		struct{
			Lecroy::Pstat scope_T0;
			Lecroy::Pstat scope_M0;
			Lecroy::Pstat scope_M1;
			Lecroy::Pstat scope_M2;
		} res;	


	SPTRMeasurement(TTree* tree, int Nsample=10000){
		treeQ=tree;
		treeQ->Branch("scope_T0",   &(this->res.scope_T0),"average/F:high/F:low/F:last/F:sigma/F:sweeps/F");
		treeQ->Branch("scope_M0",   &(this->res.scope_M0),"average/F:high/F:low/F:last/F:sigma/F:sweeps/F");
		treeQ->Branch("scope_M1",   &(this->res.scope_M1),"average/F:high/F:low/F:last/F:sigma/F:sweeps/F");
		treeQ->Branch("scope_M2",   &(this->res.scope_M2),"average/F:high/F:low/F:last/F:sigma/F:sweeps/F");
		SetupNSample(Nsample);
	};
	~SPTRMeasurement(){};

	void Prepare(){
		setup.dev.scope->Command("WAIT");
		setup.dev.scope->Command("*OPC?");
	}

	void SetupNSample(int Nsample){ for(int i=0;i<Nsample;i++) sample_list.push_back(i);}

	void Run(){
		unsigned long timeout = 60000;
		Seq::Settling::Settle();
		printf("SPTR Measurements starts......\n");
		Seq::Sequence<int>(treeQ,"Index","Index/I").LoopOver(sample_list,
		[this, timeout](Seq::Sequence<int>* this_seq){
			std::cout<<"\r   Sample = "<<this_seq->GetObservable();
                        int count = this_seq->GetObservable();
			fflush(stdout);

			//READ SCOPE
			setup.dev.scope->ClearSweeps();
			setup.dev.scope->Command("TRMD SINGLE");
			setup.dev.scope->Arm(timeout);


			// get the simulation results
			this->res.scope_M0 	= setup.dev.scope->GetMeasurement(1);
			this->res.scope_T0 	= setup.dev.scope->GetMeasurement(2);
			this->res.scope_M1 	= setup.dev.scope->GetMeasurement(3);
			this->res.scope_M2 	= setup.dev.scope->GetMeasurement(4);

			//printf("Amp=%.4f, Delay=%.4f\n",
                        //       1.0e3*(this->res.scope_M0.average-this->res.scope_M1.average),
                        //      1.0e9*this->res.scope_T0.average);
			this_seq->GetTree()->Fill();
                        //usleep(1e5);
                        if((count%100)==99) this_seq->GetTree()->AutoSave();
			
			return true;
		});
		std::cout<<"Data acquisition done!"<<std::endl;
	}

};
