#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TH1F.h"

#include <iostream>
#include <vector>
#include <iterator>

#include "spsAnalyzer.h"
using namespace std;

struct scope{
	float	average;
	float	high;
	float	low;
	float	last;
	float	sigma;
	float	sweeps;
};

std::vector<std::vector<double> > getMesurement(const char* fname){

	TFile* ifile = new TFile(fname);
	TTree* sptr = (TTree*)ifile->Get("sptr");

	// datas
	scope M0, T0, M1, M2;
	sptr->SetBranchAddress("scope_T0",&T0); // delay to trigger
	sptr->SetBranchAddress("scope_M0",&M0); // amp
	sptr->SetBranchAddress("scope_M1",&M1); // ped
	sptr->SetBranchAddress("scope_M2",&M2); // laser trig before trigger
        float   hv;
        sptr->SetBranchAddress("hv",&hv);


        int  val1, val2,val3,val4;

        std::vector<std::vector<double> > data;
        for(int i=0;i<sptr->GetEntries();i++){
            sptr->GetEntry(i);
            if(M0.sweeps == 1 && T0.sweeps ==1 && M1.sweeps == 1 && M2.sweeps == 1){
                std::vector<double> single; single.resize(0);
                val4= (int)(1000*M2.average);   if(val4>0) continue;
                val3= (int)(1000*M1.average);   if(val3>600 || val3<580) continue;
                single.push_back(hv);
                single.push_back(1.0e3*(M0.average-M1.average));
                single.push_back(1.0e9* T0.average);
                data.push_back(single);
            }
        }

        return data;
}

int main(int argc, char* argv[]){
    if(argc<2){
        printf("Usage: analysis <filename>");
        return -1;
    }

    auto data = getMesurement(argv[1]);
    int NUM = 8;

    std::vector<float> hvs;
    for(int i=0;i<NUM;i++) hvs.push_back(58.0+0.5*i);
    std::vector<TH1F*> sps;
    for(int i=0;i<NUM;i++) sps.push_back(new TH1F("","",250,0,250));
    std::vector<TGraph*> grs;
    for(int i=0;i<NUM;i++) grs.push_back(new TGraph());

    for(int i=0;i<data.size();i++){
        int hv10 = (int)(10*data[i][0]);
        for(int j=0;j<hvs.size();j++){
            if((int)(10*hvs[j])!=hv10) continue;
            sps[j]->Fill(data[i][1]/4);
            grs[j]->SetPoint(grs[j]->GetN(),data[i][1],data[i][2]);
        }
    }

    TGraph* gain = new TGraph();
    std::vector<TGraphErrors*> gainfit;
    for(int i=0;i<NUM;i++) gainfit.push_back(new TGraphErrors());
    for(int i=0;i<NUM;i++){
        spsAnalyzer* ana = new spsAnalyzer(sps[i],0.0);
        gainfit[i] = ana->ExtractGainAndThreshold();
        printf("HV=%.2f, gain=%.2f\n",hvs[i],ana->gain_fit);
        //if(ana->gain_fit < 5)  continue;
        gain->SetPoint(gain->GetN(),hvs[i],ana->gain_fit);
    }

    printf("Finish the data process, and start to write the data\n");

    TFile* ofile = new TFile("result.root","recreate");
    for(int i=0;i<NUM;i++) sps[i]->Write(Form("sps%d",(int)(10*hvs[i])));
    for(int i=0;i<NUM;i++) grs[i]->Write(Form( "tw%d",(int)(10*hvs[i])));
    for(int i=0;i<NUM;i++) gainfit[i]->Write(Form( "gainfit%d",(int)(10*hvs[i])));
    gain->Write("gain");
    ofile->Close();

    printf("Done\n");
    return 1;

}

