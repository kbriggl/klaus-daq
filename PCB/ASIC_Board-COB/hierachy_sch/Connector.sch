EESchema Schematic File Version 2
LIBS:klaus
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Si5340
LIBS:Si514
LIBS:SN65LVDS104
LIBS:TPS7A7001
LIBS:BSH-060-01-X-D-A
LIBS:power_special
LIBS:ADN4661
LIBS:Mechanical
LIBS:THS4541
LIBS:ISL21080-09
LIBS:ISL60002-11
LIBS:ISL60002-18
LIBS:LTC6248
LIBS:MAX6120
LIBS:OPA2354
LIBS:REFDES
LIBS:klaus5_tb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date "2017-08-11"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X30 P1
U 1 1 5990A9E5
P 2900 3600
F 0 "P1" H 2978 3641 50  0000 L CNN
F 1 "CONN_01X30" H 2978 3550 50  0000 L CNN
F 2 "klaus:Conn_2X15" H 2900 3600 50  0001 C CNN
F 3 "" H 2900 3600 50  0000 C CNN
	1    2900 3600
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X30 P2
U 1 1 5990AA57
P 8700 3500
F 0 "P2" H 8778 3541 50  0000 L CNN
F 1 "CONN_01X30" H 8778 3450 50  0000 L CNN
F 2 "klaus:Conn_2X15" H 8700 3500 50  0001 C CNN
F 3 "" H 8700 3500 50  0000 C CNN
	1    8700 3500
	-1   0    0    -1  
$EndComp
Text HLabel 3300 2150 2    60   Output ~ 0
di_rst
Text HLabel 3300 2250 2    60   Output ~ 0
di_fifo_ext_trig
Text HLabel 3300 2350 2    60   Output ~ 0
di_cs
Text HLabel 3300 2450 2    60   Output ~ 0
di_srst
Text HLabel 3300 2550 2    60   Output ~ 0
di_sclk
Text HLabel 3300 2650 2    60   Output ~ 0
di_sdi
Text HLabel 3300 2750 2    60   Input ~ 0
do_fifo_full
Text HLabel 3300 2850 2    60   Input ~ 0
do_sdo
Text HLabel 3300 2950 2    60   Input ~ 0
do_dbg_1
Text HLabel 3300 3450 2    60   Input ~ 0
do_TxD_N
Text HLabel 3300 3550 2    60   Input ~ 0
do_TxD_P
Text HLabel 3300 3650 2    60   Output ~ 0
di_clk_N
Text HLabel 3300 3750 2    60   Output ~ 0
di_clk_P
Text HLabel 3300 4050 2    60   Input ~ 0
do_fe_trigger_or
Text HLabel 3300 4550 2    60   Output ~ 0
di_FE_ext_trig
Text HLabel 3300 4650 2    60   Input ~ 0
do_dbg_2
Text HLabel 3300 4750 2    60   Output ~ 0
di_aqu_en
$Comp
L GNDD #PWR027
U 1 1 5990AD5E
P 3300 5050
F 0 "#PWR027" H 3300 4800 50  0001 C CNN
F 1 "GNDD" V 3305 4922 50  0000 R CNN
F 2 "" H 3300 5050 50  0000 C CNN
F 3 "" H 3300 5050 50  0000 C CNN
	1    3300 5050
	0    -1   -1   0   
$EndComp
$Comp
L VCCD18 #PWR028
U 1 1 5990AD7C
P 3300 4850
F 0 "#PWR028" H 3300 4700 50  0001 C CNN
F 1 "VCCD18" V 3317 4978 50  0000 L CNN
F 2 "" H 3300 4850 50  0000 C CNN
F 3 "" H 3300 4850 50  0000 C CNN
	1    3300 4850
	0    1    1    0   
$EndComp
$Comp
L GNDD #PWR029
U 1 1 5990AD9E
P 3300 3950
F 0 "#PWR029" H 3300 3700 50  0001 C CNN
F 1 "GNDD" V 3305 3822 50  0000 R CNN
F 2 "" H 3300 3950 50  0000 C CNN
F 3 "" H 3300 3950 50  0000 C CNN
	1    3300 3950
	0    -1   -1   0   
$EndComp
$Comp
L GNDD #PWR030
U 1 1 5990ADBC
P 3300 3150
F 0 "#PWR030" H 3300 2900 50  0001 C CNN
F 1 "GNDD" V 3305 3022 50  0000 R CNN
F 2 "" H 3300 3150 50  0000 C CNN
F 3 "" H 3300 3150 50  0000 C CNN
	1    3300 3150
	0    -1   -1   0   
$EndComp
$Comp
L VCCD18 #PWR031
U 1 1 5990ADDC
P 3300 3350
F 0 "#PWR031" H 3300 3200 50  0001 C CNN
F 1 "VCCD18" V 3317 3478 50  0000 L CNN
F 2 "" H 3300 3350 50  0000 C CNN
F 3 "" H 3300 3350 50  0000 C CNN
	1    3300 3350
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5990AE0E
P 4350 3900
F 0 "R2" H 4420 3946 50  0000 L CNN
F 1 "4.7k(DNP)" H 4420 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4280 3900 50  0001 C CNN
F 3 "" H 4350 3900 50  0000 C CNN
	1    4350 3900
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5990AEA2
P 4650 3900
F 0 "R3" H 4720 3946 50  0000 L CNN
F 1 "4.7k(DNP)" H 4720 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4580 3900 50  0001 C CNN
F 3 "" H 4650 3900 50  0000 C CNN
	1    4650 3900
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5990AF38
P 4950 3900
F 0 "R4" H 5020 3946 50  0000 L CNN
F 1 "4.7k(DNP)" H 5020 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4880 3900 50  0001 C CNN
F 3 "" H 4950 3900 50  0000 C CNN
	1    4950 3900
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5990AF3E
P 5250 3900
F 0 "R5" H 5320 3946 50  0000 L CNN
F 1 "4.7k(DNP)" H 5320 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5180 3900 50  0001 C CNN
F 3 "" H 5250 3900 50  0000 C CNN
	1    5250 3900
	1    0    0    -1  
$EndComp
$Comp
L VCCD33 #PWR032
U 1 1 5990B2E8
P 4800 3650
F 0 "#PWR032" H 4800 3500 50  0001 C CNN
F 1 "VCCD33" H 4817 3823 50  0000 C CNN
F 2 "" H 4800 3650 50  0000 C CNN
F 3 "" H 4800 3650 50  0000 C CNN
	1    4800 3650
	1    0    0    -1  
$EndComp
Text HLabel 5550 4150 2    60   BiDi ~ 0
dio_i2cB_sda
Text HLabel 5550 4350 2    60   BiDi ~ 0
dio_i2cA_sda
Text HLabel 5550 4250 2    60   Output ~ 0
di_i2cB_scl
Text HLabel 5550 4450 2    60   Output ~ 0
di_i2cA_scl
NoConn ~ 9200 4950
NoConn ~ 8900 4050
NoConn ~ 8900 3750
NoConn ~ 8900 3450
Text HLabel 9500 2050 2    60   Input ~ 0
amon_2_conn
Text HLabel 9500 2150 2    60   Input ~ 0
amon_1_conn
Text HLabel 9500 2250 2    60   Output ~ 0
ADC_test_n
$Comp
L VCCA18 #PWR033
U 1 1 5990BF9F
P 9200 2450
F 0 "#PWR033" H 9200 2300 50  0001 C CNN
F 1 "VCCA18" V 9217 2578 50  0000 L CNN
F 2 "" H 9200 2450 50  0000 C CNN
F 3 "" H 9200 2450 50  0000 C CNN
	1    9200 2450
	0    1    1    0   
$EndComp
$Comp
L GNDA #PWR034
U 1 1 5990BFC5
P 9200 2750
F 0 "#PWR034" H 9200 2500 50  0001 C CNN
F 1 "GNDA" V 9205 2622 50  0000 R CNN
F 2 "" H 9200 2750 50  0000 C CNN
F 3 "" H 9200 2750 50  0000 C CNN
	1    9200 2750
	0    -1   -1   0   
$EndComp
$Comp
L VCCA33 #PWR035
U 1 1 5990BFFD
P 9200 3150
F 0 "#PWR035" H 9200 3000 50  0001 C CNN
F 1 "VCCA33" V 9217 3278 50  0000 L CNN
F 2 "" H 9200 3150 50  0000 C CNN
F 3 "" H 9200 3150 50  0000 C CNN
	1    9200 3150
	0    1    1    0   
$EndComp
$Comp
L GNDA #PWR036
U 1 1 5990C023
P 9200 3350
F 0 "#PWR036" H 9200 3100 50  0001 C CNN
F 1 "GNDA" V 9205 3222 50  0000 R CNN
F 2 "" H 9200 3350 50  0000 C CNN
F 3 "" H 9200 3350 50  0000 C CNN
	1    9200 3350
	0    -1   -1   0   
$EndComp
$Comp
L GNDA #PWR037
U 1 1 5990C055
P 9200 3950
F 0 "#PWR037" H 9200 3700 50  0001 C CNN
F 1 "GNDA" V 9205 3822 50  0000 R CNN
F 2 "" H 9200 3950 50  0000 C CNN
F 3 "" H 9200 3950 50  0000 C CNN
	1    9200 3950
	0    -1   -1   0   
$EndComp
$Comp
L GNDD #PWR038
U 1 1 5990C0A5
P 9200 4250
F 0 "#PWR038" H 9200 4000 50  0001 C CNN
F 1 "GNDD" V 9205 4122 50  0000 R CNN
F 2 "" H 9200 4250 50  0000 C CNN
F 3 "" H 9200 4250 50  0000 C CNN
	1    9200 4250
	0    -1   -1   0   
$EndComp
$Comp
L VCCD33 #PWR039
U 1 1 5990C0D7
P 9200 4550
F 0 "#PWR039" H 9200 4400 50  0001 C CNN
F 1 "VCCD33" V 9217 4678 50  0000 L CNN
F 2 "" H 9200 4550 50  0000 C CNN
F 3 "" H 9200 4550 50  0000 C CNN
	1    9200 4550
	0    1    1    0   
$EndComp
Text HLabel 9200 3650 2    60   Output ~ 0
HV
Text HLabel 9500 2950 2    60   Output ~ 0
ADC_test_p
Wire Wire Line
	3300 3150 3300 3050
Wire Wire Line
	3300 3350 3300 3250
Wire Wire Line
	3300 3950 3300 3850
Wire Wire Line
	3300 4950 3300 4850
Wire Wire Line
	4350 4050 4350 4150
Connection ~ 4350 4150
Wire Wire Line
	4650 4050 4650 4250
Connection ~ 4650 4250
Wire Wire Line
	4950 4050 4950 4350
Connection ~ 4950 4350
Wire Wire Line
	5250 4050 5250 4450
Connection ~ 5250 4450
Wire Wire Line
	4350 3750 4350 3650
Wire Wire Line
	4350 3650 5250 3650
Wire Wire Line
	5250 3650 5250 3750
Wire Wire Line
	4950 3750 4950 3650
Connection ~ 4950 3650
Wire Wire Line
	4650 3750 4650 3650
Connection ~ 4650 3650
Connection ~ 4800 3650
Wire Wire Line
	9000 2550 9000 2350
Connection ~ 9000 2450
Wire Wire Line
	9000 2850 9000 2650
Connection ~ 9000 2750
Wire Wire Line
	9000 3050 9000 3150
Connection ~ 9000 3150
Wire Wire Line
	9000 3250 9000 3350
Connection ~ 9000 3350
Wire Wire Line
	9000 3550 9000 3650
Connection ~ 9000 3650
Wire Wire Line
	9000 3850 9000 3950
Connection ~ 9000 3950
Wire Wire Line
	9000 4350 9000 4150
Connection ~ 9000 4250
Wire Wire Line
	9000 4650 9000 4450
Connection ~ 9000 4550
Wire Wire Line
	9500 2950 8900 2950
Wire Wire Line
	8900 3050 9000 3050
Wire Wire Line
	8900 3150 9200 3150
Wire Wire Line
	8900 3350 9200 3350
Wire Wire Line
	8900 3250 9000 3250
Wire Wire Line
	8900 2850 9000 2850
Wire Wire Line
	8900 2750 9200 2750
Wire Wire Line
	9000 2650 8900 2650
Wire Wire Line
	8900 2550 9000 2550
Wire Wire Line
	8900 2450 9200 2450
Wire Wire Line
	9000 2350 8900 2350
Wire Wire Line
	8900 2250 9500 2250
Wire Wire Line
	9500 2150 8900 2150
Wire Wire Line
	8900 2050 9500 2050
Wire Wire Line
	8900 3550 9000 3550
Wire Wire Line
	8900 3650 9200 3650
Wire Wire Line
	8900 3850 9000 3850
Wire Wire Line
	8900 3950 9200 3950
Wire Wire Line
	9000 4150 8900 4150
Wire Wire Line
	8900 4250 9200 4250
Wire Wire Line
	9000 4450 8900 4450
Wire Wire Line
	8900 4550 9200 4550
Wire Wire Line
	8900 4650 9000 4650
Wire Wire Line
	9200 4950 8900 4950
Wire Wire Line
	3300 2150 3100 2150
Wire Wire Line
	3100 2350 3300 2350
Wire Wire Line
	3100 2450 3300 2450
Wire Wire Line
	3300 2550 3100 2550
Wire Wire Line
	3300 2650 3100 2650
Wire Wire Line
	3300 2750 3100 2750
Wire Wire Line
	3300 2850 3100 2850
Wire Wire Line
	3300 3250 3100 3250
Wire Wire Line
	3300 3350 3100 3350
Wire Wire Line
	3300 3450 3100 3450
Wire Wire Line
	3300 3550 3100 3550
Wire Wire Line
	3100 3650 3300 3650
Wire Wire Line
	3100 3750 3300 3750
Wire Wire Line
	3300 3850 3100 3850
Wire Wire Line
	3100 3950 3300 3950
Wire Wire Line
	3100 4050 3300 4050
Wire Wire Line
	3300 4850 3100 4850
Wire Wire Line
	3100 4950 3300 4950
Wire Wire Line
	3100 5050 3300 5050
Wire Wire Line
	3100 4150 5550 4150
Wire Wire Line
	5550 4250 3100 4250
Wire Wire Line
	3100 4350 5550 4350
Wire Wire Line
	5550 4450 3100 4450
Wire Wire Line
	3300 2950 3100 2950
Wire Wire Line
	3300 3050 3100 3050
Wire Wire Line
	3100 3150 3300 3150
Connection ~ 3300 3150
Wire Wire Line
	3300 2250 3100 2250
Wire Wire Line
	3300 4550 3100 4550
Wire Wire Line
	3300 4650 3100 4650
Wire Wire Line
	3300 4750 3100 4750
Wire Wire Line
	9000 4350 8900 4350
Connection ~ 3300 4850
Connection ~ 9200 4550
Connection ~ 3300 3350
Connection ~ 3300 3950
Wire Wire Line
	9200 4750 8900 4750
Wire Wire Line
	9200 4850 8900 4850
Text HLabel 9200 4750 2    60   Output ~ 0
di_FE_T0_etrig_P
Text HLabel 9200 4850 2    60   Output ~ 0
di_FE_T0_etrig_N
$EndSCHEMATC
