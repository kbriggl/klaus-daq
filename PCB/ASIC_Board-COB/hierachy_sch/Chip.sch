EESchema Schematic File Version 2
LIBS:kicad-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:CX3225SB
LIBS:Si514
LIBS:SN65LVDS104
LIBS:TPS7A7001
LIBS:BSH-060-01-X-D-A
LIBS:power_special
LIBS:ADN4661
LIBS:Mechanical
LIBS:klaus
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L klaus U?
U 1 1 59897469
P 1800 6650
F 0 "U?" H 4400 12250 60  0000 C CNN
F 1 "klaus" H 4400 12144 60  0000 C CNN
F 2 "" H 4400 9770 60  0000 C CNN
F 3 "" H 4400 9770 60  0000 C CNN
	1    1800 6650
	1    0    0    -1  
$EndComp
Text HLabel 1250 6050 0    60   Input ~ 0
SiPM_in[0]
Text HLabel 1250 5950 0    60   Input ~ 0
SiPM_in[1]
Text HLabel 1250 5850 0    60   Input ~ 0
SiPM_in[2]
Text HLabel 1250 5750 0    60   Input ~ 0
SiPM_in[3]
Text HLabel 1250 5650 0    60   Input ~ 0
SiPM_in[4]
Text HLabel 1250 5550 0    60   Input ~ 0
SiPM_in[5]
Text HLabel 1250 5450 0    60   Input ~ 0
SiPM_in[6]
Text HLabel 1250 5350 0    60   Input ~ 0
SiPM_in[7]
Text HLabel 1250 5250 0    60   Input ~ 0
SiPM_in[8]
Text HLabel 1250 5150 0    60   Input ~ 0
SiPM_in[9]
Text HLabel 1250 5050 0    60   Input ~ 0
SiPM_in[10]
Text HLabel 1250 4950 0    60   Input ~ 0
SiPM_in[11]
Text HLabel 1250 4850 0    60   Input ~ 0
SiPM_in[12]
Text HLabel 1250 4750 0    60   Input ~ 0
SiPM_in[13]
Text HLabel 1250 4650 0    60   Input ~ 0
SiPM_in[14]
Text HLabel 1250 4550 0    60   Input ~ 0
SiPM_in[15]
Text HLabel 1250 4450 0    60   Input ~ 0
SiPM_in[16]
Text HLabel 1250 4350 0    60   Input ~ 0
SiPM_in[17]
Text HLabel 1250 3750 0    60   Input ~ 0
SiPM_in[18]
Text HLabel 1250 3650 0    60   Input ~ 0
SiPM_in[19]
Text HLabel 1250 3550 0    60   Input ~ 0
SiPM_in[20]
Text HLabel 1250 3450 0    60   Input ~ 0
SiPM_in[21]
Text HLabel 1250 3350 0    60   Input ~ 0
SiPM_in[22]
Text HLabel 1250 3250 0    60   Input ~ 0
SiPM_in[23]
Text HLabel 1250 3150 0    60   Input ~ 0
SiPM_in[24]
Text HLabel 1250 3050 0    60   Input ~ 0
SiPM_in[25]
Text HLabel 1250 2950 0    60   Input ~ 0
SiPM_in[26]
Text HLabel 1250 2850 0    60   Input ~ 0
SiPM_in[27]
Text HLabel 1250 2750 0    60   Input ~ 0
SiPM_in[28]
Text HLabel 1250 2650 0    60   Input ~ 0
SiPM_in[29]
Text HLabel 1250 2550 0    60   Input ~ 0
SiPM_in[30]
Text HLabel 1250 2450 0    60   Input ~ 0
SiPM_in[31]
Text HLabel 1250 2350 0    60   Input ~ 0
SiPM_in[32]
Text HLabel 1250 2250 0    60   Input ~ 0
SiPM_in[33]
Text HLabel 1250 2150 0    60   Input ~ 0
SiPM_in[34]
Text HLabel 1250 2050 0    60   Input ~ 0
SiPM_in[35]
Text HLabel 1250 4050 0    60   Input ~ 0
REF_1p2
$Comp
L VCCA18 #PWR?
U 1 1 5989874D
P 2200 7150
F 0 "#PWR?" H 2200 7000 50  0001 C CNN
F 1 "VCCA18" V 2218 7277 50  0000 L CNN
F 2 "" H 2200 7150 50  0000 C CNN
F 3 "" H 2200 7150 50  0000 C CNN
	1    2200 7150
	0    -1   -1   0   
$EndComp
$Comp
L VCCA33 #PWR?
U 1 1 59898766
P 2200 7050
F 0 "#PWR?" H 2200 6900 50  0001 C CNN
F 1 "VCCA33" V 2218 7177 50  0000 L CNN
F 2 "" H 2200 7050 50  0000 C CNN
F 3 "" H 2200 7050 50  0000 C CNN
	1    2200 7050
	0    -1   -1   0   
$EndComp
$Comp
L GNDA #PWR?
U 1 1 5989877F
P 2200 6950
F 0 "#PWR?" H 2200 6700 50  0001 C CNN
F 1 "GNDA" V 2205 6823 50  0000 R CNN
F 2 "" H 2200 6950 50  0000 C CNN
F 3 "" H 2200 6950 50  0000 C CNN
	1    2200 6950
	0    1    1    0   
$EndComp
$Comp
L GNDA #PWR?
U 1 1 598987B4
P 1450 4150
F 0 "#PWR?" H 1450 3900 50  0001 C CNN
F 1 "GNDA" V 1455 4023 50  0000 R CNN
F 2 "" H 1450 4150 50  0000 C CNN
F 3 "" H 1450 4150 50  0000 C CNN
	1    1450 4150
	0    1    1    0   
$EndComp
$Comp
L GNDA #PWR?
U 1 1 598987D0
P 1450 3950
F 0 "#PWR?" H 1450 3700 50  0001 C CNN
F 1 "GNDA" V 1455 3823 50  0000 R CNN
F 2 "" H 1450 3950 50  0000 C CNN
F 3 "" H 1450 3950 50  0000 C CNN
	1    1450 3950
	0    1    1    0   
$EndComp
$Comp
L VCCA33 #PWR?
U 1 1 598987EB
P 1450 3850
F 0 "#PWR?" H 1450 3700 50  0001 C CNN
F 1 "VCCA33" V 1468 3977 50  0000 L CNN
F 2 "" H 1450 3850 50  0000 C CNN
F 3 "" H 1450 3850 50  0000 C CNN
	1    1450 3850
	0    -1   -1   0   
$EndComp
$Comp
L VCCA33 #PWR?
U 1 1 59898804
P 1450 4250
F 0 "#PWR?" H 1450 4100 50  0001 C CNN
F 1 "VCCA33" V 1468 4377 50  0000 L CNN
F 2 "" H 1450 4250 50  0000 C CNN
F 3 "" H 1450 4250 50  0000 C CNN
	1    1450 4250
	0    -1   -1   0   
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59898C1B
P 5300 7350
F 0 "#PWR?" H 5300 7100 50  0001 C CNN
F 1 "GNDD" H 5305 7177 50  0000 C CNN
F 2 "" H 5300 7350 50  0000 C CNN
F 3 "" H 5300 7350 50  0000 C CNN
	1    5300 7350
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59898C31
P 4800 7350
F 0 "#PWR?" H 4800 7100 50  0001 C CNN
F 1 "GNDD" H 4805 7177 50  0000 C CNN
F 2 "" H 4800 7350 50  0000 C CNN
F 3 "" H 4800 7350 50  0000 C CNN
	1    4800 7350
	1    0    0    -1  
$EndComp
$Comp
L VCCD18 #PWR?
U 1 1 59898C47
P 5500 7350
F 0 "#PWR?" H 5500 7200 50  0001 C CNN
F 1 "VCCD18" V 5517 7478 50  0000 L CNN
F 2 "" H 5500 7350 50  0000 C CNN
F 3 "" H 5500 7350 50  0000 C CNN
	1    5500 7350
	0    1    1    0   
$EndComp
$Comp
L VCCD18 #PWR?
U 1 1 59898C62
P 4300 7350
F 0 "#PWR?" H 4300 7200 50  0001 C CNN
F 1 "VCCD18" V 4317 7478 50  0000 L CNN
F 2 "" H 4300 7350 50  0000 C CNN
F 3 "" H 4300 7350 50  0000 C CNN
	1    4300 7350
	0    1    1    0   
$EndComp
$Comp
L GNDA #PWR?
U 1 1 5989925B
P 2650 1200
F 0 "#PWR?" H 2650 950 50  0001 C CNN
F 1 "GNDA" V 2655 1073 50  0000 R CNN
F 2 "" H 2650 1200 50  0000 C CNN
F 3 "" H 2650 1200 50  0000 C CNN
	1    2650 1200
	0    1    1    0   
$EndComp
$Comp
L VCCA33 #PWR?
U 1 1 59899272
P 2650 1100
F 0 "#PWR?" H 2650 950 50  0001 C CNN
F 1 "VCCA33" V 2668 1227 50  0000 L CNN
F 2 "" H 2650 1100 50  0000 C CNN
F 3 "" H 2650 1100 50  0000 C CNN
	1    2650 1100
	0    -1   -1   0   
$EndComp
$Comp
L VCCA18 #PWR?
U 1 1 59899290
P 2650 1000
F 0 "#PWR?" H 2650 850 50  0001 C CNN
F 1 "VCCA18" V 2668 1127 50  0000 L CNN
F 2 "" H 2650 1000 50  0000 C CNN
F 3 "" H 2650 1000 50  0000 C CNN
	1    2650 1000
	0    -1   -1   0   
$EndComp
$Comp
L VCCD33 #PWR?
U 1 1 598992BD
P 4100 1250
F 0 "#PWR?" H 4100 1100 50  0001 C CNN
F 1 "VCCD33" H 4117 1423 50  0000 C CNN
F 2 "" H 4100 1250 50  0000 C CNN
F 3 "" H 4100 1250 50  0000 C CNN
	1    4100 1250
	1    0    0    -1  
$EndComp
Connection ~ 4400 1000
Wire Wire Line
	4400 1250 4400 1000
Connection ~ 4900 1000
Wire Wire Line
	4900 1250 4900 1000
Wire Wire Line
	5000 1000 5000 1250
Wire Wire Line
	4300 1000 5000 1000
Wire Wire Line
	4300 1250 4300 1000
Connection ~ 4500 1200
Wire Wire Line
	4500 1250 4500 1200
Connection ~ 4600 1200
Wire Wire Line
	4600 1250 4600 1200
Connection ~ 4700 1200
Wire Wire Line
	4700 1250 4700 1200
Wire Wire Line
	4800 1200 4800 1250
Wire Wire Line
	4200 1200 4800 1200
Wire Wire Line
	4200 1250 4200 1200
Connection ~ 3500 1000
Wire Wire Line
	3500 1250 3500 1000
Connection ~ 3400 1000
Wire Wire Line
	3400 1250 3400 1000
Connection ~ 3800 1000
Wire Wire Line
	3800 1250 3800 1000
Wire Wire Line
	3900 1000 3900 1250
Wire Wire Line
	2650 1000 3900 1000
Wire Wire Line
	3100 1100 3100 1250
Wire Wire Line
	2650 1100 3100 1100
Connection ~ 3000 1200
Wire Wire Line
	3000 1250 3000 1200
Connection ~ 3200 1200
Wire Wire Line
	3200 1250 3200 1200
Connection ~ 3300 1200
Wire Wire Line
	3300 1250 3300 1200
Connection ~ 3600 1200
Wire Wire Line
	3600 1250 3600 1200
Wire Wire Line
	3700 1200 3700 1250
Wire Wire Line
	2650 1200 3700 1200
Connection ~ 5600 6950
Wire Wire Line
	5600 6850 5600 6950
Wire Wire Line
	5700 6950 5700 6850
Connection ~ 4300 7150
Wire Wire Line
	5000 7150 5000 6850
Connection ~ 5300 6950
Wire Wire Line
	5300 6950 5700 6950
Connection ~ 5500 7150
Connection ~ 5000 7150
Connection ~ 4800 6950
Connection ~ 4700 6950
Wire Wire Line
	4700 6850 4700 6950
Connection ~ 4600 6950
Wire Wire Line
	4600 6850 4600 6950
Wire Wire Line
	4500 6950 4800 6950
Wire Wire Line
	4300 7150 5000 7150
Wire Wire Line
	1450 3850 1600 3850
Wire Wire Line
	1450 3950 1600 3950
Wire Wire Line
	3300 7150 2200 7150
Wire Wire Line
	2500 7050 2200 7050
Wire Wire Line
	3500 6950 2200 6950
Wire Wire Line
	1250 4050 1600 4050
Wire Wire Line
	1450 4250 1600 4250
Wire Wire Line
	1450 4150 1600 4150
Connection ~ 5400 6950
Wire Wire Line
	5400 6850 5400 6950
Wire Wire Line
	5300 6850 5300 7350
Connection ~ 5200 7150
Wire Wire Line
	5200 6850 5200 7150
Wire Wire Line
	5500 6850 5500 7350
Wire Wire Line
	5100 7150 5500 7150
Wire Wire Line
	5100 6850 5100 7150
Wire Wire Line
	4800 6850 4800 7350
Wire Wire Line
	4500 6850 4500 6950
Connection ~ 4400 7150
Wire Wire Line
	4400 6850 4400 7150
Connection ~ 4900 7150
Wire Wire Line
	4900 6850 4900 7150
Wire Wire Line
	4300 6850 4300 7350
Wire Wire Line
	1250 6050 1600 6050
Wire Wire Line
	1250 5950 1600 5950
Wire Wire Line
	1250 5850 1600 5850
Wire Wire Line
	1250 5750 1600 5750
Wire Wire Line
	1250 5650 1600 5650
Wire Wire Line
	1250 5550 1600 5550
Wire Wire Line
	1250 5450 1600 5450
Wire Wire Line
	1250 5350 1600 5350
Wire Wire Line
	1250 5250 1600 5250
Wire Wire Line
	1250 5150 1600 5150
Wire Wire Line
	1250 5050 1600 5050
Wire Wire Line
	1250 4950 1600 4950
Wire Wire Line
	1250 4850 1600 4850
Wire Wire Line
	1250 4750 1600 4750
Wire Wire Line
	1250 4650 1600 4650
Wire Wire Line
	1250 4550 1600 4550
Wire Wire Line
	1250 4450 1600 4450
Wire Wire Line
	1250 4350 1600 4350
Wire Wire Line
	1250 2050 1600 2050
Wire Wire Line
	1250 2150 1600 2150
Wire Wire Line
	1250 2250 1600 2250
Wire Wire Line
	1250 2350 1600 2350
Wire Wire Line
	1250 2450 1600 2450
Wire Wire Line
	1250 2550 1600 2550
Wire Wire Line
	1250 2650 1600 2650
Wire Wire Line
	1250 2750 1600 2750
Wire Wire Line
	1250 2850 1600 2850
Wire Wire Line
	1250 2950 1600 2950
Wire Wire Line
	1250 3050 1600 3050
Wire Wire Line
	1250 3150 1600 3150
Wire Wire Line
	1250 3250 1600 3250
Wire Wire Line
	1250 3350 1600 3350
Wire Wire Line
	1250 3450 1600 3450
Wire Wire Line
	1250 3550 1600 3550
Wire Wire Line
	1250 3650 1600 3650
Wire Wire Line
	1250 3750 1600 3750
Connection ~ 2600 7150
Wire Wire Line
	2600 6850 2600 7150
Connection ~ 2800 7150
Wire Wire Line
	2800 6850 2800 7150
Connection ~ 2900 7150
Wire Wire Line
	2900 6850 2900 7150
Connection ~ 3200 7150
Wire Wire Line
	3200 6850 3200 7150
Wire Wire Line
	3300 7150 3300 6850
Wire Wire Line
	2500 6850 2500 7050
Connection ~ 3400 6950
Wire Wire Line
	3400 6850 3400 6950
Connection ~ 3100 6950
Wire Wire Line
	3100 6850 3100 6950
Connection ~ 3000 6950
Wire Wire Line
	3000 6850 3000 6950
Connection ~ 2700 6950
Wire Wire Line
	2700 6850 2700 6950
Connection ~ 2400 6950
Wire Wire Line
	2400 6850 2400 6950
Wire Wire Line
	3500 6850 3500 6950
Wire Wire Line
	5100 1250 5100 1000
Wire Wire Line
	5100 1000 5200 1000
Wire Wire Line
	5200 1000 5200 1250
Wire Wire Line
	5300 1250 5300 1200
Wire Wire Line
	5300 1200 6050 1200
Wire Wire Line
	5900 1200 5900 1250
Wire Wire Line
	5400 1250 5400 1200
Connection ~ 5400 1200
Wire Wire Line
	5800 1250 5800 1000
Wire Wire Line
	5700 1250 5700 1000
Wire Wire Line
	5600 1250 5600 1000
Wire Wire Line
	5500 1000 5500 1250
$Comp
L VCCD18 #PWR?
U 1 1 59899A63
P 5200 1000
F 0 "#PWR?" H 5200 850 50  0001 C CNN
F 1 "VCCD18" H 5217 1173 50  0000 C CNN
F 2 "" H 5200 1000 50  0000 C CNN
F 3 "" H 5200 1000 50  0000 C CNN
	1    5200 1000
	1    0    0    -1  
$EndComp
$Comp
L VCCD18 #PWR?
U 1 1 59899A79
P 4700 1000
F 0 "#PWR?" H 4700 850 50  0001 C CNN
F 1 "VCCD18" H 4717 1173 50  0000 C CNN
F 2 "" H 4700 1000 50  0000 C CNN
F 3 "" H 4700 1000 50  0000 C CNN
	1    4700 1000
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59899B23
P 4800 1200
F 0 "#PWR?" H 4800 950 50  0001 C CNN
F 1 "GNDD" H 4805 1027 50  0000 C CNN
F 2 "" H 4800 1200 50  0000 C CNN
F 3 "" H 4800 1200 50  0000 C CNN
	1    4800 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7200 5750 7300 5750
Wire Wire Line
	7300 5750 7300 2350
Wire Wire Line
	7300 2350 7200 2350
Wire Wire Line
	7200 2450 7300 2450
Connection ~ 7300 2450
Wire Wire Line
	7200 4150 7300 4150
Connection ~ 7300 4150
Wire Wire Line
	7200 4350 7300 4350
Connection ~ 7300 4350
Wire Wire Line
	7200 4650 7300 4650
Connection ~ 7300 4650
Wire Wire Line
	7200 2550 7350 2550
Wire Wire Line
	7350 2550 7350 5850
Wire Wire Line
	7350 5850 7200 5850
Wire Wire Line
	7200 4450 7350 4450
Connection ~ 7350 4450
Wire Wire Line
	7200 4550 7350 4550
Connection ~ 7350 4550
Wire Wire Line
	7200 4250 7350 4250
Connection ~ 7350 4250
Wire Wire Line
	7200 2650 7350 2650
Connection ~ 7350 2650
Wire Wire Line
	7200 2850 7350 2850
Connection ~ 7350 2850
Wire Wire Line
	7400 2750 7200 2750
Wire Wire Line
	7400 2100 7400 2750
$Comp
L GNDD #PWR?
U 1 1 5989A346
P 7350 5850
F 0 "#PWR?" H 7350 5600 50  0001 C CNN
F 1 "GNDD" H 7355 5677 50  0000 C CNN
F 2 "" H 7350 5850 50  0000 C CNN
F 3 "" H 7350 5850 50  0000 C CNN
	1    7350 5850
	1    0    0    -1  
$EndComp
$Comp
L VCCD18 #PWR?
U 1 1 5989A367
P 7300 2350
F 0 "#PWR?" H 7300 2200 50  0001 C CNN
F 1 "VCCD18" H 7317 2523 50  0000 C CNN
F 2 "" H 7300 2350 50  0000 C CNN
F 3 "" H 7300 2350 50  0000 C CNN
	1    7300 2350
	1    0    0    -1  
$EndComp
$Comp
L VCCD33 #PWR?
U 1 1 5989A386
P 7400 2100
F 0 "#PWR?" H 7400 1950 50  0001 C CNN
F 1 "VCCD33" H 7417 2273 50  0000 C CNN
F 2 "" H 7400 2100 50  0000 C CNN
F 3 "" H 7400 2100 50  0000 C CNN
	1    7400 2100
	1    0    0    -1  
$EndComp
$Comp
L VCCA33 #PWR?
U 1 1 5989A45B
P 5800 1000
F 0 "#PWR?" H 5800 850 50  0001 C CNN
F 1 "VCCA33" H 5817 1173 50  0000 C CNN
F 2 "" H 5800 1000 50  0000 C CNN
F 3 "" H 5800 1000 50  0000 C CNN
	1    5800 1000
	1    0    0    -1  
$EndComp
Text HLabel 5600 1000 1    60   Output ~ 0
do_dbg_1
Text HLabel 5500 1000 1    60   Output ~ 0
do_dbg_2
Text HLabel 5700 1000 1    60   Output ~ 0
FE_fired
$Comp
L GNDD #PWR?
U 1 1 5989A4AC
P 6050 1200
F 0 "#PWR?" H 6050 950 50  0001 C CNN
F 1 "GNDD" V 6055 1072 50  0000 R CNN
F 2 "" H 6050 1200 50  0000 C CNN
F 3 "" H 6050 1200 50  0000 C CNN
	1    6050 1200
	0    -1   -1   0   
$EndComp
Connection ~ 5900 1200
Text HLabel 4000 1000 1    60   Input ~ 0
ADCtest_p
Wire Wire Line
	4000 1000 4000 1250
Text HLabel 3600 7150 3    60   Input ~ 0
ADCtest_n
Text HLabel 3800 7150 3    60   Output ~ 0
amon_1
Text HLabel 3700 7150 3    60   Output ~ 0
amon_2
Wire Wire Line
	3600 6850 3600 7150
Wire Wire Line
	3700 6850 3700 7150
Wire Wire Line
	3800 6850 3800 7150
Text HLabel 3900 7150 3    60   BiDi ~ 0
ref_cm
Text HLabel 4000 7150 3    60   BiDi ~ 0
reg_g
Text HLabel 4100 7150 3    60   BiDi ~ 0
ref_v
Text HLabel 4200 7150 3    60   Input ~ 0
REF_1p8
Wire Wire Line
	4200 7150 4200 6850
Wire Wire Line
	4100 7150 4100 6850
Wire Wire Line
	4000 7150 4000 6850
Wire Wire Line
	3900 7150 3900 6850
Wire Wire Line
	7500 4750 7200 4750
Wire Wire Line
	7500 4850 7200 4850
Text HLabel 7500 4750 2    60   Output ~ 0
do_TxD_n
Text HLabel 7500 4850 2    60   Output ~ 0
do_Txd_p
Wire Wire Line
	7500 5250 7200 5250
Wire Wire Line
	7500 5350 7200 5350
Wire Wire Line
	7500 5150 7200 5150
Wire Wire Line
	7500 5450 7200 5450
Wire Wire Line
	7500 5550 7200 5550
Wire Wire Line
	7500 5650 7200 5650
Text HLabel 7500 5150 2    60   Input ~ 0
di_FE_T0_etrig_n
Text HLabel 7500 5250 2    60   Input ~ 0
di_FE_T0_etrig_p
Text HLabel 7500 5350 2    60   Input ~ 0
di_FIFO_etrig_n
Text HLabel 7500 5450 2    60   Input ~ 0
di_FIFO_etrig_p
Text HLabel 7500 5550 2    60   Input ~ 0
di_FE_etrig_n
Text HLabel 7500 5650 2    60   Input ~ 0
di_FE_etrig_p
Wire Wire Line
	7200 4950 8550 4950
Wire Wire Line
	7200 5050 8550 5050
$Comp
L R R?
U 1 1 5989B38D
P 8900 5000
F 0 "R?" H 8970 5046 50  0000 L CNN
F 1 "100" H 8970 4955 50  0000 L CNN
F 2 "" V 8830 5000 50  0000 C CNN
F 3 "" H 8900 5000 50  0000 C CNN
	1    8900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 4950 8550 4850
Wire Wire Line
	8550 4850 9250 4850
Wire Wire Line
	8550 5050 8550 5150
Wire Wire Line
	8550 5150 9250 5150
Connection ~ 8900 5150
Connection ~ 8900 4850
Text HLabel 9250 4850 2    60   Input ~ 0
di_ser_clk_n
Text HLabel 9250 5150 2    60   Input ~ 0
di_ser_clk_p
Wire Wire Line
	7550 2950 7200 2950
Wire Wire Line
	7550 3050 7200 3050
Wire Wire Line
	7550 3150 7200 3150
Wire Wire Line
	7550 3250 7200 3250
Wire Wire Line
	7550 3350 7200 3350
Wire Wire Line
	7550 3450 7200 3450
Wire Wire Line
	7550 3550 7200 3550
Wire Wire Line
	7550 3650 7200 3650
Wire Wire Line
	7550 3750 7200 3750
Wire Wire Line
	7550 3850 7200 3850
Wire Wire Line
	7550 3950 7200 3950
Wire Wire Line
	7550 4050 7200 4050
Text HLabel 7550 4050 2    60   Input ~ 0
di_rst
Text HLabel 7550 3950 2    60   Input ~ 0
di_i2cB_scl
Text HLabel 7550 3850 2    60   BiDi ~ 0
dio_i2cB_sda
Text HLabel 7550 3650 2    60   BiDi ~ 0
dio_i2cA_sda
Text HLabel 7550 3750 2    60   Input ~ 0
di_i2cA_scl
Text HLabel 7550 3550 2    60   Output ~ 0
do_fifo_full
Text HLabel 7550 3450 2    60   Input ~ 0
di_acq_en
Text HLabel 7550 3350 2    60   Input ~ 0
di_srst
Text HLabel 7550 3250 2    60   Output ~ 0
do_sdo
Text HLabel 7550 3150 2    60   Input ~ 0
di_sclk
Text HLabel 7550 3050 2    60   Input ~ 0
di_sdi
Text HLabel 7550 2950 2    60   Input ~ 0
di_cs
$EndSCHEMATC
