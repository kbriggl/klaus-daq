klaus5_tb

Gerberfiles of the copper layers:
Layer1(Top):	klaus5_tb-F.Cu.gbr
Layer2(In1):	klaus5_tb-In1.Cu.gbr
Layer3(In2):	klaus5_tb-In2.Cu.gbr
Layer4(In3):	klaus5_tb-In3.Cu.gbr
Layer5(In4):	klaus5_tb-In4.Cu.gbr
Layer6(Bot):	klaus5_tb-B.Cu.gbr

Gerberfiles of the soldermask:
klaus5_tb-F.Mask.gbr
klaus5_tb-B.Mask.gbr

Gerberfiles of the pastemask:
klaus5_tb-F.Paste.gbr
klaus5_tb-B.Paste.gbr

Gerberfiles of the silkscreen:
klaus5_tb-F.SilkS.gbr
klaus5_tb-B.SilkS.gbr

Gerberfiles of the edge cuts:
klaus5_tb-Edge.Cuts.gbr

Drilling files: 
klaus5_tb-PTH.drl
klaus5_tb-NPTH.drl
Caution: All drilling diameters in the drilling file are finished dimensions (after plated-through and tinning) of the finished board !!!

Production: 
10 pieces klaus5_tb Outer 
dimensions 90 x 120 mm ~ 1,55 mm thick 
6 layers Standard layer construction, FR4 (TG 150) 
Outer layers (TOP, BOTTOM) Copper 35um, chem. Nickel-Gold Surface 
Interiors (LAYER2, LAYER3, LAYER4, LAYER5) Copper 35um 
Solder stop mask on both sides, with E-test                        
External contour of individual PCB milled 


Delivery time: Standard 7 AT
