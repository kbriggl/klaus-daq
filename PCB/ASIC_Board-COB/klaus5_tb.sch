EESchema Schematic File Version 2
LIBS:klaus
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Si5340
LIBS:Si514
LIBS:SN65LVDS104
LIBS:TPS7A7001
LIBS:BSH-060-01-X-D-A
LIBS:power_special
LIBS:ADN4661
LIBS:Mechanical
LIBS:THS4541
LIBS:ISL21080-09
LIBS:ISL60002-11
LIBS:ISL60002-18
LIBS:LTC6248
LIBS:MAX6120
LIBS:OPA2354
LIBS:REFDES
LIBS:klaus5_tb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title "Simple Testboard for KLauS5"
Date "2017-08-11"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7500 1000 2500 5000
U 598A4542
F0 "klaus_chip" 60
F1 "./hierachy_sch/klaus_chip.sch" 60
F2 "SiPM_in[0]" I L 7500 5800 60 
F3 "SiPM_in[1]" I L 7500 5700 60 
F4 "SiPM_in[2]" I L 7500 5600 60 
F5 "SiPM_in[3]" I L 7500 5500 60 
F6 "SiPM_in[4]" I L 7500 5400 60 
F7 "SiPM_in[5]" I L 7500 5300 60 
F8 "SiPM_in[6]" I L 7500 5200 60 
F9 "SiPM_in[7]" I L 7500 5100 60 
F10 "SiPM_in[8]" I L 7500 5000 60 
F11 "SiPM_in[9]" I L 7500 4900 60 
F12 "SiPM_in[10]" I L 7500 4800 60 
F13 "SiPM_in[11]" I L 7500 4700 60 
F14 "SiPM_in[12]" I L 7500 4600 60 
F15 "SiPM_in[13]" I L 7500 4500 60 
F16 "SiPM_in[14]" I L 7500 4400 60 
F17 "SiPM_in[15]" I L 7500 4300 60 
F18 "SiPM_in[16]" I L 7500 4200 60 
F19 "SiPM_in[17]" I L 7500 4100 60 
F20 "SiPM_in[18]" I L 7500 4000 60 
F21 "SiPM_in[19]" I L 7500 3900 60 
F22 "SiPM_in[20]" I L 7500 3800 60 
F23 "SiPM_in[21]" I L 7500 3700 60 
F24 "SiPM_in[22]" I L 7500 3600 60 
F25 "SiPM_in[23]" I L 7500 3500 60 
F26 "SiPM_in[24]" I L 7500 3400 60 
F27 "SiPM_in[25]" I L 7500 3300 60 
F28 "SiPM_in[26]" I L 7500 3200 60 
F29 "SiPM_in[27]" I L 7500 3100 60 
F30 "SiPM_in[28]" I L 7500 3000 60 
F31 "SiPM_in[29]" I L 7500 2900 60 
F32 "SiPM_in[30]" I L 7500 2800 60 
F33 "SiPM_in[31]" I L 7500 2700 60 
F34 "SiPM_in[32]" I L 7500 2600 60 
F35 "SiPM_in[33]" I L 7500 2500 60 
F36 "SiPM_in[34]" I L 7500 2400 60 
F37 "SiPM_in[35]" I L 7500 2300 60 
F38 "REF_1p2" I L 7500 2050 60 
F39 "do_dbg_1" O R 10000 1100 60 
F40 "do_dbg_2" O R 10000 1200 60 
F41 "FE_fired" O R 10000 1300 60 
F42 "ADCtest_p" I L 7500 1100 60 
F43 "ADCtest_n" I L 7500 1200 60 
F44 "amon_1" O R 10000 5850 60 
F45 "amon_2" O R 10000 5750 60 
F46 "ref_cm" B L 7500 1300 60 
F47 "reg_g" B L 7500 1400 60 
F48 "ref_v" B L 7500 1500 60 
F49 "REF_1p8" I L 7500 1600 60 
F50 "di_rst" I R 10000 3500 60 
F51 "di_i2cB_scl" I R 10000 3400 60 
F52 "dio_i2cB_sda" B R 10000 3300 60 
F53 "dio_i2cA_sda" B R 10000 3100 60 
F54 "di_i2cA_scl" I R 10000 3200 60 
F55 "do_fifo_full" O R 10000 1750 60 
F56 "di_acq_en" I R 10000 3000 60 
F57 "di_srst" I R 10000 2900 60 
F58 "do_sdo" O R 10000 2800 60 
F59 "di_sclk" I R 10000 2700 60 
F60 "di_sdi" I R 10000 2600 60 
F61 "di_cs" I R 10000 2500 60 
F62 "di_FE_T0_etrig_N" I R 10000 4450 60 
F63 "di_FE_T0_etrig_P" I R 10000 4350 60 
F64 "di_FIFO_etrig_N" I R 10000 4250 60 
F65 "di_FIFO_etrig_P" I R 10000 4150 60 
F66 "di_FE_etrig_N" I R 10000 4050 60 
F67 "di_FE_etrig_P" I R 10000 3950 60 
F68 "di_ser_clk_N" I R 10000 3800 60 
F69 "di_ser_clk_P" I R 10000 3700 60 
F70 "do_TxD_N" O R 10000 1500 60 
F71 "do_Txd_P" O R 10000 1600 60 
$EndSheet
$Sheet
S 1400 1000 2150 3350
U 598A4600
F0 "Connector" 60
F1 "./hierachy_sch/Connector.sch" 60
F2 "di_rst" O R 3550 1200 60 
F3 "di_fifo_ext_trig" O L 1400 1150 60 
F4 "di_cs" O R 3550 1300 60 
F5 "di_srst" O R 3550 1400 60 
F6 "di_sclk" O R 3550 1500 60 
F7 "di_sdi" O R 3550 1600 60 
F8 "do_fifo_full" I L 1400 1250 60 
F9 "do_sdo" I R 3550 1700 60 
F10 "do_dbg_1" I R 3550 1950 60 
F11 "do_fe_trigger_or" I L 1400 1350 60 
F12 "di_FE_ext_trig" O L 1400 1450 60 
F13 "do_dbg_2" I R 3550 2050 60 
F14 "di_aqu_en" O L 1400 1550 60 
F15 "dio_i2cB_sda" B L 1400 2350 60 
F16 "dio_i2cA_sda" B L 1400 2650 60 
F17 "di_i2cB_scl" O L 1400 2450 60 
F18 "di_i2cA_scl" O L 1400 2750 60 
F19 "amon_2_conn" I L 1400 4100 60 
F20 "amon_1_conn" I L 1400 4000 60 
F21 "ADC_test_n" O L 1400 3600 60 
F22 "HV" O R 3550 3200 60 
F23 "ADC_test_p" O L 1400 3500 60 
F24 "do_TxD_N" I R 3550 2350 60 
F25 "do_TxD_P" I R 3550 2450 60 
F26 "di_clk_N" O R 3550 2550 60 
F27 "di_clk_P" O R 3550 2650 60 
F28 "di_FE_T0_etrig_P" O R 3550 3500 60 
F29 "di_FE_T0_etrig_N" O R 3550 3600 60 
$EndSheet
$Sheet
S 1050 4700 2150 2700
U 598A4603
F0 "Reference" 60
F1 "./hierachy_sch/Reference.sch" 60
F2 "refv_fe" O R 3200 5100 60 
F3 "refv_v" O R 3200 5300 60 
F4 "refv_cm" O R 3200 5500 60 
F5 "ADC_test_n" O R 3200 6000 60 
F6 "ADC_test_p" O R 3200 6150 60 
F7 "refv_1p8" O R 3200 5700 60 
$EndSheet
$Sheet
S 3750 4300 1450 850 
U 598A4606
F0 "Power_filter" 60
F1 "./hierachy_sch/Power_filter.sch" 60
$EndSheet
$Sheet
S 5300 2200 1700 3800
U 598C3CFE
F0 "SiPM_input" 60
F1 "./hierachy_sch/SiPM_input.sch" 60
F2 "HV" I L 5300 3200 60 
F3 "SiPM_in[0]" O R 7000 5800 60 
F4 "SiPM_in[1]" O R 7000 5700 60 
F5 "SiPM_in[2]" O R 7000 5600 60 
F6 "SiPM_in[3]" O R 7000 5500 60 
F7 "SiPM_in[4]" O R 7000 5400 60 
F8 "SiPM_in[5]" O R 7000 5300 60 
F9 "SiPM_in[6]" O R 7000 5200 60 
F10 "SiPM_in[7]" O R 7000 5100 60 
F11 "SiPM_in[8]" O R 7000 5000 60 
F12 "SiPM_in[9]" O R 7000 4900 60 
F13 "SiPM_in[10]" O R 7000 4800 60 
F14 "SiPM_in[11]" O R 7000 4700 60 
F15 "SiPM_in[12]" O R 7000 4600 60 
F16 "SiPM_in[13]" O R 7000 4500 60 
F17 "SiPM_in[14]" O R 7000 4400 60 
F18 "SiPM_in[15]" O R 7000 4300 60 
F19 "SiPM_in[16]" O R 7000 4200 60 
F20 "SiPM_in[17]" O R 7000 4100 60 
F21 "SiPM_in[18]" O R 7000 4000 60 
F22 "SiPM_in[19]" O R 7000 3900 60 
F23 "SiPM_in[20]" O R 7000 3800 60 
F24 "SiPM_in[21]" O R 7000 3700 60 
F25 "SiPM_in[22]" O R 7000 3600 60 
F26 "SiPM_in[23]" O R 7000 3500 60 
F27 "SiPM_in[24]" O R 7000 3400 60 
F28 "SiPM_in[25]" O R 7000 3300 60 
F29 "SiPM_in[26]" O R 7000 3200 60 
F30 "SiPM_in[27]" O R 7000 3100 60 
F31 "SiPM_in[28]" O R 7000 3000 60 
F32 "SiPM_in[29]" O R 7000 2900 60 
F33 "SiPM_in[30]" O R 7000 2800 60 
F34 "SiPM_in[31]" O R 7000 2700 60 
F35 "SiPM_in[32]" O R 7000 2600 60 
F36 "SiPM_in[33]" O R 7000 2500 60 
F37 "SiPM_in[34]" O R 7000 2400 60 
F38 "SiPM_in[35]" O R 7000 2300 60 
$EndSheet
Wire Wire Line
	7000 5800 7500 5800
Wire Wire Line
	7000 5700 7500 5700
Wire Wire Line
	7000 5600 7500 5600
Wire Wire Line
	7000 5500 7500 5500
Wire Wire Line
	7000 5400 7500 5400
Wire Wire Line
	7000 5300 7500 5300
Wire Wire Line
	7000 5200 7500 5200
Wire Wire Line
	7000 5100 7500 5100
Wire Wire Line
	7000 5000 7500 5000
Wire Wire Line
	7000 4900 7500 4900
Wire Wire Line
	7000 4800 7500 4800
Wire Wire Line
	7000 4700 7500 4700
Wire Wire Line
	7000 4600 7500 4600
Wire Wire Line
	7000 4500 7500 4500
Wire Wire Line
	7000 4400 7500 4400
Wire Wire Line
	7000 4300 7500 4300
Wire Wire Line
	7000 4200 7500 4200
Wire Wire Line
	7000 4100 7500 4100
Wire Wire Line
	7000 4000 7500 4000
Wire Wire Line
	7000 3900 7500 3900
Wire Wire Line
	7000 3800 7500 3800
Wire Wire Line
	7000 3700 7500 3700
Wire Wire Line
	7000 3600 7500 3600
Wire Wire Line
	7000 3500 7500 3500
Wire Wire Line
	7000 3400 7500 3400
Wire Wire Line
	7000 3300 7500 3300
Wire Wire Line
	7000 3200 7500 3200
Wire Wire Line
	7000 3100 7500 3100
Wire Wire Line
	7000 3000 7500 3000
Wire Wire Line
	7000 2900 7500 2900
Wire Wire Line
	7000 2800 7500 2800
Wire Wire Line
	7000 2700 7500 2700
Wire Wire Line
	7000 2600 7500 2600
Wire Wire Line
	7000 2500 7500 2500
Wire Wire Line
	7000 2400 7500 2400
Wire Wire Line
	7000 2300 7500 2300
$Sheet
S 4300 1000 1350 900 
U 59954C12
F0 "Monitor" 60
F1 "./hierachy_sch/Monitor.sch" 60
F2 "amon_1" I R 5650 1150 60 
F3 "amon_2" I R 5650 1250 60 
F4 "amon_1_conn" O R 5650 1500 60 
F5 "amon_2_conn" O R 5650 1600 60 
$EndSheet
Wire Wire Line
	3700 1200 3550 1200
Wire Wire Line
	3700 1300 3550 1300
Wire Wire Line
	3700 1400 3550 1400
Wire Wire Line
	3700 1500 3550 1500
Wire Wire Line
	3700 1600 3550 1600
Wire Wire Line
	3700 1700 3550 1700
Wire Wire Line
	3700 1950 3550 1950
Wire Wire Line
	3700 2050 3550 2050
Text Label 3700 1200 0    60   ~ 0
di_rst
Text Label 3700 1300 0    60   ~ 0
di_cs
Text Label 3700 1400 0    60   ~ 0
di_srst
Text Label 3700 1500 0    60   ~ 0
di_sclk
Text Label 3700 1600 0    60   ~ 0
di_sdi
Text Label 3700 1700 0    60   ~ 0
do_sdo
Text Label 3700 1950 0    60   ~ 0
do_dbg_1
Text Label 3700 2050 0    60   ~ 0
do_dbg_2
Wire Wire Line
	3700 2350 3550 2350
Wire Wire Line
	3700 2450 3550 2450
Wire Wire Line
	3700 2550 3550 2550
Wire Wire Line
	3700 2650 3550 2650
Wire Wire Line
	3550 3200 5300 3200
Wire Wire Line
	3350 5100 3200 5100
Wire Wire Line
	3350 5300 3200 5300
Wire Wire Line
	3350 5500 3200 5500
Wire Wire Line
	3350 6000 3200 6000
Wire Wire Line
	3350 6150 3200 6150
Text Label 3350 6150 0    60   ~ 0
ADC_test_p
Text Label 3350 6000 0    60   ~ 0
ADC_test_n
Text Label 3350 5500 0    60   ~ 0
refv_cm
Text Label 3350 5300 0    60   ~ 0
refv_v
Text Label 3350 5100 0    60   ~ 0
refv_fe
Wire Wire Line
	7300 2050 7500 2050
Wire Wire Line
	7300 1600 7500 1600
Wire Wire Line
	7300 1500 7500 1500
Wire Wire Line
	6900 1400 7500 1400
Wire Wire Line
	7300 1100 7500 1100
Wire Wire Line
	7300 1200 7500 1200
Wire Wire Line
	7300 1300 7500 1300
Text Label 7300 1100 2    60   ~ 0
ADC_test_p
Text Label 7300 1200 2    60   ~ 0
ADC_test_n
Text Label 7300 1300 2    60   ~ 0
refv_cm
Text Label 7300 1500 2    60   ~ 0
refv_v
Wire Wire Line
	10150 5750 10000 5750
Wire Wire Line
	10150 5850 10000 5850
Text Label 10150 5850 0    60   ~ 0
amon_1
Text Label 10150 5750 0    60   ~ 0
amon_2
Wire Wire Line
	10150 2500 10000 2500
Wire Wire Line
	10150 2600 10000 2600
Wire Wire Line
	10150 2700 10000 2700
Wire Wire Line
	10150 2800 10000 2800
Wire Wire Line
	10150 2900 10000 2900
Wire Wire Line
	10150 3000 10000 3000
Wire Wire Line
	10150 3100 10000 3100
Wire Wire Line
	10150 3200 10000 3200
Wire Wire Line
	10150 3300 10000 3300
Wire Wire Line
	10150 3400 10000 3400
Wire Wire Line
	10150 3500 10000 3500
Text Label 10150 2500 0    60   ~ 0
di_cs
Text Label 10150 2600 0    60   ~ 0
di_sdi
Text Label 10150 2700 0    60   ~ 0
di_sclk
Text Label 10150 2800 0    60   ~ 0
do_sdo
Text Label 10150 2900 0    60   ~ 0
di_srst
Text Label 10150 3000 0    60   ~ 0
di_aqu_en
Text Label 10150 3100 0    60   ~ 0
dio_i2cA_sda
Text Label 10150 3200 0    60   ~ 0
di_i2cA_scl
Text Label 10150 3300 0    60   ~ 0
dio_i2cB_sda
Text Label 10150 3400 0    60   ~ 0
di_i2cB_scl
Text Label 10150 3500 0    60   ~ 0
di_rst
Wire Wire Line
	10150 3700 10000 3700
Wire Wire Line
	10150 3800 10000 3800
Wire Wire Line
	10150 3950 10000 3950
Wire Wire Line
	10150 4050 10000 4050
Wire Wire Line
	10150 4150 10000 4150
Wire Wire Line
	10150 4250 10000 4250
Wire Wire Line
	10150 4350 10000 4350
Wire Wire Line
	10150 4450 10000 4450
Wire Wire Line
	1250 2350 1400 2350
Wire Wire Line
	1250 2450 1400 2450
Wire Wire Line
	1250 2650 1400 2650
Wire Wire Line
	1250 2750 1400 2750
Text Label 1250 2750 2    60   ~ 0
di_i2cA_scl
Text Label 1250 2650 2    60   ~ 0
dio_i2cA_sda
Text Label 1250 2450 2    60   ~ 0
di_i2cB_scl
Text Label 1250 2350 2    60   ~ 0
dio_i2cB_sda
Wire Wire Line
	1250 3500 1400 3500
Wire Wire Line
	1250 3600 1400 3600
Wire Wire Line
	1250 4000 1400 4000
Wire Wire Line
	1250 4100 1400 4100
Text Label 1250 4100 2    60   ~ 0
amon_2_conn
Text Label 1250 4000 2    60   ~ 0
amon_1_conn
Text Label 1250 3600 2    60   ~ 0
ADC_test_n
Text Label 1250 3500 2    60   ~ 0
ADC_test_p
Wire Wire Line
	5800 1150 5650 1150
Wire Wire Line
	5800 1250 5650 1250
Wire Wire Line
	5800 1500 5650 1500
Wire Wire Line
	5800 1600 5650 1600
Text Label 5800 1150 0    60   ~ 0
amon_1
Text Label 5800 1250 0    60   ~ 0
amon_2
Text Label 5800 1500 0    60   ~ 0
amon_1_conn
Text Label 5800 1600 0    60   ~ 0
amon_2_conn
Text Label 7300 2050 2    60   ~ 0
refv_fe
Text Label 3700 2350 0    60   ~ 0
do_TxD_N
Text Label 3700 2450 0    60   ~ 0
do_TxD_P
Text Label 3700 2550 0    60   ~ 0
di_clk_N
Text Label 3700 2650 0    60   ~ 0
di_clk_P
Wire Wire Line
	10200 1100 10000 1100
Wire Wire Line
	10200 1200 10000 1200
Wire Wire Line
	10200 1300 10000 1300
Wire Wire Line
	10200 1500 10000 1500
Wire Wire Line
	10200 1600 10000 1600
Wire Wire Line
	10200 1750 10000 1750
Text Label 10200 1750 0    60   ~ 0
do_fifo_full
Text Label 10200 1600 0    60   ~ 0
do_TxD_P
Text Label 10200 1500 0    60   ~ 0
do_TxD_N
Text Label 10200 1100 0    60   ~ 0
do_dbg_1
Text Label 10200 1200 0    60   ~ 0
do_dbg_2
Text Label 10150 3700 0    60   ~ 0
di_clk_P
Text Label 10150 3800 0    60   ~ 0
di_clk_N
Wire Wire Line
	1250 1250 1400 1250
Wire Wire Line
	1250 1550 1400 1550
Text Label 1250 1550 2    60   ~ 0
di_aqu_en
Text Label 1250 1250 2    60   ~ 0
do_fifo_full
Wire Wire Line
	3350 5700 3200 5700
Text Label 3350 5700 0    60   ~ 0
refv_1p8
Text Label 7300 1600 2    60   ~ 0
refv_1p8
$Comp
L GNDA #PWR01
U 1 1 599B35F1
P 6900 1400
F 0 "#PWR01" H 6900 1150 50  0001 C CNN
F 1 "GNDA" V 6905 1273 50  0000 R CNN
F 2 "" H 6900 1400 50  0000 C CNN
F 3 "" H 6900 1400 50  0000 C CNN
	1    6900 1400
	0    1    1    0   
$EndComp
Text Notes 6450 850  0    60   ~ 0
Where to connect the ref_g?\nTo nearby GNDA?
Wire Notes Line
	6500 900  6500 1400
Wire Notes Line
	6500 1400 6550 1400
Text Label 10200 1300 0    60   ~ 0
FE_fired
Wire Wire Line
	1250 1350 1400 1350
Text Label 1250 1350 2    60   ~ 0
FE_fired
Wire Wire Line
	1250 1450 1400 1450
Wire Wire Line
	1250 1150 1400 1150
$Sheet
S 4200 6550 1250 850 
U 5997A102
F0 "LVDS_to_CMOS" 60
F1 "./hierachy_sch/LVDS.sch" 60
F2 "di_fifo_etrig" I R 5450 6650 60 
F3 "di_fe_etrig" I R 5450 6750 60 
F4 "di_fifo_etrig_N" O R 5450 6850 60 
F5 "di_fifo_etrig_P" O R 5450 6950 60 
F6 "di_fe_etrig_N" O R 5450 7050 60 
F7 "di_fe_etrig_P" O R 5450 7150 60 
$EndSheet
Wire Wire Line
	5600 6650 5450 6650
Wire Wire Line
	5600 6750 5450 6750
Wire Wire Line
	5600 6850 5450 6850
Wire Wire Line
	5600 6950 5450 6950
Wire Wire Line
	5600 7050 5450 7050
Wire Wire Line
	5600 7150 5450 7150
Text Label 5600 6650 0    60   ~ 0
di_fifo_etrig
Text Label 5600 6750 0    60   ~ 0
di_fe_etrig
Text Label 5600 6850 0    60   ~ 0
di_fifo_etrig_N
Text Label 5600 6950 0    60   ~ 0
di_fifo_etrig_P
Text Label 5600 7050 0    60   ~ 0
di_fe_etrig_N
Text Label 5600 7150 0    60   ~ 0
di_fe_etrig_P
Text Label 1250 1150 2    60   ~ 0
di_fifo_etrig
Text Label 1250 1450 2    60   ~ 0
di_fe_etrig
Text Label 10150 4050 0    60   ~ 0
di_fe_etrig_N
Text Label 10150 3950 0    60   ~ 0
di_fe_etrig_P
Text Label 10150 4150 0    60   ~ 0
di_fifo_etrig_P
Text Label 10150 4250 0    60   ~ 0
di_fifo_etrig_N
Text Label 10150 4350 0    60   ~ 0
di_FE_T0_etrig_P
Text Label 10150 4450 0    60   ~ 0
di_FE_T0_etrig_N
Wire Wire Line
	3700 3500 3550 3500
Wire Wire Line
	3700 3600 3550 3600
Text Label 3700 3500 0    60   ~ 0
di_FE_T0_etrig_P
Text Label 3700 3600 0    60   ~ 0
di_FE_T0_etrig_N
$Comp
L MountingHole H1
U 1 1 59B98112
P 4150 5400
F 0 "H1" H 4249 5453 60  0000 L CNN
F 1 "MountingHole" H 4249 5347 60  0000 L CNN
F 2 "Moutings:Mountinghole_3mm_P5mm" H 4150 5400 60  0001 C CNN
F 3 "" H 4150 5400 60  0001 C CNN
	1    4150 5400
	1    0    0    -1  
$EndComp
$Comp
L MountingHole H2
U 1 1 59B9A094
P 4150 5600
F 0 "H2" H 4249 5653 60  0000 L CNN
F 1 "MountingHole" H 4249 5547 60  0000 L CNN
F 2 "Moutings:Mountinghole_3mm_P5mm" H 4150 5600 60  0001 C CNN
F 3 "" H 4150 5600 60  0001 C CNN
	1    4150 5600
	1    0    0    -1  
$EndComp
$Comp
L MountingHole H3
U 1 1 59B9A44A
P 4150 5800
F 0 "H3" H 4249 5853 60  0000 L CNN
F 1 "MountingHole" H 4249 5747 60  0000 L CNN
F 2 "Moutings:Mountinghole_3mm_P5mm" H 4150 5800 60  0001 C CNN
F 3 "" H 4150 5800 60  0001 C CNN
	1    4150 5800
	1    0    0    -1  
$EndComp
$Comp
L MountingHole H4
U 1 1 59B9A800
P 4150 6000
F 0 "H4" H 4249 6053 60  0000 L CNN
F 1 "MountingHole" H 4249 5947 60  0000 L CNN
F 2 "Moutings:Mountinghole_3mm_P5mm" H 4150 6000 60  0001 C CNN
F 3 "" H 4150 6000 60  0001 C CNN
	1    4150 6000
	1    0    0    -1  
$EndComp
$Comp
L REFDES REF1
U 1 1 59BA0370
P 4950 6250
F 0 "REF1" H 5090 6303 60  0000 L CNN
F 1 "REFDES" H 5090 6197 60  0000 L CNN
F 2 "Moutings:REF_marker" H 4950 6250 60  0001 C CNN
F 3 "" H 4950 6250 60  0001 C CNN
	1    4950 6250
	1    0    0    -1  
$EndComp
$Comp
L REFDES REF2
U 1 1 59BA0730
P 5650 6250
F 0 "REF2" H 5790 6303 60  0000 L CNN
F 1 "REFDES" H 5790 6197 60  0000 L CNN
F 2 "Moutings:REF_marker" H 5650 6250 60  0001 C CNN
F 3 "" H 5650 6250 60  0001 C CNN
	1    5650 6250
	1    0    0    -1  
$EndComp
$Comp
L REFDES REF3
U 1 1 59BA0AF8
P 6350 6250
F 0 "REF3" H 6490 6303 60  0000 L CNN
F 1 "REFDES" H 6490 6197 60  0000 L CNN
F 2 "Moutings:REF_marker" H 6350 6250 60  0001 C CNN
F 3 "" H 6350 6250 60  0001 C CNN
	1    6350 6250
	1    0    0    -1  
$EndComp
$Comp
L REFDES REF4
U 1 1 59BA0ECC
P 7050 6250
F 0 "REF4" H 7190 6303 60  0000 L CNN
F 1 "REFDES" H 7190 6197 60  0000 L CNN
F 2 "Moutings:REF_marker" H 7050 6250 60  0001 C CNN
F 3 "" H 7050 6250 60  0001 C CNN
	1    7050 6250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
