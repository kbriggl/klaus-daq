EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title "KLauS-6 Chip On Board"
Date "2019-10-15"
Rev ""
Comp "KIP, Uni-Heidelberg"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1150 5900 0    60   Input ~ 0
SiPM_in[0]
Text HLabel 1150 5800 0    60   Input ~ 0
SiPM_in[1]
Text HLabel 1150 5700 0    60   Input ~ 0
SiPM_in[2]
Text HLabel 1150 5600 0    60   Input ~ 0
SiPM_in[3]
Text HLabel 1150 5500 0    60   Input ~ 0
SiPM_in[4]
Text HLabel 1150 5400 0    60   Input ~ 0
SiPM_in[5]
Text HLabel 1150 5300 0    60   Input ~ 0
SiPM_in[6]
Text HLabel 1150 5200 0    60   Input ~ 0
SiPM_in[7]
Text HLabel 1150 5100 0    60   Input ~ 0
SiPM_in[8]
Text HLabel 1150 5000 0    60   Input ~ 0
SiPM_in[9]
Text HLabel 1150 4900 0    60   Input ~ 0
SiPM_in[10]
Text HLabel 1150 4800 0    60   Input ~ 0
SiPM_in[11]
Text HLabel 1150 4700 0    60   Input ~ 0
SiPM_in[12]
Text HLabel 1150 4600 0    60   Input ~ 0
SiPM_in[13]
Text HLabel 1150 4500 0    60   Input ~ 0
SiPM_in[14]
Text HLabel 1150 4400 0    60   Input ~ 0
SiPM_in[15]
Text HLabel 1150 4300 0    60   Input ~ 0
SiPM_in[16]
Text HLabel 1150 4200 0    60   Input ~ 0
SiPM_in[17]
Text HLabel 1150 3600 0    60   Input ~ 0
SiPM_in[18]
Text HLabel 1150 3500 0    60   Input ~ 0
SiPM_in[19]
Text HLabel 1150 3400 0    60   Input ~ 0
SiPM_in[20]
Text HLabel 1150 3300 0    60   Input ~ 0
SiPM_in[21]
Text HLabel 1150 3200 0    60   Input ~ 0
SiPM_in[22]
Text HLabel 1150 3100 0    60   Input ~ 0
SiPM_in[23]
Text HLabel 1150 3000 0    60   Input ~ 0
SiPM_in[24]
Text HLabel 1150 2900 0    60   Input ~ 0
SiPM_in[25]
Text HLabel 1150 2800 0    60   Input ~ 0
SiPM_in[26]
Text HLabel 1150 2700 0    60   Input ~ 0
SiPM_in[27]
Text HLabel 1150 2600 0    60   Input ~ 0
SiPM_in[28]
Text HLabel 1150 2500 0    60   Input ~ 0
SiPM_in[29]
Text HLabel 1150 2400 0    60   Input ~ 0
SiPM_in[30]
Text HLabel 1150 2300 0    60   Input ~ 0
SiPM_in[31]
Text HLabel 1150 2200 0    60   Input ~ 0
SiPM_in[32]
Text HLabel 1150 2100 0    60   Input ~ 0
SiPM_in[33]
Text HLabel 1150 2000 0    60   Input ~ 0
SiPM_in[34]
Text HLabel 1150 1900 0    60   Input ~ 0
SiPM_in[35]
Text HLabel 1150 3900 0    60   Input ~ 0
REF_1p2
Connection ~ 5500 6800
Wire Wire Line
	5500 6700 5500 6800
Wire Wire Line
	5600 6800 5600 6700
Wire Wire Line
	4900 7000 4900 6700
Connection ~ 5200 6800
Wire Wire Line
	5200 6800 5300 6800
Connection ~ 4700 6800
Connection ~ 4600 6800
Wire Wire Line
	4600 6700 4600 6800
Connection ~ 4500 6800
Wire Wire Line
	4500 6700 4500 6800
Wire Wire Line
	4400 6800 4500 6800
Wire Wire Line
	4200 7000 4300 7000
Wire Wire Line
	3200 7000 3100 7000
Wire Wire Line
	2400 6900 2100 6900
Wire Wire Line
	3400 6800 3300 6800
Wire Wire Line
	1150 3900 1500 3900
Connection ~ 5300 6800
Wire Wire Line
	5300 6700 5300 6800
Wire Wire Line
	5200 6700 5200 6800
Connection ~ 5100 7000
Wire Wire Line
	5100 6700 5100 7000
Wire Wire Line
	5400 6700 5400 7000
Wire Wire Line
	5000 7000 5100 7000
Wire Wire Line
	5000 6700 5000 7000
Wire Wire Line
	4700 6700 4700 6800
Wire Wire Line
	4400 6700 4400 6800
Connection ~ 4300 7000
Wire Wire Line
	4300 6700 4300 7000
Connection ~ 4800 7000
Wire Wire Line
	4800 6700 4800 7000
Wire Wire Line
	4200 6700 4200 7000
Wire Wire Line
	1150 5900 1500 5900
Wire Wire Line
	1150 5800 1500 5800
Wire Wire Line
	1150 5700 1500 5700
Wire Wire Line
	1150 5600 1500 5600
Wire Wire Line
	1150 5500 1500 5500
Wire Wire Line
	1150 5400 1500 5400
Wire Wire Line
	1150 5300 1500 5300
Wire Wire Line
	1150 5200 1500 5200
Wire Wire Line
	1150 5100 1500 5100
Wire Wire Line
	1150 5000 1500 5000
Wire Wire Line
	1150 4900 1500 4900
Wire Wire Line
	1150 4800 1500 4800
Wire Wire Line
	1150 4700 1500 4700
Wire Wire Line
	1150 4600 1500 4600
Wire Wire Line
	1150 4500 1500 4500
Wire Wire Line
	1150 4400 1500 4400
Wire Wire Line
	1150 4300 1500 4300
Wire Wire Line
	1150 4200 1500 4200
Wire Wire Line
	1150 1900 1500 1900
Wire Wire Line
	1150 2000 1500 2000
Wire Wire Line
	1150 2100 1500 2100
Wire Wire Line
	1150 2200 1500 2200
Wire Wire Line
	1150 2300 1500 2300
Wire Wire Line
	1150 2400 1500 2400
Wire Wire Line
	1150 2500 1500 2500
Wire Wire Line
	1150 2600 1500 2600
Wire Wire Line
	1150 2700 1500 2700
Wire Wire Line
	1150 2800 1500 2800
Wire Wire Line
	1150 2900 1500 2900
Wire Wire Line
	1150 3000 1500 3000
Wire Wire Line
	1150 3100 1500 3100
Wire Wire Line
	1150 3200 1500 3200
Wire Wire Line
	1150 3300 1500 3300
Wire Wire Line
	1150 3400 1500 3400
Wire Wire Line
	1150 3500 1500 3500
Wire Wire Line
	1150 3600 1500 3600
Connection ~ 2500 7000
Wire Wire Line
	2500 6700 2500 7000
Connection ~ 2700 7000
Wire Wire Line
	2700 6700 2700 7000
Connection ~ 2800 7000
Wire Wire Line
	2800 6700 2800 7000
Connection ~ 3100 7000
Wire Wire Line
	3100 6700 3100 7000
Wire Wire Line
	3200 7000 3200 6700
Wire Wire Line
	2400 6700 2400 6900
Connection ~ 3300 6800
Wire Wire Line
	3300 6700 3300 6800
Connection ~ 3000 6800
Wire Wire Line
	3000 6700 3000 6800
Connection ~ 2900 6800
Wire Wire Line
	2900 6700 2900 6800
Connection ~ 2600 6800
Wire Wire Line
	2600 6700 2600 6800
Connection ~ 2300 6800
Wire Wire Line
	2300 6700 2300 6800
Wire Wire Line
	3400 6700 3400 6800
Wire Wire Line
	7100 5600 7200 5600
Wire Wire Line
	7200 5600 7200 4500
Wire Wire Line
	7200 2200 7100 2200
Wire Wire Line
	7100 2300 7200 2300
Connection ~ 7200 2300
Wire Wire Line
	7100 4000 7200 4000
Connection ~ 7200 4000
Wire Wire Line
	7100 4200 7200 4200
Connection ~ 7200 4200
Wire Wire Line
	7100 4500 7200 4500
Connection ~ 7200 4500
Wire Wire Line
	7100 2400 7250 2400
Wire Wire Line
	7250 2400 7250 2500
Wire Wire Line
	7250 5700 7100 5700
Wire Wire Line
	7100 4300 7250 4300
Connection ~ 7250 4300
Wire Wire Line
	7100 4400 7250 4400
Connection ~ 7250 4400
Wire Wire Line
	7100 4100 7250 4100
Connection ~ 7250 4100
Wire Wire Line
	7100 2500 7250 2500
Connection ~ 7250 2500
Wire Wire Line
	7100 2700 7250 2700
Connection ~ 7250 2700
Text HLabel 3500 7000 3    60   Input ~ 0
ADCn
Text HLabel 3700 7000 3    60   Output ~ 0
amon_1
Text HLabel 3600 7000 3    60   Output ~ 0
amon_2
Wire Wire Line
	3500 6700 3500 7000
Wire Wire Line
	3600 6700 3600 7000
Wire Wire Line
	3700 6700 3700 7000
Text HLabel 3800 7000 3    60   BiDi ~ 0
ref_cm
Text HLabel 3900 7000 3    60   BiDi ~ 0
reg_g
Text HLabel 4000 7000 3    60   BiDi ~ 0
ref_v
Text HLabel 4100 7000 3    60   Input ~ 0
REF_1p8
Wire Wire Line
	4100 7000 4100 6700
Wire Wire Line
	4000 7000 4000 6700
Wire Wire Line
	3900 7000 3900 6700
Wire Wire Line
	3800 7000 3800 6700
Wire Wire Line
	7400 4600 7100 4600
Wire Wire Line
	7400 4700 7100 4700
Text HLabel 7400 4600 2    60   Output ~ 0
do_TxD_N
Text HLabel 7400 4700 2    60   Output ~ 0
do_Txd_P
Wire Wire Line
	7400 5100 7100 5100
Wire Wire Line
	7400 5000 7100 5000
Text HLabel 7400 5000 2    60   Input ~ 0
di_FE_T0_etrig_N
Text HLabel 7400 5100 2    60   Input ~ 0
di_FE_T0_etrig_P
Text HLabel 7400 5200 2    60   Input ~ 0
di_FIFO_etrig_N
Text HLabel 7400 5300 2    60   Input ~ 0
di_FIFO_etrig_P
Text HLabel 7400 5400 2    60   Input ~ 0
di_FE_etrig_N
Text HLabel 7400 5500 2    60   Input ~ 0
di_FE_etrig_P
Wire Wire Line
	7100 4800 8450 4800
Wire Wire Line
	7100 4900 8450 4900
Wire Wire Line
	8450 4800 8450 4700
Wire Wire Line
	8450 4900 8450 5000
Text HLabel 9150 4700 2    60   Input ~ 0
di_ser_clk_N
Text HLabel 9150 5000 2    60   Input ~ 0
di_ser_clk_P
Wire Wire Line
	7450 2800 7100 2800
Wire Wire Line
	7450 2900 7100 2900
Wire Wire Line
	7450 3000 7100 3000
Wire Wire Line
	7450 3100 7100 3100
Wire Wire Line
	7450 3200 7100 3200
Wire Wire Line
	7450 3400 7100 3400
Wire Wire Line
	7450 3500 7100 3500
Wire Wire Line
	7450 3600 7100 3600
Wire Wire Line
	7450 3700 7100 3700
Wire Wire Line
	7450 3800 7100 3800
Wire Wire Line
	7450 3900 7100 3900
Text HLabel 7450 3900 2    60   Input ~ 0
di_rst
Text HLabel 7450 3800 2    60   Input ~ 0
di_i2cB_scl
Text HLabel 7450 3700 2    60   BiDi ~ 0
dio_i2cB_sda
Text HLabel 7450 3500 2    60   BiDi ~ 0
dio_i2cA_sda
Text HLabel 7450 3600 2    60   Input ~ 0
di_i2cA_scl
Text HLabel 7450 3400 2    60   Output ~ 0
do_fifo_full
Text HLabel 7450 3300 2    60   Input ~ 0
di_acq_en
Text HLabel 7450 3200 2    60   Input ~ 0
di_srst
Text HLabel 7450 3100 2    60   Output ~ 0
do_sdo
Text HLabel 7450 3000 2    60   Input ~ 0
di_sclk
Text HLabel 7450 2900 2    60   Input ~ 0
di_sdi
Text HLabel 7450 2800 2    60   Input ~ 0
di_cs
Wire Wire Line
	7400 5200 7100 5200
Wire Wire Line
	7400 5300 7100 5300
Wire Wire Line
	7400 5400 7100 5400
Wire Wire Line
	7400 5500 7100 5500
Wire Wire Line
	7100 3300 7450 3300
Wire Wire Line
	5800 6700 5800 6950
Wire Wire Line
	5900 6700 5900 6900
Wire Wire Line
	5900 6900 6000 6900
Wire Wire Line
	6000 6900 6000 6950
Wire Wire Line
	6000 6700 6000 6850
Wire Wire Line
	6000 6850 6200 6850
Wire Wire Line
	6200 6850 6200 6950
Wire Wire Line
	6100 6700 6100 6800
Wire Wire Line
	6100 6800 6400 6800
Wire Wire Line
	6400 6800 6400 6950
Wire Wire Line
	6200 6700 6200 6750
Wire Wire Line
	6200 6750 6600 6750
Wire Wire Line
	6600 6750 6600 6950
Wire Wire Line
	6600 7350 6600 7250
Wire Wire Line
	6400 7250 6400 7350
Connection ~ 6400 7350
Wire Wire Line
	6200 7250 6200 7350
Connection ~ 6200 7350
Wire Wire Line
	6000 7250 6000 7350
Connection ~ 6000 7350
Wire Wire Line
	5500 6800 5600 6800
Wire Wire Line
	5200 6800 5200 7200
Wire Wire Line
	4700 6800 4700 7200
Wire Wire Line
	4600 6800 4700 6800
Wire Wire Line
	4500 6800 4600 6800
Wire Wire Line
	5300 6800 5500 6800
Wire Wire Line
	5100 7000 5400 7000
Wire Wire Line
	4300 7000 4800 7000
Wire Wire Line
	4800 7000 4900 7000
Wire Wire Line
	2500 7000 2100 7000
Wire Wire Line
	2700 7000 2500 7000
Wire Wire Line
	2800 7000 2700 7000
Wire Wire Line
	3100 7000 2800 7000
Wire Wire Line
	3300 6800 3000 6800
Wire Wire Line
	3000 6800 2900 6800
Wire Wire Line
	2900 6800 2600 6800
Wire Wire Line
	2600 6800 2300 6800
Wire Wire Line
	2300 6800 2100 6800
Wire Wire Line
	7200 2300 7200 2200
Wire Wire Line
	7200 4000 7200 2300
Wire Wire Line
	7200 4200 7200 4000
Wire Wire Line
	7200 4500 7200 4200
Wire Wire Line
	7250 4300 7250 4400
Wire Wire Line
	7250 4400 7250 5700
Wire Wire Line
	7250 4100 7250 4300
Wire Wire Line
	7250 2500 7250 2700
Wire Wire Line
	7250 2700 7250 4100
Wire Wire Line
	6400 7350 6600 7350
Wire Wire Line
	6200 7350 6400 7350
Wire Wire Line
	6000 7350 6200 7350
Wire Wire Line
	8450 4700 8650 4700
Wire Wire Line
	8450 5000 8650 5000
$Comp
L KLauS-ASIC:klaus U1
U 1 1 5DA761C0
P 1700 6500
F 0 "U1" H 4300 9250 60  0000 C CNN
F 1 "klaus" H 4300 9100 60  0000 C CNN
F 2 "KLauS-Custom:KLauS-COB" H 4300 9620 60  0001 C CNN
F 3 "" H 4300 9620 60  0000 C CNN
	1    1700 6500
	1    0    0    -1  
$EndComp
$Comp
L power_special:VCCA33 #PWR027
U 1 1 5DA85F38
P 1200 3700
F 0 "#PWR027" H 1200 3550 50  0001 C CNN
F 1 "VCCA33" V 1200 3800 50  0000 L CNN
F 2 "" H 1200 3700 50  0000 C CNN
F 3 "" H 1200 3700 50  0000 C CNN
	1    1200 3700
	0    -1   -1   0   
$EndComp
$Comp
L power_special:VCCA33 #PWR030
U 1 1 5DA85F60
P 1200 4100
F 0 "#PWR030" H 1200 3950 50  0001 C CNN
F 1 "VCCA33" V 1200 4200 50  0000 L CNN
F 2 "" H 1200 4100 50  0000 C CNN
F 3 "" H 1200 4100 50  0000 C CNN
	1    1200 4100
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR029
U 1 1 5DA86110
P 1200 4000
F 0 "#PWR029" H 1200 3750 50  0001 C CNN
F 1 "GNDA" V 1205 3873 50  0000 R CNN
F 2 "" H 1200 4000 50  0000 C CNN
F 3 "" H 1200 4000 50  0000 C CNN
	1    1200 4000
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR028
U 1 1 5DA86136
P 1200 3800
F 0 "#PWR028" H 1200 3550 50  0001 C CNN
F 1 "GNDA" V 1205 3673 50  0000 R CNN
F 2 "" H 1200 3800 50  0000 C CNN
F 3 "" H 1200 3800 50  0000 C CNN
	1    1200 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 3700 1500 3700
Wire Wire Line
	1500 3800 1200 3800
Wire Wire Line
	1500 4000 1200 4000
Wire Wire Line
	1500 4100 1200 4100
$Comp
L power_special:VCCA18 #PWR033
U 1 1 5DAA67F2
P 2100 7000
F 0 "#PWR033" H 2100 6850 50  0001 C CNN
F 1 "VCCA18" V 2100 7100 50  0000 L CNN
F 2 "" H 2100 7000 50  0000 C CNN
F 3 "" H 2100 7000 50  0000 C CNN
	1    2100 7000
	0    -1   -1   0   
$EndComp
$Comp
L power_special:VCCA33 #PWR032
U 1 1 5DAA6821
P 2100 6900
F 0 "#PWR032" H 2100 6750 50  0001 C CNN
F 1 "VCCA33" V 2100 7000 50  0000 L CNN
F 2 "" H 2100 6900 50  0000 C CNN
F 3 "" H 2100 6900 50  0000 C CNN
	1    2100 6900
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR031
U 1 1 5DAA687A
P 2100 6800
F 0 "#PWR031" H 2100 6550 50  0001 C CNN
F 1 "GNDA" V 2105 6673 50  0000 R CNN
F 2 "" H 2100 6800 50  0000 C CNN
F 3 "" H 2100 6800 50  0000 C CNN
	1    2100 6800
	0    1    1    0   
$EndComp
$Comp
L power_special:VCCD18 #PWR036
U 1 1 5DAA68DE
P 5400 7200
F 0 "#PWR036" H 5400 7050 50  0001 C CNN
F 1 "VCCD18" V 5400 7450 50  0000 C CNN
F 2 "" H 5400 7200 50  0000 C CNN
F 3 "" H 5400 7200 50  0000 C CNN
	1    5400 7200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 7000 4900 7000
Connection ~ 5000 7000
Connection ~ 4900 7000
Wire Wire Line
	5400 7200 5400 7000
Connection ~ 5400 7000
$Comp
L power_special:VCCD33 #PWR037
U 1 1 5DABFB15
P 5700 7200
F 0 "#PWR037" H 5700 7050 50  0001 C CNN
F 1 "VCCD33" V 5700 7450 50  0000 C CNN
F 2 "" H 5700 7200 50  0000 C CNN
F 3 "" H 5700 7200 50  0000 C CNN
	1    5700 7200
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR034
U 1 1 5DABFB9E
P 4700 7200
F 0 "#PWR034" H 4700 6950 50  0001 C CNN
F 1 "GNDD" V 4700 7000 50  0000 C CNN
F 2 "" H 4700 7200 50  0000 C CNN
F 3 "" H 4700 7200 50  0000 C CNN
	1    4700 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR035
U 1 1 5DABFBC3
P 5200 7200
F 0 "#PWR035" H 5200 6950 50  0001 C CNN
F 1 "GNDD" V 5200 7000 50  0000 C CNN
F 2 "" H 5200 7200 50  0000 C CNN
F 3 "" H 5200 7200 50  0000 C CNN
	1    5200 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR038
U 1 1 5DABFBE8
P 6600 7350
F 0 "#PWR038" H 6600 7100 50  0001 C CNN
F 1 "GNDD" H 6605 7177 50  0000 C CNN
F 2 "" H 6600 7350 50  0000 C CNN
F 3 "" H 6600 7350 50  0000 C CNN
	1    6600 7350
	1    0    0    -1  
$EndComp
Connection ~ 6600 7350
Wire Wire Line
	5700 6700 5700 7200
$Comp
L Device:R R9
U 1 1 5DAC8302
P 5800 7100
F 0 "R9" H 5870 7146 50  0000 L CNN
F 1 "4k7" H 5870 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5730 7100 50  0001 C CNN
F 3 "" H 5800 7100 50  0000 C CNN
	1    5800 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 7350 6000 7350
Wire Wire Line
	5800 7250 5800 7350
$Comp
L Device:R R10
U 1 1 5DAD91CF
P 6000 7100
F 0 "R10" H 6070 7146 50  0000 L CNN
F 1 "4k7" H 6070 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5930 7100 50  0001 C CNN
F 3 "" H 6000 7100 50  0000 C CNN
	1    6000 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5DAD91EB
P 6200 7100
F 0 "R11" H 6270 7146 50  0000 L CNN
F 1 "4k7" H 6270 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6130 7100 50  0001 C CNN
F 3 "" H 6200 7100 50  0000 C CNN
	1    6200 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5DAD920B
P 6400 7100
F 0 "R12" H 6470 7146 50  0000 L CNN
F 1 "4k7" H 6470 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6330 7100 50  0001 C CNN
F 3 "" H 6400 7100 50  0000 C CNN
	1    6400 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5DAD9229
P 6600 7100
F 0 "R13" H 6670 7146 50  0000 L CNN
F 1 "4k7" H 6670 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 7100 50  0001 C CNN
F 3 "" H 6600 7100 50  0000 C CNN
	1    6600 7100
	1    0    0    -1  
$EndComp
$Comp
L power_special:VCCD18 #PWR024
U 1 1 5DAD9383
P 7400 2200
F 0 "#PWR024" H 7400 2050 50  0001 C CNN
F 1 "VCCD18" V 7417 2328 50  0000 L CNN
F 2 "" H 7400 2200 50  0000 C CNN
F 3 "" H 7400 2200 50  0000 C CNN
	1    7400 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 2200 7200 2200
Connection ~ 7200 2200
$Comp
L power:GNDD #PWR025
U 1 1 5DAE1C9A
P 7400 2400
F 0 "#PWR025" H 7400 2150 50  0001 C CNN
F 1 "GNDD" V 7405 2272 50  0000 R CNN
F 2 "" H 7400 2400 50  0000 C CNN
F 3 "" H 7400 2400 50  0000 C CNN
	1    7400 2400
	0    -1   -1   0   
$EndComp
$Comp
L power_special:VCCD33 #PWR026
U 1 1 5DAE1D01
P 7400 2600
F 0 "#PWR026" H 7400 2450 50  0001 C CNN
F 1 "VCCD33" V 7417 2728 50  0000 L CNN
F 2 "" H 7400 2600 50  0000 C CNN
F 3 "" H 7400 2600 50  0000 C CNN
	1    7400 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 2600 7400 2600
Wire Wire Line
	7400 2400 7250 2400
Connection ~ 7250 2400
Wire Wire Line
	2700 1000 3300 1000
Wire Wire Line
	3800 1000 3800 1100
Wire Wire Line
	3700 1100 3700 1000
Connection ~ 3700 1000
Wire Wire Line
	3700 1000 3800 1000
Wire Wire Line
	3400 1100 3400 1000
Connection ~ 3400 1000
Wire Wire Line
	3400 1000 3700 1000
Wire Wire Line
	3300 1100 3300 1000
Connection ~ 3300 1000
Wire Wire Line
	3300 1000 3400 1000
Wire Wire Line
	2700 900  2900 900 
Wire Wire Line
	3600 900  3600 1100
Wire Wire Line
	3500 1100 3500 900 
Connection ~ 3500 900 
Wire Wire Line
	3500 900  3600 900 
Wire Wire Line
	3200 1100 3200 900 
Connection ~ 3200 900 
Wire Wire Line
	3200 900  3500 900 
Wire Wire Line
	3100 1100 3100 900 
Connection ~ 3100 900 
Wire Wire Line
	3100 900  3200 900 
Wire Wire Line
	2900 1100 2900 900 
Connection ~ 2900 900 
Wire Wire Line
	2900 900  3100 900 
Wire Wire Line
	2700 800  3000 800 
Wire Wire Line
	3000 800  3000 1100
Wire Wire Line
	2700 700  3900 700 
Wire Wire Line
	3900 700  3900 1100
Wire Wire Line
	4100 1100 4100 1000
Wire Wire Line
	4100 1000 4400 1000
Wire Wire Line
	4700 1000 4700 1100
Wire Wire Line
	4600 1100 4600 1000
Connection ~ 4600 1000
Wire Wire Line
	4600 1000 4700 1000
Wire Wire Line
	4500 1100 4500 1000
Connection ~ 4500 1000
Wire Wire Line
	4500 1000 4600 1000
Wire Wire Line
	4400 1100 4400 1000
Connection ~ 4400 1000
Wire Wire Line
	4400 1000 4500 1000
Wire Wire Line
	4200 1100 4200 900 
Wire Wire Line
	4200 900  4300 900 
Wire Wire Line
	4900 900  4900 1100
Wire Wire Line
	4800 1100 4800 900 
Connection ~ 4800 900 
Wire Wire Line
	4800 900  4900 900 
Wire Wire Line
	4300 1100 4300 900 
Connection ~ 4300 900 
Wire Wire Line
	4300 900  4800 900 
Wire Wire Line
	4000 900  4000 1100
Wire Wire Line
	5700 900  5700 1000
Wire Wire Line
	6400 1100 6400 1000
Wire Wire Line
	6400 1000 5900 1000
Wire Wire Line
	5800 1000 5800 1100
Wire Wire Line
	5900 1100 5900 1000
Connection ~ 5900 1000
Wire Wire Line
	5900 1000 5800 1000
Wire Wire Line
	5800 900  5800 1000
Connection ~ 5800 1000
Wire Wire Line
	6300 900  6300 1100
Wire Wire Line
	6000 900  6000 1100
Wire Wire Line
	6100 900  6100 1100
Wire Wire Line
	6200 900  6200 1100
Wire Wire Line
	5200 900  5200 1000
Wire Wire Line
	5400 900  5400 1000
Text HLabel 5200 900  1    50   Output ~ 0
PLLVC
Wire Wire Line
	5100 1100 5100 1000
Wire Wire Line
	5100 1000 5200 1000
Connection ~ 5200 1000
Wire Wire Line
	5200 1000 5200 1100
Wire Wire Line
	5400 1000 5300 1000
Wire Wire Line
	5300 1000 5300 1100
Connection ~ 5400 1000
Wire Wire Line
	5400 1000 5400 1100
Wire Wire Line
	5600 1100 5600 1000
Wire Wire Line
	5600 1000 5700 1000
Connection ~ 5700 1000
Wire Wire Line
	5700 1000 5700 1100
Text HLabel 5400 900  1    50   Output ~ 0
PLLVCTRL
Text HLabel 6000 900  1    50   Output ~ 0
DBG2
Text HLabel 6100 900  1    50   Output ~ 0
DBG1
Text HLabel 6200 900  1    50   Output ~ 0
OR-36
$Comp
L power_special:VCCD33 #PWR021
U 1 1 5DCC61D2
P 6300 900
F 0 "#PWR021" H 6300 750 50  0001 C CNN
F 1 "VCCD33" V 6300 1150 50  0000 C CNN
F 2 "" H 6300 900 50  0000 C CNN
F 3 "" H 6300 900 50  0000 C CNN
	1    6300 900 
	1    0    0    -1  
$EndComp
$Comp
L power_special:VCCD18 #PWR019
U 1 1 5DCC623A
P 5700 900
F 0 "#PWR019" H 5700 750 50  0001 C CNN
F 1 "VCCD18" V 5700 1150 50  0000 C CNN
F 2 "" H 5700 900 50  0000 C CNN
F 3 "" H 5700 900 50  0000 C CNN
	1    5700 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR020
U 1 1 5DCC6275
P 5800 900
F 0 "#PWR020" H 5800 650 50  0001 C CNN
F 1 "GNDD" V 5800 700 50  0000 C CNN
F 2 "" H 5800 900 50  0000 C CNN
F 3 "" H 5800 900 50  0000 C CNN
	1    5800 900 
	-1   0    0    1   
$EndComp
$Comp
L power_special:VCCD33 #PWR016
U 1 1 5DCC632F
P 4000 900
F 0 "#PWR016" H 4000 750 50  0001 C CNN
F 1 "VCCD33" V 4000 1150 50  0000 C CNN
F 2 "" H 4000 900 50  0000 C CNN
F 3 "" H 4000 900 50  0000 C CNN
	1    4000 900 
	1    0    0    -1  
$EndComp
$Comp
L power_special:VCCD18 #PWR018
U 1 1 5DCC6346
P 4200 900
F 0 "#PWR018" H 4200 750 50  0001 C CNN
F 1 "VCCD18" V 4200 1150 50  0000 C CNN
F 2 "" H 4200 900 50  0000 C CNN
F 3 "" H 4200 900 50  0000 C CNN
	1    4200 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR017
U 1 1 5DCC635D
P 4100 900
F 0 "#PWR017" H 4100 650 50  0001 C CNN
F 1 "GNDD" V 4100 700 50  0000 C CNN
F 2 "" H 4100 900 50  0000 C CNN
F 3 "" H 4100 900 50  0000 C CNN
	1    4100 900 
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 1000 4100 900 
Connection ~ 4100 1000
$Comp
L power:GNDA #PWR015
U 1 1 5DCD576E
P 2700 900
F 0 "#PWR015" H 2700 650 50  0001 C CNN
F 1 "GNDA" V 2705 773 50  0000 R CNN
F 2 "" H 2700 900 50  0000 C CNN
F 3 "" H 2700 900 50  0000 C CNN
	1    2700 900 
	0    1    1    0   
$EndComp
$Comp
L power_special:VCCA18 #PWR022
U 1 1 5DCD57AF
P 2700 1000
F 0 "#PWR022" H 2700 850 50  0001 C CNN
F 1 "VCCA18" V 2700 1100 50  0000 L CNN
F 2 "" H 2700 1000 50  0000 C CNN
F 3 "" H 2700 1000 50  0000 C CNN
	1    2700 1000
	0    -1   -1   0   
$EndComp
$Comp
L power_special:VCCA33 #PWR014
U 1 1 5DCD57FC
P 2700 800
F 0 "#PWR014" H 2700 650 50  0001 C CNN
F 1 "VCCA33" V 2700 900 50  0000 L CNN
F 2 "" H 2700 800 50  0000 C CNN
F 3 "" H 2700 800 50  0000 C CNN
	1    2700 800 
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR023
U 1 1 5DCD584F
P 2050 1100
F 0 "#PWR023" H 2050 850 50  0001 C CNN
F 1 "GNDA" V 2050 900 50  0000 C CNN
F 2 "" H 2050 1100 50  0000 C CNN
F 3 "" H 2050 1100 50  0000 C CNN
	1    2050 1100
	-1   0    0    1   
$EndComp
Text HLabel 2700 700  0    50   Input ~ 0
ADCp
$Comp
L Device:R R5
U 1 1 5DCD5B80
P 8650 4850
F 0 "R5" H 8720 4896 50  0000 L CNN
F 1 "100" H 8720 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8580 4850 50  0001 C CNN
F 3 "" H 8650 4850 50  0000 C CNN
	1    8650 4850
	1    0    0    -1  
$EndComp
Connection ~ 8650 4700
Wire Wire Line
	8650 4700 9150 4700
Connection ~ 8650 5000
Wire Wire Line
	8650 5000 9150 5000
$Comp
L Device:R R6
U 1 1 5DA8058E
P 8650 5400
F 0 "R6" H 8720 5446 50  0000 L CNN
F 1 "100" H 8720 5355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8580 5400 50  0001 C CNN
F 3 "" H 8650 5400 50  0000 C CNN
	1    8650 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5200 8650 5200
Wire Wire Line
	8650 5200 8650 5250
Wire Wire Line
	9050 5600 8650 5600
Wire Wire Line
	8650 5600 8650 5550
$Comp
L Device:R R7
U 1 1 5DAAF981
P 8650 6050
F 0 "R7" H 8720 6096 50  0000 L CNN
F 1 "100" H 8720 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8580 6050 50  0001 C CNN
F 3 "" H 8650 6050 50  0000 C CNN
	1    8650 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5850 8650 5850
Wire Wire Line
	8650 5850 8650 5900
Wire Wire Line
	9050 6250 8650 6250
Wire Wire Line
	8650 6250 8650 6200
$Comp
L Device:R R8
U 1 1 5DABFC7F
P 9500 6050
F 0 "R8" H 9570 6096 50  0000 L CNN
F 1 "100" H 9570 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9430 6050 50  0001 C CNN
F 3 "" H 9500 6050 50  0000 C CNN
	1    9500 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 5850 9500 5850
Wire Wire Line
	9500 5850 9500 5900
Wire Wire Line
	9900 6250 9500 6250
Wire Wire Line
	9500 6250 9500 6200
Text Label 9050 5200 2    50   ~ 0
di_FE_T0_etrig_P
Text Label 9050 5600 2    50   ~ 0
di_FE_T0_etrig_N
Text Label 9050 5850 2    50   ~ 0
di_FIFO_etrig_P
Text Label 9050 6250 2    50   ~ 0
di_FIFO_etrig_N
Text Label 9900 5850 2    50   ~ 0
di_FE_etrig_P
Text Label 9900 6250 2    50   ~ 0
di_FE_etrig_N
$Comp
L Device:R R99
U 1 1 5DB5718B
P 10000 4300
F 0 "R99" H 10070 4346 50  0000 L CNN
F 1 "100" H 10070 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9930 4300 50  0001 C CNN
F 3 "" H 10000 4300 50  0000 C CNN
	1    10000 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4050 10000 4050
Wire Wire Line
	10000 4050 10000 4150
Wire Wire Line
	9750 4550 10000 4550
Wire Wire Line
	10000 4550 10000 4450
Text Label 9750 4050 0    50   ~ 0
do_Txd_P
Text Label 9750 4550 0    50   ~ 0
do_TxD_N
$EndSCHEMATC
