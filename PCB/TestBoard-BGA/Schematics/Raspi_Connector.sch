EESchema Schematic File Version 4
LIBS:TestBoard-COB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 11
Title "DAQ connection for Raspberrypi and FPGA"
Date "2019-10-16"
Rev ""
Comp "KIP, Uni-Heidelberg"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5650 3650 0    60   Output ~ 0
o_SPI_sclk
Text HLabel 6850 3650 2    60   Output ~ 0
o_SPI_cs
Text HLabel 5650 3450 0    60   Output ~ 0
o_SPI_sdo
Text HLabel 5650 3550 0    60   Input ~ 0
i_SPI_sdi
Text HLabel 3350 2300 0    60   BiDi ~ 0
OSC_sda_nc
Text HLabel 5650 3250 0    60   Output ~ 0
OSC_scl_oe
Text HLabel 6850 2950 2    60   Output ~ 0
pregena_VCCA33
Text HLabel 6850 3350 2    60   Output ~ 0
pregena_VCCD33
Text HLabel 6850 2850 2    60   Output ~ 0
pregena_VCCA18
Text HLabel 6850 3250 2    60   Output ~ 0
pregena_VCCD18
Text HLabel 5650 3050 0    60   Output ~ 0
o_rst
Text HLabel 6850 3050 2    60   Output ~ 0
o_srst
Text HLabel 2350 3250 0    60   Input ~ 0
i_TB_TxD_N
Text HLabel 2350 3150 0    60   Input ~ 0
i_TB_TxD_P
Text HLabel 5650 2750 0    60   Output ~ 0
o_I2CA_scl
Text HLabel 6850 3850 2    60   Output ~ 0
o_I2CB_scl
Text HLabel 5650 2650 0    60   BiDi ~ 0
io_I2CA_sda
Text HLabel 5650 3850 0    60   BiDi ~ 0
io_I2CB_sda
Text HLabel 5650 2850 0    60   Input ~ 0
i_fifo_full
Text HLabel 5650 3150 0    60   Output ~ 0
o_aqu_ena
$Comp
L Connector_Generic:Conn_02x20_Odd_Even P36
U 1 1 57FEC7D9
P 6150 3450
F 0 "P36" H 6150 4500 50  0000 C CNN
F 1 "CONN_02X20" V 6150 3450 50  0000 C CNN
F 2 "Connector-Custom:CONN_02X20" H 6150 2400 50  0000 C CNN
F 3 "" H 6150 2500 50  0000 C CNN
	1    6150 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R98
U 1 1 58020735
P 6850 2550
F 0 "R98" V 6930 2550 50  0000 C CNN
F 1 "DNP" V 6850 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6780 2550 50  0001 C CNN
F 3 "" H 6850 2550 50  0000 C CNN
	1    6850 2550
	0    1    1    0   
$EndComp
Text Notes 6800 2300 0    60   ~ 0
Place 0Ohm to power from bpi
NoConn ~ 5950 2550
$Comp
L power:GNDD #PWR0219
U 1 1 58020CB3
P 5850 4600
F 0 "#PWR0219" H 5850 4350 50  0001 C CNN
F 1 "GNDD" H 5850 4450 50  0000 C CNN
F 2 "" H 5850 4600 50  0000 C CNN
F 3 "" H 5850 4600 50  0000 C CNN
	1    5850 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0220
U 1 1 58020D4A
P 6550 4600
F 0 "#PWR0220" H 6550 4350 50  0001 C CNN
F 1 "GNDD" H 6550 4450 50  0000 C CNN
F 2 "" H 6550 4600 50  0000 C CNN
F 3 "" H 6550 4600 50  0000 C CNN
	1    6550 4600
	1    0    0    -1  
$EndComp
NoConn ~ 5950 3350
$Comp
L power:GNDD #PWR0218
U 1 1 58021493
P 2300 3850
F 0 "#PWR0218" H 2300 3600 50  0001 C CNN
F 1 "GNDD" H 2300 3700 50  0000 C CNN
F 2 "" H 2300 3850 50  0000 C CNN
F 3 "" H 2300 3850 50  0000 C CNN
	1    2300 3850
	1    0    0    -1  
$EndComp
Text HLabel 3350 2500 0    60   Output ~ 0
OSC_scl_oe
Text HLabel 2850 3250 2    60   Input ~ 0
FPGA_clk_N
Text HLabel 2850 3150 2    60   Input ~ 0
FPGA_clk_P
$Comp
L Connector_Generic:Conn_02x06_Odd_Even P35
U 1 1 5805BC21
P 2550 3350
F 0 "P35" H 2550 3905 50  0000 C CNN
F 1 "CONN_02X06" H 2550 3814 50  0000 C CNN
F 2 "Connector-Custom:CONN_02X06" H 2550 3723 50  0000 C CNN
F 3 "" H 2550 2150 50  0000 C CNN
	1    2550 3350
	1    0    0    -1  
$EndComp
NoConn ~ 2350 3650
NoConn ~ 2850 3650
Text Notes 1650 3150 2    60   ~ 0
FPGA_PMOD1_P1
Text Notes 1650 3250 2    60   ~ 0
FPGA_PMOD1_P2
Text Notes 3500 3250 0    60   ~ 0
FPGA_PMOD1_P8\n
Text Label 2000 3450 2    60   ~ 0
o_SPI_sclk
Text Label 3200 3450 0    60   ~ 0
o_SPI_cs
Text Label 3200 3350 0    60   ~ 0
o_SPI_sdo
Text Label 2000 3350 2    60   ~ 0
i_SPI_sdi
$Comp
L Connector_Generic:Conn_02x06_Odd_Even P38
U 1 1 5807E8DA
P 2550 4750
F 0 "P38" H 2550 5305 50  0000 C CNN
F 1 "CONN_02X06" H 2550 5214 50  0000 C CNN
F 2 "Connector-Custom:CONN_02X06" H 2550 5123 50  0000 C CNN
F 3 "" H 2550 3550 50  0000 C CNN
	1    2550 4750
	1    0    0    -1  
$EndComp
NoConn ~ 2350 5050
NoConn ~ 2850 5050
Text Label 3100 4550 0    60   ~ 0
o_rst
Text Label 2100 4550 2    60   ~ 0
io_I2CB_sda
Text Label 2100 4650 2    60   ~ 0
o_I2CB_scl
Text Label 3100 4650 0    60   ~ 0
o_aqu_ena
$Comp
L power:GNDD #PWR0222
U 1 1 58082BBF
P 2250 5200
F 0 "#PWR0222" H 2250 4950 50  0001 C CNN
F 1 "GNDD" H 2250 5050 50  0000 C CNN
F 2 "" H 2250 5200 50  0000 C CNN
F 3 "" H 2250 5200 50  0000 C CNN
	1    2250 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2550 6600 2550
Wire Wire Line
	6600 2550 6600 2650
Wire Wire Line
	6600 2650 6450 2650
Connection ~ 6600 2550
Wire Wire Line
	7000 2550 7050 2550
Wire Wire Line
	7050 2550 7050 2500
Wire Wire Line
	5950 2950 5850 2950
Wire Wire Line
	5850 2950 5850 3750
Wire Wire Line
	5950 3750 5850 3750
Connection ~ 5850 3750
Wire Wire Line
	5950 4450 5850 4450
Connection ~ 5850 4450
Wire Wire Line
	6550 2750 6550 3150
Wire Wire Line
	6550 2750 6450 2750
Wire Wire Line
	6450 3150 6550 3150
Connection ~ 6550 3150
Wire Wire Line
	6450 3450 6550 3450
Connection ~ 6550 3450
Wire Wire Line
	6450 3950 6550 3950
Connection ~ 6550 3950
Wire Wire Line
	6450 4150 6550 4150
Connection ~ 6550 4150
Wire Wire Line
	5950 3850 5650 3850
Wire Wire Line
	6450 3850 6850 3850
Wire Wire Line
	6450 3650 6850 3650
Wire Wire Line
	5950 3650 5650 3650
Wire Wire Line
	5650 3550 5950 3550
Wire Wire Line
	5950 3450 5650 3450
Wire Wire Line
	5950 3050 5650 3050
Wire Wire Line
	5950 2850 5650 2850
Wire Wire Line
	5950 3150 5650 3150
Wire Wire Line
	5950 3250 5650 3250
Wire Wire Line
	6850 3050 6450 3050
Wire Wire Line
	6850 3250 6450 3250
Wire Wire Line
	6850 3350 6450 3350
Wire Wire Line
	2300 3550 2350 3550
Wire Wire Line
	2850 3550 2950 3550
Wire Wire Line
	2950 3550 2950 3750
Wire Wire Line
	2950 3750 2300 3750
Wire Wire Line
	2300 3550 2300 3750
Connection ~ 2300 3750
Wire Wire Line
	2850 3450 3200 3450
Wire Wire Line
	2850 3350 3200 3350
Wire Wire Line
	2350 3350 2000 3350
Wire Wire Line
	2350 3450 2000 3450
Wire Wire Line
	6450 2850 6850 2850
Wire Wire Line
	6850 2950 6450 2950
Wire Wire Line
	2850 4550 3100 4550
Wire Wire Line
	2850 4650 3100 4650
Wire Wire Line
	2850 4750 3100 4750
Wire Wire Line
	2350 4550 2100 4550
Wire Wire Line
	2350 4650 2100 4650
Wire Wire Line
	2250 5200 2250 5150
Wire Wire Line
	2250 4950 2350 4950
Wire Wire Line
	2250 5150 2950 5150
Wire Wire Line
	2950 5150 2950 4950
Wire Wire Line
	2950 4950 2850 4950
Connection ~ 2250 5150
Wire Wire Line
	2850 4850 3100 4850
Wire Wire Line
	2350 4850 2100 4850
Wire Wire Line
	2350 4750 2100 4750
NoConn ~ 3100 4750
NoConn ~ 3100 4850
NoConn ~ 2100 4750
NoConn ~ 2100 4850
Wire Wire Line
	6600 2550 6700 2550
Wire Wire Line
	5850 3750 5850 4450
Wire Wire Line
	5850 4450 5850 4600
Wire Wire Line
	6550 3150 6550 3450
Wire Wire Line
	6550 3450 6550 3950
Wire Wire Line
	6550 3950 6550 4150
Wire Wire Line
	6550 4150 6550 4600
Wire Wire Line
	2300 3750 2300 3850
Wire Wire Line
	2250 5150 2250 4950
Wire Wire Line
	5650 2650 5950 2650
Wire Wire Line
	5950 2750 5650 2750
$Comp
L KLauS-ASIC:TP1 P33
U 1 1 5DA8FF6C
P 3550 2200
F 0 "P33" H 3677 2151 50  0000 L CNN
F 1 "TP1" H 3677 2060 50  0000 L CNN
F 2 "MountingHoles-Custom:TP1" H 3550 2200 50  0001 C CNN
F 3 "" H 3550 2200 50  0001 C CNN
	1    3550 2200
	1    0    0    -1  
$EndComp
$Comp
L KLauS-ASIC:TP1 P34
U 1 1 5DA8FFE2
P 3550 2400
F 0 "P34" H 3677 2351 50  0000 L CNN
F 1 "TP1" H 3677 2260 50  0000 L CNN
F 2 "MountingHoles-Custom:TP1" H 3550 2400 50  0001 C CNN
F 3 "" H 3550 2400 50  0001 C CNN
	1    3550 2400
	1    0    0    -1  
$EndComp
$Comp
L KLauS-ASIC:MountingHole H7
U 1 1 5DB44016
P 2450 6650
F 0 "H7" H 2549 6703 60  0000 L CNN
F 1 "MountingHole" H 2549 6597 60  0000 L CNN
F 2 "MountingHoles-Custom:Mountinghole_3mm_NP" H 2450 6650 60  0001 C CNN
F 3 "" H 2450 6650 60  0001 C CNN
	1    2450 6650
	1    0    0    -1  
$EndComp
$Comp
L KLauS-ASIC:MountingHole H8
U 1 1 5DB44040
P 2450 6850
F 0 "H8" H 2549 6903 60  0000 L CNN
F 1 "MountingHole" H 2549 6797 60  0000 L CNN
F 2 "MountingHoles-Custom:Mountinghole_3mm_NP" H 2450 6850 60  0001 C CNN
F 3 "" H 2450 6850 60  0001 C CNN
	1    2450 6850
	1    0    0    -1  
$EndComp
Text Label 5650 4150 0    50   ~ 0
TCALIB
Wire Wire Line
	5500 4150 5950 4150
Text HLabel 5500 4150 0    50   Output ~ 0
TCALIB
Text Notes 3500 3150 0    60   ~ 0
FPGA_PMOD1_P7\n
NoConn ~ 6450 3550
NoConn ~ 6450 3750
NoConn ~ 6450 4050
NoConn ~ 5950 3950
NoConn ~ 5950 4050
NoConn ~ 5950 4250
NoConn ~ 5950 4350
NoConn ~ 6450 4250
NoConn ~ 6450 4350
NoConn ~ 6450 4450
$Comp
L power:+5V #PWR0217
U 1 1 60BA0165
P 7050 2500
F 0 "#PWR0217" H 7050 2350 50  0001 C CNN
F 1 "+5V" H 7065 2673 50  0000 C CNN
F 2 "" H 7050 2500 50  0001 C CNN
F 3 "" H 7050 2500 50  0001 C CNN
	1    7050 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
