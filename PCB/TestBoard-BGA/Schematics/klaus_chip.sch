EESchema Schematic File Version 4
LIBS:TestBoard-COB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title "KLauS-6 Chip On Board"
Date "2019-10-15"
Rev ""
Comp "KIP, Uni-Heidelberg"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6900 3200 0    60   Input ~ 0
SiPM_in[0]
Text HLabel 6900 3300 0    60   Input ~ 0
SiPM_in[1]
Text HLabel 6900 3400 0    60   Input ~ 0
SiPM_in[2]
Text HLabel 6900 3500 0    60   Input ~ 0
SiPM_in[3]
Text HLabel 6900 3600 0    60   Input ~ 0
SiPM_in[4]
Text HLabel 6900 3700 0    60   Input ~ 0
SiPM_in[5]
Text HLabel 6900 3800 0    60   Input ~ 0
SiPM_in[6]
Text HLabel 6900 3900 0    60   Input ~ 0
SiPM_in[7]
Text HLabel 6900 4000 0    60   Input ~ 0
SiPM_in[8]
Text HLabel 6900 4100 0    60   Input ~ 0
SiPM_in[9]
Text HLabel 6900 4200 0    60   Input ~ 0
SiPM_in[10]
Text HLabel 6900 4300 0    60   Input ~ 0
SiPM_in[11]
Text HLabel 6900 4400 0    60   Input ~ 0
SiPM_in[12]
Text HLabel 6900 4500 0    60   Input ~ 0
SiPM_in[13]
Text HLabel 6900 4600 0    60   Input ~ 0
SiPM_in[14]
Text HLabel 6900 4700 0    60   Input ~ 0
SiPM_in[15]
Text HLabel 6900 4800 0    60   Input ~ 0
SiPM_in[16]
Text HLabel 6900 4900 0    60   Input ~ 0
SiPM_in[17]
Text HLabel 8550 3200 2    60   Input ~ 0
SiPM_in[18]
Text HLabel 8550 3300 2    60   Input ~ 0
SiPM_in[19]
Text HLabel 8550 3400 2    60   Input ~ 0
SiPM_in[20]
Text HLabel 8550 3500 2    60   Input ~ 0
SiPM_in[21]
Text HLabel 8550 3600 2    60   Input ~ 0
SiPM_in[22]
Text HLabel 8550 3700 2    60   Input ~ 0
SiPM_in[23]
Text HLabel 8550 3800 2    60   Input ~ 0
SiPM_in[24]
Text HLabel 8550 3900 2    60   Input ~ 0
SiPM_in[25]
Text HLabel 8550 4000 2    60   Input ~ 0
SiPM_in[26]
Text HLabel 8550 4100 2    60   Input ~ 0
SiPM_in[27]
Text HLabel 8550 4200 2    60   Input ~ 0
SiPM_in[28]
Text HLabel 8550 4300 2    60   Input ~ 0
SiPM_in[29]
Text HLabel 8550 4400 2    60   Input ~ 0
SiPM_in[30]
Text HLabel 8550 4500 2    60   Input ~ 0
SiPM_in[31]
Text HLabel 8550 4600 2    60   Input ~ 0
SiPM_in[32]
Text HLabel 8550 4700 2    60   Input ~ 0
SiPM_in[33]
Text HLabel 8550 4800 2    60   Input ~ 0
SiPM_in[34]
Text HLabel 8550 4900 2    60   Input ~ 0
SiPM_in[35]
Text HLabel 2700 1950 0    60   Input ~ 0
REF_1p2
Wire Wire Line
	6900 3200 7050 3200
Wire Wire Line
	6900 3300 7050 3300
Wire Wire Line
	6900 3400 7050 3400
Wire Wire Line
	6900 3500 7050 3500
Wire Wire Line
	6900 3600 7050 3600
Wire Wire Line
	6900 3700 7050 3700
Wire Wire Line
	6900 3800 7050 3800
Wire Wire Line
	6900 3900 7050 3900
Wire Wire Line
	6900 4000 7050 4000
Wire Wire Line
	6900 4100 7050 4100
Wire Wire Line
	6900 4200 7050 4200
Wire Wire Line
	6900 4300 7050 4300
Wire Wire Line
	6900 4400 7050 4400
Wire Wire Line
	6900 4500 7050 4500
Wire Wire Line
	6900 4600 7050 4600
Wire Wire Line
	6900 4700 7050 4700
Wire Wire Line
	6900 4800 7050 4800
Wire Wire Line
	6900 4900 7050 4900
Wire Wire Line
	8550 4900 8450 4900
Wire Wire Line
	8550 4800 8450 4800
Wire Wire Line
	8550 4700 8450 4700
Wire Wire Line
	8550 4600 8450 4600
Wire Wire Line
	8550 4500 8450 4500
Wire Wire Line
	8550 4400 8450 4400
Wire Wire Line
	8550 4300 8450 4300
Wire Wire Line
	8550 4200 8450 4200
Wire Wire Line
	8550 4100 8450 4100
Wire Wire Line
	8550 4000 8450 4000
Wire Wire Line
	8550 3900 8450 3900
Wire Wire Line
	8550 3800 8450 3800
Wire Wire Line
	8550 3700 8450 3700
Wire Wire Line
	8550 3600 8450 3600
Wire Wire Line
	8550 3500 8450 3500
Wire Wire Line
	8550 3400 8450 3400
Wire Wire Line
	8550 3300 8450 3300
Wire Wire Line
	8550 3200 8450 3200
Text HLabel 2700 2550 0    60   Input ~ 0
ADCn
Text HLabel 4400 1950 2    60   Output ~ 0
amon_1
Text HLabel 4400 2050 2    60   Output ~ 0
amon_2
Wire Wire Line
	4350 2050 4400 2050
Wire Wire Line
	4350 1950 4400 1950
Text HLabel 2700 2250 0    60   BiDi ~ 0
ref_cm
Text HLabel 2700 2350 0    60   BiDi ~ 0
reg_g
Text HLabel 2700 2150 0    60   BiDi ~ 0
ref_v
Text HLabel 2700 2050 0    60   Input ~ 0
REF_1p8
Wire Wire Line
	2700 2050 2900 2050
Wire Wire Line
	2700 2150 2900 2150
Wire Wire Line
	2700 2250 2900 2250
Wire Wire Line
	2700 2350 2900 2350
Text HLabel 4350 3450 2    60   Output ~ 0
do_TxD_N
Text HLabel 4350 3350 2    60   Output ~ 0
do_Txd_P
Text HLabel 4350 3850 2    60   Input ~ 0
di_FE_T0_etrig_N
Text HLabel 4350 3750 2    60   Input ~ 0
di_FE_T0_etrig_P
Text HLabel 4350 4050 2    60   Input ~ 0
di_FIFO_etrig_N
Text HLabel 4350 3950 2    60   Input ~ 0
di_FIFO_etrig_P
Text HLabel 4350 4250 2    60   Input ~ 0
di_FE_etrig_N
Text HLabel 4350 4150 2    60   Input ~ 0
di_FE_etrig_P
Wire Wire Line
	5200 3550 5200 3450
Wire Wire Line
	5200 3650 5200 3750
Text HLabel 5650 3750 2    60   Input ~ 0
di_ser_clk_N
Text HLabel 5650 3450 2    60   Input ~ 0
di_ser_clk_P
Text HLabel 2900 3850 0    60   Input ~ 0
di_rst
Text HLabel 2900 3550 0    60   Input ~ 0
di_i2cB_scl
Text HLabel 2900 3650 0    60   BiDi ~ 0
dio_i2cB_sda
Text HLabel 2900 3450 0    60   BiDi ~ 0
dio_i2cA_sda
Text HLabel 2900 3350 0    60   Input ~ 0
di_i2cA_scl
Text HLabel 2900 3950 0    60   Output ~ 0
do_fifo_full
Text HLabel 2900 4050 0    60   Input ~ 0
di_acq_en
Text HLabel 2900 4250 0    60   Input ~ 0
di_srst
Text HLabel 2900 4150 0    60   Output ~ 0
do_sdo
Text HLabel 2900 4350 0    60   Input ~ 0
di_sclk
Text HLabel 2900 4450 0    60   Input ~ 0
di_sdi
Wire Wire Line
	2900 2850 1600 2850
Wire Wire Line
	2900 2950 1650 2950
Wire Wire Line
	1650 2950 1650 3050
Wire Wire Line
	1650 3050 1600 3050
Wire Wire Line
	2900 3050 1700 3050
Wire Wire Line
	1700 3050 1700 3250
Wire Wire Line
	1700 3250 1600 3250
Wire Wire Line
	2900 3150 1750 3150
Wire Wire Line
	1750 3150 1750 3450
Wire Wire Line
	1750 3450 1600 3450
Wire Wire Line
	2900 3250 1800 3250
Wire Wire Line
	1800 3250 1800 3650
Wire Wire Line
	1800 3650 1600 3650
Wire Wire Line
	1200 3650 1300 3650
Wire Wire Line
	1300 3450 1200 3450
Connection ~ 1200 3450
Wire Wire Line
	1300 3250 1200 3250
Connection ~ 1200 3250
Wire Wire Line
	1300 3050 1200 3050
Connection ~ 1200 3050
Wire Wire Line
	1200 3450 1200 3650
Wire Wire Line
	1200 3250 1200 3450
Wire Wire Line
	1200 3050 1200 3250
Wire Wire Line
	5200 3750 5400 3750
Wire Wire Line
	5200 3450 5400 3450
$Comp
L power:GNDD #PWR038
U 1 1 5DABFBE8
P 1200 3650
F 0 "#PWR038" H 1200 3400 50  0001 C CNN
F 1 "GNDD" H 1205 3477 50  0000 C CNN
F 2 "" H 1200 3650 50  0000 C CNN
F 3 "" H 1200 3650 50  0000 C CNN
	1    1200 3650
	0    1    1    0   
$EndComp
Connection ~ 1200 3650
$Comp
L Device:R R9
U 1 1 5DAC8302
P 1450 2850
F 0 "R9" H 1520 2896 50  0000 L CNN
F 1 "4.7K" H 1520 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 2850 50  0001 C CNN
F 3 "" H 1450 2850 50  0000 C CNN
	1    1450 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 2850 1200 3050
Wire Wire Line
	1300 2850 1200 2850
$Comp
L Device:R R10
U 1 1 5DAD91CF
P 1450 3050
F 0 "R10" H 1520 3096 50  0000 L CNN
F 1 "4.7K" H 1520 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 3050 50  0001 C CNN
F 3 "" H 1450 3050 50  0000 C CNN
	1    1450 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5DAD91EB
P 1450 3250
F 0 "R11" H 1520 3296 50  0000 L CNN
F 1 "4.7K" H 1520 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 3250 50  0001 C CNN
F 3 "" H 1450 3250 50  0000 C CNN
	1    1450 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DAD920B
P 1450 3450
F 0 "R12" H 1520 3496 50  0000 L CNN
F 1 "4.7K" H 1520 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 3450 50  0001 C CNN
F 3 "" H 1450 3450 50  0000 C CNN
	1    1450 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5DAD9229
P 1450 3650
F 0 "R13" H 1520 3696 50  0000 L CNN
F 1 "4.7K" H 1520 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 3650 50  0001 C CNN
F 3 "" H 1450 3650 50  0000 C CNN
	1    1450 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 2950 4450 2950
Wire Wire Line
	4550 3150 4450 3150
Text HLabel 4550 2950 2    50   Output ~ 0
PLLVC
Wire Wire Line
	4350 2850 4450 2850
Wire Wire Line
	4450 2850 4450 2950
Connection ~ 4450 2950
Wire Wire Line
	4450 2950 4350 2950
Wire Wire Line
	4450 3150 4450 3050
Wire Wire Line
	4450 3050 4350 3050
Connection ~ 4450 3150
Wire Wire Line
	4450 3150 4350 3150
Text HLabel 4550 3150 2    50   Output ~ 0
PLLVCTRL
Text HLabel 4350 4650 2    50   Output ~ 0
DBG2
Text HLabel 4350 4550 2    50   Output ~ 0
DBG1
Text HLabel 4350 4450 2    50   Output ~ 0
OR-36
Text HLabel 2700 2450 0    50   Input ~ 0
ADCp
$Comp
L Device:R R5
U 1 1 5DCD5B80
P 5400 3600
F 0 "R5" H 5470 3646 50  0000 L CNN
F 1 "100" H 5470 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5330 3600 50  0001 C CNN
F 3 "" H 5400 3600 50  0000 C CNN
	1    5400 3600
	1    0    0    1   
$EndComp
Connection ~ 5400 3750
Wire Wire Line
	5400 3750 5650 3750
Connection ~ 5400 3450
Wire Wire Line
	5400 3450 5650 3450
$Comp
L Device:R R6
U 1 1 5DA8058E
P 1100 6350
F 0 "R6" H 1170 6396 50  0000 L CNN
F 1 "100" H 1170 6305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1030 6350 50  0001 C CNN
F 3 "" H 1100 6350 50  0000 C CNN
	1    1100 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6150 1100 6150
Wire Wire Line
	1100 6150 1100 6200
Wire Wire Line
	1500 6550 1100 6550
Wire Wire Line
	1100 6550 1100 6500
$Comp
L Device:R R7
U 1 1 5DAAF981
P 1100 7000
F 0 "R7" H 1170 7046 50  0000 L CNN
F 1 "100" H 1170 6955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1030 7000 50  0001 C CNN
F 3 "" H 1100 7000 50  0000 C CNN
	1    1100 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6800 1100 6800
Wire Wire Line
	1100 6800 1100 6850
Wire Wire Line
	1500 7200 1100 7200
Wire Wire Line
	1100 7200 1100 7150
$Comp
L Device:R R8
U 1 1 5DABFC7F
P 1950 7000
F 0 "R8" H 2020 7046 50  0000 L CNN
F 1 "100" H 2020 6955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 7000 50  0001 C CNN
F 3 "" H 1950 7000 50  0000 C CNN
	1    1950 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 6800 1950 6800
Wire Wire Line
	1950 6800 1950 6850
Wire Wire Line
	2350 7200 1950 7200
Wire Wire Line
	1950 7200 1950 7150
Text Label 1500 6150 2    50   ~ 0
di_FE_T0_etrig_P
Text Label 1500 6550 2    50   ~ 0
di_FE_T0_etrig_N
Text Label 1500 6800 2    50   ~ 0
di_FIFO_etrig_P
Text Label 1500 7200 2    50   ~ 0
di_FIFO_etrig_N
Text Label 2350 6800 2    50   ~ 0
di_FE_etrig_P
Text Label 2350 7200 2    50   ~ 0
di_FE_etrig_N
$Comp
L KLauS-ASIC-BGA:KLauS-ASIC U1
U 1 1 6094A52E
P 7750 3050
F 0 "U1" H 7750 3165 50  0000 C CNN
F 1 "KLauS-ASIC" H 7750 3074 50  0000 C CNN
F 2 "KLauS-BGA:KLauS-BGA" H 7750 3050 50  0001 C CNN
F 3 "" H 7750 3050 50  0001 C CNN
	1    7750 3050
	1    0    0    -1  
$EndComp
$Comp
L KLauS-ASIC-BGA:KLauS-ASIC U1
U 2 1 6094A5CB
P 3600 1800
F 0 "U1" H 3625 1915 50  0000 C CNN
F 1 "KLauS-ASIC" H 3625 1824 50  0000 C CNN
F 2 "KLauS-BGA:KLauS-BGA" H 3600 1800 50  0001 C CNN
F 3 "" H 3600 1800 50  0001 C CNN
	2    3600 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1950 2900 1950
Wire Wire Line
	2700 2550 2900 2550
Wire Wire Line
	2900 2450 2700 2450
Text Notes 4900 2800 0    50   ~ 0
TODO: Check renaming of PLL pins
Text HLabel 2900 4550 0    60   Input ~ 0
di_cs
Wire Wire Line
	4350 3650 5200 3650
Wire Wire Line
	4350 3550 5200 3550
NoConn ~ 2900 2650
$EndSCHEMATC
