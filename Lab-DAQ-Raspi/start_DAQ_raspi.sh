#!/bin/bash
sudo ./daq-i2c/disable_rpi_led.sh
sudo ./daq-i2c/change_i2c_speed.sh

tmux new-session -s "DAQ" -d 'top; bash -i'
tmux split-window -v  'echo ---- CONFIGURATION SERVER ----  ; cd config-common/ ; ./config_server 1 ; bash -i'
sleep 1
tmux split-window -v  'echo ---- DAQ SERVER ---- ; cd daq-i2c/ ; ./klaus_i2c_daqserver /dev/i2c-1 ; bash -i'
tmux select-layout tiled
tmux a
