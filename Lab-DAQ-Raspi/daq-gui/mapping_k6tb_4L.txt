;inFile >> handleID >> channel >> point.x >> point.y
; as seen from led system side, opposide of tiles:
;2 5 8
;1 4 7
;0 3 6
; connection to test board (back to front layer)
; 8 -> 27, 18, 17, 8
; 7 -> 28, 19, 16, 7
; 6 -> 29, 20, 15, 6
; 5 -> 30, 21, 14, 5
; 4 -> 31, 22, 13, 4
; 3 -> 32, 23, 12, 3
; 2 -> 33, 24, 11, 2
; 1 -> 34, 25, 10, 1
; 0 -> 35, 26,  9, 0

; Mapping from tile side, first layer leftmost, spacing between layers dx=1
0 0   2 2
0 1   2 1
0 2   2 0
0 3   1 2
0 4   1 1
0 5   1 0
0 6   0 2
0 7   0 1
0 8   0 0

0 9   6 2
0 10  6 1
0 11  6 0
0 12  5 2
0 13  5 1
0 14  5 0
0 15  4 2
0 16  4 1
0 17  4 0

0 26 10 2
0 25 10 1
0 24 10 0
0 23  9 2
0 22  9 1
0 21  9 0
0 20  8 2
0 19  8 1
0 18  8 0

0 35 14 2
0 34 14 1
0 33 14 0
0 32 13 2
0 31 13 1
0 30 13 0
0 29 12 2
0 28 12 1
0 27 12 0

