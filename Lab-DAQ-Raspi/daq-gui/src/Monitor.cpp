/*
	Monitor objects for the different ASIC DAQ environments
	Author: kbriggl, based on code of the whole group (in stic_dump.cpp)
	Adapted for KLauS ASIC after redevelopment in V2 DAQ framework
*/
#include "Monitor.h"
#include <fstream>
#include "TThread.h"
#include "TF1.h"
#include "TH2.h"
#include "TSpectrum.h"
#include "TMath.h"
#include "TStyle.h"
#include "stdio.h"
#include "iostream"
#include "sstream"
//#include "tdc_ch_values.h" TODO: DNL calibration
//#define DEBUG_MAPPING
//#include "stdlib.h"
//Monitor class: Single monitor
using namespace std;
const float binSize = 1.0e12*1./(106e6*32.0);//1.0e12*tdc_ch_values::PLL_REFCLK_FREQ/32.0; // in ps  

Monitors::Monitor::Monitor(){
	//printf("Monitors::Monitor::Monitor(), this=%p\n",this);
	hist=NULL;
	cut_method=NULL;
	fMode=MODE_NONE;
	previous_events=new klaus_event[2];
	needsUpdate=true;
	needsFit=false;
//	mult_count = 0;
//	mult_count_tower[4] = {0};
}

Monitors::Monitor::Monitor(mode m, int channel1, int channel2){
	//printf("Monitors::Monitor::Monitor(), this=%p, mode=%d, ch1=%d\n",this,m,channel1);
	hist=NULL;
	cut_method=NULL;
	previous_events=new klaus_event[2];
	SetMode(m,channel1,channel2);
}

Monitors::Monitor::~Monitor(){
	//printf("Monitors::Monitor::~Monitor(), this=%p\n",this);
	if(!previous_events)
		delete[] previous_events;
}
void Monitors::Monitor::SetMode(mode m, int channel1, int channel2){//four Modes not set: MODE_E_MEAN/MODE_E_RMS/MODE_F_MEAN/MODE_F_RMS
	fMode=m;
	fChannels[0]=channel1;
	fChannels[1]=channel2;
	needsUpdate=true;
};

void Monitors::reset_DNLmap() //TODO
{
//	map_dnl_correct.clear(); TODO
	n_event_dnl=0;
	this->DNL_reset = false;
	cout<<"DNL reseted !!"<<endl;
};

void Monitors::build_DNLhist(klaus_event* event) //TODO
{
/*
	ostringstream hist_number; 
	hist_number.clear();
	hist_number.str("");
	hist_number << event->handleID*64+event->channel;
	map_dnl_correct[event->channel+64*event->handleID]=new tdc_ch_values(0,31,hist_number.str().c_str());
	cout<<"building a new tdc_ch_values "<<map_dnl_correct.count(event->handleID*64+event->channel)  <<endl;
*/
};

void Monitors::update_DNLmap()
{
/*
	map<unsigned short, tdc_ch_values*>::iterator it_dnl;
	for(it_dnl = map_dnl_correct.begin(); it_dnl!=map_dnl_correct.end(); it_dnl++)
	{
		it_dnl->second->update_histos();
		cout<<"tdc histogram for ch"<<it_dnl->first<<"'s Entries: "<<it_dnl->second->get_cdt()->GetEntries()<<endl;
	}
	cout<< n_event_dnl << " events filled to dnl_hist!"<< endl;
*/
};

void Monitors::do_DNLcorr(klaus_event* event)
{
/*
	event->T_fine = (int)map_dnl_correct[event->handleID*64+event->channel]->get_dith_value(event->T_fine);
	event->time = (int)map_dnl_correct[event->handleID*64+event->channel]->get_dith_value(event->time);
	//DNLcorr_did = true;
*/
};
void Monitors::Monitor::UpdateMode(){
	//printf("Monitors::Monitor::UpdateMode(), this=%p, hist=%p, pad=%p\n",this,this->GetHistogram(),this->GetPad());
	if(hist) delete hist;
	int nBitsDNL=0;
	if(fMode==MODE_BEAM_PROFILE)
	{
		hist= new TH2F("h2D","h2D",15,0,15,3,0,3);
		hist->SetNameTitle("mapping","Hit map (L0-left)");
		hist->SetObjectStat(0);
		hist->SetStats(0);
/*
	}
	else if(fMode==MODE_MULT_TOWER)
	{
		hist= new TH2F("h2D","h2D",6,0,6,20,0,20);
		hist->SetNameTitle("mult_tower","multiplicity per frame per tower");
		hist->SetObjectStat(0);
		hist->SetStats(0);
	}
	else if(fMode==MODE_CLUSTERING_CH_MULT_CORRELATION){
		hist= new TH2F("h2D","h2D",25,0,25,192,0,192);
		hist->SetNameTitle("cluster_mult_correl","Channel per Clustersize multiplicity");
		hist->SetObjectStat(0);
		hist->SetStats(0);
*/
	}else
	{
		hist= new TH1F("h1D","h1D",10,0,10);
		switch(fMode)
		{
			case MODE_CHANNEL:
				hist->SetBins(37,0,37);
				hist->SetNameTitle("all_channels","all_channels");
				hist->GetXaxis()->SetRangeUser(0,37);
				break;
			case MODE_ADC10:
				hist->SetBins(512,512,1024);
				hist->SetNameTitle(TString::Format("ADC_10b_ch%d",fChannels[0]),TString::Format("Charge (10b ADC), Channel %d",fChannels[0]));
				hist->GetXaxis()->SetRangeUser(650,1024);
				break;
			case MODE_ADC10_HGs:
				hist->SetBins(512,512,1024);
				hist->SetNameTitle(TString::Format("ADC_10b_HG_ch%d",fChannels[0]),TString::Format("Charge (10b ADC), High gain, Channel %d",fChannels[0]));
				hist->GetXaxis()->SetRangeUser(650,1024);
				break;
			case MODE_ADC10_LGs:
				hist->SetBins(512,512,1024);
				hist->SetNameTitle(TString::Format("ADC_10b_LG_ch%d",fChannels[0]),TString::Format("Charge (10b ADC), Low gain, Channel %d",fChannels[0]));
				hist->GetXaxis()->SetRangeUser(650,1024);
				break;
			case MODE_ADC12:
				hist->SetBins(32*256,32*256,64*256);
				hist->SetNameTitle(TString::Format("ADC_12b_ch%d",fChannels[0]),TString::Format("Charge (12b ADC), Channel %d",fChannels[0]));
				//hist->GetXaxis()->SetRangeUser();
				break;
			case MODE_TSINGLE_9B:
				hist->SetBins(1<<9,0,1<<9);
				hist->SetNameTitle(TString::Format("TDC_8LSB_ch%d",fChannels[0]),TString::Format("Single hit time (lower 8b), Channel %d",fChannels[0]));
			//	hist->GetXaxis()->SetRangeUser(0,50000);
				break;
			case MODE_TBADHIT:
				hist->SetBins(2,0,2);
				hist->SetNameTitle(TString::Format("TDC_badhit_ch%d",fChannels[0]),TString::Format("TDC bad hit bit, Channel %d",fChannels[0]));
				//hist->GetXaxis()->SetRangeUser();
				break;
			case MODE_PERIOD:
				//TODO: Use DNL correction switch /nBitsDNL
				hist->SetBins(100000,0,100000);
				hist->SetNameTitle(TString::Format("period_ch%d",fChannels[0]),TString::Format("Period/DT, Channel %d%s",fChannels[0],nBitsDNL>0?" (DNL corrected)":""));
			//	hist->GetXaxis()->SetRangeUser(0,50000);
				break;
			case MODE_T:
				//TODO: Use DNL correction switch /nBitsDNL
				hist->SetBins(1<<(8+nBitsDNL),-1<<(7+nBitsDNL),1<<(7+nBitsDNL));
				hist->SetNameTitle(TString::Format("T_ch%d_T_ch%d",fChannels[0],fChannels[1]),TString::Format("Time difference between ch%d and ch%d%s",fChannels[0],fChannels[1],nBitsDNL>0?" (DNL corrected)":""));
				hist->GetXaxis()->SetRangeUser(-50,50);
				break;
/*
		        case MODE_MULT:
				hist->SetBins(100,0,100);
				hist->SetNameTitle("Multiplicity","Multiplicity per frame");
				hist->GetXaxis()->SetRangeUser(0,20); // change as you see fit
				break;
			case MODE_CLUSTERING_ESUM:
				hist->SetBins(1000,0,40000);
				hist->SetNameTitle("CLUSTER_ESUM","Cluster energy sum");
				hist->GetXaxis()->SetRangeUser(0,10000);
			break;
			case MODE_CLUSTERING_MULTIPLICITY:
				hist->SetBins(25,0,25);
				hist->SetNameTitle("CLUSTER_MULT","Cluster multiplicity");
				hist->GetXaxis()->SetRangeUser(0,25);
			break;
		        case MODE_CLUSTERING_T:
			  // hist->SetBins(1<<(5+this->runDNL*((unsigned short)log2(tdc_ch_values::MAPPING_N_BINS/32))),-1<<(4+this->runDNL*((unsigned short)log2(tdc_ch_values::MAPPING_N_BINS/32))),1<<(4+this->runDNL*((unsigned short)log2(tdc_ch_values::MAPPING_N_BINS/32))));
			  //  hist->SetBins(1<<5,-1<<4,1<<4);
			  hist->SetBins(1<<21,-1<<20,1<<20);
			  hist->SetNameTitle(TString::Format("CLUSTER_T_ch%d-ch%d",fChannels[0],fChannels[1]),TString::Format("time (T_thr) difference between ch%d and ch%d (using clustering)",fChannels[0],fChannels[1]));
			  hist->GetXaxis()->SetRangeUser(-100,100);
			break;

*/
				
			default:
				printf("Error: Mode not found!!");
		}
	}
	hist->SetDirectory(0);
	if (pad){
	  pad->cd();
	  if (fMode==MODE_BEAM_PROFILE){
	    hist->Draw("colz");
	    fHistoMapping.SetBarOffset(0.2);
	    fHistoMapping2.SetBarOffset(-0.2);
	    fHistoMapping.SetMarkerSize(1.8);
	    fHistoMapping2.SetMarkerSize(1.8);
	    fHistoMapping2.SetMarkerColor(kBlue);
	    fHistoMapping.Draw("same TEXT");
	    //fHistoMapping2.Draw("same TEXT");
	  }
/*
	  else if (fMode==MODE_MULT_TOWER)
	    hist->Draw("colz");
*/
	  else
	    hist->Draw();
	  pad->Update();
	  pad->Modified();
	}
	//printf("Monitors::Monitor::SetMode(), this=%p, hist=%p, pad=%p\n",this,this->GetHistogram(),this->GetPad());
	needsUpdate=false;
}

std::map<unsigned short, Monitors::Monitor::tPosition> Monitors::Monitor::fBeamProfileMapping= std::map<unsigned short, Monitors::Monitor::tPosition>();
TH2D Monitors::Monitor::fHistoMapping=TH2D("fHistoMapping","Channel numbering",15,0,15,3,0,3);
TH2D Monitors::Monitor::fHistoMapping2=TH2D("fHistoMapping2","Channel numbering",15,0,15,3,0,3);
int Monitors::Monitor::ReadMappingFile(std::string filename){
  unsigned short channel, handleID;
	std::ifstream inFile;
	std::string line;
	inFile.open(filename);
	if(!inFile.good()) {
		printf("GetBeamProfileMapping: Cannot open file: %s\n", filename.c_str());
		return -1;
	}
	printf("Reading Beam Profile Mapping File: %s\n",filename.c_str());
	tPosition point;
	while (std::getline(inFile, line)){
		std::istringstream iss(line);
		int a, b;
	  	if (!(iss >> handleID >> channel >> point.x >> point.y)){
			//printf("Ignoring line \"%s\"\n",line.c_str());
			continue;
		}
		fBeamProfileMapping[channel+handleID*36] = point;
		if (channel < 1000){
			fHistoMapping.Fill(point.x,point.y,channel+handleID*64);
			fHistoMapping2.Fill(point.x,point.y,channel);
		}
		//printf("%d:%d (%d) -> %d %d\n",handleID,channel,channel+handleID*64,point.x,point.y);
	}
	inFile.close();
	return 0;
}

struct Monitors::Monitor::tPosition Monitors::Monitor::GetChannelPosition(unsigned short channel) {
	if(fBeamProfileMapping.find(channel)!=fBeamProfileMapping.end() && channel<1000)
		//channel number bigger than 1000 is used to seperate the array
	{
	return fBeamProfileMapping.at(channel);
	}
	else
	{
		cout<<"channel "<< channel<<" doesn't exist!"<<endl;
		tPosition p; p.x=-1;p.y=-1;
		return p;
	}
}


void Monitors::Monitor::UpdateCanvas(){
	//printf("Monitors::Monitor::Update(),\t\t this=%p, hist=%p, pad=%p\n",this,this->GetHistogram(),this->GetPad());
	if(this->GetHistogram() && this->GetPad()){
		this->GetPad()->Update();
		this->GetPad()->Modified();
	}
}
void Monitors::Monitor::Reset(){
	//printf("Monitors::Monitor::Reset(), this=%p, hist=%p, pad=%p\n",this,this->GetHistogram(),this->GetPad());
	if (hist) hist->Reset();
}

void Monitors::Monitor::SetECut(int ch1_low, int ch1_high, int ch2_low, int ch2_high)
{
	ECut_low[0]=ch1_low;
	ECut_high[0]=ch1_high;
	ECut_low[1]=ch2_low;
	ECut_high[1]=ch2_high;

}

void Monitors::Monitor::ProcessHit(const klaus_event current_event)
{

	if(needsFit) Fit();
	if(needsUpdate) UpdateMode();
	if(hist==NULL) {
		printf("hist in the monitor does not exist!!!");
		return;
	}

	switch(fMode)
	    {
	    case MODE_CHANNEL : 
	      hist->Fill( (current_event.channel/*+current_event.chipID*36*/) );
	      break;
            case MODE_BEAM_PROFILE:
#ifdef DEBUG_MAPPING
	      printf("MODE_BEAM_PROFILE: channel: %hd (%u %u)\n", current_event.GetChannelUnique(),current_event.handleID,current_event.channel);
	      printf("MODE_BEAM_PROFILE: position: %d	%d \n",(GetChannelPosition(current_event.GetChannelUnique()).x),(GetChannelPosition(current_event.GetChannelUnique()).y));
#endif
	      hist->Fill( (GetChannelPosition(current_event.channel).x), (GetChannelPosition(current_event.channel).y));
	      break;
	    case MODE_ADC10:
	      if (current_event.channel == fChannels[0]) hist->Fill( current_event.ADC_10b); 
	      break;

	    case MODE_ADC10_HGs:
	      if (current_event.channel == fChannels[0] && !current_event.gainsel_evt) hist->Fill( current_event.ADC_10b); 
	      break;

	    case MODE_ADC10_LGs:
	      if (current_event.channel == fChannels[0] &&  current_event.gainsel_evt) hist->Fill( current_event.ADC_10b); 
	      break;
//TODO: ADC with autogain scaling
//TODO: ADC DNL correction already here? - would need database read-in
	    case MODE_ADC12:
		//TODO: should include scaling factor, offset, etc.
	      if ((current_event.channel) == fChannels[0]) hist->Fill( current_event.ADC_6b*256 + current_event.ADC_PIPE); 
	      break;

	    case MODE_TSINGLE_9B:
	      if ((current_event.channel) == fChannels[0]) hist->Fill( current_event.GetTime()&0x1ff); 
	      break;
	    //case MODE_TBADHIT:
	    //  if ((current_event.channel) == fChannels[0]) hist->Fill( current_event.badhit); 
	    //  break;
            //TODO: MODE_TSINGLE_FC ; MODE_TSINGLE_MC ; MODE_TSINGLE_CC
	    case MODE_PERIOD:
	      if (current_event.channel == fChannels[0]){
		if(previous_events[0].GetTime() != 0 && current_event.GetTime() != 0)
			//TODO: use DNL correction switch; use corrected time
			hist->Fill( current_event.DiffTime(previous_events[0]));
		previous_events[0]=current_event;
	      }
	      break;
	    case MODE_T :
	      if((current_event.channel) == fChannels[0])
		{
		  if( abs(previous_events[1].acq_blk - current_event.acq_blk) < 300 \
		      //&& current_event.GetEnergy() > ECut_low[0]		\
		      //&& current_event.GetEnergy() < ECut_high[0]		\
		      //&& previous_events[1].GetEnergy() > ECut_low[1]	\
		      //&& previous_events[1].GetEnergy() < ECut_high[1]
		      )
		    {
		      hist->Fill( (long int)previous_events[1].GetTime() - (long int)current_event.GetTime() );
		    }
		  //copy the new event
		  //memcpy(&previous_events[0],current_event,sizeof(stic3_data_t));
                  previous_events[0]=current_event;
		}
	      else if((current_event.channel) == fChannels[1] )
		{
		  if( abs(previous_events[1].acq_blk - current_event.acq_blk) < 300 \
		      //&& current_event.GetEnergy() > ECut_low[1] \
		      //&& current_event.GetEnergy() < ECut_high[1] \
		      //&& previous_events[0].GetEnergy() > ECut_low[0] \
		      //&& previous_events[0].GetEnergy() < ECut_high[0]
		      )
		    {
			//TODO: does this work correctly?
		      //hist->Fill((long int)current_event.GetTime() - (long int)previous_events[0].GetTime());
		    }
		  //copy the new event
                  previous_events[1]=current_event;
		}
	      break;

//TODO / FEATURE REQUEST:
//ADC vs Time (Power pulsing...)
//DT12 vs ADC1 with ecut on ADC2

	    default :
	      printf("monitor is not assigned (type=%d)!!! hist not filled!!\n",fMode);
	      	     	      
	      
	}//switch
}
/*
void Monitors::Monitor::ProcessCluster(cluster* cluster_in){
	if(hist==NULL) return;
	cluster candidA;
	cluster candidB;
	switch(fMode){
		case MODE_CLUSTERING_ESUM:
			hist->Fill(cluster_in->GetEnergySum());
			break;
		case MODE_CLUSTERING_MULTIPLICITY:
			hist->Fill(cluster_in->GetSize());
			break;
		case MODE_CLUSTERING_CH_MULT_CORRELATION:
			for(cluster::const_iterator it=cluster_in->GetHits().begin(); it!=cluster_in->GetHits().end();++it)
				hist->Fill(cluster_in->GetSize(),it->GetChannelUnique());
			break;
	        case MODE_CLUSTERING_T:
				candidA=cluster_in->AllHitsOfChannel(fChannels[0]);
				candidB=cluster_in->AllHitsOfChannel(fChannels[1]);
				for(cluster::const_iterator itA=candidA.GetHits().begin(); itA!=candidA.GetHits().end(); ++itA){
					for(cluster::const_iterator itB=candidB.GetHits().begin(); itB!=candidB.GetHits().end(); ++itB){
						if(runDNL) 	hist->Fill((long int)itA->GetTimeDNL(*Monitors::map_dnl_correct[fChannels[0]]) - (long int)itB->GetTimeDNL(*Monitors::map_dnl_correct[fChannels[1]]));
						else 		hist->Fill((long int)itA->GetTime() - (long int)itB->GetTime());
				}
			}
		        break;
	        case MODE_CLUSTERING_ET:
			candidA=cluster_in->AllHitsOfChannel(fChannels[0]);
			candidB=cluster_in->AllHitsOfChannel(fChannels[1]);
			for(cluster::const_iterator itA=candidA.GetHits().begin(); itA!=candidA.GetHits().end(); ++itA){
				for(cluster::const_iterator itB=candidB.GetHits().begin(); itB!=candidB.GetHits().end(); ++itB){
					if(runDNL) 	hist->Fill((long int)itA->GetETimeDNL(*Monitors::map_dnl_correct[fChannels[0]]) - (long int)itB->GetETimeDNL(*Monitors::map_dnl_correct[fChannels[1]]));
					else		hist->Fill((long int)itA->GetETime() - (long int)itB->GetETime());
				}
			}
		        break;
	}
}
*/

Monitors::Monitors(int nx, int ny, std::string filename)
{
	int n_mon = nx*ny;
	//build canvas
	canvas =new TCanvas("c1","Readout Monitor",800,600);
	canvas->cd();	
	canvas->Divide(nx,ny);
	//build monitors one by one
	for(int i=0; i<n_mon; i++)
	{
		switch(i){
			case 0:
				m_monitors.emplace_back(Monitor::MODE_CHANNEL,0,0);
				break;
			default:
				m_monitors.emplace_back(Monitor::MODE_ADC10,i-1,0);
				break;
		}
		m_monitors.back().SetPad(canvas->GetPad(i+1));
	}
	if(filename!="")
	{Monitors::Monitor::ReadMappingFile(filename);}//channel number bigger than 1000 is used to seperate the SiPM arrays
}

Monitors::~Monitors(){
	delete canvas;
}

//process all hits in list of leightweight_stic3_data_t objects. Convert to stic3_data_t to allow use of DNL correction as is.
void Monitors::ProcessAcquisition(const klaus_acquisition* acq){
	//TODO: implement clustering
	//std::vector<cluster_event> event_buffer;
	//event_buffer.reserve(acq->nEvents);
        for(const auto& iASIC : acq->data){//iterate over ASICs in object
		for(const auto& iHit : iASIC.second){//iterate over hits
			//TODO: for multiple ASICs, a modification in klaus_event or Monitors::ProcessEvent is needed to carry the ASIC ID.
			//BuildDNL(&evt_converted); //TODO: DNL correction, then need to modify the event
			ProcessHit(iHit);
			//event_buffer.emplace_back(iHit);
		}
	}
	/*
	//clustering
	clusterlist res_clusterlist;
	cluster_processor.ClusterEventList(event_buffer, res_clusterlist,true);
	for(cluster c: res_clusterlist.GetClusters()){
		for(Monitors::Monitor m : m_monitors)
			m.ProcessCluster(&c);
	}
	*/
}

/*
void Monitors::BuildDNL(stic3_data_t* event){
	if(runDNL)
	{
		if(DNL_reset) reset_DNLmap();
		if(map_dnl_correct.count(event->GetChannelUnique()) == 0) build_DNLhist(event);
		map_dnl_correct[event->GetChannelUnique()]->fill(event->GetT_fine());
		n_event_dnl++;
		if(n_event_dnl%100000==0) update_DNLmap();
	}
}
*/

void Monitors::ProcessHit(const klaus_event event){
	//if(runDNL and map_dnl_correct[event->GetChannelUnique()]->get_cdt()->GetEntries()>20000 and n_event_dnl>100000) do_DNLcorr(event);
	for(std::vector<Monitor>::iterator it=m_monitors.begin();it!=m_monitors.end();++it)
		{it->ProcessHit(event);}
}

void Monitors::UpdateModes(){
	for(std::vector<Monitor>::iterator it=m_monitors.begin();it!=m_monitors.end();++it)
		it->UpdateMode();	
}
void Monitors::UpdateCanvas()
{
	for(std::vector<Monitor>::iterator it=m_monitors.begin();it!=m_monitors.end();++it)
		it->UpdateCanvas();	
}

void Monitors::ResetHistos()
{
	cout<<"Monitor::running ResetHistos()..."<<endl;
	for(std::vector<Monitor>::iterator it=m_monitors.begin();it!=m_monitors.end();++it)
		it->Reset();
}


void Monitors::Monitor::SetFit(Monitors::Monitor::fit_name fuc_name)
{
	m_fit=fuc_name;
	printf("fit function has been set!\n");
//	needsFit=true;
//	Fit();
}
double Monitors::Monitor::find_peak(TH1* hist_in)	//get the peak of histogram//TODO 
{
	double		x_curr = hist_in->GetMaximumBin();
	double		y_curr = hist_in->GetBinContent(int(x_curr));
	x_curr += hist_in->GetXaxis()->GetBinLowEdge(1);		
	//TH1* hist = (TH1*) hist_in->Clone("hist");
	//TSpectrum* 	s = new TSpectrum();
	//int 		nfound = s->Search(hist,2,"",0.1);
	//double*		xpeaks = s->GetPositionX();
	//double 		x_curr;
	//double 		y_curr=0.;
	//for(int i=0; i<nfound; i++)
	//{
	//	if(hist->GetBinContent(xpeaks[i])>y_curr)
	//	{
	//		x_curr = xpeaks[i];
	//	}
	//}
	//delete s;


	if(y_curr>0.)
	{
		printf("Peak located at: %d!!!!!!!!\n",x_curr);
		return x_curr;
	}else 
	{
		printf("Peak in this histogram NOT found!!!!!!!!!!!\n");
		return -1.;
	}
}
void Monitors::Monitor::Fit()
{
	
	needsFit =true;
	if(this->GetHistogram()&&this->GetPad())
	{
		pad->cd();
		TF1 	f("ftmp","exp([Constant]+[DCR]*(-1.6e-9)*x)",1,10e3);
		TF1	gs("gstmp",Form("[Constant]*exp(-0.5*TMath::Power(((x-[Mean])/([Sigma(ps)]/%f)),2.))",binSize/(/*runDNL?(tdc_ch_values::MAPPING_N_BINS/32):*/1)),-1e6,1e6);
		switch(m_fit)
		{

			case F_NONE:
				printf("No need to fit!\n");
				break;
				
			case F_EXPO:
				this->GetHistogram()->Fit(&f,"R","",-500,500);
				break;
			case F_GAUS:
				gs.SetParameters(this->GetHistogram()->GetMaximum(),find_peak(this->GetHistogram()),30.);
				this->GetHistogram()->Fit(&gs,"R"," ",find_peak(this->GetHistogram())-200.,find_peak(this->GetHistogram())+200.);
				//this->GetHistogram()->Print();
				//gs.Print();
				break;
			default:
				printf("mode not found!\n");

		}
		gStyle->SetOptFit(1111);
		this->GetPad()->Update();
		this->GetPad()->Modified();
	}
	needsFit = false;
}
void Monitors::Set_runDNL(bool input)
{
	runDNL = input;		//runDNL in class Monitors
	for(std::vector<Monitor>::iterator it=m_monitors.begin();it!=m_monitors.end();++it)
	{
		it->Set_runDNL(input); //run DNL in single Monitor
		it->Set_Update(true); //run DNL in single Monitor
		cout<<"input bool: "<<input<<"! runDNL in Monitor: "<< it->Get_runDNL() <<"============"<<endl;
	}
}
