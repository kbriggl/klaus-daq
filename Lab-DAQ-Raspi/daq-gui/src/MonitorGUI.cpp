/*
   DAQ class to read the STiC3 data sent over UDP by the FPGA
   Provides Access to basic commands for the FPGA,
   and saves the data in a ROOT Branch. For Monitoring, a ROOT histogram can be filled.

Author: kbriggl, tharion

*/

#include "MonitorGUI.h"

#include "iostream"
//#include ""

#include "TGFrame.h"
#include "TGTab.h"
#include "TGLabel.h"
#include "TGProgressBar.h"
#include "TF1.h"
#include "TStyle.h"
#include "TThread.h"
ClassImp (MonitorGUI);

MonitorGUI::MonitorGUI(Monitors* mons, int n_win, DAQctrl* daq)
{
	m_daq = daq;
	m_monitors = mons;
	n_windows = n_win;
	comboBox_mode.resize(n_windows);
	entry_ch1_select.resize(n_windows);
	entry_ch2_select.resize(n_windows);
	comboBox_fit.resize(n_windows);

	StartGUI();
}

MonitorGUI::~MonitorGUI(){


}

void MonitorGUI::StartDAQ(){
	if(m_daq) m_daq->ReadChipAsyncStart();
}
void MonitorGUI::StopDAQ(){
	if(m_daq) m_daq->ReadChipAsyncStop();
}

void MonitorGUI::ResetHistos()
{
	/*for(int i=0; i<n_windows; i++)
	{
		m_monitors->at(i).hist->Reset();
	}*/
	m_monitors->ResetHistos();
}


void MonitorGUI::StartGUI()
{
printf("Debuf:0!");
	TGLayoutHints* layout1 = new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4);

	TGMainFrame *mainFrame = new TGMainFrame(gClient->GetRoot(), 100, 100, kMainFrame | kVerticalFrame);
	mainFrame->SetWindowName("Monitor GUI");
	mainFrame->SetIconName("Monitor GUI");
	mainFrame->Move(1600,850);
	mainFrame->SetWMPosition(1600,850);

	TGGroupFrame *group_frame = new TGGroupFrame(mainFrame, "Monitor GUI", kVerticalFrame);
	mainFrame->AddFrame(group_frame, new TGLayoutHints(kLHintsTop | kLHintsCenterY, 2, 2, 2, 2));
printf("Debug: 1!");


	TGHorizontalFrame *frame_label = new TGHorizontalFrame(group_frame, 0, 0);
	group_frame->AddFrame(frame_label, new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

	TGLabel* label_mode = new TGLabel(frame_label,"MODE");
	frame_label->AddFrame(label_mode, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 35, 60, 4, 4));
	TGLabel* label_ch1 = new TGLabel(frame_label,"CH 1");
	frame_label->AddFrame(label_ch1, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4));
	TGLabel* label_ch2 = new TGLabel(frame_label,"CH 2");
	frame_label->AddFrame(label_ch2, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 35, 20, 4, 4));
	TGLabel* label_fit = new TGLabel(frame_label,"Fit");
	frame_label->AddFrame(label_fit, new TGLayoutHints(kLHintsLeft | kLHintsCenterY,15 , 25, 4, 4));

	std::vector<TGCompositeFrame*> frame2;
	frame2.resize(n_windows);

	for(int n=0;n<n_windows;n++)
	{
		printf("Setting up selector %d\n",n);
		
		frame2[n] = new TGHorizontalFrame(group_frame, 0, 0);
		group_frame->AddFrame(frame2[n], new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

		comboBox_mode[n] = new TGComboBox(frame2[n],-1, kHorizontalFrame | kSunkenFrame | kDoubleBorder | kOwnBackground);
		frame2[n]->AddFrame(comboBox_mode[n], layout1);

		comboBox_mode[n]->AddEntry("CHANNEL",		Monitors::Monitor::MODE_CHANNEL);
		comboBox_mode[n]->AddEntry("ADC 10b",		Monitors::Monitor::MODE_ADC10);
		comboBox_mode[n]->AddEntry("ADC 10b HG select",	Monitors::Monitor::MODE_ADC10_HGs);
		comboBox_mode[n]->AddEntry("ADC 10b LG select",	Monitors::Monitor::MODE_ADC10_LGs);
		comboBox_mode[n]->AddEntry("ADC 12b",		Monitors::Monitor::MODE_ADC12);
		comboBox_mode[n]->AddEntry("T-single (9b)",	Monitors::Monitor::MODE_TSINGLE_9B);
		comboBox_mode[n]->AddEntry("T-badhit)",		Monitors::Monitor::MODE_TBADHIT);
		comboBox_mode[n]->AddEntry("T period",		Monitors::Monitor::MODE_PERIOD);
		comboBox_mode[n]->AddEntry("T",			Monitors::Monitor::MODE_T);
		comboBox_mode[n]->AddEntry("BEAM_PROFILE",	Monitors::Monitor::MODE_BEAM_PROFILE);
//	        comboBox_mode[n]->AddEntry("MULTIPLICITY",Monitors::Monitor::MODE_MULT);
//	        comboBox_mode[n]->AddEntry("Clustering : Energy sum",Monitors::Monitor::MODE_CLUSTERING_ESUM);
//	        comboBox_mode[n]->AddEntry("Clustering : Cluster size",Monitors::Monitor::MODE_CLUSTERING_MULTIPLICITY);
//	        comboBox_mode[n]->AddEntry("Clustering : Size/channel correlation",Monitors::Monitor::MODE_CLUSTERING_CH_MULT_CORRELATION);
//	        comboBox_mode[n]->AddEntry("Clustering : T",Monitors::Monitor::MODE_CLUSTERING_T);
//	        comboBox_mode[n]->AddEntry("Clustering : ET",Monitors::Monitor::MODE_CLUSTERING_ET);
		comboBox_mode[n]->Resize(200,22);
		comboBox_mode[n]->Select(Monitors::Monitor::MODE_CHANNEL);
		
		entry_ch1_select[n] = new TGNumberEntry(frame2[n],10,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,256);
		frame2[n]->AddFrame(entry_ch1_select[n], layout1);

		entry_ch2_select[n] = new TGNumberEntry(frame2[n],15,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,256);
		frame2[n]->AddFrame(entry_ch2_select[n], layout1);

		comboBox_fit[n] = new TGComboBox(frame2[n],-1, kHorizontalFrame | kSunkenFrame | kDoubleBorder | kOwnBackground);
		frame2[n]->AddFrame(comboBox_fit[n], layout1);

		comboBox_fit[n]->AddEntry("NONE",Monitors::Monitor::F_NONE);
		comboBox_fit[n]->AddEntry("EXPO",Monitors::Monitor::F_EXPO);
		comboBox_fit[n]->AddEntry("GAUS",Monitors::Monitor::F_GAUS);

		if(n==0)	comboBox_mode[n]->Select(Monitors::Monitor::MODE_CHANNEL);  //initial all the box with E
		else		comboBox_mode[n]->Select(Monitors::Monitor::MODE_ADC10);  //initial all the box with E
	}

	//DNL Button
	TGCheckButton *button_dnl = new TGCheckButton(mainFrame,"Enable DNL");
	mainFrame->AddFrame(button_dnl, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));
	
	//Reset Button
	TGTextButton *button_DNLreset = new TGTextButton(mainFrame," DNL Reset ");
	mainFrame->AddFrame(button_DNLreset, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));
	
	//Fit Button
	TGTextButton *button_fit = new TGTextButton(mainFrame," Fit ");	
	mainFrame->AddFrame(button_fit, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));

	//Reset Button
	TGHorizontalFrame *histRstFrame = new TGHorizontalFrame(mainFrame,200,40);
	mainFrame->AddFrame(histRstFrame,new TGLayoutHints(kLHintsCenterX , 2, 2, 5, 5));

	TGTextButton *button_reset = new TGTextButton(histRstFrame," Reset ");
	histRstFrame->AddFrame(button_reset, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));

	TGCheckButton *button_resetAuto = new TGCheckButton(histRstFrame,"Auto");
	histRstFrame->AddFrame(button_resetAuto, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));
	//Start the Auto reset timer
	CanvasUpdateTimer = new TTimer();

	//Start/Stop DAQ Buttons
	TGHorizontalFrame *daqCtrlFrame = new TGHorizontalFrame(mainFrame,200,40);
	mainFrame->AddFrame(daqCtrlFrame,new TGLayoutHints(kLHintsCenterX , 2, 2, 5, 5));

	TGTextButton *button_startDAQ = new TGTextButton(daqCtrlFrame,"Start DAQ async");
	daqCtrlFrame->AddFrame(button_startDAQ, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));

	TGTextButton *button_stopDAQ = new TGTextButton(daqCtrlFrame,"Stop DAQ async");
	daqCtrlFrame->AddFrame(button_stopDAQ, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));




	//Energy cuts
	TGGroupFrame *energy_frame = new TGGroupFrame(mainFrame, "Monitor GUI", kVerticalFrame);
	mainFrame->AddFrame(energy_frame, new TGLayoutHints(kLHintsTop | kLHintsRight, 2, 2, 2, 20));

	TGHorizontalFrame *frame_Elabel = new TGHorizontalFrame(energy_frame, 0);
	energy_frame->AddFrame(frame_Elabel, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 0, 0, 0, 0));

	TGLabel* label_Ech1 = new TGLabel(frame_Elabel,"CH1_ECUT");
	frame_Elabel->AddFrame(label_Ech1, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4));
	TGLabel* label_Ech2 = new TGLabel(frame_Elabel,"CH2_ECUT");
	frame_Elabel->AddFrame(label_Ech2, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 45, 20, 4, 4));

/*
	//Fit Button
	TGGroupFrame *fit_frame = new TGGroupFrame(mainFrame, "Monitor GUI", kVerticalFrame);
	mainFrame->AddFrame(fit_frame, new TGLayoutHints(kLHintsTop | kLHintsRight, 2, 2, 2, 2));
	
	TGHorizontalFrame *frame_fit = new TGHorizontalFrame(fit_frame, 0, 0);
	entry_fit_select = new TGNumberEntry(frame_fit,1,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,1,n_windows);
	frame_fit->AddFrame(entry_fit_select, layout1);
	entry_fit_select->Connect("ValueSet(Long_t)", "MonitorGUI", this, "ExpoFit()");
*/
	//Energy Cuts for T Monitor
	TGHorizontalFrame *frame_ecuts = new TGHorizontalFrame(energy_frame, 0, 0);

	entry_ch1_eCut[0] = new TGNumberEntry(frame_ecuts,0,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,40000);
	frame_ecuts->AddFrame(entry_ch1_eCut[0], layout1);

	entry_ch1_eCut[1] = new TGNumberEntry(frame_ecuts,40000,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,40000);
	frame_ecuts->AddFrame(entry_ch1_eCut[1], layout1);



	entry_ch2_eCut[0] = new TGNumberEntry(frame_ecuts,0,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,40000);
	frame_ecuts->AddFrame(entry_ch2_eCut[0], layout1);

	entry_ch2_eCut[1] = new TGNumberEntry(frame_ecuts,40000,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,40000);
	frame_ecuts->AddFrame(entry_ch2_eCut[1], layout1);

	energy_frame->AddFrame(frame_ecuts, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 0, 0, 8, 0));

	UseGUIValues();
	UpdateECuts();

//Connect all signals. We do this after everything is set up, because some root versions tend to emit already during setup, running into segfaults.
	for(int n=0;n<n_windows;n++){
		comboBox_mode[n]->Connect("Selected(Int_t)", "MonitorGUI", this, "UseGUIValues()");
		entry_ch1_select[n]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UseGUIValues()");
		entry_ch2_select[n]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UseGUIValues()");
		comboBox_fit[n]->Connect("Selected(Int_t)", "MonitorGUI", this, "SetFit()");
	}
	button_dnl->Connect("Toggled(Bool_t)", "MonitorGUI", this,"set_dnl(Bool_t)");
	button_DNLreset->Connect("Clicked()", "MonitorGUI", this, "reset_DNL()");
	button_fit->Connect("Clicked()", "MonitorGUI", this, "RunFit()");
	button_reset->Connect("Clicked()", "MonitorGUI", this, "ResetHistos()");
	button_resetAuto->Connect("Toggled(Bool_t)", "MonitorGUI", this,"UpdateAutoReset(Bool_t)");
	CanvasUpdateTimer->Connect("Timeout()","MonitorGUI",this,"ResetHistos()");
	button_startDAQ->Connect("Clicked()", "MonitorGUI", this, "StartDAQ()");
	button_stopDAQ->Connect("Clicked()", "MonitorGUI", this, "StopDAQ()");
	entry_ch1_eCut[0]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");
	entry_ch1_eCut[1]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");
	entry_ch2_eCut[0]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");
	entry_ch2_eCut[1]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");


	mainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
	mainFrame->MapSubwindows();
	mainFrame->Resize(mainFrame->GetDefaultSize());
	mainFrame->MapWindow();
}

int MonitorGUI::UseGUIValues(){
  for(int n=0;n<n_windows;n++)
		if(UseGUIValues(n)<0) return -1;
	return 0;
}

int MonitorGUI::UseGUIValues(int n){
	if(m_monitors==0)
	{
		printf("MonitorGUI: DAQ not set.");
		return -1;
	}
	printf("MonitorGUI::UseGUIValues():  updating values for %d\n",n);
	cout<<"mode_ch1_select["<<n<<"]: "<<comboBox_mode[n]->GetSelected()<<endl;
	cout<<"entry_ch1_select["<<n<<"]: "<<entry_ch1_select[n]->GetNumber()<<endl;
	cout<<"entry_ch2_select["<<n<<"]: "<<entry_ch2_select[n]->GetNumber()<<endl;
	m_monitors->GetMonitor(n).SetMode(Monitors::Monitor::mode(comboBox_mode[n]->GetSelected()),entry_ch1_select[n]->GetNumber(),entry_ch2_select[n]->GetNumber());


//	if(Monitors::Monitor::mode(comboBox_mode[n]->GetSelected())==Monitors::Monitor::MODE_TSINGLE_8B){
//		klaus_event::moffset = int(entry_ch2_select[n]->GetNumber())%8;
//		printf("Moffset: %u\n",klaus_event::moffset);
//	}
	return 0;
}

void MonitorGUI::UpdateAutoReset(Bool_t enabled){
	//printf("UpdateAutoReset(%d)\n",enabled);
	if(enabled)
		CanvasUpdateTimer->Start(4000,kFALSE);
	else
		CanvasUpdateTimer->Stop();

}

void MonitorGUI::UpdateECuts()
{
	if(m_monitors==0) return;
	for(int n=0;n<n_windows;n++)
	{
		m_monitors->GetMonitor(n).SetECut(
				entry_ch1_eCut[0]->GetNumber(),
				entry_ch1_eCut[1]->GetNumber(),
				entry_ch2_eCut[0]->GetNumber(),
				entry_ch2_eCut[1]->GetNumber()
				);
		/**
		printf("Changed Ch1 ECut to %f %f \n",entry_ch1_eCut[0]->GetNumber(),entry_ch1_eCut[1]->GetNumber());
		m_monitors->GetMonitors().at(n).ECut_low[0] = entry_ch1_eCut[0]->GetNumber();
		m_monitors->GetMonitors().at(n).ECut_high[0] = entry_ch1_eCut[1]->GetNumber();

		printf("Changed Ch2 ECut to %f %f \n",entry_ch2_eCut[0]->GetNumber(),entry_ch2_eCut[1]->GetNumber());
		m_monitors->GetMonitors().at(n).ECut_low[1] = entry_ch2_eCut[0]->GetNumber();
		m_monitors->GetMonitors().at(n).ECut_high[1] = entry_ch2_eCut[1]->GetNumber();
	*/
	}
}

void MonitorGUI::SetFit()
{
	Monitors::Monitor::fit_name fuc;
	for(int n=0; n<n_windows;n++)
		m_monitors->GetMonitor(n).SetFit(Monitors::Monitor::fit_name(comboBox_fit[n]->GetSelected()));
}
void MonitorGUI::set_dnl(bool runDNL)
{
	m_monitors->Set_runDNL(runDNL);	
	printf("MonitorGUI: runDNL = %d\n",runDNL?1:0);
}

void MonitorGUI::reset_DNL()
{
	m_monitors->reset_DNL();
	cout << "MonitorGUI:: running resetDNL()	..."<<endl;
}


void MonitorGUI::RunFit()
{
	for(int n=0; n<n_windows;n++)
	{
		m_monitors->GetMonitor(n).Fit();
	}
}
