#include "Monitor.h"
#include "MonitorGUI.h"
#include "EventListDAQ_client.h"
#include "EventType.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <signal.h>
#include "string.h"
#include <map>
#include <list>

#include "TApplication.h"
#include "TCanvas.h"
#include "TStyle.h"
//#include "TFile.h"
#include "TSystem.h"
#include "TThread.h"
#include "TROOT.h"
#include "TRint.h"

using namespace std;

bool running = true;


Monitors*		monitors;
MonitorGUI*		mon_gui;
TThread*		reader;
EventListDAQ*	DAQ;
TApplication*	theApp;
//std::map<unsigned short, tdc_ch_values*> map_dnl_correct;	//Contaiiner fot TDC_DNL values and correction

void handler_sigint(int sig)
{
	printf("Caught SIGINT!\n");
	running = false;
	reader->Join();
	printf("Reader thread finished.\n");
	theApp->Terminate(0);
}

void StartDAQ(void*)
{
	cout<<"Start Thread StartDAQ..."<<endl;


	int n_list=0;
	int n_event_dnl =0;	

	int pQueueSize = 10000;
	DAQ->RegisterQueue(pQueueSize,1);//buffersize, prescale
	cout<<"Start Thread StartDAQ..."<<endl;
	monitors->UpdateModes();

	while(running)
	{
		if(n_list%10==0)
		{
			monitors->UpdateCanvas();
			monitors->GetCanvas()->Update();
			monitors->GetCanvas()->Modified();
		}

		TList* li;
{
		R__LOCKGUARD(gROOTMutex);
		li=DAQ->FetchResults();
}
//
		if(li!=NULL){
			auto curr_entry=li->FirstLink();
			while(curr_entry){
				klaus_acquisition* curr_acqu=(klaus_acquisition*)curr_entry->GetObject();
				monitors->ProcessAcquisition(curr_acqu);
				curr_entry=curr_entry->Next();
			}
		}else
		{
			//printf("List is NULL...\n");
			usleep(100000);
			//running=false;
			//theApp->Terminate(0);
		}
		n_list++;
	}
}	

int main(int argc, char **argv)
{
	int nx=1;
	int ny=1;
	string path, mapping_filename;
	//Build theApp
	if((theApp=gROOT->GetApplication())==NULL)
		theApp = new TApplication("theApp", 0, 0);

	signal(SIGINT,&handler_sigint);

	//command line related 
	if(argc<2)
	{
		printf("Usage: %s [host] [NPad_x=1] [NPad_y=1] [mapping_filename=none] [port=9090]\n" , argv[0]);	
		return -1;
	}
	//get parameters
	path=argv[1];
	if(argc>2) nx=atoi(argv[2]);
	if(argc>3) ny=atoi(argv[3]);
	if(argc>4)
		mapping_filename=argv[4];
	else
		mapping_filename="";

	unsigned int port=9090;
	if(argc>5){
		port=atoi(argv[5]);
	}

	printf("Connecting to host %s port %u\n",path.c_str(),port);	
	DAQ=new EventListDAQ(argv[1],port);
	if(!DAQ->Good()){
		printf("DAQ not connected, exiting.\n");
		delete DAQ;
		return -1;
	}

	//set up canvas
	gStyle->SetOptStat(11111111);
	/*
	   gStyle->SetPadTopMargin(0.01);	
	   gStyle->SetPadBottomMargin(0.1);	
	   gStyle->SetPadLeftMargin(0.1);	
	   gStyle->SetPadRightMargin(0.01);	
	   */
	//monitors to collect object
	monitors = new Monitors(nx, ny, mapping_filename);
	monitors->GetCanvas()->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
	//connect to monitor_gui to show the graphx
	mon_gui = new MonitorGUI(monitors, nx*ny, DAQ);
	//connect the canvas to Application

	reader = new TThread("StartDAQ", StartDAQ);
	reader->Run();
	theApp->Run(kTRUE);
//--------------------------------------------
	//clean up
	delete DAQ;
	//delete map_dnl_correct;
	monitors->GetCanvas()->Disconnect("Closed()");
	delete monitors;
	delete mon_gui;
	delete theApp;
	printf("The END ...\n");
}



