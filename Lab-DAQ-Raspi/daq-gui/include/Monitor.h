#ifndef __MONITOR_H
#define __MONITOR_H

#include "stdlib.h"
#include "TH1.h"
#include "TH2.h"
#include <map>
#include "TCanvas.h"
#include "TPad.h"
#include "TMutex.h"
#include <iostream>
#include "TSpectrum.h"
#include "EventType.h"
//#include "Clustering.h"
//#include "ClusterTypes.h"
//#include "tdc_ch_values.h" --DNL correction
using namespace std;
class Monitors{

public:
	class Monitor{
		public:

	  enum mode{MODE_NONE,MODE_CHANNEL,MODE_ADC10,MODE_ADC10_HGs,MODE_ADC10_LGs,MODE_ADC12,MODE_TSINGLE_9B,MODE_TBADHIT,MODE_PERIOD,MODE_T,MODE_BEAM_PROFILE};
	  enum fit_name{F_NONE,F_EXPO,F_GAUS};
	  Monitor();
			Monitor(mode m, int channel1, int channel2);
			~Monitor();
			void SetMode( mode m, int channel1, int channel2=0);
			void SetECut(int ch1_low, int ch1_high, int ch2_low=0, int ch2_high=0);
			//process a new event
			void ProcessHit(const klaus_event event);
			//process a cluster object
			//void ProcessCluster(cluster* cluster);
			//reset the histogram
			void Reset();
			//Update the underlying pad
			void UpdateCanvas();
			void UpdateMode();
			//load mapping file from text file, store in fBeamProfileMapping. The file contains 3 coloumns: channel, x, y.
			//beam profile mapping
			struct tPosition{
				int x;
				int y;
			};
			static int		ReadMappingFile(std::string filename);
			static tPosition	GetChannelPosition(unsigned short channel);
			//map holding the position for each channel
			static std::map<unsigned short,tPosition> fBeamProfileMapping;
			static TH2D fHistoMapping;
			static TH2D fHistoMapping2;
			//for the beam profile histogram. If not found in the list, return (-1,-1)

			TH1*		GetHistogram(){return hist;}
			//SetPad
			void		SetPad(TVirtualPad* subPad){pad=subPad;}
			TVirtualPad*	GetPad(){return pad;}
			mode		GetMode(){return fMode;}
			int		GetChannel_1(){return fChannels[0];}
			int		GetChannel_2(){return fChannels[1];}
			void 		SetFit(fit_name fuc_name);
			void 		Fit();
			void		Set_runDNL(bool input){runDNL = input;}
			bool		Get_runDNL(){return runDNL;}
			void		Set_Update(bool input){needsUpdate = input;};
		private:
			bool		runDNL = false;
			bool 		needsUpdate;
			bool 		needsFit;
			double		find_peak( TH1* hist_in);	//get the peak of histogram 
			TH1*		hist;
			mode		fMode;
			fit_name	m_fit;
			int		fChannels[2]={0,0};
			TVirtualPad*	pad;
			int		ECut_low[2], ECut_high[2];
		
			klaus_event*   previous_events;
			bool		(*cut_method)(klaus_event*);
			//int             mult_count;          // mulitplicity counter
			//int             mult_count_tower[4]; // multiplicity counter for the single tower
			//std::vector<std::vector<int> > tower {{8,136,141,146,151}, {13,137,140,147,150}, {18,138,142,145,149}, {23,139,143,144,148} }; // Tower 1 DUT
			//std::vector<std::vector<int> > tower {{9,136,141,146,151}, {12,137,140,147,150}, {19,138,142,145,149}, {22,139,143,144,148} }; // Tower 2 DUT
			//std::vector<std::vector<int> > tower {{10,136,141,146,151}, {14,137,140,147,150}, {17,138,142,145,149}, {21,139,143,144,148} }; // Tower 3 DUT
			//std::vector<std::vector<int> > tower {{11,136,141,146,151}, {15,137,140,147,150}, {16,138,142,145,149}, {20,139,143,144,148} }; // Tower 4 DUT

	};


//	typedef std::vector<Monitor>::iterator monitor_ref;

	Monitors(int nx,int ny, std::string filename); //constructor arguments: pads on canvas in x, y direction
	~Monitors();
//monitoring histogram: reference by an ID (accesses m_monitors.at(ID))
	void 	 		AddMonitor(Monitor mon);
	Monitor&		GetMonitor(int i) {return m_monitors.at(i);}
	void			RemoveMonitor(int i);

//work on histograms
	void			ResetHistos();
//process new event for all monitors in the list
	void ProcessAcquisition(const klaus_acquisition* event);
	void ProcessHit(const klaus_event event);
//graphical
	void			UpdateModes();
	void			UpdateCanvas();
	TCanvas*		GetCanvas(){return canvas;}
//dnl correction 
	bool 			runDNL = false;
	bool			DNL_reset = true;
	void			reset_DNL(){DNL_reset = true;ResetHistos();cout<<"Monitors:: running reset_DNL() ..."<<endl;}
	void			Set_runDNL(bool input);
	
	//std::map<unsigned short, tdc_ch_values*> map_dnl_correct;       //Contaiiner fot TDC_DNL values and correction. TODO
	void 			reset_DNLmap();
	void 			build_DNLhist(klaus_event* event);
	void 			update_DNLmap();
	void 			do_DNLcorr(klaus_event* event);
	//bool			DNLcorr_did = false;		//TODO: Do not Process the event before the first update of the DNLmap
protected:
//monitoring
	std::vector<Monitor> 	m_monitors;
	TCanvas*		canvas;
	int 			n_event_dnl;
	//Clustering		cluster_processor;
};

#endif
