#ifndef MonitorGUI_H_
#define MonitorGUI_H_

#include "TGComboBox.h"
#include "TGNumberEntry.h"
#include "TTimer.h"
#include <RQ_OBJECT.h>

#include "vector"

#include "Monitor.h"
using namespace std;

#include "DAQctrl_client.h"

class MonitorGUI{
  RQ_OBJECT("MonitorGUI")

public:
	MonitorGUI(Monitors* mons, int n_win, DAQctrl* daq=NULL);
	~MonitorGUI();
	
//	void	SetMonitorCollection( Monitors* mon ){ m_monitors = mon; };
	
	void	StartGUI();
	int	UseGUIValues(int n);
	int	UseGUIValues();
	void	UpdateECuts();
	void	ResetHistos();
	//Monitors*	GetMonitors(){return m_monitors;
	void 	SetFit();
	void 	set_dnl(bool runDNL);
	void 	reset_DNL();
	void	RunFit();
//	void	GausFit(int n);
	void UpdateAutoReset(Bool_t);
	void StartDAQ();
	void StopDAQ();
private:  
    //number of pad in the canvas    
	int n_windows;
	
	DAQctrl* m_daq;
	Monitors*	m_monitors;
  
	std::vector<TGComboBox*>	comboBox_mode;
	std::vector<TGNumberEntry*>	entry_ch1_select;
	std::vector<TGNumberEntry*>	entry_ch2_select;
	std::vector<TGComboBox*>	comboBox_fit;

	TGNumberEntry*	entry_ch1_eCut[2];
	TGNumberEntry*	entry_ch2_eCut[2];
	TTimer*	CanvasUpdateTimer;

	ClassDef(MonitorGUI,1)
};

#endif /* MonitorGUI_H_ */
