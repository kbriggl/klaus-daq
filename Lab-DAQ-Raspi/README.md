# How to operate the DAQ
In order to operate the DAQ, an raspberry pi and PC are needed. 
They should stay in the same networks so that the PC can ssh to the raspberry pi and connect the servers

## Build the DAQ
### Raspi (recommend debian with ROOT installed)
Go the *software* directory, do the command: *make clean all*

### PC (recommend debian jessie/stretch/uid or ubutun 16.04/18.04, ROOT installed)
Go the *software* directory, do the command: **make clean all**

## Start servers on raspberry pi
### Setup system (disable LEDs, set up permissions and i2c speed, start servers in tmux session)
Run the command: *./start_DAQ_raspi.sh*
### Alternatively start the individual binaries
Go to the *software/config-common*, do the command: *sudo ./config_server 1 [start_addr=0x40]*
Go to the *software/daq-i2c*, do the command: *./klaus_daqserver /dev/i2c-1*

### PC - Configuration gui
Go to the *software/config-common*, do the command: *./gui_romote name_of_raspi*. 
### PC - Data taking ; store into root tree
Go to the *software/daq-host*, do the command: *rm -rf test.root; ./TreeDAQ name_of_raspi test.root*. 
### PC - Data taking ; data monitoring gui 
Go to the *software/daq-gui*, do the command: *./daq_gui name_of_raspi* for monitoring the data

