// ifstream constructor.
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

std::string ReadoutTemperature(std::vector<std::string> ids){
  std::cout<<"********* Temperature readout **"<<std::endl;
  std::stringstream sout;
  sout<<"\"temperatures\":{";
  bool first=true;
  for (auto id:ids){
	  std::string path="/sys/bus/w1/devices/28-"+id+"/temperature";
	  std::ifstream ifs (path, std::ifstream::in);
	  float T;
	  if(!ifs.good()){
		std::cout<<"Temperature readout: ID "<<id<<" not Good"<<std::endl;
		continue;
	  }
	  //std::cout<<"Temperature readout: ID "<<id<<std::endl;
	  ifs >> T;
	  T/=1000;
	  std::cout<<id<<" -> "<<T<<std::endl;
	  if(!first) sout<<",";
	  first=false;

  	  sout<<"\""<<id<<"\""<<":"<<T;
	  ifs.close();
  }
  sout<<"}"<<std::endl;
  std::cout<<"********************************"<<std::endl;
  return sout.str();
}
/*
int main () {

  std::vector<std::string> ids;
  ids.push_back("000007558abe");
  ids.push_back("000007577bb3");
  auto s=ReadoutTemperature(ids);
  std::cout<<s;
  return 0;
}
*/
