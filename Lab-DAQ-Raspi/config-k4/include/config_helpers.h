#ifndef __K4_CONFIG_HELPERS_H
#define __K4_CONFIG_HELPERS_H

#include "VirtualConfig.h"
void SetParForChannel(TKLauS4Config* config, int channel, const char* parname,unsigned long long value){
	char full_name[127];
	sprintf(full_name,"channel%d/%s",channel,parname);
	if(config->SetParValue(full_name,value)<0){
		printf("SetParAllChannel(): Wrong parameter name \"%s\"\n",full_name);
	};
}


void SetParAllChannel(TKLauS4Config* config, const char* parname,unsigned long long value){
	for(int ch=0;ch<36;ch++){
		SetParForChannel(config,ch,parname,value);
	}
}

#include "TObjString.h"
#include <sstream>
void SaveConfigInROOT(TVirtualConfig* config, const char* name){
	std::stringstream ss;
	config->Print(true,ss);
	TObjString s(ss.str().c_str());
	s.Write(name);
}
void SaveConfigStatusInROOT(TVirtualConfig* config, const char* name){
	std::stringstream ss;
	config->Print(false,ss);
	TObjString s(ss.str().c_str());
	s.Write(name);
}
#endif
