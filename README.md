# KLauS-DAQ

The Data-Acquisition System for the KLauS ASIC is now converged in this version 
since the design of KLauS ASIC comes to its end. The contents included in this 
repository are listed as following:

1. PCBs
2. Lab-DAQ-Raspi
3. Lab-Characterization
4. Lab-Testbeam
5. HBU-DAQ
6. HBU-Characterization
7. HBU-Testbeam

## PCBs
This directory includes all the PCB design files for the Lab-test and the schematics
of related CALICE PCB board in *.pdf* format.

1. **CALICE**: schematic for HBU6_HD, CIB, DIF, CALIB and POWER from DESY
2. **MB-KLauS6-COB** : design of main board for KLauS-6 test(Chip on Board).

## HBU-DAQ
This directory includes the firmware of HBU with KLauS ASIC. 
